$(document).ready(function() {
    

    // window.onscroll = function() {myFunction()};

    // var navbar = document.getElementById("navbar-2");
    // var sticky = navbar.offsetTop;

    // function myFunction() {
    //   if (window.pageYOffset >= sticky) {
    //     navbar.classList.add("sticky")
    //   } else {
    //     navbar.classList.remove("sticky");
    //   }
    // }

    initHomeSlider();
      
      //owlCrouselFeatureSlide();
});



function initHomeSlider()
  {
    

    $(".owl-course").owlCarousel({
 
            items: 4,
            slideSpeed: 0.5,
            nav: true,
            margin: 20,
            autoplay: false,
            dots: false,
            loop: false,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 3,
                    nav: true
                },
                900: {
                    items: 4,
                    nav: true
                },
            },navText: [
                "<i class='fa fa-angle-left'></i>",
                "<i class='fa fa-angle-right'></i>"
              ]
    });
  }

