
 $(document).ready(function () {
	 	$('.fancybox-close').click(function(){
		    alert(1);
		    location.reload();
		  });
	    var linkId = $("#linkId").val();
	    setLoginAfterurl(siteUrl+'course/detail/'+linkId);
	    //light box
	    $(".various5").fancybox({
	            maxWidth    : 820,
	            maxHeight   : 650,
	            fitToView   : false,
	            width       : '100%',
	            height      : '100%',
	            autoSize    : false,
	            closeClick  : false,
	            helpers     : { 
							        overlay : {closeClick: false} 
							    },
	            openEffect  : 'none',
	            closeEffect : 'none',
	            afterClose:function () { parent.location.reload(true);}
	    });

	     $(".various-m").fancybox({
	            maxWidth    : 640,
	            maxHeight   : 200,
	            fitToView   : false,
	            width       : '100%',
	            height      : '100%',
	            autoSize    : false,
	            closeClick  : false,
	            openEffect  : 'none',
	            closeEffect : 'none'
	    });

	      $('.fancybox').fancybox();



    $('.modalLogin').click(function(){
    	$('#login').modal('show')
    });
    $('.register').click(function(){
    	var courseId = $("#courseId").val();

		$.post(siteUrl+'/course/registerCourse',{csrfToken: csrfToken,courseId:courseId },function(data){
				
			//if(data){ 

				  window.location=siteUrl+'course/register/'+courseId;
		          
		    // }else{
		     
		    // }

	    }); 
    });

    $('.registerSave').click(function(){
    	var courseId = $("#courseId").val();
    	var promotionId = $("#promotionId").val();
		$.post(siteUrl+'/course/registerSave',{csrfToken: csrfToken,courseId:courseId,promotionId:promotionId },function(data){
				
			// if(data){ 

			// 	  window.location=siteUrl+'course/register';
		          
		 //    }else{
		     
		 //    }

	    }); 
    });

	$('#review-form-submit-btn').click(function() {
		 var score = $('input[name=rating]:checked').val();
		 var comment = $('#review-form-comment').val();
		  var courseId = $("#courseId").val();
         if(!score){
         	alert('กรุณาเลือกคะแนน');
         	return false;
         }
         if(comment==""){
         	alert('กรุณากรอกความเห็น');
         	return false;
         }
//alert(courseId);
         $.post(siteUrl+'/course/reviews_stars',{csrfToken: csrfToken,courseId:courseId,score:score,comment:comment },function(data){
				
			if(data){ 
				var res = JSON.parse(data);
				$('#review-form-comment').val('');
				$('input[name="rating"]').prop('checked', false);
				$("#reviews_stars_result").html(res.t);
				$("#p-value").html(res.v);
				$("#p-value1").html(res.v1);
				$("#p-value2").html(res.v2);
				$("#p-value3").html(res.v3);
				$("#p-value4").html(res.v4);
				$("#p-value5").html(res.v5);

				$("#p1").html(res.p1);
				$("#p2").html(res.p2);
				$("#p3").html(res.p3);
				$("#p4").html(res.p4);
				$("#p5").html(res.p5);
		          
		    }else{
		     
		    }

	    }); 
         //alert(comment);
	   
	});
	$('#review-form-cancel-btn').click(function() {
		  $('#review-form-comment').val('');
	});

    $('#btn-comment').click(function() {
		 
		 var title = $('#c_subject').val();
		 var detail = $('#c_message').val();
		 var courseId = $("#courseId").val();
         if(title==""){
         	alert('กรุณากรอกหัวข้อ');
         	return false;
         }
         if(detail==""){
         	alert('กรุณากรอกรายละเอียด');
         	return false;
         }
//alert(courseId);
         $.post(siteUrl+'/course/comment_save',{csrfToken: csrfToken,courseId:courseId,title:title,detail:detail },function(data){
				
			if(data){ 
				var res = JSON.parse(data);
				//console.log(data);
				$("#comment-list").html(res.t);
				$('#c_subject').val('');
				$('#c_message').val('');
		          
		    }else{
		     
		    }

	    }); 
         //alert(comment);
	   
	});

 getReviewsStar();
 getCommentList();

}); 

function getReviewsStar(){
	 var courseId = $("#courseId").val();
	 $.post(siteUrl+'/course/get_reviews_stars2',{csrfToken: csrfToken,courseId:courseId },function(data){
			
			if(data){ 
				var res = JSON.parse(data);
				$('#review-form-comment').val('');
				$("#reviews_stars_result").html(res.t);
				$("#p-value").html(res.v);
		        $("#p-value1").html(res.v1);
				$("#p-value2").html(res.v2);
				$("#p-value3").html(res.v3);
				$("#p-value4").html(res.v4);
				$("#p-value5").html(res.v5);

				$("#p1").html(res.p1);
				$("#p2").html(res.p2);
				$("#p3").html(res.p3);
				$("#p4").html(res.p4);
				$("#p5").html(res.p5);
		    }else{
		     
		    }

	  }); 
}
function getCommentList(){
	 var courseId = $("#courseId").val();
	 $.post(siteUrl+'/course/getCommentList',{csrfToken: csrfToken,courseId:courseId },function(data){
			
			if(data){ 
				var res = JSON.parse(data);
				
				$("#comment-list").html(res.t);
				
		    }else{
		     
		    }

	  }); 
}

function reply(id){
	//alert(id);
	$('#form-reply-'+id).show(100);

	$('#btn-comment-reply-'+id).click(function() {


		 
		 var title =$('#c_subject_reply-'+id).val();
		 var detail = $('#c_message_reply-'+id).val();
		 var courseId = $("#courseId").val();
		 var commentsId = id;

		// alert(detail);
        
         if(detail==""){
         	alert('กรุณากรอกรายละเอียด');
         	return false;
         }
//alert(courseId);
         $.post(siteUrl+'/course/comment_save_reply',{csrfToken: csrfToken,courseId:courseId,title:title,detail:detail,commentsId:commentsId },function(data){
				
			if(data){ 
				var res = JSON.parse(data);
				//console.log(data);
				$("#comment-list").html(res.t);
				$('#c_message_reply-'+id).val('');
		          
		    }else{
		     
		    }

	    }); 
         //alert(comment);
	   
	});
}


// การเรียกใช้
//var iCountDown=setInterval("getPromotion()",1000); 

function getPromotion(){
	    var courseId = $("#courseId").val();

		$.post(siteUrl+'/course/promotionByid',{csrfToken: csrfToken,courseId:courseId },function(data){
				
		if(data){ 

			  //console.log(data);
		      var res = JSON.parse(data);
		      var timeA = new Date(); // วันเวลาปัจจุบัน
	      
	     

	         var timeB = new Date(res.dd);
	         var timeDifference = timeB.getTime()-timeA.getTime();    
	         if(timeDifference>=0){

		        // if(res.discount!=0){
		        //     $('.btn-price-'+res.courseId).addClass('btn-price-addpro').show();
		        // }

	            $('.box-pro').show(100);
	            
	            
	            //$('.product-'+res.courseId).addClass('promotion-content').show();
	              timeDifference=timeDifference/1000;
	              timeDifference=Math.floor(timeDifference);
	              var wan=Math.floor(timeDifference/86400);
	              var l_wan=timeDifference%86400;
	              var hour=Math.floor(l_wan/3600);
	              var l_hour=l_wan%3600;
	              var minute=Math.floor(l_hour/60);
	              var second=l_hour%60;
	              var showPart=document.getElementById('showRemain');
	             var showPart2=document.getElementById('showRemain2');
	              //var showPart=document.getElementsByClassName('showRemain');
	              
	              if(wan==0){
	                showPart.innerHTML=""+hour+":"
	              +minute+":"+second+""; 
	               showPart2.innerHTML=""+hour+":"
	              +minute+":"+second+""; 
	              }else{
	                showPart.innerHTML=""+wan+" วัน "+hour+":"
	              +minute+":"+second+""; 
	              showPart2.innerHTML=""+wan+" วัน "+hour+":"
	              +minute+":"+second+""; 
	              }
	              // showPart.innerHTML=""+wan+" วัน "+hour+":"
	              // +minute+":"+second+""; 
	                  if(wan==0 && hour==0 && minute==0 && second==0){
	                      clearInterval(iCountDown); // ยกเลิกการนับถอยหลังเมื่อครบ
	                      // เพิ่มฟังก์ชันอื่นๆ ตามต้องการ
	                  }
	        }
	      
	          
	    }else{
	     
	    }

    }); 

}

 // var handstickPlayerD = new Vimeo.Player('handstick-d');
 //  handstickPlayerD.on('play', function() {
 //        console.log('played the handstick video!');
 //  });
 //  handstickPlayerD.on('ended', function() {
 //           //alert('end');
 //  });


  
  // handstickPlayerC.on('play', function() {
  //       console.log('played the handstick video!');
  // });

  //var pauseButton = document.getElementById("pause-button");
 function pausePlayer(id){
  	//alert(1);
  	var handstickPlayerC = new Vimeo.Player('handstick-c-'+id);
    handstickPlayerC.pause();
    $('#contentVideo-'+id).modal('hide')
 }


 