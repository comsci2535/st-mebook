"use strict";

$(document).ready(function () {
    
    $('input#filter-category').keyup(function () {
        var that = this, $allListElements = $('ul.filter-category > li');
        var $matchingListElements = $allListElements.filter(function (i, li) {
            var listItemText = $(li).text().toUpperCase(), searchText = that.value.toUpperCase();
            return ~listItemText.indexOf(searchText);
        });
        $allListElements.hide();
        $matchingListElements.show();
    });

    // $(".icheck").on("ifChecked", function(){
    //     // alert($(this).attr('data-id'));
    //     $('.icheck-inline .text-point').val(0);
    //     $('#'+$(this).attr('data-id')).val($(this).val());
    // });

  
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})

function readURL(input, val) {
    console.log(input.files[0].type);
    if(input.files[0].type =="image/png"){
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.'+val).attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
            $('#'+val).val(input.files[0].name);
        }
    }else{
        alert('กรุณาเลือกไฟล์ .png');
    } 
}
