"use strict";
//  $('#data-list').on( 'click', 'tbody td:not(.child), tbody span.dtr-data', function (e) {
//         // Ignore the Responsive control and checkbox columns
//     if ( $(this).hasClass( 'control' ) || $(this).hasClass('select-checkbox') ) {
//         return;
//     }

//     editor.inline( this );
// } );
$(document).ready(function () {

    //$("input[type='checkbox']").bootstrapToggle();
    
      var table = dataList.DataTable({
        language: {url: "assets/bower_components/datatables.net/Thai.json"},
        serverSide: true,
        bFilter: false,
        responsive: true,
        ajax: {
            url: "admin/"+controller+"/data_index",
            type: 'POST',
            data: {'csrfToken': get_cookie('csrfCookie'),frmFilter:(function(){return $("#frm-filter").serialize()})},
        },
        order: [[3, "desc"]],
        pageLength: 25,
        columns: [
            
            {data: "checkbox", width: "20px", className: "text-center", orderable: false},
            {data: "name", className: "", orderable: true},
             //{data: "username",width: "400px", className: "", orderable: false},
            {data: "phone", width: "100px", className: "", orderable: true},
            {data: "email", width: "100px", className: "", orderable: true},
            {data: "lastLogin", width: "100px", className: "", orderable: true},
            {data: "updateDate",width: "100px",  className: "", orderable: true},
            {data: "verify", width: "50px", className: "text-center", orderable: false},
            {data: "active", width: "50px", className: "text-center", orderable: false},
            {data: "action", width: "30px", className: "text-center", orderable: false},
        ],"fnDrawCallback": function() {
            $(".toggle-demo").bootstrapToggle();
            $('.tb-check-single').iCheck(iCheckboxOpt);

        },
    }).on('draw', function () {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    }).on('processing', function(e, settings, processing) {
        if ( processing ) {
            $('#overlay-box').removeClass('hidden');
        } else {
            $('#overlay-box').addClass('hidden');
        }
    })

    // new $.fn.dataTable.FixedHeader( table ).on('draw', function () {
    //     $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
    //     $('.tb-check-single').iCheck(iCheckboxOpt);  })
    



})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})


function changeActive(courseId,userId){
   //alert($('#'+courseId+userId).val());
  // alert(courseId);
  // alert(userId);

  bootbox.dialog({
      message: "<span class='bigger-110'><i class='fa fa-question-circle text-primary'></i> กรุณายืนยันการทำรายการ</span>",
      className : "my_width",
      buttons:
      {
        "success" :
        {
          "label" : "<i class='fa fa-check'></i> ตกลง",
          "className" : "btn-sm btn-success",
          "callback": function() {
            
            $.ajax({
                  url: siteUrl+'/admin/member/change_active',
                  type: 'POST',
                  dataType: 'json',
                  data: {
                        csrfToken: csrfToken,
                        courseId : courseId ,
                        userId : userId ,
                        manage_val : $('#'+courseId+userId).val()
                   },
            })
            .done(function(data) {
                console.log(data);
                  if(data.manage_status == 1)
                  {
                        $('#'+courseId+userId).val(data.manage_val);
                        //$('#data-list').DataTable().ajax.reload(); 

                  }
                  else if(data.manage_status == 0)
                  {
                        $('#'+courseId+userId).val(data.manage_val);
                        //$('#data-list').DataTable().ajax.reload(); 
                     
                        
                  }
            })
            .fail(function() {
            
                         
            });

            }

        },
        "cancel" :
        {
          "label" : "<i class='fa fa-times'></i> ยกเลิก",
          "className" : "btn-sm btn-white",
          "callback": function() {
            $('#data-list').DataTable().ajax.reload(); 
          }
        }
      }
    });
    
}


function memberPrivilege(userId){
   //$("#myModal .modal-header h4").html("Request for Change");
    $("#myModal .userId").val(userId);
    $("#myModal").modal("show");

    $('#myModal').on('shown.bs.modal', function() {
      $('#endStudyDate').datepicker({
        format: "dd/mm/yyyy",
        todayBtn: "linked",
        autoclose: true,
        todayHighlight: true,
        container: '#myModal modal-body'
      });
    });

}
function memberAllPrivilege(){

    $("#myModal2").modal("show");


}
$("[id=memberCnf]").click(function() {

 
  var userIdArr = $("#userId").val();
  var endStudyDateNum = $("#endStudyDateNum").val();
   $("#endStudyDateNumSpan").html('')
  if(endStudyDateNum==""){
     $("#endStudyDateNumSpan").html('<font color=red>ระบุจำนวนวันที่ชดเชย</font>')
     return false;
  }

  if(confirm("ยืนยันกำหนดจำนวนวันที่ชดเชย (รายคน) ")){
      $('#img-wait-member').html('<img src="'+baseUrl+'assets/images/loading1.gif" alt="loading" title="loading" style="display:inline">');
      $.ajax({
            url: siteUrl+'/admin/member/privilege',
            type: 'POST',
            dataType: 'json',
            data: {
                  csrfToken: csrfToken,
                  endStudyDateNum : endStudyDateNum ,
                  userId : userIdArr ,
                  type : 'member'
             },
      })
      .done(function(data) {
         
            if(data.manage_status == 1)
            {
                 $('#img-wait-member').html('');
                 $("#myModal").modal("hide");
                 $('#data-list').DataTable().ajax.reload(); 
            }
            else if(data.manage_status == 0)
            {
                //$('#'+courseId+userId).val(data.manage_val);
                //$('#data-list').DataTable().ajax.reload(); 
            }
      })
      .fail(function() {
      
                   
      });

  }else{
        return false;
  }
 
  //alert(endStudyDateNum);
  //$("#img-wait-member").html("");
  
});

$("[id=memberAllCnf]").click(function() {

  var endStudyDateNum = $("#endStudyDateNum2").val();
   $("#endStudyDateNumSpan2").html('')
  if(endStudyDateNum==""){
     $("#endStudyDateNumSpan2").html('<font color=red>ระบุจำนวนวันที่ชดเชย</font>')
     return false;
  }
  if(confirm("ยืนยันกำหนดจำนวนวันที่ชดเชย (สมาชิกทั้งหมด)")){

      $('#img-wait-member-all').html('<img src="'+baseUrl+'assets/images/loading1.gif" alt="loading" title="loading" style="display:inline">');
      $.ajax({
            url: siteUrl+'/admin/member/privilege',
            type: 'POST',
            dataType: 'json',
            data: {
                  csrfToken: csrfToken,
                  endStudyDateNum : endStudyDateNum ,
                  type : 'member_all'
             },
      })
      .done(function(data) {
         
            if(data.manage_status == 1)
            {
                $('#img-wait-member').html('');
                 $("#myModal2").modal("hide");
                 $('#data-list').DataTable().ajax.reload(); 
            }
            else if(data.manage_status == 0)
            {
                //$('#'+courseId+userId).val(data.manage_val);
                //$('#data-list').DataTable().ajax.reload(); 
            }
      })
      .fail(function() {
      
                   
      });

  }else{
        return false;
  }
 
  //alert(endStudyDateNum);
  //$("#img-wait-member").html("");
  
});