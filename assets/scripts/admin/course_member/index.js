"use strict";

$(document).ready(function () {
    var courseId = $('#courseId').val();
    dataList.DataTable({
        language: {url: "assets/bower_components/datatables.net/Thai.json"},
        serverSide: true,
        bFilter: false,
        ajax: {
            url: "admin/"+controller+"/data_index",
            type: 'POST',
            data: {csrfToken:get_cookie('csrfCookie'),courseId:courseId, frmFilter:(function(){return $("#frm-filter").serialize()})},
        },
        order: [[5, "desc"]],
        pageLength: 25,
        columns: [
             {data: "checkbox", width: "20px", className: "text-center", orderable: false},
            {data: "name", className: "", orderable: true},
            {data: "email",width: "200px", className: "", orderable: false},
            {data: "phone", width: "100px", className: "", orderable: false},
            {data: "lastLogin", width: "120px", className: "", orderable: true},
            {data: "createDate", width: "120px", className: "", orderable: true},
            {data: "updateDate", width: "50px", className: "text-center", orderable: false},
           
        ]
    }).on('draw', function () {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt)
        $('.tb-check-single').iCheck(iCheckboxOpt)
    }).on('processing', function(e, settings, processing) {
        if ( processing ) {
            $('#overlay-box').removeClass('hidden');
        } else {
            $('#overlay-box').addClass('hidden');
        }
    })
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
