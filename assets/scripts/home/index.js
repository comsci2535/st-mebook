$(document).ready(function() {
    

    

    $(".various5").fancybox({
              maxWidth    : 820,
              maxHeight   : 650,
              fitToView   : false,
              width       : '100%',
              height      : '100%',
              autoSize    : false,
              closeClick  : false,
              helpers     : { 
                      overlay : {closeClick: false} 
                  },
              openEffect  : 'none',
              closeEffect : 'none',
              afterClose:function () { parent.location.reload(true);}
      });

    


      initHomeSlider();
      
      //owlCrouselFeatureSlide();
});

function registerCourse(courseId){
  //alert(1);
   $.post(siteUrl+'/course/registerCourse',{csrfToken: csrfToken,courseId:courseId },function(data){
        
      //if(data){ 

          window.location=siteUrl+'course/register/'+courseId;
              
        // }else{
         
        // }

      }); 
   
}

function initHomeSlider()
  {
    if($('.home_slider').length)
    {
      var homeSlider = $('.home_slider');
      homeSlider.owlCarousel(
      {
        items:1,
        loop:true,
        autoplay:true,
        nav:true,
        dots:false,
        smartSpeed:1200,
        navText: [
                "<i class='fa fa-angle-left'></i>",
                "<i class='fa fa-angle-right'></i>"
              ]
      });

      if($('.home_slider_prev').length > 1)
      {
        var prev = $('.home_slider_prev');
        prev.on('click', function()
        {
          homeSlider.trigger('prev.owl.carousel');
        });
      }

      if($('.home_slider_next').length > 1)
      {
        var next = $('.home_slider_next');
        next.on('click', function()
        {
          homeSlider.trigger('next.owl.carousel');
        });
      }
    }


    $(".owl-course").owlCarousel({
 
            items: 4,
            slideSpeed: 0.5,
            nav: true,
            margin: 20,
            autoplay: false,
            dots: false,
            loop: false,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 3,
                    nav: true
                },
                900: {
                    items: 4,
                    nav: true
                },
            },navText: [
                "<i class='fa fa-angle-left'></i>",
                "<i class='fa fa-angle-right'></i>"
              ]
    });
  }

// Owl Carousel
  // var owlCrouselFeatureSlide = function() {
  //   var owl = $('.course-home');
  //   owl.owlCarousel({
  //      loop: true,
  //      margin: 30,
  //      nav: true,
  //      dots: false,
  //      autoplay: false,
  //      autoplayHoverPause: true,
  //      smartSpeed: 500,
  //      responsive:{
  //         0:{
  //            items:1
  //         },
  //          600:{
  //            items:2
  //         },
  //         1000:{
  //            items:4
  //         }
  //      },
  //      navText: [
  //         "<i class='icon-chevron-left owl-direction'></i>",
  //         "<i class='icon-chevron-right owl-direction'></i>"
  //       ]
  //   });

  //   var owl2 = $('.owl-carousel2');
  //   owl2.owlCarousel({
  //     animateOut: 'fadeOut',
  //      animateIn: 'fadeIn',
  //      autoplay: true,
  //      loop:true,
  //      margin:0,
  //      nav:true,
  //      dots: false,
  //      autoHeight: true,
  //      items: 1,
  //      navText: [
  //         "<i class='icon-chevron-left owl-direction'></i>",
  //         "<i class='icon-chevron-right owl-direction'></i>"
  //       ]
  //   });
  // };