<section class="blog_area single-post-area p-25">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 posts-list">
                        <div class="single-post row">
                            <div class="col-lg-12">
                                <div class="feature-img">
                                    <img class="img-fluid" src="<?php echo $info['image']; ?>" alt="" style="width: 100%">
                                </div>  
                                <div class="p-10">
                                  <?php echo Modules::run('social/share_button'); ?>
                                </div>                                
                            </div>
                           <div class="col-lg-12 col-md-12 blog_details">
                                <h2><?php echo $info['title'] ?></h2>
                                <p class="excert">
                                    <?php echo $info['excerpt'] ?>
                                </p>
                               
                            </div>
                            <div class="blog-activity col-lg-12 col-md-12 col-ms-12 ">
                                <div class="content-activity">
                                  
                                    
                                    <p>วัน : <?php echo $info['date'];?></p>
                                    <p>เวลา : <?php echo $info['time'];?></p>
                                    <p>สถานที่ : <?php echo $info['location'];?></p>
                                    
                                    <?php if($info['promotion']!=""){ ?>
                                    
                                    <p>ราคา : <span style="font-size:18px; text-decoration: pne-through;text-decoration-color: red;"><?php echo number_format($info['price']);?> </span> บาท</p>
                                    <p>โปรโมชั่น : <?php echo number_format($info['promotion']);?> บาท</p>
                                    
                                    <?php }else{ ?>
                                   
                                    <p>ราคา : <?php echo number_format($info['price']);?> บาท</p>
                                    
                                   <?php } ?>

                                    
                                </div>
                                <div class="content-detail">
                                        <?php echo html_entity_decode($info['detail']); ?>
                                </div><!-- single-post -->
                                 <div class="socila-link">
                                   <!--  <div class="sharethis-inline-share-buttons"></div> -->
                                     <?php echo Modules::run('social/share_button'); ?>
                                </div>
                                <?php if(!empty($image_g)){ ?>

                                     <div class="col-xs-12">  
                                        <br>   
                                        <center><h3> <?php echo $info['title_gallery']; ?> </h3></center>
                                        <br>
                                        <div id="gallery" style="display:none;">
                                            <?php foreach ($image_g as $key => $value) { ?>
                                                    <a href="#">
                                                    <img alt="gallery"
                                                         src="<?php echo $value;?>"
                                                         data-image="<?php echo $value;?>"
                                                         data-description="This is a Lemon Slice"
                                                         style="display:none">
                                                    </a>
                                             <?php } ?>
                                        </div>
                                    </div>   
                                 <?php } ?>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!--================Blog Area =================-->
    <input type="hidden" name="linkId" id="linkId" value="<?php echo $title_link; ?>">