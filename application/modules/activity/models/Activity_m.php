<?php defined('BASEPATH') OR exit('No direct script access allowed.');

class Activity_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->from('activity a')
                        ->join('user c', 'a.createBy = c.userId', 'left')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('activity a')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {
        //$this->db->where('a.grpContent', $param['grpContent']);
        $this->db->where('a.recycle',0);
        $this->db->where('a.active',1);


        if ( isset($param['keyword']) ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['keyword'])
                    ->or_like('a.excerpt', $param['keyword'])
                    ->or_like('a.detail', $param['keyword'])
                    ->group_end();
        }

        if ( isset($param['title_link']) ) {
            $this->db->like('a.title_link', $param['title_link']);
        }

        

        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $param['createStartDate'] = $param['createStartDate']." 00:00";
            $param['createEndDate'] = $param['createEndDate']." 23:59";
            
            $this->db->where('a.createDate >=', $param['createStartDate']);
            $this->db->where('a.createDate <=', $param['createEndDate']);

        }
        
        

       
       
        if ( isset($param['activityId']) ) 
             $this->db->where('a.activityId', $param['activityId']);
         if ( isset($param['recommend']) ) 
             $this->db->where('a.recommend', $param['recommend']);

        if ( !in_array($this->router->method, array("profile","check_password","check_email")))
            $this->db->where('a.activityId !=', $this->session->activity['activityId']);
        
        if (isset($param['exclude'])) {
            $this->db
                    ->where('a.activityId !=', $param['exclude']);
        }

        if ( isset($param['recent']) && $param['recent'] != "" ) {
           $this->db->where("(a.endDate >='".date('Y-m-d')."')");
           $this->db->order_by('a.startDate', 'ASC');
        }

        $this->db->order_by('recommend', 'DESC');
        $this->db->order_by('createDate', 'DESC');
        $this->db->order_by('updateDate', 'DESC');


    }
    
    public function get_by_id($id)
    {
        $query = $this->db
                        ->select('a.*')
                        ->from('activity a')
                        ->where('a.activityId', $id)
                        ->get()
                        ->row_array();
        return $query;
    }
    
    public function plus_view($id)
    {
        $sql = "UPDATE activity SET view = (view+1) WHERE activityId=?";
        $this->db->query($sql, array($id));
    }
    
    public function get_unread($readNews)
    {
        $query = $this->db
                        ->select('a.*')
                        ->from('news a')
                        ->where('a.newsActive', 1)
                        ->where('a.newsRecycle', 0)
                        ->where('a.approve', 1)        
                        ->where_not_in('a.newsId', $readNews)
                        ->order_by('a.newsDateCreate', 'desc')
                        ->get()
                        ->result_array();
        return $query;
    }

    public function get_uplode($param) 
    {
        $this->_condition_uplode($param);
        $query = $this->db
                        ->select('a.*,b.*')
                        ->from('upload a')
                        ->join('upload_content b', 'a.uploadId = b.uploadId', 'left')
                        ->get();
        return $query;
    }

    private function _condition_uplode($param) 
    {

        $this->db->where('a.grpContent', $param['grpContent']);
        
        if ( isset($param['contentId']) ) 
             $this->db->where('b.contentId', $param['contentId']);

        if ( isset($param['grpType']) ) 
             $this->db->where('b.grpType', $param['grpType']);

    }
        
}