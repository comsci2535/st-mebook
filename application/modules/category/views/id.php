    <!-- banner-department -->
    <div class="panel-heading1 fontLv4"><?php echo $info['categoryName'] ?></div>
    <div class="banner-department">
        <img style="width:100%" src="<?php echo $info['image'] ?>" class="img-responsive" alt="" align="center">
    </div>
    <!-- banner-department -->

    <!-- menu-department -->    
    <div class="menu-department container-fluid">
        <div class="col-xs-12 col-sm-4 link-department">
            <a href="<?php echo base_url("news?categoryId={$info['categoryID']}") ?>">
            <div class="col-xs-12 top">
                <img src="<?php echo base_url() ?>assets/include/images/icon/icon-department1.png" class="img-responsive" alt="">
            </div>
            <div class="col-xs-12 bottom text-center">
                <h3 class="color5">ข่าว</h3>
                <h1 class="color4 bold"><?php //echo $info['categoryName'] ?></h1>                
            </div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-4 link-department">
            <a href="<?php echo base_url("video?categoryId={$info['categoryID']}") ?>">
            <div class="col-xs-12 top">
                <img src="<?php echo base_url() ?>assets/include/images/icon/icon-department2.png" class="img-responsive" alt="">
            </div>
            <div class="col-xs-12 bottom text-center">
                <h3 class="color5">วีดีโอ</h3>
                <h1 class="color4 bold"><?php //echo $info['categoryName'] ?></h1>                
            </div>
            </a>            
        </div>
        <div class="col-xs-12 col-sm-4 link-department">
            <a href="<?php echo base_url("image?categoryId={$info['categoryID']}") ?>">
            <div class="col-xs-12 top">
                <img src="<?php echo base_url() ?>assets/include/images/icon/icon-department3.png" class="img-responsive" alt="">
            </div>
            <div class="col-xs-12 bottom text-center">
                <h3 class="color5">รูปภาพ</h3>
                <h1 class="color4 bold"><?php //echo $info['categoryName'] ?></h1>                
            </div>       
            </a>     
        </div>        
    </div>
    <!-- menu-department --> 