<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Category extends MX_Controller {
    
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('category_m');
    }
    public function get_()
    {
        $input['categoryType'] = $this->_grpContent;
        $data['category'] = $this->category_m->get_rows($input)->result_array();
    }
    public function get_list()
    {
        $type=$this->input->get('type');
        $input['recycle'] = 0;
        $input['active'] = 1;
        $input['categoryType'] = $type;
        $info = $this->category_m->get_rows($input);
        
        //print_r($info->result());exit();
        return $info->result();

    }
    
    public function id($id=0)
    {
        $this->load->module('front');
        Modules::run('track/front', $id);
        $info = $this->category_m->get_by_id($id);
        if (empty($info)) show_error ("ขออภัยค่ะ ไม่พบหน้าที่ต้องการ", 404, "โรงพยาบาลเกมษราษฏร์");
        
        if ( is_file($info['categoryPath'].$info['categoryFile'])) {
            $info['image'] = base_url($info['categoryPath'].$info['categoryFile']);
        } else {
            $info['image'] = base_url('assets/include/images/category_df.png');
        }
        $this->_social_share();
        $data['info'] = $info;
        $data['contentView'] = "category/id";
        $this->front->layout($data);
    }
    
    public function dd()
    {
        $info = $this->category_m->get_list_dd();
        if (!empty($info)) {
            $temp = array(""=>'ศูนย์รักษาทั้งหมด');
            foreach ($info as $rs) $temp[$rs['categoryID']] = $rs['categoryName'];
        } else {
            $temp = array(""=>'ไม่พบหมวดหมู่');
        }
        return $temp;
    }
    
    public function dd_sub($categoryId)
    {
        if ( $categoryId || TRUE ) {
            $info = $this->category_m->get_list_dd_sub($categoryId);
            if (!empty($info)) {
                $temp = array(""=>'หมวดหมู่ทั้งหมด');
                foreach ($info as $rs) $temp[$rs['categoryID']] = $rs['categoryName'];
            } else {
               $temp = array(""=>'ไม่พบหมวดหมู่');
            }
        } else {
            $temp = array(""=>'เลือกหมวดหมู่');
        }

        return $temp;
    }
    
    public function all_child($currentId)
    {
        $info = $this->category_m->all_child_category("category", "categoryId", "categoryMain", $currentId);
        return $info;
    }
    
    public function all_parent($currentId)
    {
        $info = $this->category_m->all_parent_category("category", "categoryId", "categoryMain", $currentId);
        return $info;
    }
    
    public function _social_share()
    {
        $param['ogType'] = "webiste";
        $param['ogUrl'] = site_url();
        $param['ogTitle'] = 'Page title';
        $param['ogDescription'] = config_item('metaDescription');
        $param['ogImage'] = config_item('metaOgImage');
        $param['twImage'] = config_item('metaTwImage');
        
        Modules::run('social/set_share', $param);
    }    
    
    public function name($id=0)
    {
        $info = $this->category_m->get_by_id($id);
        if ( !empty($info) ) {
            return $info['categoryName'];
        } else {
            return NULL;
        }
    }
}
