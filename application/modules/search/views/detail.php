<!-- Article Starts -->
					<article >
						<a href="#"><h4><?php echo $info['title'] ?></h4></a>
						<!-- Figure Starts -->
						<figure class="blog-figure">
							<img class="responsive-img" src="<?php echo $info['image'] ?>" alt="">
						</figure>
					      <div class="clearfix"></div>
						<!-- Figure Ends -->
						<!-- Excerpt Starts -->

						<div class="blog-excerpt second-font" >
							
							<!-- Meta Starts -->
							<div class="meta second-font">
								
								<span><i class="fa fa-user"></i> <a href="#"><?php echo $info['username'] ?></a></span>
                                <span class="date"><i class="fa fa-calendar"></i> <?php echo date_language($info['createDate'],false,'th') ?></span>

               					<div class="date" style="margin-top: 15px;color: #fa5b0f;">สถานที่ <?php echo $info['location']; ?></div>
               					<?php if($info['statusBuy']==0){ ?>
               					<div class="date" style="margin-top: 1px;color: #fa5b0f;">ราคา <?php echo price_th(str_replace(",","",$info['price'])) ?></div>
                				
                            <div style="margin-top: 15px;">

							   <?=$info['detail'];?>
							</div>
							<?php }else{ ?>

						    <div style="margin-top: 15px; text-align: center; color: red; font-size: 30px;">

							   ขายแล้ว
							</div>
							<?php } ?>

							<!-- Meta Ends -->
							
							<!-- <div class="meta second-font">
							</div> -->
						</div>
						<br>
						<h6>Gallery </h6>
                      <!-- <div id="gallery1"></div> -->

                      <div id="gallery" style="display:none;">
                      	 <?php foreach ($image_g as $key => $value) { ?>
                      	 		<a href="#">
								<img alt="gallery"
								     src="<?php echo $value;?>"
								     data-image="<?php echo $value;?>"
								     data-description="This is a Lemon Slice"
								     style="display:none">
								</a>
                      	 <?php } ?>

                      </div>


						<!-- Excerpt Ends -->
						<?php if($info['latitude']!="" && $info['longitude']!="" && $info['statusBuy']==0){ ?>
							<br>
						<map name="map">
							<h6>ตำแหน่งที่ตั้ง </h6>
				         <div id="map" style="height:300px"></div>
				          <script async defer src="https://maps.googleapis.com/maps/api/js?=v3&key=AIzaSyDaz8JbxbUyy8acSAPy4ENzPhCaY4o_kj4&language=th&callback=initMap"></script>
				          <script>
				                 
				                  var latitude =<?php if(!empty($info['latitude'])){echo $info['latitude'];}?>;
				                  var lontitude =<?php if(!empty($info['longitude'])){echo $info['longitude'];}?>;
				                 
				                  var zoom =16;
				                  var image = '';
				                  function initMap() {
				                    var cairo = { lat:latitude, lng:lontitude };
				                    var center = { lat: latitude, lng: lontitude };
				                    var map = new google.maps.Map(document.getElementById('map'), {
				                      scaleControl: true,
				                      center: center,
				                      zoom: zoom,
				                      icon: image
				                    });
				                    var infowindow = new google.maps.InfoWindow;
				                    infowindow.setContent('<b> <?php echo $info['title'];?> </b>');
				                    var marker = new google.maps.Marker({map: map, position: cairo});
				                    marker.addListener('click', function() {
				                      infowindow.open(map, marker);
				                    });
				                  }
				                </script>
				          </div>
				        </map>
				    <?php } ?>
					</article>
					<!-- Article Ends -->
					<input type="hidden" name="contentId" id="contentId" value="<?php echo $info['contentId'];?>">
					<script>
						
						// var images_g = [
			   //               <?php foreach ($image_g as $key => $value) { ?>
			   //                '<?php echo $value;?>',
			   //               <?php } ?>
			   //          ];
					</script>