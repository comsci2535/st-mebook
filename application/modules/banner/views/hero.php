

<div class="home">
    <div class="home_slider_container" id="home_slider">
      
      <!-- Home Slider -->
      <div class="owl-carousel owl-theme home_slider">
        
        <!-- Home Slider Item -->
         <?php foreach ($info as $rs) :?>
        <div class="owl-item">
          

            <div class="home_slider_background" style="background-image:url(<?php echo $rs->image['coverImage'] ?>)"></div>
            <!-- <div class="mobile-banner home_slider_background" style="background-image:url(<?php echo $rs->image['contentImage'] ?>)"></div> -->
            <div class="home_slider_content">
              <div class="container">
                <div class="row">
                   <div class="col text-center">
                    <div class="home_slider_title"><?php echo $rs->excerpt ?></div> 
                    <div class="home_slider_subtitle"><?php echo $rs->detail ?></div> 
                     <?php if ($rs->link){ ?>
      
                    <a class="main_btn" href="<?php echo $rs->link ?>">อ่านต่อ</a>
                  <?php } ?>
                  </div> 
                </div>
              </div>
            </div>
          
          
        </div>

         <?php endforeach; ?>

      </div>
    </div>

    <!-- Home Slider Nav -->

    <!-- <div class="home_slider_nav home_slider_prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
    <div class="home_slider_nav home_slider_next"><i class="fa fa-angle-right" aria-hidden="true"></i></div> -->
  </div>

