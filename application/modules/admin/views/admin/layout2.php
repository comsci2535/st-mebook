<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <base href="<?php echo site_url(); ?>">
        <title><?php echo $pageTitle; ?></title>

        <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/Ionicons/css/ionicons.min.css">

        <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
        <link href="<?php echo base_url() ?>assets/plugins/icheck/skins/square/grey.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/plugins/toastr/toastr.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/bower_components/select2/dist/css/select2.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/plugins/cropper-master/css/cropper.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css"/>
        <?php if (in_array($this->router->method, array('order'))) : ?>  
        <link href="<?php echo base_url() ?>assets/node_modules/nestable2/jquery.nestable.css" rel="stylesheet" type="text/css"/>
        <?php endif; ?>
        
        <?php if (in_array($this->router->method, array('edit','create'))) : ?>
        <link href="<?php echo base_url() ?>assets/plugins/jQuery-File-Upload-master/css/jquery.fileupload.css" rel="stylesheet" type="text/css"/>
        <?php endif; ?>

        <link rel="stylesheet" href="<?php echo base_url("assets/") ?>dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="<?php echo base_url("assets/") ?>dist/css/skins/_all-skins.min.css">
        
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-toggle-master/css/bootstrap-toggle.css">
        <link href="<?php echo base_url("assets/") ?>css/admin/custom.css" rel="stylesheet" type="text/css"/>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <?php $this->load->view('header'); ?>
            <?php $this->load->view('sidebar'); ?>
            <div class="content-wrapper">
                <?php $this->load->view('content_header'); ?>
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php $this->load->view($contentView); ?>
                        </div>
                    </div>
                </section>
            </div> 
                <footer class="main-footer">
                    <div class="pull-right hidden-xs">
                        <b>เวอร์ชั่น</b> 1.0.0
                    </div>
                    <strong>Copyright &copy; 2018 <a href="https://www.praram9.com/">โรงพยาบาลพระรามเก้า</a></strong> สงวนลิขสิทธิ์.
                </footer>
               
            <?php $this->load->view('asside'); ?>
            <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>

        <script src="<?php echo base_url() ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
        
        <script src="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?php echo base_url() ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
        <script src="<?php echo base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/bower_components/moment/moment.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/plugins/bootstrap-toggle-master/js/bootstrap-toggle.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/icheck/icheck.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/plugins/toastr/toastr.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/plugins/jquery.form.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/jquery-validation/dist/localization/messages_th.js"></script>  
        <script src="<?php echo base_url() ?>assets/plugins/jQueryUI/jquery-ui.min.js" type="text/javascript"></script>

        <script src="<?php echo base_url() ?>assets/node_modules/highcharts/highcharts.js"></script>
        <script src="<?php echo base_url() ?>assets/node_modules/highcharts/highcharts-3d.js"></script>
        <script src="<?php echo base_url() ?>assets/node_modules/highcharts/modules/exporting.js"></script>
        <script src="<?php echo base_url() ?>assets/node_modules/highcharts/modules/export-data.js"></script>
        
    <?php if (in_array($this->router->class, array('news', 'user_profile', 'user'))) : ?>
        <?php if (in_array($this->router->method, array('edit','create'))) : ?>      
        <script src="<?php echo base_url() ?>assets/plugins/cropper-master/js/cropper.js" type="text/javascript"></script>
        <!--<script src="<?php echo base_url() ?>assets/plugins/canvas-to-blob.js" type="text/javascript"></script>-->
        <script src="<?php echo base_url() ?>assets/plugins/jQuery-File-Upload-master/js/vendor/jquery.ui.widget.js" type="text/javascript"></script>
        <script src="https://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
        <script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
        <script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/jQuery-File-Upload-master/js/jquery.iframe-transport.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/plugins/jQuery-File-Upload-master/js/jquery.fileupload.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/plugins/jQuery-File-Upload-master/js/jquery.fileupload-process.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/plugins/jQuery-File-Upload-master/js/jquery.fileupload-image.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/plugins/jQuery-File-Upload-master/js/jquery.fileupload-audio.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/plugins/jQuery-File-Upload-master/js/jquery.fileupload-video.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/plugins/jQuery-File-Upload-master/js/jquery.fileupload-validate.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/plugins/jQuery-File-Upload-master/js/jquery.fileupload-ui.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/plugins/jQuery-File-Upload-master/js/main.js" type="text/javascript"></script>
        <!--[if (gte IE 8)&(lt IE 10)]>
        <script src="<?php echo base_url() ?>assets/plugins/jQuery-File-Upload-master/js/cors/jquery.xdr-transport.js"></script>
        <![endif]-->
        <?php endif; ?>
    <?php endif; ?>
        
        <?php if (in_array($this->router->method, array('order'))) : ?>  
        <script src="<?php echo base_url() ?>assets/node_modules/nestable2/jquery.nestable.js" type="text/javascript"></script>
        <?php endif; ?>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url("assets/") ?>dist/js/adminlte.min.js"></script>
        <script src="<?php echo base_url("assets/") ?>dist/js/demo.js"></script>
        <script src="<?php echo base_url("assets/") ?>scripts/admin/app.js?v=<?php echo rand(0,100) ?>"></script>
        <?php echo $pageScript; ?>
        <script>
            var siteUrl = "<?php echo site_url(); ?>";
            var baseUrl = "<?php echo base_url(); ?>";
            var controller = "<?php echo $this->router->class ?>";
            var method = "<?php echo $this->router->method ?>";
            $(document).ready(function () {
                <?php if ($this->session->toastr) : ?>
                    setTimeout(function () {
                        toastr.<?php echo $this->session->toastr['type']; ?>('<?php echo $this->session->toastr['lineTwo']; ?>', '<?php echo $this->session->toastr['lineOne']; ?>');
                    }, 500);
                    <?php $this->session->unset_userdata('toastr'); ?>
                <?php endif; ?>
            });            
        </script>
    </body>
</html>
