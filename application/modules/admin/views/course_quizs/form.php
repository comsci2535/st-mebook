<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">แบบฟอร์ม</h3>
    </div>
    <?php echo form_open_multipart($frmAction, array('class' => 'form-horizontal frm-main', 'method' => 'post', 'autocomplete'=>'off')) ?>
    <div class="box-body">
        <div class="form-group">
            <label class="col-sm-3 control-label">คำถาม</label>
            <div class="col-sm-7">
                <input value="<?=!empty($title)? $title : '' ?>" type="text" id="input-title" class="form-control" name="title" placeholder="ระบุคำถาม" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">คำตอบ</label>
            <div class="col-sm-7">
                <input value="<?=!empty($answer)? $answer : '' ?>" type="text" id="" class="form-control" name="answer" placeholder="ระบุคำตอบ" required>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-7 col-sm-offset-3">
                <table class="table">
                    <thead>
                        <tr>
                            <th style="width:10%">เลือก</th>
                            <th style="width:20%">รูป</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <label class="icheck-inline">
                                    <?php 
                                        !empty($quizs_detail[0]->point) && $quizs_detail[0]->point > 0? $checked_0 = 'checked' : $checked_0 = ''
                                    ?>
                                    <input type="radio" name="point[]" value="0" class="icheck" <?=$checked_0?>/>
                                </label>
                            </td>
                            <td>
                                <img src="<?=!empty($quizs_detail[0]->file)? base_url().$quizs_detail[0]->file : base_url('assets/images/no_image.png')?>" id="" class="img-thumbnail text-img-file_0" alt="Cinque Terre" style="width: 150px;">
                                <input type="file" class="form-control" name="files_0" data-img="text-img-file_0" onchange="readURL(this, 'text-img-file_0');">
                                <input type="hidden" name="old_file_0" value="<?=!empty($quizs_detail[0]->file)? $quizs_detail[0]->file : ''?>">
                                <input type="hidden" name="title_detail[]" value="1">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="icheck-inline">
                                    <?php 
                                        !empty($quizs_detail[1]->point) && $quizs_detail[1]->point > 0? $checked_1 = 'checked' : $checked_1 = ''
                                    ?>
                                    <input type="radio" name="point[]" value="1" class="icheck" <?=$checked_1?>/>
                                </label>
                            </td>
                            <td>
                                <img src="<?=!empty($quizs_detail[1]->file)? base_url().$quizs_detail[1]->file : base_url('assets/images/no_image.png')?>" id="" class="img-thumbnail text-img-file_1" alt="Cinque Terre" style="width: 150px;">
                                <input type="file" class="form-control text-img-file" name="files_1" data-img="text-img-file_1" onchange="readURL(this, 'text-img-file_1');">
                                <input type="hidden" name="old_file_1" value="<?=!empty($quizs_detail[2]->file)? $quizs_detail[1]->file : ''?>">
                                <input type="hidden" name="title_detail[]" value="1">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="icheck-inline">
                                    <?php 
                                        !empty($quizs_detail[2]->point) && $quizs_detail[2]->point > 0? $checked_2 = 'checked' : $checked_2 = ''
                                    ?>
                                    <input type="radio" name="point[]" value="2" class="icheck" <?=$checked_2?>/>
                                </label>
                            </td>
                            <td>
                                <img src="<?=!empty($quizs_detail[2]->file)? base_url().$quizs_detail[2]->file : base_url('assets/images/no_image.png')?>" id="" class="img-thumbnail text-img-file_2" alt="Cinque Terre" style="width: 150px;">
                                <input type="file" class="form-control text-img-file" name="files_2" data-img="text-img-file_2" onchange="readURL(this, 'text-img-file_2');">
                                <input type="hidden" name="old_file_2" value="<?=!empty($quizs_detail[2]->file)? $quizs_detail[2]->file : ''?>">
                                <input type="hidden" name="title_detail[]" value="1">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="icheck-inline">
                                    <?php 
                                        !empty($quizs_detail[3]->point) && $quizs_detail[3]->point > 0? $checked_3 = 'checked' : $checked_3 = ''
                                    ?>
                                    <input type="radio" name="point[]" value="3" class="icheck" <?=$checked_3?>/>
                                </label>
                            </td>
                            <td>
                                <img src="<?=!empty($quizs_detail[3]->file)? base_url().$quizs_detail[3]->file : base_url('assets/images/no_image.png')?>" id="" class="img-thumbnail text-img-file_3" alt="Cinque Terre" style="width: 150px;">
                                <input type="file" class="form-control text-img-file" name="files_3" data-img="text-img-file_3" onchange="readURL(this, 'text-img-file_3');">
                                <input type="hidden" name="old_file_3" value="<?=!empty($quizs_detail[3]->file)? $quizs_detail[3]->file : ''?>">
                                <input type="hidden" name="title_detail[]" value="1">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>      
    <div class="box-footer">
        <div class="col-sm-7 col-sm-offset-3">
            <button class="btn btn-primary btn-flat pullleft" type="submit"><i class="fa fa-save"></i> บันทึก</button> 
        </div>
    </div>
    <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
    <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->course_quizsId) ? encode_id($info->course_quizsId) : 0 ?>">
    <input type="hidden" name="course_contentId" id="input-contentId" value="<?php echo isset($course_contentId) ? $course_contentId : 0 ?>">
    <?php echo form_close() ?>
</div>




