<style>
    .datepicker {
      z-index: 1600 !important; /* has to be larger than 1050 */
    }
</style>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">รายการ</h3>
        <div class="box-tools pull-right">
            <?php echo $boxAction; ?>
        </div>
    </div>
    <div class="box-body filter">
        <form id="frm-filter" role="form">
             
            <div class="col-md-3 form-group">
                <label for="active">สถานะ</label>
                <?php $activeDD = array(""=>'ทั้งหมด', 1=>'เปิด', 0=>'ปิด') ?>
                <?php echo form_dropdown('active', $activeDD, null, 'class="from-control select2"') ?>
            </div> 
            
            <div class="col-md-7 form-group">
                <label for="keyword">คำค้นหา</label>
                <input class="form-control" name="keyword" type="text">
            </div>           
            <div class="col-md-1 form-group">
                <button type="button" class="btn btn-primary btn-flat btn-block btn-filter"><i class="fa fa-filter"></i> ค้นหา</button>
            </div>
        </form>
    </div>
    <div class="box-body">
        <form  role="form">
          <!--   <div style="overflow-x: auto !important; min-width: 700px;"> -->
    
            <table id="data-list" class="table table-hover dataTable  table-striped table-bordered nowrap" width="100%">
                <thead>
                    <tr>
                       
                        <th><input name="tbCheckAll" type="checkbox" class="icheck tb-check-all"></th>
                        <th >ชื่อ-นามสกุล</th>
                        <!--  <th>คอร์สเรียน</th> -->
                        <th>เบอร์โทร</th>
                        <th >อีเมล์</th>
                        <th >วันที่สิ้นสุดแพ็กเกจ/วันที่เหลือ</th>
                        <th >ใช้งานล่าสุด</th>
                        <th >ยืนยันอีเมล์</th>
                        <th >สถานะ</th>
                        <th ></th>
                    </tr>
                </thead>
            </table>
            
            <!-- </div> -->
        </form>
    </div>  
    <div id="overlay-box" class="overlay">
        <i class="fa fa-circle-o-notch fa-spin"></i>
    </div>         
</div>


 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">กำหนดจำนวนวันที่ชดเชย (รายคน)</h4>
        </div>
        <div class="modal-body">
             <input type='hidden' class='form-control ' id='userType' name='userType' value="member" />
            <input type='hidden' class='form-control userId' id='userId' name='userId' />
         <div class='form-group'>
            <input type='number' class='form-control' id='endStudyDateNum' name='endStudyDateNum' />
            <div id="endStudyDateNumSpan"></div>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" id="memberCnf"><span id="img-wait-member"></span> บันทึก</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
        </div>
      </div>
      
    </div>
  </div>

    <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">กำหนดจำนวนวันที่ชดเชย (สมาชิกทั้งหมด)</h4>
        </div>
        <div class="modal-body">
            
         <div class='form-group'>
            <input type='number' class='form-control' id='endStudyDateNum2' name='endStudyDateNum2' />
            <div id="endStudyDateNumSpan2"></div>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" id="memberAllCnf"><span id="img-wait-member-all"></span> บันทึก</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
        </div>
      </div>
      
    </div>
  </div>