<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
	}
	
    public function get_by_email($param)
    {
        $query = $this->db
                        ->select('a.*')
                        ->select('b.active policyActive')
                        ->from('user a')
                        ->join('policy b', 'a.policyId = b.policyId', 'left')
                        ->where('a.email', $param['email'])
                        ->where('a.recycle', 0)
                        ->get()
                        ->row_array();
        return $query;                
    }
    public function get_by_username($param)
    {
        $query = $this->db
                        ->select('a.*')
                        ->select('b.active policyActive')
                        ->from('user a')
                        ->join('policy b', 'a.policyId = b.policyId', 'left')
                        ->where('a.username', $param['username'])
                        ->where('a.recycle', 0)
                        ->get()
                        ->row_array();
        return $query;                
    }
    
    public function update_last_login()
    {
        $this->db->where('userId', $this->session->user['userId'])
                ->update('user', array('lastLogin'=>db_datetime_now()));
    }       

}
