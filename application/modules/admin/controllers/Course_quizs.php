<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Course_quizs extends MX_Controller {

    private $_title = "จัดการแบบฝึกหัด";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับจัดการแบบฝึกหัด";
    private $_grpContent = "course_quizs";
    private $_requiredExport = true;
    private $_permission;

    public function __construct() 
    {
        parent::__construct();
        $this->_permission = Modules::run('admin/permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->model("course_quizs_m");
    }
    
    public function index($id) 
    {

        $data['course_contentId'] = $id;

        $this->load->module('admin/admin');
        
        // toobar
        $export = array(
            'excel' => site_url("admin/{$this->router->class}/excel"),
            'pdf'   => site_url("admin/{$this->router->class}/pdf"),
        );

        $action[1][]        = action_refresh(site_url("admin/{$this->router->class}/index/{$id}"));
        $action[1][]        = action_filter();
        $action[2][]        = action_add(site_url("admin/{$this->router->class}/create/{$id}"));
        $action[2][]        = action_export_group($export);
       
        // 
        $action[3][]        = action_trash_multi("admin/{$this->router->class}/action/trash");
        $action[3][]        = action_trash_view(site_url("admin/{$this->router->class}/trash"));
        $data['boxAction']  = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("admin/{$this->router->class}"));
        
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/index";
        $this->admin->layout($data);
    }    

    public function data_index() 
    {
        $input = $this->input->post();
        parse_str($_POST['frmFilter'], $frmFilter);
        if ( !empty($frmFilter) ) {
            foreach ( $frmFilter as $key => $rs )
                $input[$key] = $rs;
        }
        $input['recycle']   = 0;
        $input['course_contentId']   = decode_id($input['course_contentId']);
        
        $info               = $this->course_quizs_m->get_rows($input);
        $infoCount          = $this->course_quizs_m->get_count($input);
        $column             = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        
        foreach ($info->result() as $key => $rs) {
            $id                         = encode_id($rs->course_quizsId);
            $action                     = array();
            $action[1][]                = table_edit(site_url("admin/{$this->router->class}/edit/{$id}"));
            $active                     = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId']   = $id;
            $column[$key]['checkbox']   = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title']      = $rs->title;
            $column[$key]['active']     = toggle_active($active, "admin/{$this->router->class}/action/active");
            $column[$key]['createDate'] = datetime_table($rs->createDate);
            $column[$key]['updateDate'] = datetime_table($rs->updateDate);
            $column[$key]['action']     = Modules::run('admin/utils/build_button_group', $action);
        }
        $data['data']               = $column;
        $data['recordsTotal']       = $info->num_rows();
        $data['recordsFiltered']    = $infoCount;
        $data['draw']               = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
    
    public function create($id) 
    {
        $this->load->module('admin/admin');

        $data['grpContent']     = $this->_grpContent;
        $data['frmAction']      = site_url("admin/{$this->router->class}/save");
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("admin/{$this->router->class}/index/{$id}"));
        $data['breadcrumb'][]   = array('สร้าง', site_url("admin/{$this->router->class}/create"));
        
        // page detail
        $data['pageHeader']         = $this->_title;
        $data['pageExcerpt']        = $this->_pageExcerpt;
        $data['contentView']        = "admin/{$this->router->class}/form";
        $data['course_contentId']   = $id;

        $this->admin->layout($data);
    }
    
    public function save() 
    {
        $input = $this->input->post(null, true);
        $value = $this->_build_data($input);
        $result = $this->course_quizs_m->insert($value);
        if ( $result ) {
            $result_detail = $this->_build_upload_quizs_detail($result, $input);
            $this->course_quizs_m->insert_course_quizs_detail($result_detail);
            Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}/index/{$input['course_contentId']}"));
    }
    
    public function edit($id="") 
    {
        $this->load->module('admin/admin');
        
        $id = decode_id($id);
        $input['course_quizsId'] = $id;
        $info = $this->course_quizs_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $info = $info->row();
        $data['info'] = $info;
        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("admin/{$this->router->class}/update");
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}/index/{$info->course_contentId}"));
        $data['breadcrumb'][] = array('แก้ไข', site_url("admin/{$this->router->class}/edit"));
        
        // page detail
        $data['pageHeader']         = $this->_title;
        $data['pageExcerpt']        = $this->_pageExcerpt;
        $data['contentView']        = "admin/{$this->router->class}/form";
        $data['course_contentId']   = $info->course_contentId;
        $data['course_quizsId']               = $info->course_quizsId;
        $data['title']               = $info->title;
        $data['answer']               = $info->answer;
        $data['quizs_detail']       = $this->course_quizs_m->get_course_quizs_detail($info->course_quizsId)->result();
        $this->admin->layout($data);
    }
    
    public function update() 
    {
        $input  = $this->input->post(null, true);
        $id     = decode_id($input['id']);
        $value  = $this->_build_data($input);
        $result = $this->course_quizs_m->update($id, $value);
        if ( $result ) {
            $result_detail = $this->_build_update_quizs_detail($id, $input);
            $this->db->delete('course_quizs_detail', array('course_quizsId' => $id));
            $this->course_quizs_m->insert_course_quizs_detail($result_detail);
            
            Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}/index/{$input['course_contentId']}"));
    }
    
    private function _build_data($input) 
    {
        
        $value['title']                 = $input['title'];
        $value['answer']                 = $input['answer'];
        $value['course_contentId']      = $input['course_contentId'];
        $value['quizs_type_id']         = $input['quizs_type_id'];
        
        $path   = 'course_quizs';
        $upload = $this->do_upload('files', TRUE, $path);
        $file   = '';
		if(isset($upload['index'])){
            $file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
            $outfile = $input['outfile'];
			if(isset($outfile)){
				$this->load->helper("file");
				unlink($outfile);
            }
            $value['file'] = $file;
        }
        
        if ( $input['mode'] == 'create' ) {
            $value['createDate']    = db_datetime_now();
            $value['createBy']      = $this->session->user['userId'];
            $value['updateDate']    = db_datetime_now();
            $value['updateBy']      = $this->session->user['userId'];
        } else {
            $value['updateDate']    = db_datetime_now();
            $value['updateBy']      = $this->session->user['userId'];
        }
        return $value;
    }
    
    private function _build_upload_quizs_detail($id, $input)
    {
        $value_arr    = array();
        $createDate   = db_datetime_now();
        $createBy     = $this->session->user['userId'];
        $point        = $input['point'][0];
        if(!empty($input['title_detail'])){
            foreach ($input['title_detail'] as $key => $value) {
                
                $path   = 'course_quizs';
                $upload = $this->do_upload('files_'.$key, TRUE, $path);
                $file   = '';
                if(isset($upload['index'])){
                    $file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
                }
                array_push($value_arr, array(
                    'course_quizsId'=> $id,
                    'title'         => '',
                    'file'          => $file,
                    // 'point'         => !empty($input['point'][$key]) ? $input['point'][$key] : 0,
                    'point'         => ($key ==  $point) ? 1 : 0,
                    'createDate'    => $createDate,
                    'createBy'      => $value,
                    'updateDate'    => $createDate,
                    'updateBy'      => $createBy,
                    'active'        => 1,
                ));
            }
        }

        return $value_arr;
    } 
    
    private function _build_update_quizs_detail($id, $input)
    {
        $value_arr    = array();
        $createDate   = db_datetime_now();
        $createBy     = $this->session->user['userId'];
        $point        = $input['point'][0];
        if(!empty($input['title_detail'])){
            foreach ($input['title_detail'] as $key => $value) {
                
                $path   = 'course_quizs';
                $upload = $this->do_upload('files_'.$key, TRUE, $path);
                $file   = '';
                if(isset($upload['index'])){
                    $file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
                    $outfile = $input['old_file_'.$key];
                    if(isset($outfile)){
                        $this->load->helper("file");
                        unlink($outfile);
                    }
                }else{
                    $file = $input['old_file_'.$key];
                }
                
                array_push($value_arr, array(
                    'course_quizsId'=> $id,
                    'title'         => '',
                    'file'          => $file,
                    // 'point'         => !empty($input['point'][$key]) ? $input['point'][$key] : 0,
                    'point'         => ($key ==  $point) ? 1 : 0,
                    'createDate'    => $createDate,
                    'createBy'      => $value,
                    'updateDate'    => $createDate,
                    'updateBy'      => $createBy,
                    'active'        => 1,
                ));
            }
        }
        return $value_arr;
    }
    
    public function excel()
    {
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle'] = 0;
        $input['grpContent'] = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info = $this->course_quizs_m->get_rows($input);
        $fileName = "course_quizs";
        $sheetName = "Sheet name";
        $sheetTitle = "Sheet title";
        
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        
        $styleH = excel_header();
        $styleB = excel_body();
        $colums = array(
            'A' => array('data'=>'รายการ', 'width'=>50),
            'B' => array('data'=>'เนื้อหาย่อ','width'=>50),
            'C' => array('data'=>'วันที่สร้าง', 'width'=>16),            
            'D' => array('data'=>'วันที่แก้ไข', 'width'=>16),
        );
        $fields = array(
            'A' => 'title',
            'B' => 'excerpt',
            'C' => 'createDate',            
            'D' => 'updateDate',
        );
        $sheet->setTitle($sheetName);
        
        //title
        $sheet->setCellValue('A1', $sheetTitle);
        $sheet->setCellValue('D1', 'วันที่สร้าง: '.date("d/m/Y, H:i"));
        $sheet->getStyle('D1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $sheet->getRowDimension(1)->setRowHeight(20);
        
        //header
        $rowCount = 2;
        foreach ( $colums as $colum => $data ) {
            $sheet->getStyle($colum . $rowCount)->applyFromArray($styleH);
            $sheet->SetCellValue($colum.$rowCount, $data['data']);
            $sheet->getColumnDimension($colum)->setWidth($data['width']);
        }
        
        // data
        $rowCount++;
        foreach ( $info->result() as $row ){
            foreach ( $fields as $key => $field ){
                $value = $row->{$field};
                if ( $field == 'createDate' || $field == 'updateDate' )
                    $value = datetime_table ($value);
                $sheet->getStyle($key . $rowCount)->applyFromArray($styleB);
                $sheet->SetCellValue($key . $rowCount, $value); 
            }
            $sheet->getRowDimension($rowCount)->setRowHeight(20);
            $rowCount++;
        }
        $sheet->getRowDimension($rowCount)->setRowHeight(20);  
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
        $writer->save('php://output');
        exit; 
    }
    
    public function pdf()
    {
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle'] = 0;
        $input['grpContent'] = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info = $this->course_quizs_m->get_rows($input);
        $data['info'] = $info;
        
        $mpdfConfig = [
            'default_font_size' => 9,
            'default_font' => 'garuda'
        ];
        $mpdf = new \Mpdf\Mpdf($mpdfConfig);
        $content = $this->load->view("{$this->router->class}/pdf", $data, TRUE);
        $mpdf->WriteHTML($content);
        
        
        $download = FALSE;
        $saveFile = FALSE;
        $viewFile = TRUE;
        $fileName = "course_quizs.pdf";
        $pathFile = "uploads/pdf/course_quizs/";
        if ( $saveFile ) {
            create_dir($pathFile);
            $fileName = $pathFile.$fileName;
            $mpdf->Output($fileName, 'F');
        }
        if ( $download ) 
            $mpdf->Output($fileName, 'D');
        if ( $viewFile ) 
            $mpdf->Output();
    }
    
    public function trash() 
    {
        $this->load->module('admin/admin');
        
        // toobar
        $action[1][] = action_list_view(site_url("admin/{$this->router->class}"));
        $action[2][] = action_delete_multi(base_url("admin/{$this->router->class}/action/delete"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array("ถังขยะ", site_url("admin/{$this->router->class}/trash"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/trash";
        
        $this->admin->layout($data);
    }

    public function data_trash() 
    {
        $input = $this->input->post();
        $input['recycle'] = 1;
        $info = $this->course_quizs_m->get_rows($input);
        $infoCount = $this->course_quizs_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->course_quizsId);
            $action = array();
            $action[1][] = table_restore("admin/{$this->router->class}/action/restore");         
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title'] = $rs->title;
            $column[$key]['excerpt'] = $rs->excerpt;
            $column[$key]['recycleDate'] = datetime_table($rs->recycleDate);
            $column[$key]['action'] = Modules::run('admin/utils/build_toolbar', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }    
    
    public function action($type="")
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updateDate'] = $dateTime;
            $value['updateBy'] = $this->session->user['userId'];
            $result = false;
            if ( $type == "active" ) {
                $value['active'] = $input['status'] == "true" ? 1 : 0;
                $result = $this->course_quizs_m->update_in($input['id'], $value);
            }
            if ( $type == "trash" ) {
                $value['active'] = 0;
                $value['recycle'] = 1;
                $value['recycleDate'] = $dateTime;
                $value['recycleBy'] = $this->session->user['userId'];
                $result = $this->course_quizs_m->update_in($input['id'], $value);
            }
            if ( $type == "restore" ) {
                $value['active'] = 0;
                $value['recycle'] = 0;
                $result = $this->course_quizs_m->update_in($input['id'], $value);
            }
            if ( $type == "delete" ) {
                $value['active'] = 0;
                $value['recycle'] = 2;
                $result = $this->course_quizs_m->update_in($input['id'], $value);
            }   
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }  

    public function do_upload($file,$type = TRUE ,$path , $allowed_types = '') {
		
		$upload_path='/uploads/'.$path.'/'.date('Y').'/'.date('m').'/';
		
		if (!is_dir($upload_path ))
		{
			@mkdir('./'.$upload_path , 0777, true);
		}

		if(!empty($allowed_types)):
			$allowed_types = $allowed_types;
		else:
			$allowed_types = 'gif|jpg|jpeg|png|pdf|bmp|doc|docx|txt|xls|xlsx|csv|pptx|zip|7z';
		endif;

		$config['upload_path']          = 'uploads/'.$path.'/'.date('Y').'/'.date('m');
		$config['allowed_types']        = $allowed_types;
        $config['encrypt_name']         = TRUE;
        $this->upload->initialize($config);
        // 
         if ( !$this->upload->do_upload($file) ) {
		
			$data = array('error' => $this->upload->display_errors());
		}
		else
		{
			$data = array('index' => $this->upload->data());
		}
		
		return $data;
		
	}
    
}
