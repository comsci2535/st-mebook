<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_profile extends MX_Controller {

    private $_title = 'ผู้ดูแลระบบ';
    private $_pageExcerpt = 'การจัดการเกี่ยวกับผู้ดูแลระบบ';
    private $_permission;
    private $_grpContent = 'user';

    public function __construct() {
        parent::__construct();
        $this->_permission = Modules::run('admin/permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขออภัยคุณไม่ได้รับสิทธินี้');
            redirect_back();
        }
        $this->load->library('encryption');
        $this->load->model("user_profile_m");
    }
    
    public function edit($id="") {
        $this->load->module('admin/admin');
        
        $id = $this->session->user['userId'];
        $input['userId'] = $id;
        $input['recycle'] = 0;
        $info = $this->user_profile_m->get_rows($input);
        if ( $info->num_rows() == 0 ){
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบข้อมูลที่ต้องการแก้ไข');
            redirect($this->agent->referrer());
        }
            
        $info = $info->row();
        $info->image = base_url("assets/images/no_image.png");
        $info->uploadId = 0;
        $upload = Modules::run('admin/upload/get_upload', $info->userId, 'user', 'image');
        
        if ( $upload->num_rows() != 0 ) {
            $row = $upload->row();
            $info->uploadId = $row->uploadId;
            if ( is_file("{$row->path}/{$row->filename}") )
                $info->image = base_url("{$row->path}thumbnail/{$row->filename}");
        }
       
        $data['coverImage'] = Modules::run('admin/upload/get_upload_tmpl', $info->userId, $this->_grpContent, 'coverImage');
        $data['info'] = $info;
        $data['grpContent'] = $this->_grpContent;
        $data['policyDD'] = Modules::run('admin/policy/dropdown');
        $data['frmAction'] = site_url("admin/{$this->router->class}/update");
        $data['frmActionPassword'] = site_url("admin/{$this->router->class}/update_password");
        
        // breadcrumb
        $data['breadcrumb'][] = array("ตั้งค่า", "javascript:void(0)");
        $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('แก้ไข', base_url("admin/{$this->router->class}/edit"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/edit";
        $data['pageScript'] = "assets/scripts/admin/{$this->router->class}/form.js";
        
        $this->admin->layout($data);
    }
    
    public function update() {
        $input = $this->input->post();
        $input['id'] = decode_id($input['id']);
        $value = $this->_build_data($input);
        $result = $this->user_profile_m->update($input['id'], $value);
        if ( $result ) {
            $value = $this->_build_upload_content($input['id'], $input);
            Modules::run('admin/upload/update_content', $value);
            $image =  Modules::run('admin/upload/get_upload_image', $input['id'], $this->_grpContent, 'coverImage');
            $user = $this->session->user;
            if ( $image->image ) {
                $user['image'] = $image->image;
            } else {
                $user['image'] = base_url('uploads/user.png');
            }
            $user['name'] = $input['firstname'].' '.$input['lastname'];
            $user['email'] = $input['email'];
            $this->session->set_userdata('user', $user);
            Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}/edit"));
    }
    
    public function update_password() {
        $input = $this->input->post();
        $input['userId'] = decode_id($input['id']);
        $value['password'] = $this->encryption->encrypt($input['newPassword']);
        $value['updateDate'] = db_datetime_now();
        $value['updateBy'] = $this->session->user['userId'];
        $result = $this->user_profile_m->update($input['userId'], $value);
        if ( $result ) {
            Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}/edit"));
    }
    
    public function check_password() {
        $input = $this->input->post();
        $input['userId'] = decode_id($input['id']);
        $input['recycle'] = 0;
        $info = $this->user_profile_m->get_rows($input);
        $row = $info->row();
        if ( $this->encryption->decrypt( $row->password ) == $input['oldPassword'] ) {
            $rs = TRUE;
        } else {
            $rs = FALSE;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($rs));
    }

    private function _build_data($input) {
        $value['email'] = $input['email'];
        $value['firstname'] = $input['firstname'];
        $value['lastname'] = $input['lastname'];
        if ( $input['mode'] == 'create' ) {
            $value['createDate'] = db_datetime_now();
            $value['createBy'] = $this->session->user['userId'];
            $value['password'] = $this->encryption->encrypt($input['password']);
            $value['type'] = "officer";
            $value['verify'] = 1;
        } else {
            $value['updateDate'] = db_datetime_now();
            $value['updateBy'] = $this->session->user['userId'];
        }
        return $value;
    }
    
    private function _build_upload_content($id, $input) {
        $value = array();
        if ( isset($input['coverImageId']) ) {
            $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent,
                'grpType' => 'coverImage',
                'uploadId' => $input['coverImageId']
            );
        } else {
            $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent,
                'grpType' => 'coverImage',
                'uploadId' => null,
            );
        }
        return $value;
    } 
    
    public function check_email()
    {
        $input = $this->input->post();
        $input['recycle'] = array(0,1);
        $info = $this->user_profile_m->get_rows($input);
        if ( $info->num_rows() > 0 ) {
            if ($input['mode'] == 'create') {
                $rs = FALSE;
            } else {
                $row = $info->row();
                if ($row->userId == decode_id($input['id'])) {
                    $rs = TRUE;
                } else {
                    $rs = FALSE;
                }
            }
        }else{
            $rs =  TRUE;
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($rs));
    }
    
}
