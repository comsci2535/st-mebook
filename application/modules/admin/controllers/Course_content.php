<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Course_content extends MX_Controller {

    private $_title = 'จัดการเนื้อหาคอร์สเรียน';
    private $_pageExcerpt = 'การจัดการข้อมูลเกี่ยวกับเนื้อหาคอร์สเรียน';
    private $_grpContent = 'course_content';
    private $_permission;
    private $_treeData;
    private $_orderData = array();

    public function __construct() {
        parent::__construct();
        $this->_permission = Modules::run('admin/permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('admin/utils/toastr', 'error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->_treeData = new stdClass();
        $this->load->model('course_content_m');
        $this->load->model("course_m");
        $this->load->model("upload_m");
        $this->load->library('image_moo');
        $this->load->library('ckeditor');
    }

    public function index($courseId) {
        $this->load->module('admin/admin');

        // toobar
        $action[1][] = action_refresh(base_url("admin/{$this->router->class}/index/{$courseId}"));
        $action[1][] = action_filter();
        $action[2][] = action_order(base_url("admin/{$this->router->class}/order/{$courseId}"));
        $action[2][] = action_add(base_url("admin/{$this->router->class}/create/{$courseId}"));
        $action[3][] = action_trash_multi("admin/{$this->router->class}/action/trash");
        $action[3][] = action_trash_view(base_url("admin/{$this->router->class}/trash/{$courseId}"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);


        
        $input['courseId'] = $courseId;
        $course = $this->course_m->get_rows($input)->row();
        
        $data['courseId'] = $courseId;

        // breadcrumb
        $data['breadcrumb'][] = array("", "javascript:void(0)");
        $data['breadcrumb'][] = array($course->title, base_url("admin/course"));
        $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}"));
        

        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/index";

        $this->admin->layout($data);
    }

    public function data_index() 
    {
        $input               = $this->input->post();
        $input['recycle']    = 0;
        $input['grpContent'] = $this->_grpContent;
        $info                = $this->course_content_m->get_rows($input);
        $infoCount           = $this->course_content_m->get_count($input);
        $tree                = $this->_build_tree($info);
        $treeData            = $this->_print_tree($tree);
        $column              = array();
        $key = 0;
        foreach ($treeData as $rsTree) {
            $rs = $rsTree->info;
            if ( isset($input['search']['value']) && $input['search']['value'] != "" ) {
                if ( mb_stripos($rsTree->title, $input['search']['value']) !== false ) {
                    $found = true;
                } else {
                    $found = false;
                }
            } else {
                $found = true;
            }
            
            if ( $found ) {
               
                $id                         = encode_id($rs->course_contentId);
                $quiz = '';
                if($rs->parentId > 0):
                    $quiz = action_add(base_url("admin/course_quizs/index/{$rs->course_contentId}"));
                endif;
                $action                     = array();
                $action[1][]                = table_edit(site_url("admin/{$this->router->class}/edit/{$input['courseId']}/{$id}"));
                $active                     = $rs->active ? "checked" : null;
                $column[$key]['DT_RowId']   = $id;
                $column[$key]['checkbox']   = "<input type='checkbox' class='icheck tb-check-single'>";
                $column[$key]['title']      = $rsTree->title;
                $column[$key]['quiz']       = $quiz;
                $column[$key]['descript']   = $rs->descript;
                $column[$key]['active']     = toggle_active($active, "admin/{$this->router->class}/action/active");
                $column[$key]['createDate'] = datetime_table($rs->createDate);
                $column[$key]['updateDate'] = datetime_table($rs->updateDate);
                $column[$key]['action']     = Modules::run('admin/utils/build_button_group', $action);
                $key++;
            }
        }

        $data['data']               = $column;
        $data['recordsTotal']       = $info->num_rows();
        $data['recordsFiltered']    = $infoCount;
        $data['draw']               = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }  

    public function trash($courseId) {
        $this->load->module('admin/admin');

        // toobar
        $action[1][] = action_list_view(site_url("admin/{$this->router->class}/index/{$courseId}"));
        $action[2][] = action_delete_multi(base_url("admin/{$this->router->class}/action/delete"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}/index/{$courseId}"));
        $data['breadcrumb'][] = array("ถังขยะ", site_url("admin/{$this->router->class}/trash/{$courseId}"));

        $data['courseId'] = $courseId;
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/trash";

        $this->admin->layout($data);
    }

    public function data_trash() 
    {
        $input = $this->input->post();
        $input['recycle'] = 1;
        $input['grpContent'] = $this->_grpContent;
        $info = $this->course_content_m->get_rows($input);
        $infoCount = $this->course_content_m->get_count($input);
        $tree = $this->_build_tree($info);
        $treeData = $this->_print_tree($tree);
        $column = array();
        $key = 0;
        foreach ($treeData as $rsTree) {
            $rs = $rsTree->info;
            if ( isset($input['search']['value']) && $input['search']['value'] != "" ) {
                if ( mb_stripos($rsTree->title, $input['search']['value']) !== false ) {
                    $found = true;
                } else {
                    $found = false;
                }
            } else {
                $found = true;
            }
            if ( $found ) {
                $id = encode_id($rs->course_contentId);
                $action = array();
                $action = array();
                $action[1][] = table_restore("admin/{$this->router->class}/action/restore");
                $active = $rs->active ? "checked" : null;
                $column[$key]['DT_RowId'] = $id;
                $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
                $column[$key]['title'] = $rsTree->title;
                $column[$key]['descript'] = $rs->descript;
                 $column[$key]['recycleDate'] = datetime_table($rs->recycleDate);
                $column[$key]['action'] = Modules::run('admin/utils/build_button_group', $action);
                $key++;
            }
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }  

    private function _parent_dropdwon($deep,$courseId) {
        $info = $this->course_content_m->get_course_content($courseId);
        foreach ($info as &$rs ){
            $rs['id'] = $rs['course_contentId'];
        }
        $tree = Modules::run('admin/utils/build_tree', $info);
        $print_tree = Modules::run('admin/utils/print_tree', $tree, $deep);
        return $print_tree;
    }

    public function create($courseId) {

        $this->load->module('admin/admin');
        
        $info['active'] = 1;
        $data['info'] = $info;

        $dropDown = form_dropdown('parentId', $this->_parent_dropdwon(1,$courseId), '', 'class="form-control select2" required');
        $data['parentModule'] = $dropDown;

        
        
        $data['frmAction'] = site_url("admin/{$this->router->class}/save");

        
        $input['courseId'] = $courseId;
        $course = $this->course_m->get_rows($input)->row();
        $data['courseId'] = $courseId;

        // breadcrumb
        $data['breadcrumb'][] = array("", "javascript:void(0)");
        $data['breadcrumb'][] = array($course->title, base_url("admin/course"));
        $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}/index/{$courseId}"));
        $data['breadcrumb'][] = array("เพิ่มใหม่", base_url("admin/{$this->router->class}/create"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/form";

        $this->admin->layout($data);
    }

    public function save() {
        $input = $this->input->post();
        if($input['fileTypeUploadType']=='1'){
             
            $file=$this->upload_ci('input-file-preview');
            $input['docAttachId']=array();
            $input['docAttachId'][]=$file['insertId'];
        }else{
            $input['docAttachId']=array();
            $input['docAttachId'][]="";
        }

        //arr($file);exit();

        $value = $this->_build_data($input);
        $id = $this->course_content_m->insert($value);
        $value['course_contentId'] = $id;
        if ( $id ) {
            $value = $this->_build_upload_content($id, $input);
            Modules::run('admin/upload/update_content', $value);
           Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(base_url("admin/{$this->router->class}/index/{$input['courseId']}"));
    }

    public function edit($courseId,$id = 0) {
        $this->load->module('admin/admin');
        
        

        $id = decode_id($id);
        $info = $this->course_content_m->get_by_id($id);

        $dropDown = form_dropdown('parentId', $this->_parent_dropdwon(1,$courseId), $info['parentId'], 'class="form-control select2" required');
        $data['parentModule'] = $dropDown;
        $data['info'] = $info;

        
        $data['frmAction'] = base_url("admin/{$this->router->class}/update");

        $input['courseId'] = $courseId;
        $course = $this->course_m->get_rows($input)->row();
        
        $data['courseId'] = $courseId;

        $data['docAttach'] = Modules::run('admin/upload/get_upload_tmpl', $id, $this->_grpContent, 'docAttach');

         //arr($data['docAttach']);exit();

        // breadcrumb
        // $data['breadcrumb'][] = array("ตั้งค่า", "javascript:void(0)");
        // $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}"));
        // $data['breadcrumb'][] = array("แก้ไข", base_url("admin/{$this->router->class}/create"));
        
       // breadcrumb
        $data['breadcrumb'][] = array("", "javascript:void(0)");
        $data['breadcrumb'][] = array($course->title, base_url("admin/course"));
        $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}/index/{$courseId}"));
        $data['breadcrumb'][] = array("แก้ไข", base_url("admin/{$this->router->class}/create"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/form";

        $this->admin->layout($data);
    }

    public function update() {

        $input = $this->input->post();
        if($input['fileTypeUploadType']=='1' && isset($_FILES['input-file-preview'])&&$_FILES['input-file-preview']['tmp_name']){
            $file=$this->upload_ci('input-file-preview');
            $input['docAttachId']=array();
            $input['docAttachId'][]=$file['insertId'];
        }else{
            $input['docAttachId']=array();
            $input['docAttachId'][]="";
        }

        $value = $this->_build_data($input);
        $input['id'] = decode_id($input['id']);
        $result = $this->course_content_m->update($value, $input['id']);
        if ( $result ) {
             if($value['fileTypeUploadType']=='1' && isset($_FILES['input-file-preview'])&&$_FILES['input-file-preview']['tmp_name']){
                 $value = $this->_build_upload_content($input['id'], $input);
                 Modules::run('admin/upload/update_content', $value);
            }else{
                 $value = $this->_build_upload_content($input['id'], $input);
                 Modules::run('admin/upload/update_content', $value);
            }
           
           Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(base_url("admin/{$this->router->class}/index/{$input['courseId']}"));
    }

    public function delete() {
        Modules::run('admin/utility/permission', 2);
        $input = $this->input->post();
        $param = $input['id'];
        $id = $this->course_content_m->delete($param);
        if ($id) {
            Modules::run('admin/utility/notify', 'warning', 'ลบรายการเรียบร้อย', 'โปรดปรับปรุงสิทธิผู้ดูแลระบบ');
        } else {
            Modules::run('admin/utility/notify', 'error', 'ลบรายการไม่สำเร็จ');
        }
        redirect(base_url("admin/{$this->router->class}"));
    }

    public function order($courseId) {
        $this->load->module('admin/admin');
        $input['courseId'] = $courseId;
        $input['recycle'] = 0;
        $input['order'][0]['column'] = 1;
        $input['order'][0]['dir'] = 'asc';
        $info = $this->course_content_m->get_rows($input);
       //arrx($info->result());
        foreach ($info->result() as $rs) {
            $thisref = &$refs[$rs->course_contentId];
            $thisref['id'] = $rs->course_contentId;
            $thisref['content'] = $rs->title;
            if ($rs->parentId != 0) {
                $refs[$rs->parentId]['children'][] = &$thisref;
            } else {
                $treeData[] = &$thisref;
            }
        }
       //arrx($treeData);
        $data['treeData'] = $treeData;
        $data['frmAction'] = site_url("admin/{$this->router->class}/update_order");
        
        
        // toobar
        $action[1][] = action_refresh(base_url("admin/{$this->router->class}/order/{$courseId}"));
        $action[1][] = action_list_view(base_url("admin/{$this->router->class}/index/{$courseId}"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb

        
        $course = $this->course_m->get_rows($input)->row();
        $data['courseId'] = $courseId;

        // breadcrumb
        $data['breadcrumb'][] = array("", "javascript:void(0)");
        $data['breadcrumb'][] = array($course->title, base_url("admin/course"));
        $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}/index/{$courseId}"));
        $data['breadcrumb'][] = array('จัดลำดับ', base_url("admin/{$this->router->class}/order/{$courseId}"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/order";
        $data['pageScript'] = "assets/scripts/admin/{$this->router->class}/order.js";

        $this->admin->layout($data);
    }
    
    public function update_order(){
        $input = $this->input->post();
        $order = json_decode($input['order']);
        $value = $this->_build_data_order($order);
        $result = $this->db->update_batch('course_content', $value, 'course_contentId');
        if ( $result ) {
            Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}/order/{$input['courseId']}"));
    }
    
    private function _build_data_order($info, $parentId=0){
        $order = 0;
        foreach ($info as $rs) {
            $order++;
            $this->_orderData[$rs->id]['course_contentId'] = $rs->id; 
            $this->_orderData[$rs->id]['order'] = $order;
            $this->_orderData[$rs->id]['parentId'] = $parentId;
            if ( isset($rs->children) )
                $this->_build_data_order ($rs->children, $rs->id);
        }
        return $this->_orderData;
    }

    private function _build_tree($info, $parentId=0) {
        $tree = new stdClass();
        foreach ($info->result() as $d) {
            if ($d->parentId == $parentId) {
                $children = $this->_build_tree($info, $d->course_contentId);
                if ( !empty($children) ) {
                    $d->children = $children;
                }
                $tree->{$d->course_contentId} = $d;
            }
        }
        return $tree;
    }
    
    private function _print_tree($tree, $level = 5, $r = 1, $p = null, $d = NULL) {
        foreach ( $tree as $t ) {
            $dash = ($t->parentId == 0) ? '' : $d . ' &#8594; ';
            $this->_treeData->{$t->course_contentId} = new stdClass();
            $this->_treeData->{$t->course_contentId}->title = $dash . $t->title;
            $this->_treeData->{$t->course_contentId}->info = $t;
            if ( isset($t->children) ) {
                if ( $r < $level ) {
                    $this->_print_tree($t->children, $level, $r + 1, $t->parentId, $dash . $t->title);
                }
            }
        }
        return $this->_treeData;
    }

    private function _build_data($input) {

        //
        $value = array();
        $value['courseId'] = (int)$input['courseId'];
        $value['active'] = (int)$input['active'];
        $value['type'] = (int)$input['type'];
        $value['parentId'] = (int)$input['parentId'];
        $value['title'] = html_escape($input['title']);
        $value['videoLink'] = $input['videoLink'];
        $value['videoLength'] = $input['videoLength'];
        $value['descript'] = html_escape($input['descript']);
        $value['fileTypeUploadType'] = $input['fileTypeUploadType'];
        $value['fileUrl'] = $input['fileUrl'];
        if($input['fileTypeUploadType']=='1'){
             $value['fileUrl'] = "";
        }
       

        if ($input['mode'] == 'create') {
            $value['createDate'] = db_datetime_now();
            $value['createBy'] = $this->session->user['userId'];
        } else {
            $value['updateBy'] = $this->session->user['userId'];
            $value['updateDate'] = db_datetime_now();
        }
        //arr($value);exit();
        return $value;
    }

    private function _build_upload_content($id, $input) {
        $value = array();
        if ( isset($input['coverImageId']) )
            $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent,
                'grpType' => 'coverImage',
                'uploadId' => $input['coverImageId'],
                'title' => $input['coverImageTitle'],
            );
        if ( isset($input['contentImageId']) )
            $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent,
                'grpType' => 'contentImage',
                'uploadId' => $input['contentImageId'],
                'title' => $input['contentImageTitle'],
            );
        if ( isset($input['galleryImageId']) ) {
            foreach ( $input['galleryImageId'] as $key=>$rs ) {
                $value[] = array(
                    'contentId' => $id,
                    'grpContent' => $this->_grpContent,
                    'grpType' => 'galleryImage',
                    'uploadId' => $rs,
                    'title' => $input['galleryImageTitle'][$key]
                );
            }
        }
        if ( isset($input['docAttachId']) ) {
            foreach ( $input['docAttachId'] as $key=>$rs ) {
                $value[] = array(
                    'contentId' => $id,
                    'grpContent' => $this->_grpContent,
                    'grpType' => 'docAttach',
                    'uploadId' => $rs,
                    'title' => $input['docAttachTitle'][$key]
                );
            }
        }
        return $value;
    }    
    
    
    public function action($type="")
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ($input['id'] as &$rs) $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updateDate'] = $dateTime;
            $value['updateBy'] = $this->session->user['userId'];
            $result = false;
            if ( $type == "active" ) {
                $value['active'] = $input['status'] == "true" ? 1 : 0;
                $result = $this->course_content_m->update_in($input['id'], $value);
            }
            if ( $type == "trash" ) {
                $value['active'] = 0;
                $value['recycle'] = 1;
                $value['recycleDate'] = $dateTime;
                $value['recycleBy'] = $this->session->user['userId'];
                $result = $this->course_content_m->update_in($input['id'], $value);
            }
            if ( $type == "restore" ) {
                $value['active'] = 0;
                $value['recycle'] = 0;
                $result = $this->course_content_m->update_in($input['id'], $value);
            }
            if ( $type == "delete" ) {
                $value['active'] = 0;
                $value['recycle'] = 2;
                $result = $this->course_content_m->update_in($input['id'], $value);
            }   
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    } 

    public function upload_ci($inputFileName="userfile")  {
        $config = $this->_get_config();
        create_dir($config['upload_path']);
        $this->upload->initialize($config);
       // 

        if ( !$this->upload->do_upload($inputFileName) ) {
            $result['result'] = $this->upload->display_errors();
            $result['success'] = false;
        } else {
            $result = $this->upload->data();
            $result['success'] = true;
            $pathinfo = pathinfo("{$config['upload_path']}{$result['file_name']}");
            $result = array_merge($result, $pathinfo);      
            $value = array(
                'grpContent' => $this->_grpContent,
                'filename' => $result['file_name'],
                'path' => $result['dirname'].'/',
                'size' => $result['file_size']*1024,
                'extension' => $result['extension'],
                'mime' => $result['file_type'],
                'createDate' => db_date_now(),
                'createBy' => $this->session->user['userId']
            );
            //$this->resizeImage($result['file_name']);
            $result['insertId'] = $this->upload_m->insert($value);
        }

        //print_r($result);exit();
        return $result;
    }  

    
    
    private function _get_config(){

        $config['upload_path'] = 'uploads/';
        $config['max_size'] = file_upload_max_size();
       // $config['resize'] = true;
        $config['upload_path'] = 'uploads/'.date('Y').'/'.date('m').'/'.$this->_grpContent.'/';
        $config['encrypt_name'] = true;
        $config['allowed_types'] = '*';
        //$config['resizeOverwrite'] = true;
        //$config['quality']= '90%';
        
    
        
        return $config;
    }   
    

}
