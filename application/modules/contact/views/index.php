<!-- <section class="banner_area">
            <div class="banner_inner d-flex align-items-center">
            	<div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
				<div class="container">
					<div class="banner_content text-center">
						<h2>ติดต่อเรา</h2>
						<div class="page_link">
							<a href="<?php echo site_url(); ?>">หน้าหลัก</a>
							<a href="<?php echo site_url('contact'); ?>">ติดต่อเรา</a>
						</div>
					</div>
				</div>
            </div>
        </section> -->
        <!--================End Home Banner Area =================-->
        
        <!--================Contact Area =================-->

        <section class="contact_area p-25">

            <div class="container">
                 
                <div class="row">
                    <div class="col-md-12 p-15 btn-register-l">
                        <h3>ติดต่อเรา</h3>
                    </div>
                   
                    <div class="col-lg-6 p-15">
                     <!--    <form class="row contact_form" action="contact_process.php" method="post" id="contactForm" novalidate="novalidate"> -->
                        	<?php echo form_open($frmAction, array('class' => 'row login_form', 'id'=>'frm-save' , 'method' => 'post')) ?>
                            <div class="col-md-6">
                                <div class="form-group">
	                               <input type="text" class="form-control" id="fname" name="fname" placeholder="ชื่อ">
		                            <div class="alert_name"></div>
                                </div>
                            </div>
                             <div class="col-md-6">
                                <div class="form-group">
                                      <input type="text" class="form-control" id="lname" name="lname" placeholder="นามสกุล">
                                    <!-- <div class="alert_name"></div> -->
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="อีเมล">
                                    <div class="alert_email"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="tel" name="tel" placeholder="เบอร์โทรศัพท์">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea class="form-control" name="massage" id="massage" rows="1" placeholder="ข้อความ"></textarea>
                                </div>
                            </div>
                             <div class="col-md-12">
							     	<input type="hidden" name="mode" value="create" />
							          <div id="html_element" style="margin-bottom: 10px"></div>
					                  <input type="hidden" name="robot"  class="form-control">
					                 <div id="form-success-div" class="text-success"></div> 
                                    <div id="form-error-div" class="text-danger"></div>
					        </div>
                            <div class="col-md-12 ">
                                <button type="submit" value="submit" class="btn submit_btn"><span id="form-img-div"></span>  ส่งข้อความ</button>
                            </div>
                        <!-- </form> -->
                        		<?php echo form_close() ?>
                    </div>
                    <div class="col-lg-1  p-15">
                    </div>
                     <div class="col-lg-5  p-15">
                        <div class="contact_info">
                            
                            <div class="info_item">
                                <i class="lnr lnr-phone-handset"></i>
                                <h6><a href="tel:<?php echo $phoneNumber; ?>"><?php echo $phoneNumber; ?></a></h6>
                                <p><?php echo $phoneContact; ?></p>
                            </div>
                            <div class="info_item">
                                <i class="lnr lnr-envelope"></i>
                                <h6><a href="#"><?php echo $mailDefault->value; ?></a></h6>
                                <p>ส่งคำถามของคุณมาหาเราได้ตลอดเวลา!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>