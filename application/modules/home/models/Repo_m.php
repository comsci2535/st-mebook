<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Repo_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    public function get_conditions()
    {
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->select('*');
        $query = $this->db->get('conditions');
        return $query;
    }

}
