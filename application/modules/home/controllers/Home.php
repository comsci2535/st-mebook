<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("repo_m");
        $this->load->model('seo_m');

    }

    public function index() {
        Modules::run('track/front','');
        $this->load->module('front');

        $this->session->unset_userdata('urlreffer');
        $urlreffer['url'] = site_url();
        $this->session->set_userdata('urlreffer', $urlreffer);
        
        $data['home_act'] = 'active';
        $data['contentView'] = 'home/index';
        $data['pageHeader'] = 'หน้าหลัก';
        

        $input_seo['grpContent'] ='seo_home';
        $seo = $this->seo_m->get_rows($input_seo);
        $seo_ = $seo->row();

        //arr($seo_);exit();

        $imgSeo = config_item('metaOgImage');
        $metaTitle="หน้าหลัก";
        $metaDescription="หน้าหลัก";
        $metaKeyword="หน้าหลัก";
        
        if(!empty($seo_)){           
            $input_seo['contentId'] = $seo_->seoId;
            $file = $this->seo_m->get_uplode($input_seo)->row_array();
            if (!empty($file) && is_file($file['path'].$file['filename'])) {
                $imgSeo = base_url($file['path'].$file['filename']);
            }
           
            $metaTitle=$seo_->metaTitle;
            $metaDescription=$seo_->metaDescription;
            $metaKeyword=$seo_->metaKeyword;
        }
        
        //echo CI_VERSION ; exit();

        $data['metaTitle'] = $metaTitle;
        $data['metaDescription_'] = $metaDescription;
        $data['metaKeyword_'] = $metaKeyword;

        $share['ogTitle']=$metaTitle;
        $share['ogDescription']=$metaDescription;
        $share['ogUrl']= 'home';
        $share['ogImage']= $imgSeo;
        $this->_social_share($share);

        $this->front->layout($data);
    }

    public function conditions() {
        Modules::run('track/front','');
        $this->load->module('front');

        $this->session->unset_userdata('urlreffer');
        $urlreffer['url'] = site_url();
        $this->session->set_userdata('urlreffer', $urlreffer);
        
        $data['home_act'] = 'active';
        $data['contentView'] = 'home/conditions';
        $data['pageHeader'] = 'หน้าหลัก';
        

        $input_seo['grpContent'] ='seo_home';
        $seo = $this->seo_m->get_rows($input_seo);
        $seo_ = $seo->row();

        //arr($seo_);exit();

        $imgSeo = config_item('metaOgImage');
        $metaTitle="หน้าหลัก";
        $metaDescription="หน้าหลัก";
        $metaKeyword="หน้าหลัก";
        
        if(!empty($seo_)){           
            $input_seo['contentId'] = $seo_->seoId;
            $file = $this->seo_m->get_uplode($input_seo)->row_array();
            if (!empty($file) && is_file($file['path'].$file['filename'])) {
                $imgSeo = base_url($file['path'].$file['filename']);
            }
           
            $metaTitle=$seo_->metaTitle;
            $metaDescription=$seo_->metaDescription;
            $metaKeyword=$seo_->metaKeyword;
        }
        
        //echo CI_VERSION ; exit();

        $get_conditions = $this->repo_m->get_conditions()->result();

        $data['get_conditions'] = $get_conditions;

        $data['metaTitle'] = $metaTitle;
        $data['metaDescription_'] = $metaDescription;
        $data['metaKeyword_'] = $metaKeyword;

        $share['ogTitle']=$metaTitle;
        $share['ogDescription']=$metaDescription;
        $share['ogUrl']= 'home';
        $share['ogImage']= $imgSeo;
        $this->_social_share($share);

        $this->front->layout($data);
    }

    public function _social_share($input)
    {
        $param['ogType'] = "webiste";
        $param['ogUrl'] = $input['ogUrl'];
        $param['ogTitle'] = $input['ogTitle'];
        $param['ogDescription'] = $input['ogDescription'];
        $param['ogImage'] = $input['ogImage'];
        $param['twImage'] = $input['ogImage'];
        
        Modules::run('social/set_share', $param);
    }  


    public function payment_method() {
        Modules::run('track/front','');
        $this->load->module('front');

        $this->session->unset_userdata('urlreffer');
        $urlreffer['url'] = site_url();
        $this->session->set_userdata('urlreffer', $urlreffer);
        
        $data['payment_method_act'] = 'active';
        $data['contentView'] = 'home/payment_method';
        $data['pageHeader'] = 'วิธีชำระเงิน';
        

        $input_seo['grpContent'] ='seo_home';
        $seo = $this->seo_m->get_rows($input_seo);
        $seo_ = $seo->row();

        //arr($seo_);exit();

        $imgSeo = config_item('metaOgImage');
        $metaTitle="วิธีชำระเงิน";
        $metaDescription="วิธีชำระเงิน";
        $metaKeyword="วิธีชำระเงิน";
        
        if(!empty($seo_)){           
            $input_seo['contentId'] = $seo_->seoId;
            $file = $this->seo_m->get_uplode($input_seo)->row_array();
            if (!empty($file) && is_file($file['path'].$file['filename'])) {
                $imgSeo = base_url($file['path'].$file['filename']);
            }
           
            $metaTitle=$seo_->metaTitle;
            $metaDescription=$seo_->metaDescription;
            $metaKeyword=$seo_->metaKeyword;
        }
        
        //echo CI_VERSION ; exit();

        $data['metaTitle'] = $metaTitle;
        $data['metaDescription_'] = $metaDescription;
        $data['metaKeyword_'] = $metaKeyword;

        $share['ogTitle']=$metaTitle;
        $share['ogDescription']=$metaDescription;
        $share['ogUrl']= 'home';
        $share['ogImage']= $imgSeo;
        $this->_social_share($share);

        $this->front->layout($data);
    }

    public function help() {
        Modules::run('track/front','');
        $this->load->module('front');

        $this->session->unset_userdata('urlreffer');
        $urlreffer['url'] = site_url();
        $this->session->set_userdata('urlreffer', $urlreffer);
        
        $data['help_act'] = 'active';
        $data['contentView'] = 'home/help';
        $data['pageHeader'] = 'ช่วยเหลือ';
        

        $input_seo['grpContent'] ='seo_home';
        $seo = $this->seo_m->get_rows($input_seo);
        $seo_ = $seo->row();

        //arr($seo_);exit();

        $imgSeo = config_item('metaOgImage');
        $metaTitle="ช่วยเหลือ";
        $metaDescription="ช่วยเหลือ";
        $metaKeyword="ช่วยเหลือ";
        
        if(!empty($seo_)){           
            $input_seo['contentId'] = $seo_->seoId;
            $file = $this->seo_m->get_uplode($input_seo)->row_array();
            if (!empty($file) && is_file($file['path'].$file['filename'])) {
                $imgSeo = base_url($file['path'].$file['filename']);
            }
           
            $metaTitle=$seo_->metaTitle;
            $metaDescription=$seo_->metaDescription;
            $metaKeyword=$seo_->metaKeyword;
        }
        
        //echo CI_VERSION ; exit();

        $data['metaTitle'] = $metaTitle;
        $data['metaDescription_'] = $metaDescription;
        $data['metaKeyword_'] = $metaKeyword;

        $share['ogTitle']=$metaTitle;
        $share['ogDescription']=$metaDescription;
        $share['ogUrl']= 'home';
        $share['ogImage']= $imgSeo;
        $this->_social_share($share);

        $this->front->layout($data);
    }

}
