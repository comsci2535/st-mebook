<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends MX_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_m');
        $this->load->library('encryption');
        $this->load->model('user_m');
        $this->load->model('front/front_m');
        

        $this->load->model("admin/upload_m");
        $this->load->library('image_moo');
    }


    public function index()
    {

        $this->load->module('front');

        $data['image'] = base_url('uploads/user.png');
        $data['frmAction'] = site_url("user/register/save");
        $data['contentView'] = 'user/register/form';
        $data['pageScript'] = 'assets/scripts/user/register/index.js';
        
        $this->front->layout($data);
    }
    public function save()
    {

        $input = $this->input->post();

	    $ip = $_SERVER['REMOTE_ADDR'];

	    
    	if($this->input->post('robot') != null){
    		$captcha = "ok";
    		$responseKeys["success"] = 1;
    	} else {
    		$captcha = false;
    		$responseKeys["success"] = false;
    	}


	    if(intval($responseKeys["success"]) !== 1||!$captcha) {
		    echo json_encode(array('info_txt'=>'error','msg' => '* Captcha error','msg2'=>'กรุณาลองใหม่อีกครั้ง!'));
	    } else {
        
	        // $username=$this->check_username($input['username']);
	        // //arr($username);exit();
	        // if($username == 1){

	        //     $resp_msg = array('info_txt'=>"error",'msg'=>'พบ Username ซ้ำในระบบ','msg2'=>'กรุณาลองใหม่อีกครั้ง!');
	        //     echo json_encode($resp_msg);
	        //     return false;
	            
	        // }

            $checkEmail=$this->check_email($input['email']);
            //arr($username);exit();
            if($checkEmail == 1){

                $resp_msg = array('info_txt'=>"error",'msg'=>'พบ Email ซ้ำในระบบ','msg2'=>'กรุณาลองใหม่อีกครั้ง!');
                echo json_encode($resp_msg);
                return false;
                
            }

	        $value = $this->_build_data($input);
	        $userId = $this->user_m->insert($value);

            $images_u=$this->upload_ci('profileImage');
            $input['coverImageId']=$images_u['insertId'];
	        if ( $userId ) {
                $input_l['username']=$value['username'];
                $info = $this->login_m->get_by_username($input_l);

                $value_u = $this->_build_upload_content($userId, $input);
                Modules::run('admin/upload/update_content', $value_u);
                
                //if ( $info['password'] == $value['password'] ) {
                    // $image = base_url('uploads/user.png');
                    // $upload = Modules::run('admin/upload/get_upload', $info['userId'], 'user', 'coverImage');
                    // if ( $upload->num_rows() != 0 ) {
                    //     $row = $upload->row();
                    //     if ( is_file("{$row->path}/{$row->filename}") )
                    //         $image = base_url("{$row->path}{$row->filename}");
                    // }
                    // $member['userId'] = $info['userId'];
                    // $member['name'] = $info['firstname'];//." ".$info['lastname'];
                    // $member['image'] = $image;
                    // $member['email'] = $info['email'];
                    // $member['sectionId'] = $info['sectionId'];
                    // $member['partyId'] = $info['partyId'];
                    // $member['positionId'] = $info['positionId'];
                    // $member['degree'] = $info['degree'];
                    // $member['type'] = $info['type'];
                    // $member['policyId'] = $info['policyId'];
                    // $member['isBackend'] = TRUE;
                    // $this->session->set_userdata('member', $member);
                    // $this->session->set_flashdata('firstTime', '1');
                    // $this->login_m->update_last_login();

                //}
                $data['info']=$info;
                $data['linkConf']=site_url("user/register/userConferm/".encode_id($userId));
                $email=$info['email'];

                $input['type'] = 'mail';

                $input['variable'] = 'SMTPserver';
                $SMTPserver=$this->front_m->getConfig($input)->row();

                $input['variable'] = 'SMTPusername';
                $SMTPusername=$this->front_m->getConfig($input)->row();

                $input['variable'] = 'SMTPpassword';
                $SMTPpassword=$this->front_m->getConfig($input)->row();

                $input['variable'] = 'SMTPport';
                $SMTPport=$this->front_m->getConfig($input)->row();

                $input['variable'] = 'senderEmail';
                $senderEmail=$this->front_m->getConfig($input)->row();

                $input['variable'] = 'senderName';
                $senderName=$this->front_m->getConfig($input)->row();

                $input['variable'] = 'mailDefault';
                $mailDefault=$this->front_m->getConfig($input)->row();
                
                $textEmail = $this->load->view('user/register/mail/mailer_form.php',$data , TRUE);
                              
                               

                  require 'application/third_party/phpmailer/PHPMailerAutoload.php';
                  $mail = new PHPMailer;

                  //$mail->SMTPDebug = 3;                               // Enable verbose debug output

                  $mail->isSMTP();                                      // Set mailer to use SMTP
                  $mail->Host = $SMTPserver->value;              // Specify main and backup SMTP servers
                  $mail->SMTPAuth = true;                               // Enable SMTP authentication
                  $mail->Username = $SMTPusername->value;                // SMTP username
                  $mail->Password = $SMTPpassword->value;                          // SMTP password
                  $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                  $mail->Port = $SMTPport->value;                                           // TCP port to connect to
                  $mail->CharSet = 'UTF-8';
                  $mail->From = $senderEmail->value;
                  $mail->FromName = $senderName->value;
                  $mail->addAddress($email);               // Name is optional
                  
                  $mail->isHTML(true);                                  // Set email format to HTML

                  $mail->Subject = $email.' : สมัครสมาชิก';
                  $mail->Body    = $textEmail;
                  $mail->AltBody = $textEmail;
                  //$mail->send();

               if (!$mail->send()) {

                    $this->login_m->delete_user_byid($userId);

                    $resp_msg = array('info_txt'=>"error",'msg'=>'ส่งอีเมล์ไม่สำเร็จ','msg2'=>'กรุณาลองใหม่อีกครั้ง!');
                    echo json_encode($resp_msg);
                    return false;
                    
                }else{

                    $resp_msg = array('info_txt'=>"success",'msg'=>'สมัครสมาชิกสำเร็จ','msg2'=>'กรุณายืนยันตัวตนอีเมล์');
                    echo json_encode($resp_msg);
                    return false;
                    
                } 

	             
	        } else {
	            $resp_msg = array('info_txt'=>"error",'msg'=>'สมัครสมาชิกไม่สำเร็จ','msg2'=>'กรุณาลองใหม่อีกครั้ง!');
	            echo json_encode($resp_msg);
	            return false;
	        }
	    }
    }

    public function userConferm($userId)
    {

        $this->load->module('front');

         $userId = decode_id($userId);

         $input['userId']=$userId;
         $info = $this->login_m->get_by_userId($input);

         $this->login_m->update_verify($userId);

        $image = base_url('uploads/user.png');
        $upload = Modules::run('admin/upload/get_upload', $info['userId'], 'user', 'coverImage');
        if ( $upload->num_rows() != 0 ) {
            $row = $upload->row();
            if ( is_file("{$row->path}/{$row->filename}") )
                $image = base_url("{$row->path}{$row->filename}");
        }
        $member['userId'] = $info['userId'];
        $member['name'] = $info['firstname'];//." ".$info['lastname'];
        $member['fullname'] = $info['firstname']." ".$info['lastname'];
        $member['image'] = $image;
        $member['email'] = $info['email'];
        $member['sectionId'] = $info['sectionId'];
        $member['partyId'] = $info['partyId'];
        $member['positionId'] = $info['positionId'];
        $member['degree'] = $info['degree'];
        $member['type'] = $info['type'];
        $member['policyId'] = $info['policyId'];
        $member['isBackend'] = TRUE;
        $this->session->set_userdata('member', $member);
        $this->session->set_flashdata('firstTime', '1');
        $this->login_m->update_last_login();


         // $resp_msg = array('info_txt'=>"success",'msg'=>'สมัครสมาชิกสำเร็จ','msg2'=>'กรุณายืนยันตัวตนอีเมล์');
         //            echo json_encode($resp_msg);
         //            return false;
        
        $data['contentView'] = 'user/register/success';
        $data['pageScript'] = 'assets/scripts/user/register/userConferm.js';
        
        
        $this->front->layout($data);
    }


    public function success()
    {

        $this->load->module('front');
        
        $data['contentView'] = 'user/register/success';
        
        
        $this->front->layout($data);
    }

    private function _build_data($input) {
       
        
        $value['firstname'] = $input['fname'];
        $value['lastname'] = $input['lname'];
        $value['phone'] = $input['tel'];
         $value['birthday'] = $input['birthday'];
          $value['sex'] = $input['sex'];
       
        

        if($input['newPassword']!=""){
             $value['password'] = $this->encryption->encrypt($input['newPassword']);
        }

        if ( $input['mode'] == 'create' ) {
            $value['username'] = $input['email'];//$input['username'];
            $value['email'] = $input['email'];
            $value['oauth_provider'] = 'register';
            $value['createDate'] = db_datetime_now();
            $value['password'] = $this->encryption->encrypt($input['password']);
            $value['type'] = "member";
            $value['verify'] = 0;
        } else {
            $value['updateDate'] = db_datetime_now();
            $value['updateBy'] = $this->session->user['userId'];
        }
       // arr($value);exit();
        return $value;
    }



    public function check_username($username)
    {
        
        $input['recycle'] = array(0,1);
        $input['username'] = $username;
        $info = $this->user_m->get_rows($input);
        //arrx($info->num_rows());
        if ( $info->num_rows() > 0 ) {         
            $rs = 1;
        } else {
            $rs =  0;
        }
        return $rs;
    }

    public function check_email($email)
    {
        
        $input['recycle'] = array(0,1);
        $input['email'] = $email;
        $info = $this->user_m->get_rows($input);
        //arrx($info->num_rows());
        if ( $info->num_rows() > 0 ) {         
            $rs = 1;
        } else {
            $rs =  0;
        }
        return $rs;
    }

    private function _build_upload_content($id, $input) {
        //   print"<pre>";print_r($id);exit();
        $value = array();
        $value[] = array(
                'contentId' => $id,
                'grpContent' => 'user'
            );
        if ( isset($input['coverImageId']) )
            $value[] = array(
                'contentId' => $id,
                'grpContent' => 'user',
                'grpType' => 'coverImage',
                'uploadId' => $input['coverImageId'],
                'title' => $input['coverImageTitle']
            );
        if ( isset($input['contentImageId']) )
            $value[] = array(
                'contentId' => $id,
                'grpContent' => 'user',
                'grpType' => 'contentImage',
                'uploadId' => $input['contentImageId'],
                'title' => $input['contentImageTitle']
            );
        if ( isset($input['galleryImageId']) ) {
            foreach ( $input['galleryImageId'] as $key1 => $rs ) {
                $value[] = array(
                    'contentId' => $id,
                    'grpContent' => 'user',
                    'grpType' => 'galleryImage',
                    'uploadId' => $rs,
                    'title' => $input['galleryImageTitle'][$key1]
                );
            }
        }
        if ( isset($input['docAttachId']) ) {
            foreach ( $input['docAttachId'] as $key1 => $rs ) {
                $value[] = array(
                    'contentId' => $id,
                    'grpContent' => 'user',
                    'grpType' => 'docAttach',
                    'uploadId' => $rs,
                    'title' => $input['docAttachTitle'][$key1]
                );
            }
        }
        return $value;
    } 


    public function upload_ci($inputFileName="userfile")  {
        $config = $this->_get_config();
        create_dir($config['upload_path']);
        $this->upload->initialize($config);
       // 
        if ( !$this->upload->do_upload($inputFileName) ) {
            $result['result'] = $this->upload->display_errors();
            $result['success'] = false;
        } else {
            $result = $this->upload->data();
            $result['success'] = true;
            $pathinfo = pathinfo("{$config['upload_path']}{$result['file_name']}");
            $result = array_merge($result, $pathinfo);      
            $value = array(
                'grpContent' => 'user',
                'filename' => $result['file_name'],
                'path' => $result['dirname'].'/',
                'size' => $result['file_size']*1024,
                'extension' => $result['extension'],
                'mime' => $result['file_type'],
                'createDate' => db_date_now(),
                'createBy' => $this->session->user['userId']
            );
            $this->resizeImage($result['file_name'],'115','115');
            $result['insertId'] = $this->upload_m->insert($value);
        }

        //print_r($result);exit();
        return $result;
    }  

    

    public function resizeImage($filename,$width,$height){
      //   print_r($filename);exit();
      $source_path = 'uploads/'.date('Y').'/'.date('m').'/user/' . $filename;
      //$target_path =  'uploads/'.date('Y').'/'.date('m').'/'.$this->_grpContent.'/thumbnail/';
      $config_manip = array(
          'image_library' => 'GD2',
          'quality'=> '90%',
          'source_image' => $source_path,
          //'new_image' => $target_path,
          'create_thumb' => FALSE,
          'maintain_ratio' => FALSE,
          //'thumb_marker' => '_thumb',
          'width' => $width,
          'height' => $height
      );


      $this->load->library('image_lib', $config_manip);
      if (!$this->image_lib->resize()) {
          echo $this->image_lib->display_errors();
      }


      $this->image_lib->clear();
   }   
    
    private function _get_config(){

        $config['upload_path'] = 'uploads/';
        $config['max_size'] = file_upload_max_size();
        $config['resize'] = true;
        $config['upload_path'] = 'uploads/'.date('Y').'/'.date('m').'/user/';
        $config['encrypt_name'] = true;
        $config['allowed_types'] = 'gif|jpg|png|jpeg|image/png';
        $config['resizeOverwrite'] = true;
        $config['quality']= '90%';
        $config['resize'] = true;
        $config['width'] = 1336;
        $config['height'] = 300;
    
        
        return $config;
    } 



}