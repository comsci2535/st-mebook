<!-- <section class="banner_area">
            <div class="banner_inner d-flex align-items-center">
            	<div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
				<div class="container">
					<div class="banner_content text-center">
						<h2>สมัครสมาชิก</h2>
						<div class="page_link">
							<a href="<?php echo site_url(); ?>">หน้าหลัก</a>
							<a href="#">แบบฟอร์มสมัครสมาชิก</a>
						</div>
					</div>
				</div>
            </div>
        </section> -->
<!--================End Home Banner Area =================-->

<!--================Contact Area =================-->
<section class="contact_area p-25">
    <div class="container">

        <div class="row">
            <div class="col-lg-3">

            </div>
            <div class="col-lg-6">

                <?php echo form_open_multipart($frmAction, array('class' => 'row login_form', 'id'=>'frm-save' , 'method' => 'post')) ?>
                <div class="col-md-12 p-15 text-center">
                    <div class=" btn-register-l">
                        <h3>สร้างบัญชีผู้ใช้ใหม่</h3>
                    </div>
                    <h4>สมัครฟรีไม่เสียค่าใช้บริการ</h4>
                </div>

                <div class="col-md-12">

                    <div class="img-profile  text-center p-15">
                        <div class="circle">
                            <!-- User Profile Image -->
                            <img class="profile-pic" src="<?php echo $image;?>">

                            <!-- Default Image -->
                            <!-- <i class="fa fa-user fa-5x"></i> -->
                        </div>
                        <div class="p-image">
                            <i class="fa fa-camera upload-button"></i>
                            <input class="file-upload" type="file" accept="image/*" name="profileImage" />
                            <input type="hidden" name="profileImage_old" id="profileImage_old" value="">
                        </div>
                    </div>

                </div>
                <!--    <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="username" id="username" placeholder="ชื่อผู้ใช้งาน/Username">
                                   <div class="alert_username"></div>
                                </div>
                            </div> -->


                <div class="col-md-6">
                    <div class="form-group">

                        <input type="text" class="form-control" id="fname" name="fname" placeholder="ชื่อ">
                        <div class="alert_name"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">

                        <input type="text" class="form-control" id="lname" name="lname" placeholder="นามสกุล">
                        <div class="alert_lname"></div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="email" class="form-control" id="email" name="email" placeholder="อีเมล">
                        <div class="alert_email"></div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" id="password"
                            placeholder="รหัสผ่าน">

                        <div class="alert_password"></div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="password" class="form-control" name="passwordConf" id="passwordConf"
                            placeholder="ยืนยันรหัสผ่าน">
                        <div class="alert_passwordConf"></div>
                        <!--  <span style="color: red;font-size: 14px">* ต้องไม่น้อยกว่า 8 ตัวอักษร ประกอบด้วย a-z และ 0-9</span> -->
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="text" class="form-control" id="tel" name="tel" placeholder="เบอร์โทรศัพท์">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPassword1 " class=" btn-register-l"><b>วันเกิด</b> (ปี ค.ศ.)</label>
                        <input type="date" name="birthday" class="form-control" class="datepicker" id="birth_date"
                            placeholder="MM/DD/YYYY">
                    </div>
                </div>
                <div class="col-md-12" style="padding-bottom: 15px;">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="sex" id="inlineRadio1" value="male">
                        <label class="form-check-label  btn-register-l" for="inlineRadio1"><b>ชาย</b></label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="sex" id="inlineRadio2" value="female">
                        <label class="form-check-label  btn-register-l" for="inlineRadio2"><b>หญิง</b></label>
                    </div>

                </div>


                <div class="col-md-12 ">
                    <input type="hidden" name="userId" id="userId" value="<?php echo $profile->userId; ?>" />
                    <input type="hidden" name="mode" value="create" />
                    <div id="html_element" style="margin-bottom: 10px"></div>
                    <input type="hidden" name="robot" class="form-control">
                    <div id="form-success-div" class="text-success"></div>
                    <div id="form-error-div" class="text-danger"></div>
                </div>

                <div class="col-md-12 ">
                    <!-- <button type="submit" name="login" class="btn-facebook" ><span id="form-img-div"></span> สมัครสมาชิก</button> -->

                    <button type="submit" value="submit" class="btn submit_btn"><span id="form-img-div"></span>
                        สมัครสมาชิก</button>
                        <hr>
                    <div id="gSignInWrapper">
                        <div id="customBtn2" class="customGPlusSignIn">
                            <span class="buttonText" style="font-size: 16px;">เข้าสู่ระบบด้วย Google</span>
                        </div>
                    </div>
                    <script>
                        startApp();
                    </script>
                    <button onclick="window.location.href='<?=$authUrl?>'" class="login btn-facebook">
                 <!-- <i class="fa fa-facebook"></i>  -->เข้าสู่ระบบด้วย Facebook</button>
                </div>
                <!-- </form> -->
                <?php echo form_close() ?>
            </div>
            <div class="col-lg-3">

            </div>
        </div>
    </div>
</section>