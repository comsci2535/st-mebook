<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MeBook</title>
</head>

<body>
<table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="top" style="border: 2px solid #0071bb;">
   <img src="<?php echo base_url('assets/website/') ?>images/logo.jpg" class="ls-bg" alt="" style="width: 45px;"/> 
    </td>
  </tr>

  <tr>
    <td align="center" valign="top" bgcolor="" style="border: 2px solid #0071bb;font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000; padding:10px;"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
        <tr>
          <td align="left" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:10px; color:#525252;">

          <div style="font-family:Georgia, 'Times New Roman', Times, serif; font-size:30px; color:#000000;">เรียน คุณ <?php echo $info['firstname']." ".$info['lastname']; ?></div>
            <div style="font-size:18px;">
                ขอขอบคุณที่ลงทะเบียน โปรดคลิกที่ลิงค์ต่อไปนี้เพื่อเปิดใช้งานบัญชีผู้ใช้ของคุณ
            </div>

            <div style="font-size:16px;">
               <a href="<?php echo $linkConf;?>">คลิกยืนยันการลงทะเบียน</a>
            </div>
            </td>
        </tr>

        
      </table>
      </td>
  </tr>


  <tr>

    <td align="left" valign="top" bgcolor="#0071bb" style="background-color:#0071bb;color:#FFF;">
    <table width="100%" border="0" cellspacing="0" cellpadding="15">
      <tr>
        <td align="left" valign="top" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:13px;line-height: 20px;text-align: center;"> MeBook
          </td>
      </tr>
    </table></td>
  </tr>

</table>
</body>
</html>
