<section class="contact_area p-25">
  <div class="container">
    <div class="row">
    	<div class="col-md-12 p-15 text-center btn-register-l">
            <h3>ประวัติการชำระเงิน</h3>
        </div>
    	<div class="col-lg-12">
		<div class="t-pc">
			<table class="bank-t">
				<tr>
					<th style="text-align: center;" >เลขที่อ้างอิง</th>
					<th style="text-align: center;" >แพ็กเกจ</th>
					<th style="text-align: center;" >ราคา (บาท)</th>
					<th style="text-align: center;" >วันที่สั่งซื้อ</th>
					<th style="text-align: center;" >วันที่สิ้นสุดแพ็กเกจ</th>
					<th style="text-align: center;" >เหลือเวลาเรียน (วัน)</th>
				</tr>
				 <?php foreach ($info as $key => $rs) { ?>
					<tr>
						<td >
							 <?php echo $rs->code; ?> 
						</td>
						<td ><?php echo $rs->title; ?></td>
						<td style="text-align: right;"><?php echo number_format($rs->price); ?></td>
						<td style="text-align: center;"><?php echo date_language($rs->createDate , true , 'th'); ?></td>
						<td style="text-align: center;"><?php echo date_language($rs->endStudyDate, true , 'th'); ?></td>
						<td style="text-align: right;"><?php echo number_format($rs->endStudyDateDay); ?></td>
					</tr>

				<?php } ?> 



			</table>
		</div>
		<div class="t-mobile">
			<div style="padding-top: 20px"></div>
			<!-- <?php foreach ($bank as $key => $value) { ?>
				<div class="row" >
					<div class="col-5 col-md-6" >
						<span style="font-weight: 700;">ธนาคาร : </span>
					</div>
					<div class="col-7  col-md-6">
						<img alt="K SME" src="<?php echo $value->image; ?>" style=" width: 25px"> <?php echo $value->title; ?>
					</div>
				</div>
				<div class="row" >
					<div class="col-5 col-md-6" >
						<span style="font-weight: 700;">สาขา : </span>
					</div>
					<div class="col-7 col-md-6">
						<?php echo $value->branch; ?>
					</div>
				</div>
				<div class="row" >
					<div class="col-5 col-md-6" >
						<span style="font-weight: 700;">เลขที่บัญชี : </span>
					</div>
					<div class="col-7 col-md-6">
						<?php echo $value->accountNumber; ?>
					</div>
				</div>
				<div class="row" >
					<div class="col-5 col-md-6" >
						<span style="font-weight: 700;">ประเภทบัญชี : </span>
					</div>
					<div class="col-7 col-md-6">
						<?php echo $value->type; ?>
					</div>
				</div>
				<div class="row" >
					<div class="col-5 col-md-6" >
						<span style="font-weight: 700;">ชื่อบัญชี : </span>
					</div>
					<div class="col-7 col-md-6">
						<?php echo $value->name; ?>
					</div>
				</div>
				<div style="padding-bottom: 20px;"></div>
			<?php } ?> -->
		</div>
		</div>
	</div>
  </div>
</section>
