<style>
#customers-t {
    border-collapse: collapse;
    width: 100%;
    margin-top: 25px;
}

#customers-t td, #customers-t th {
    border: 1px solid #ddd;
    padding: 8px;
}



#customers-t th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #636c72;
    color: white;
}
</style>
<div class="col-xl-12 col-lg-12 col-md-12 col-ms-12">
	<div class="payment-2">
		<div class="title text-center">
			<br>
			<h3>ยอดเงินสะสม (<?php echo number_format($amount); ?> บาท)</h3>
			<div class="alert_price_all"></div>
		</div>
		<?php echo form_open_multipart($frmAction, array('class' => '', 'id'=>'frm-save' , 'method' => 'post')) ?>
		<table id="customers-t" >
			<thead>
				<tr>
					<th>
						ข้อมูลการถอนเงินของสมาชิก
					</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td >
						<div class="row" >
							<div class="col-sm-4 col-12 text-b" >
								โอนเงินเข้าธนาคาร :
							</div>
							<div class="col-sm-8 col-12">
								<select class="form-control" name="bankCode" id="bankCode" onchange="check_bank(this)">
									<option class="lang_please_bank" value="" selected="">โปรดเลือกชื่อธนาคาร</option>
									<?php foreach ($bank_list as $key => $value) { ?>
										<option class="lang_please_bank" value="<?php echo $value->code; ?>" style="background-image:url('<?php echo base_url('assets/images/bank/').$value->imgFile; ?>');">
										<?php echo $value->title; ?> <img src="<?php echo base_url('assets/images/bank/').$value->imgFile; ?>"></option>
									<?php } ?>
								</select>
								 <div class="alert_bankCode"></div>
							</div>
						</div>
					</td>

				</tr>
				<tr>
					<td >
						<div class="row" >
							<div class="col-sm-4 col-12 text-b" >
								จำนวนเงินที่ต้องการถอน :
							</div>
							<div class="col-sm-8 col-12">
								<input type="hidden" class="form-control" name="price_all" id="price_all" value="<?php echo $amount; ?>">
								<input type="text" class="form-control amount" name="price" id="price" >
								 <div class="alert_price"></div>
							</div>
						</div>
					</td>

				</tr>
				<tr>
					<td >
						<div class="row" >
							<div class="col-sm-4 col-12 text-b" >
								ชื่อบัญชี :
							</div>
							<div class="col-sm-8 col-12">
								<input type="text" class="form-control" name="bankName" id="bankName" >
								 <div class="alert_bankName"></div>
							</div>
						</div>
					</td>

				</tr>
				<tr>
					<td >
						<div class="row" >
							<div class="col-sm-4 col-12 text-b" >
								เลขที่บัญชี	 :
							</div>
							<div class="col-sm-8 col-12">
								<input type="text" class="form-control" name="bankNo" id="bankNo" >
								 <div class="alert_bankNo"></div>
							</div>
						</div>
					</td>

				</tr>
				<tr>
					<td >
						<div class="row" >
							<div class="col-sm-4 col-12 text-b" >
								สำเนาหน้าบัญชี	:
							</div>
							<div class="col-sm-8 col-12">
								<input type="hidden" name="fileBookbank_old" id="fileBookbank_old">
								<input type="file" name="fileBookbank" id="fileBookbank" onchange="check_file_img(this);">
						       <div class="alert_fileBookbank"></div>
							</div>
						</div>
					</td>

				</tr>
				<tr>
					<td >
						<div class="row" >
							<div class="col-sm-4 col-12 text-b" >
								สำเนาบัตรประชาชน :
							</div>
							<div class="col-sm-8 col-12">
								<input type="hidden" name="fileEvidence_old" id="fileEvidence_old">
								<input type="file" name="fileEvidence" id="fileEvidence" onchange="check_file_img(this);">
						       <div class="alert_fileEvidence"></div>
							</div>
						</div>
					</td>

				</tr>

				<tr>
					<td >
						<div class="row" >
							<div class="col-sm-12 col-12" >
								<div class="payment-b">
									<center>

										<div class="register-form">
											<div id="form-success-div" class="text-success"></div> 
											<div id="form-error-div" class="text-danger"></div>
											<button type="submit" class="button-click-2"><span id="form-img-div"></span>  <font color="#fff">แจ้งถอนเงิน</font></button> 

										</div>
									</center> 
								</div>
							</div>
							
						</div>
					</td>

				</tr>
				

			</tbody>
			
		</table>
		  <?php echo form_close() ?>
		*หมายเหตุ สามารถทำรายการถอนได้เมื่อยอดเงินสะสมครบ 3,000 บาท
	</div>
</div>

