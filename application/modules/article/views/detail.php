<div class="tap-menu">
    <!--  <div class="container">
        <div class="row"> -->
          <nav id="navbar-2" style="background: #eee;">
            <div class="container-tap">
                <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                  <a class="nav-item nav-link " href="<?php echo site_url('course/featured'); ?>"><i class="fa fa-star"></i> <br>น่าสนใจ</a>
                  <a class="nav-item nav-link" href="<?php echo site_url('course/top_chart'); ?>"  ><i class="fa fa-line-chart"></i> <br>ติดอันดับ</a>
                  <a class="nav-item nav-link " href="<?php echo site_url('course/category'); ?>"><i class="fa fa-sitemap"></i> <br>หมวดหมู่</a>
                   <a  <?php if(!$isLogin){ ?> href="javascript:void(0)" class="modalLogin nav-item nav-link "
                  <?php }else{ ?>
                    class="nav-item nav-link " href="<?php echo site_url('course/favorite'); ?>"
                  <?php } ?>><i class="fa fa-heart"></i> <br>รายการโปรด</a>
                   <a class="nav-item nav-link active" href="<?php echo site_url('article'); ?>"><i class="fa fa-clock-o"></i> <br>ข่าววันนี้</a>
                </div>
            </div>
          </nav>

       <!--  </div>
      </div> -->
 <!--  </div>
  </div> -->
</div>

<!--================Blog Area =================-->
        <section class="blog_area single-post-area p-25">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 posts-list">
                        <div class="single-post row">
                            <div class="col-lg-12">
                                <div class="feature-img">
                                    <img class="img-fluid" src="<?php echo $info['image']; ?>" alt="">
                                </div>  
                                <div class="p-10">
                                  <?php echo Modules::run('social/share_button'); ?>
                                </div>                                
                            </div>
                            <div class="col-lg-3  col-md-3">
                                <div class="blog_info text-right">
                                    <?php if(!empty($info['tags'])){ ?>
                                    <div class="post_tag">
                                         <?php foreach ($info['tags'] as $tg => $tg_) { 
                                                 $linkTags = str_replace(" ","-",$tg_);
                                                 
                                            ?>
                                                
                                            <a href="<?php echo site_url("article/tags/{$linkTags}");?>" class="btn" href="#"><?php echo $tg_; ?></a>
                                           
                                            <?php } ?>
                                    </div>
                                    <?php } ?>
                                    <ul class="blog_meta list">
                                        <li><a href="#"><?php echo $info['name'] ?> <i class="lnr lnr-list"></i></a></li>
                                        <li><?php echo date_language($info['createDate'],true,'th') ?> <i class="lnr lnr-calendar-full"></i></li>
                                        <li><?php echo number_format($info['view']); ?> อ่าน <i class="lnr lnr-eye"></i></li>
                                       <!--  <li><a href="#">06 Comments<i class="lnr lnr-bubble"></i></a></li> -->
                                    </ul>
                                    <!-- <ul class="social-links">
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-github"></i></a></li>
                                        <li><a href="#"><i class="fa fa-behance"></i></a></li>
                                    </ul> -->
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 blog_details">
                                <h2><?php echo $info['title'] ?></h2>
                                <p class="excert">
                                    <?php echo $info['excerpt'] ?>
                                </p>
                               
                            </div>
                            <div class="col-lg-12 col-md-12 blog_details">
                                
                                <p>
                                     <?php echo html_entity_decode($info['detail']); ?>
                                </p>
                                 <div class="p-10">
                                  <?php echo Modules::run('social/share_button'); ?>
                                </div>  
                            </div>
                            
                        </div>
                        
                    </div>
                    <div class="col-lg-4">
                        <div class="blog_right_sidebar">
                            <aside class="single_sidebar_widget search_widget">
                                <form action="<?php echo site_url('article');?>">
                                <div class="input-group">

                                    <input type="text" class="form-control" placeholder="ค้นหา.." name="keyword" value="<?=$keyword;?>">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit"><i class="lnr lnr-magnifier"></i></button>
                                    </span>
                                </div><!-- /input-group -->
                                  </form>
                                <div class="br"></div>
                            </aside>
                           
                            
                          
                            <aside class="single_sidebar_widget post_category_widget">
                                <h4 class="widget_title">หมวดหมู่</h4>
                                <ul class="list cat-list">
                                    <?php foreach ($category as $key => $value) { ?>
                                        <li>
                                            <a  href="<?php echo site_url("article/category/{$value['nameLink']}");?>" class="d-flex justify-content-between">
                                                <p><?=$value['name']?></p>
                                                <!-- <p>37</p> -->
                                            </a>
                                        </li>
                                    <?php } ?>
                                                                                              
                                </ul>
                                <div class="br"></div>
                            </aside>
                            <?php echo Modules::run('article/relate',$info['categoryId'],$info['contentId']) ?>
                            <?php if(!empty($tags)){ ?>
                            <aside class="single-sidebar-widget tag_cloud_widget">
                                <h4 class="widget_title">Tag Clouds</h4>
                                <ul class="list">
                                     <?php foreach ($tags as $key => $value) { 

                                         $linkTags = str_replace(" ","-",$value['tagsName']);
                                        ?>
                                        <li><a href="<?php echo site_url("article/tags/{$linkTags}");?>"><?php echo $value['tagsName'] ?></a></li>
                                    <?php } ?>
                                </ul>
                            </aside>
                        <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================Blog Area =================-->
    <input type="hidden" name="linkId" id="linkId" value="<?php echo $title_link; ?>">