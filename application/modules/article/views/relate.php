   


<aside class="single_sidebar_widget popular_post_widget">
    <h3 class="widget_title">บทความที่เกี่ยวข้อง</h3>
    <?php foreach ($info as $key => $rs) { ?>
    <div class="media post_item">
        <img src="<?php echo $rs['image']; ?>" alt="post" style="width: 100px">
        <div class="media-body">
           <a href="<?php echo site_url("article/detail/{$rs['linkId']}");?>"><?php echo $rs['title'] ?></a>
            <p>หมวดหมู่ : <?php echo $rs['name'] ?></p>
            <p><?php echo $rs['createDate'] ?></p>
        </div>
    </div>
    <?php } ?>
   
    <div class="br"></div>
</aside>