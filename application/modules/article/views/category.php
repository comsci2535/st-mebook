<div class="tap-menu">
    <!--  <div class="container">
        <div class="row"> -->
          <nav id="navbar-2" style="background: #eee;">
            <div class="container-tap">
                <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                  <a class="nav-item nav-link " href="<?php echo site_url('course/featured'); ?>"><i class="fa fa-star"></i> <br>น่าสนใจ</a>
                  <a class="nav-item nav-link" href="<?php echo site_url('course/top_chart'); ?>"  ><i class="fa fa-line-chart"></i> <br>ติดอันดับ</a>
                  <a class="nav-item nav-link " href="<?php echo site_url('course/category'); ?>"><i class="fa fa-sitemap"></i> <br>หมวดหมู่</a>
                   <a  <?php if(!$isLogin){ ?> href="javascript:void(0)" class="modalLogin nav-item nav-link "
                  <?php }else{ ?>
                    class="nav-item nav-link " href="<?php echo site_url('course/favorite'); ?>"
                  <?php } ?>><i class="fa fa-heart"></i> <br>รายการโปรด</a>
                   <a class="nav-item nav-link active" href="<?php echo site_url('article'); ?>"><i class="fa fa-clock-o"></i> <br>ข่าววันนี้</a>
                </div>
            </div>
          </nav>

       <!--  </div>
      </div> -->
 <!--  </div>
  </div> -->
</div>
<section class="blog_area p-25">
            <div class="container">
                <div class="row">
                    <!--  <div class="container">
                         <div class="col-lg-12 p-15 ">
                                <h3>บทความ</h3>
                        </div>
                    </div> -->
                    <div class="col-lg-12">
                       

                       <!--  <?php if(!empty($info)){ ?>
                            <div class="product-tile col-12 col-xs-12 col-lg-12">
                                <div class="text-title">บทความแนะนำ</div>
                                <div class="row">
                               
                                </div>
                               
                            </div>
                           <div class="container">
                           <div class="row">
                           
                            <?php foreach ($info as $key => $rs) { ?>

                             <div class="col-12 col-xs-3 col-lg-4">
                                <div class="course-content">
                                    <figure class="course-thumbnail">
                                         <a href="<?php echo site_url("article/detail/{$rs['linkId']}");?>"><img src="<?php echo $rs['image']; ?>" alt=""></a>
                                    </figure>

                                    <div class="course-content-wrap">
                                        <header class="entry-header">
                                            

                                            <div class="entry-meta  align-items-center">
                                                <div class="course-author"><a href="<?php echo site_url("article/detail/{$rs['linkId']}");?>"><?php echo $rs['title'] ?></a></div>
                                            </div>
                                             
                                           <div class="entry-title"> <a href="<?php echo site_url("article/detail/{$rs['linkId']}");?>"><?php echo  iconv_substr($rs['excerpt'],0,100, "UTF-8");?></a></div> 

                                        </header>

                                        
                                    </div>
                                    
                                </div>
                            </div>
                             
                           
                            <?php } ?>
                            
                          
                           
                           </div>
                           </div>
                           <?php } ?> -->
                            <div class="product-tile col-12 col-xs-12 col-lg-12">
                                <div class="text-title"><?=$nameLink?></div>
                                <div class="row">
                               
                                </div>
                               
                            </div>
                           <?php if(!empty($info)){ ?>
                           
                           <div class="container">
                           <div class="row">
                           
                            <?php foreach ($info as $key => $rs) { ?>

                             <div class="col-12 col-md-4 col-lg-3">
                                <div class="course-content">
                                    <div class="course-thumbnail">
                                         <a href="<?php echo site_url("article/detail/{$rs['linkId']}");?>"><img src="<?php echo $rs['image']; ?>" alt=""></a>
                                    </div>

                                    <div class="course-content-wrap">
                                        <header class="entry-header">
                                            

                                            <div class="entry-meta  align-items-center">
                                                <div class="course-author"><a href="<?php echo site_url("article/detail/{$rs['linkId']}");?>"><?php echo $rs['title'] ?></a></div>
                                            </div>
                                             
                                           <div class="entry-title"> <a href="<?php echo site_url("article/detail/{$rs['linkId']}");?>"><?php echo  iconv_substr($rs['excerpt'],0,100, "UTF-8");?></a></div> 

                                        </header>

                                        
                                    </div>
                                    
                                </div>
                            </div>
                             
                           
                            <?php } ?>
                            
                          
                           
                           </div>
                            <div class="text-center">
                                <?php echo $links ;?>   
                            </div>
                           </div>
                            <?php }else{ ?>
                            <div class="text-center">
                               
                                <h4>ไม่พบข้อมูล</h4>
                               
                            </div>
                            <?php } ?>
                    </div>
                    
                </div>
            </div>
        </section>