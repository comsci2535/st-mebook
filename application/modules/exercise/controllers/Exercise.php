<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exercise extends MX_Controller {
    
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('exercise_m');
    }

    public function start() {
        $this->session->unset_userdata('course_quizsId');
        $this->session->unset_userdata('ses_point');
        $id = base64_decode($this->uri->segment(1));
        $info_count = $this->exercise_m->count_course_quizs_all($id, null)->num_rows();
        $data['info_count'] = $info_count;

        $this->load->view('start', $data); 
    }

    public function end() {
        $this->load->view('end'); 
    }

    public function finis() {

        $id = base64_decode($this->uri->segment(1));
        $course_quizsId = $this->session->userdata('course_quizsId');

        // $page = array("page2");
        $page = array("page1","page2","page3","page4","page5");
        $random_page = array_rand($page,1);

        $info_count = $this->exercise_m->count_course_quizs_all($id, $course_quizsId)->num_rows();
        $info = $this->exercise_m->get_course_quizs($id, $course_quizsId)->result();
        if($info):
            foreach($info as $item):
                if ($page[$random_page]=='page2') {
                    $item->list_quizs_detail = $this->exercise_m->get_course_quizs_page2($item->course_quizsId)->result();
                } else {
                    $item->list_quizs_detail = $this->exercise_m->get_course_quizs_detail($item->course_quizsId)->result();
                }   
			endforeach;
        endif;
        // arr($info);
        if (count($this->session->userdata('course_quizsId'))==0) {
            $ses_course_quizsId = 1;
        } else {
            $ses_course_quizsId = count($this->session->userdata('course_quizsId'))+1;
        }

        $data['info'] = $info;
        $data['ses_course_quizsId'] = $ses_course_quizsId;
        $data['noww_page'] = $page[$random_page];
        if ($info_count<1) {
            redirect(site_url('exercise/end/'.$this->uri->segment(1)), 'refresh'); 
        } else {
            $this->load->view($page[$random_page], $data); 
        }
        
    }

    public function point() {

        // $ses_point = '1';
        $point1 = $this->input->post('point1', true);
        $point2 = $this->input->post('point2', true);
        $id = $this->input->post('id', true);

        // $this->session->unset_userdata('course_quizsId');
        $cart_data = $this->session->userdata('course_quizsId');
        if($cart_data == null || count($cart_data) == 0){
            $cart_data = array();
        }
        if (!empty($id)) {
            $cart_data[] = $id;
        }
        $this->session->set_userdata('course_quizsId', $cart_data);
        // print_r($this->session->userdata('course_quizsId'));
        
        if ($point1==$point2) {
            $ses_point = 1;
            $type = 'success';
            $title = 'คำตอบของคุณถูกต้อง (1 คะแนน)';
        } else {
            $ses_point = 0;
            $type = 'error';
            $title = 'คำตอบของคุณผิด (0 คะแนน)';
        }
        
        if ($this->session->userdata('ses_point')==null) {
			$this->session->set_userdata('ses_point', $ses_point);
		} else {
			$this->session->set_userdata('ses_point', $this->session->userdata('ses_point') + $ses_point);
        }

        $data['type'] = $type;
        $data['title'] = $title;
        $data['ses_point'] = $this->session->userdata('ses_point');
        echo json_encode($data);
    }

}
