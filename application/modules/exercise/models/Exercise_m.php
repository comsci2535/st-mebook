<?php defined('BASEPATH') OR exit('No direct script access allowed.');

class Exercise_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function count_course_quizs_all($id, $course_quizsId)
    {
        $this->db->where('course_contentId', $id);
        if (!empty($course_quizsId)) {
            $this->db->where_not_in('course_quizsId', $course_quizsId);
        }
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        return $this->db->get('course_quizs');
    }
    
    public function get_course_quizs($id, $course_quizsId)
    {
        $this->db->where('course_contentId', $id);
        if (!empty($course_quizsId)) {
            $this->db->where_not_in('course_quizsId', $course_quizsId);
        }
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('rand()');
        $this->db->limit(1);
        $this->db->select('course_contentId, course_quizsId, title, answer');
        $query = $this->db->get('course_quizs');
        
        return $query;
    }

    public function get_course_quizs_detail($id)
    {
        $this->db->where('course_quizsId', $id);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('rand()');
        $this->db->select('course_quizs_detailId, course_quizsId, file, point');
        $query = $this->db->get('course_quizs_detail');
        
        return $query;
    }

    public function get_course_quizs_page2($id)
    {
        $this->db->where('course_quizsId', $id);
        $this->db->where('point', 1);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('rand()');
        $this->db->select('course_quizs_detailId, course_quizsId, file, point');
        $query1 = $this->db->get('course_quizs_detail')->row();
                                
        $this->db->where('course_quizsId', $id);
        $this->db->where('point', 0);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('rand()');
        $this->db->select('course_quizs_detailId, course_quizsId, file, point');
        $query2 = $this->db->get('course_quizs_detail')->row();
                                
        $this->db->where('course_quizs_detailId', $query1->course_quizs_detailId);
        $this->db->or_where('course_quizs_detailId', $query2->course_quizs_detailId);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('rand()');
        $this->db->limit(2);
        $this->db->select('course_quizs_detailId, course_quizsId, file, point');
        $query3 = $this->db->get('course_quizs_detail');
        
        return $query3;
    }

}