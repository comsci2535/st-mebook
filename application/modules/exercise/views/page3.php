<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<title>MEBOOK | PAGE3</title>
	<meta name="description" content="Latest updates and statistic charts">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
	<link href="<?=base_url();?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?=base_url();?>class/css/app.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?=base_url();?>plugins/custom/animate.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
		integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<!-- sweetalert2 -->
	<link href="<?=base_url();?>plugins/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
	<!-- fancybox -->
	<link rel="stylesheet" href="<?=base_url();?>plugins/fancybox/jquery.fancybox.min.css" />
</head>

<body>
	<div class="feature3">
		<div class="container">
			<div class="layout-top bold animated fadeInDown">
				<div class="box">
					<p><i class="far fa-pause-circle"></i></p>
				</div>
				<div class="box text-center">
					<p>MEBOOK</p>
				</div>
				<div class="box text-right">
					<p>ข้อที่ <?=$ses_course_quizsId;?></p>
				</div>
			</div>
			<div class="layout-body">
				<?php foreach($info as $value) { ?>
				<div class="text-center pt-2">
					<h3><?=$value->title;?></h3>
				</div>
				<?php
				function str_shuffle_unicode($str) {
					$tmp = preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY);
					shuffle($tmp);
					return join("", $tmp);
				}
				function mb_str_split( $string ) { 
					return preg_split('/(?<!^)(?!$)/u', $string ); 
				}

				$text_me = str_replace(' ', '', $value->answer);
				$random = str_shuffle_unicode($text_me);
				$looptext = mb_str_split( $random );
				?>
				<div class="box">
					<div class="blog">
						<div class="list animated slideInLeft">
							<?php foreach ($looptext as $key => $char) { ?>
							<div class="text" data-send="<?=$char;?>" data-id="<?=$value->course_quizsId;?>"><?=$char;?></div>
							<?php } ?>
						</div>
						<div class="image">
							<?php foreach($value->list_quizs_detail as $key => $value2){ ?>
								<?php if ($value2->point==1) {?>
								<div>
									<img src="<?=base_url($value2->file);?>" class="animated bounceIn" alt="mebook">
								</div>
								<?php } ?>
							<?php } ?>
							<div class="text">
								<?php foreach ($looptext as $key => $char) { ?>
									<div class="p" data-text="<?=$key+1;?>"></div>
								<?php } ?>
							</div>
							<input type="hidden" id="chekvalue">
							<input type="hidden" id="chekposit" value="0">
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>

	<a id="direction" data-fancybox="" data-src="#fancy_images" data-modal="true" href="javascript:;" style="display: none;">Open demo</a>
	<div id="fancy_images" style="display: none;">
		<h4 class="bold text-center">แนะนำวิธีการใช้งาน</h4>
		<img src="<?=base_url();?>images/page3.png" class="ImgFluid" alt="mebook">
		<div class="mt-2 text-center">
			<span class="button-checkbox">
				<button type="button" class="btn chekbox" data-color="primary">ไม่ต้องแสดงอีก</button>
				<input id="no_show" type="checkbox" class="hidden" value="1" style="display: none;"/>
			</span>
			<button data-fancybox-close="" class="btn">CLOSE</button>
		</div>
	</div>

	<script src="<?=base_url();?>plugins/custom/jquery-3.3.1.js" type="text/javascript"></script>
	<script src="<?=base_url();?>plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<!-- cookie -->
	<script src="<?=base_url();?>plugins/custom/js.cookie.min.js" type="text/javascript"></script>
	<script src="<?=base_url();?>plugins/custom/checkbox.js" type="text/javascript"></script>
	<!-- sweetalert2 -->
	<script src="<?=base_url();?>plugins/sweetalert2/sweetalert2.all.min.js" type="text/javascript"></script>
	<!-- fancybox -->
	<script src="<?=base_url();?>plugins/fancybox/jquery.fancybox.min.js"></script>
	<script>
	function get_cookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ')
				c = c.substring(1, c.length);
			if (c.indexOf(nameEQ) == 0)
				return c.substring(nameEQ.length, c.length);
		}
		return null;
	}
	</script>

	<script>
	$(window).on('load', function(){
		$('#chekvalue').val('');
		$('#chekposit').val('0');
	});
	var random = '<?=$text_me;?>';
	var looptext = '<?=count($looptext);?>';

	$( "[data-send]" ).click(function() {
		// alert( $(this).data('id') );
		var send = $(this).attr('data-send');
		if (send!='null') {
			if ($('#chekvalue').val()) {
				$('#chekvalue').val($('#chekvalue').val() + send);
			} else {
				$('#chekvalue').val(send);
			}
			var chekposit = +$("#chekposit").val() + 1;
			$('#chekposit').val(chekposit);
			$('[data-text="'+chekposit+'"]').html( send );

			$(this).attr('data-send', 'null');
			$(this).addClass('disible');
		}

		if (chekposit==looptext) {
			// if ($('#chekvalue').val()==random) {
			// 	var retur = 'success';
			// } else {
			// 	var retur = 'error';				
			// }
			// Swal.fire({
			// 	type: retur,
			// 	title: retur,
			// 	text: 'จงเลือกคำตอบในช่องว่างให้ถูกต้อง',
			// }).then(function() {
			// 	window.location.href = "page4.html";
			// });
			var url = '<?=site_url('exercise/point');?>';
			var csrfToken = get_cookie('csrfCookie');
			$.ajax({
				url: url,
				type: 'POST',
				dataType: 'json',
				headers: { 'X-CSRF-TOKEN': csrfToken },
				data: {
					point1: $('#chekvalue').val(),
					point2: random,
					id: $(this).data('id'),
					csrfToken: csrfToken,
				},
			})
			.done(function (data) {
				console.log(data);
				Swal.fire({
					type: data['type'],
					title: data['title'],
					html: 'คุณได้ทำแบบทดสอบนี้สำเร็จแล้ว คะแนนรวม('+data['ses_point']+' คะแนน)</br> ระบบจะพาไปยังแบบทดสอบถัดไป',
				}).then(function() {
					window.location.href = "<?=site_url(uri_string());?>";
				});
			})
			.fail(function () {
				Swal.fire({
					type: 'error',
					title: 'error',
				});
			});
		}

	});
	</script>

	<script>
	$(document).ready(function(){
		if (Cookies.get('page3')==null) {
			$('#direction').trigger('click');			
		}
		$( "[data-fancybox-close]" ).click(function() {
			if ($('#no_show').is(':checked')) {
				Cookies.set('page3', '1');
				$( "#no_show" ).prop( "checked", false );			
			}
		});
	});
	</script>
</body>

</html>