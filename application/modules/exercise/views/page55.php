<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<title>MEBOOK | PAGE5</title>
	<meta name="description" content="Latest updates and statistic charts">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
	<link href="<?=base_url();?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?=base_url();?>class/css/app.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?=base_url();?>plugins/custom/animate.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
		integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="stylesheet"
		href="https://www.jqueryscript.net/demo/Drag-Touch-To-Sort-Plugin-jQuery-Amigo-Sorter/css/theme-default.css">

</head>

<body>
	<div class="feature5">
		<div class="container">
			<div class="layout-top bold animated fadeInDown">
				<div class="box">
					<p><i class="far fa-pause-circle"></i></p>
				</div>
				<div class="box text-center">
					<p>MEBOOK</p>
				</div>
				<div class="box text-right">
					<p>02:00 <i class="far fa-clock animated infinite swing"></i></p>
				</div>
			</div>
			<div class="layout-body">
				<div class="box">
					<div class="text-center pt-2">
						<h3>จงลากคำตอบที่ตรงกับภาพให้ถูกต้อง ?</h3>
					</div>
					<div class="image1">
						<img src="<?=base_url();?>images/breakfast.png" class="animated bounceIn" alt="mebook">
					</div>

					<div class="answer">
						<ul class="sorter">
							<li class="text">
								<span>Sci-Fi</span>
							</li>
							<li class="text">
								<span>Fantasy</span>
							</li>
							<li class="text">
								<span>Bondiana</span>
							</li>
						</ul>
					</div>


				</div>
			</div>
		</div>
	</div>
	<script src="<?=base_url();?>plugins/custom/jquery-3.3.1.js" type="text/javascript"></script>
	<script src="<?=base_url();?>plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<!-- cookie -->
	<script src="<?=base_url();?>plugins/custom/js.cookie.min.js" type="text/javascript"></script>
	<script src="<?=base_url();?>plugins/custom/checkbox.js" type="text/javascript"></script>

	<script
		src="https://www.jqueryscript.net/demo/Drag-Touch-To-Sort-Plugin-jQuery-Amigo-Sorter/js/amigo-sorter.min.js">
	</script>
	<script>
		$(function () {
			$('ul.sorter').amigoSorter();
		});
		$(function () {
			$('ul.sorter').amigoSorter({
				li_helper: "li_helper",
				li_empty: "empty",
				onTouchStart: function () {},
				onTouchMove: function () {},
				onTouchEnd: function () {}
			});
		});
	</script>
</body>
</html>