<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<title>MEBOOK | PAGE2</title>
	<meta name="description" content="Latest updates and statistic charts">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
	<link href="<?=base_url();?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?=base_url();?>class/css/app.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?=base_url();?>plugins/custom/animate.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
		integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<!-- sweetalert2 -->
	<link href="<?=base_url();?>plugins/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
	<!-- fancybox -->
	<link rel="stylesheet" href="<?=base_url();?>plugins/fancybox/jquery.fancybox.min.css" />
</head>

<body>
	<div class="feature2">
		<div class="container">
			<div class="layout-top bold animated fadeInDown">
				<div class="box">
					<p><i class="far fa-pause-circle"></i></p>
				</div>
				<div class="box text-center">
					<p>MEBOOK</p>
				</div>
				<div class="box text-right">
					<p>ข้อที่ <?=$ses_course_quizsId;?></p>
				</div>
			</div>
			<div class="layout-body">
				<?php foreach($info as $value) { ?>
				<div class="box">
					<div class="text-center pt-2">
						<h3><?=$value->title;?></h3>
					</div>
					<?php foreach($value->list_quizs_detail as $key => $value2){ ?>
						<?php if ($key==0) { ?>
						<div class="image1" data-point="<?=$value2->point;?>" data-id="<?=$value->course_quizsId;?>">
							<img src="<?=base_url($value2->file);?>" class="animated bounceIn" alt="mebook">
							<div class="fade-load"></div>
						</div>
						<?php } ?>

						<div class="text">
							<div id="answer">
								<h2><?=$value->answer;?></h2>
							</div>
						</div>

						<?php if ($key==1) { ?>
						<div class="image2"data-point="<?=$value2->point;?>" data-id="<?=$value->course_quizsId;?>">
							<img src="<?=base_url($value2->file);?>" class="animated bounceIn" alt="mebook">
							<div class="fade-load"></div>
						</div>
						<?php } ?>
					<?php } ?>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
	
	<a id="direction" data-fancybox="" data-src="#fancy_images" data-modal="true" href="javascript:;" style="display: none;">Open demo</a>
	<div id="fancy_images" style="display: none;">
		<h4 class="bold text-center">แนะนำวิธีการใช้งาน</h4>
		<img src="<?=base_url();?>images/page2.png" class="ImgFluid" alt="mebook">
		<div class="mt-2 text-center">
			<span class="button-checkbox">
				<button type="button" class="btn chekbox" data-color="primary">ไม่ต้องแสดงอีก</button>
				<input id="no_show" type="checkbox" class="hidden" value="1" style="display: none;"/>
			</span>
			<button data-fancybox-close="" class="btn">CLOSE</button>
		</div>
	</div>

	<script src="<?=base_url();?>plugins/custom/jquery-3.3.1.js" type="text/javascript"></script>
	<script src="<?=base_url();?>plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<!-- cookie -->
	<script src="<?=base_url();?>plugins/custom/js.cookie.min.js" type="text/javascript"></script>
	<script src="<?=base_url();?>plugins/custom/checkbox.js" type="text/javascript"></script>
	<!-- sweetalert2 -->
	<script src="<?=base_url();?>plugins/sweetalert2/sweetalert2.all.min.js" type="text/javascript"></script>
	<!-- fancybox -->
	<script src="<?=base_url();?>plugins/fancybox/jquery.fancybox.min.js"></script>

	<script>
	function get_cookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ')
				c = c.substring(1, c.length);
			if (c.indexOf(nameEQ) == 0)
				return c.substring(nameEQ.length, c.length);
		}
		return null;
	}
	</script>

	<script>
	$( ".box [data-point]" ).click(function() {
		// alert( $(this).data('id') );
		var url = '<?=site_url('exercise/point');?>';
		var csrfToken = get_cookie('csrfCookie');
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'json',
			headers: { 'X-CSRF-TOKEN': csrfToken },
			data: {
				point1: 1,
				point2: $(this).data('point'),
				id: $(this).data('id'),
				csrfToken: csrfToken,
			},
		})
		.done(function (data) {
			console.log(data);
			Swal.fire({
				type: data['type'],
				title: data['title'],
				html: 'คุณได้ทำแบบทดสอบนี้สำเร็จแล้ว คะแนนรวม('+data['ses_point']+' คะแนน)</br> ระบบจะพาไปยังแบบทดสอบถัดไป',
			}).then(function() {
				window.location.href = "<?=site_url(uri_string());?>";
			});
		})
		.fail(function () {
			Swal.fire({
				type: 'error',
				title: 'error',
			});
		});
	});
	</script>

	<script>
	$(document).ready(function(){
		if (Cookies.get('page2')==null) {
			$('#direction').trigger('click');			
		}
		$( "[data-fancybox-close]" ).click(function() {
			if ($('#no_show').is(':checked')) {
				Cookies.set('page2', '1');
				$( "#no_show" ).prop( "checked", false );			
			}
		});
	});
	</script>

</body>
</html>