<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<title>MEBOOK | Start</title>
	<meta name="description" content="Latest updates and statistic charts">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
	<link href="<?=base_url();?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?=base_url();?>class/css/app.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?=base_url();?>plugins/custom/animate.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
		integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>

<body>
	<div class="page-start">
		<div class="container">
			<div class="layout-top bold animated fadeInDown">
				<div class="box">
					<p><i class="far fa-pause-circle"></i></p>
				</div>
				<div class="box text-center">
					<p>MEBOOK</p>
				</div>
				<div class="box text-right">
					<p>02:00 <i class="far fa-clock animated infinite swing"></i></p>
				</div>
			</div>
			<div class="layout-body">
				<div class="center">
					<h1><i class="far fa-edit"></i> แบบทดสอบ</h1>
					<h4>กรุณาทำแบบทดสอบทั้งหมด (<?=$info_count;?> ข้อ) ให้ถูกต้อง</h4>
					<a href="<?=site_url('exercise/finis/'.$this->uri->segment(1));?>">
						<button type="button" class="btn btn-custom mt-4 animated infinite pulse">เริ่มได้เลย !!</button>
					</a>
					<br>
                    <a href="#" Onclick="checkconfirmclosewindow()">
						<button type="button" class="btn btn-custom mt-4 animated infinite pulse">ปิด</button>
					</a>
				</div>
			</div>
		</div>
	</div>
	
	<script src="<?=base_url();?>plugins/custom/jquery-3.3.1.js" type="text/javascript"></script>
	<script src="<?=base_url();?>plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script langauge="javascript">
    function checkconfirmclosewindow(){
        window.close();
    }
    </script>

</body>
</html>