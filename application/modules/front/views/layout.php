<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <base href="<?php echo site_url(); ?>">
    <title><?php echo $pageTitle; ?></title>
    <meta name="description" content="<?php echo $metaDescription; ?>" />
    <meta name="keywords" content="<?php echo $metaKeyword; ?>" />
    <meta name="author" content="" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url("assets/website/") ?>template/img/favicon.png" type="image/x-icon">

    <?php echo Modules::run('social/share'); ?>

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
		integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <link href="<?php echo base_url("assets/website/") ?>fonts/font.css" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>template/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>template/vendors/linericon/style.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>template/css/font-awesome.min.css">

    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>template/vendors/lightbox/simpleLightbox.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>template/vendors/nice-select/css/nice-select.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>template/vendors/animate-css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>template/vendors/popup/magnific-popup.css">
    <!-- main css -->
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>template/css/style.css">
     <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>template/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>template/css/responsive.css">
    

    <!-- add carousel -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/website/") ?>OwlCarousel2-2.2.1/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/website/") ?>OwlCarousel2-2.2.1/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/website/") ?>OwlCarousel2-2.2.1/animate.css">



    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>fancyapps-fancyBox/source/jquery.fancybox.css?v=2.1.5" />
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>fancyapps-fancyBox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>fancyapps-fancyBox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />

    <link href='<?php echo base_url("assets/website/") ?>fullcalendar-3.9.0/fullcalendar.min.css?v=2.1.5' rel='stylesheet' />
    <link href='<?php echo base_url("assets/website/") ?>fullcalendar-3.9.0/fullcalendar.print.min.css?v=2.1.5' rel='stylesheet' media='print' />

    <link href="<?php echo base_url() ?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css"/>      
     <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/timepicker/bootstrap-timepicker.min.css">


    <link rel='stylesheet' href='<?php echo base_url("assets/website/") ?>unitegallery-master/package/unitegallery/css/unite-gallery.css?v=2.1.5' type='text/css' />

    <link rel='stylesheet' href='<?php echo base_url("assets/website/") ?>sweetalert2/package/dist/sweetalert2.min.css?v=2.1.5' type='text/css' />



    <link rel="stylesheet" href="https://cdn.plyr.io/3.4.4/plyr.css">

    <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=59816b9dabd6b200117460b3&product=inline-share-buttons"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
  <!--   <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127405874-1"></script> -->
    <!-- Zun Noti -->
<!--     <script src="https://cdnjs.cloudflare.com/ajax/libs/push.js/0.0.11/push.min.js"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-127405874-1');
  </script> -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
    <script src="https://apis.google.com/js/api:client.js"></script>
    <script>
        function get_cookie(name) {

          var nameEQ = name + "=";
          var ca = document.cookie.split(';');
          for (var i = 0; i < ca.length; i++) {
              var c = ca[i];
              while (c.charAt(0) == ' ')
                  c = c.substring(1, c.length);
              if (c.indexOf(nameEQ) == 0)
                  return c.substring(nameEQ.length, c.length);
          }
          return null;
        }
        var googleUser = {};
        var startApp = function () {
            gapi.load('auth2', function () {
                auth2 = gapi.auth2.init({
                    client_id: '648947645413-iecmn8j2oes78tf3m9mj7k0avmmp859u.apps.googleusercontent.com',
                    cookiepolicy: 'single_host_origin',
                });
                attachSignin(document.getElementById('customBtn1'));
                attachSignin(document.getElementById('customBtn2'));
            });
        };

        function attachSignin(element) {
            console.log(element.id);
            auth2.attachClickHandler(element, {},
                function (googleUser) {
                    var profile = googleUser.getBasicProfile();
                    $.ajax({
                      method: "POST",
                      url: "<?=site_url('user/login/check_google');?>",
                      data: {
                        csrfToken: csrfToken,
                        ID: profile.getId(),
                        Name: profile.getName(),
                        getImageUrl: profile.getImageUrl(),
                        getEmail: profile.getEmail()
                      }
                    }).done(function( msg ) {
                      // alert( "Data Saved: " + msg );
                      window.location.replace(""+msg+"");
                    });
                },
                function (error) {
                    alert('เข้าสู่ระบบไม่สำเร็จ');
                });
        }
    </script>
    <style type="text/css">
        .customGPlusSignIn {
            cursor: pointer;
            background: #E44932;
            color: #fff;
            width: 100%;
            text-align: center;
            padding: 12px;
            margin-bottom: 5px;
        }
    </style>
 

<style type="text/css">
.red-tooltip + .tooltip > .tooltip-inner {background-color: #f00;}
.red-tooltip + .tooltip > .tooltip-arrow { border-bottom-color:#f00; }
</style>

</head>
<!-- <body class="layout-boxed"> -->
<style>
.tap-menu {
  position: absolute;
  width: 100%;
}
@media only screen and (min-width: 992px) {
  .tap-menu {
    top: 80px;
  }
  body {
    padding-top: 150px;
  }
}
@media only screen and (max-width: 991px) {
  .tap-menu {
    top: 58px;
  }
  body {
    padding-top: 128px;
  }
  .course-content .course-thumbnail-2 img {
    /* height: 100px; */
  }
}
@media only screen and (max-width: 1199px) {
  .header_area .navbar .nav .nav-item {
    margin-right: 15px;
  }
}
</style>
<body>
    <div class="wrapper">
        <?php $this->load->view('topmenu'); ?>


        <?php $this->load->view($contentView); ?>

        <?php $this->load->view('footer'); ?>
        <input type="hidden" name="after_login_url" id="after_login_url" value="<?php echo site_url('');?>">
    </div>        
    


    <script src="<?php echo base_url("assets/website/") ?>template/js/jquery-3.3.1.min.js"></script>
        <script src="<?php echo base_url("assets/website/") ?>template/js/popper.js"></script>
        <script src="<?php echo base_url("assets/website/") ?>template/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url("assets/website/") ?>template/js/stellar.js"></script>
        <script src="<?php echo base_url("assets/website/") ?>template/vendors/lightbox/simpleLightbox.min.js"></script>
        <script src="<?php echo base_url("assets/website/") ?>template/vendors/nice-select/js/jquery.nice-select.min.js"></script>
        <script src="<?php echo base_url("assets/website/") ?>template/vendors/isotope/imagesloaded.pkgd.min.js"></script>
        <script src="<?php echo base_url("assets/website/") ?>template/vendors/isotope/isotope.pkgd.min.js"></script>
        <script src="<?php echo base_url("assets/website/") ?>template/vendors/popup/jquery.magnific-popup.min.js"></script>
        <script src="<?php echo base_url("assets/website/") ?>template/js/jquery.ajaxchimp.min.js"></script>
        <script src="<?php echo base_url("assets/website/") ?>template/vendors/counter-up/jquery.waypoints.min.js"></script>
        <script src="<?php echo base_url("assets/website/") ?>template/vendors/counter-up/jquery.counterup.js"></script>
        <script src="<?php echo base_url("assets/website/") ?>template/js/mail-script.js"></script>
        <script src="<?php echo base_url("assets/website/") ?>template/js/theme.js"></script>
        <script type="text/javascript">
           $(document).ready(function() {
                $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
                    e.preventDefault();
                    $(this).siblings('a.active').removeClass("active");
                    $(this).addClass("active");
                    var index = $(this).index();
                    $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
                    $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
                });
            });

            <?php //if (in_array($this->router->class, array('home'))) : ?>

            window.onscroll = function() {myFunction()};

            var navbar = document.getElementById("navbar-2");
            var sticky = navbar.offsetTop;

            function myFunction() {
              if (window.pageYOffset >= 10) {
                $(".header_area").addClass("navbar_fixed");
                navbar.classList.add("sticky");
              } else {
                navbar.classList.remove("sticky");
                $(".header_area").removeClass("navbar_fixed");
              }
            }

           <?php //endif; ?>
        </script>


    <!--add owl-carousel-->

    <script src="<?php echo base_url("assets/website/") ?>OwlCarousel2-2.2.1/owl.carousel.js"></script>

     <script src="<?php echo base_url() ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>

      <!-- bootstrap time picker -->
<script src="<?php echo base_url();?>assets/bower_components/timepicker/bootstrap-timepicker.min.js"></script>


    <script src="<?php echo base_url("assets/website/") ?>fancyapps-fancyBox/source/jquery.fancybox.js?v=2.1.5" ></script>
    <script src="<?php echo base_url("assets/website/") ?>fancyapps-fancyBox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
    <script src="<?php echo base_url("assets/website/") ?>fancyapps-fancyBox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

    <?php if (in_array($this->router->class, array('course'))) : ?>
      <script src="<?php echo base_url("assets/website/") ?>vimeo-player/dist/player.js"></script>
     
<script src="https://cdn.plyr.io/3.4.4/plyr.js"></script>
  <?php endif; ?>
  

  <?php if (in_array($this->router->class, array('activity'))) : ?>
    <script src='<?php echo base_url("assets/website/") ?>fullcalendar-3.9.0/lib/moment.min.js'></script>
    <script src='<?php echo base_url("assets/website/") ?>fullcalendar-3.9.0/lib/jquery.min.js'></script>
    <script src='<?php echo base_url("assets/website/") ?>fullcalendar-3.9.0/fullcalendar.min.js'></script>
    <script src='<?php echo base_url("assets/website/") ?>fullcalendar-3.9.0/locale/th.js'></script>


    <script type='text/javascript' src='<?php echo base_url("assets/website/") ?>unitegallery-master/package/unitegallery/js/unitegallery.min.js'></script>  
    <script type='text/javascript' src='<?php echo base_url("assets/website/") ?>unitegallery-master/package/unitegallery/themes/tiles/ug-theme-tiles.js'></script>
<?php endif; ?>


<script type='text/javascript' src='<?php echo base_url("assets/website/") ?>sweetalert2/package/dist/sweetalert2.all.min.js'></script>

<?php //if (in_array($this->router->class, array('user'))) : ?>
<script>
  var recaptcha_sitekey = "<?php echo $recaptcha_sitekey ?>";
  var recaptcha_secretkey = "<?php echo $recaptcha_secretkey ?>";
</script>
<script src='https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit' async defer></script>
<?php //endif; ?>

<?php echo $pageScript; ?>
<script>
           <?php if($isCommunity==0){ ?>

            //vNotify.warning({text:'สิทธิพิเศษสำหรับสมาชิกรออยู่เพียบ', title:'กดเข้าร่วม Community'});

           <?php } ?>

           $('.modalLogin').click(function(){
            $('#login').modal('show')
          });

            function get_cookie(name) {

                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ')
                        c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) == 0)
                        return c.substring(nameEQ.length, c.length);
                }
                return null;
            }
           

            
            var csrfToken = get_cookie('csrfCookie');
            var siteUrl = "<?php echo site_url(); ?>";
            var baseUrl = "<?php echo base_url(); ?>";
            var controller = "<?php echo $this->router->class ?>";
            var method = "<?php echo $this->router->method ?>";


            $(document).ready(function () {
                <?php if ($this->session->toastr) : ?>
                    setTimeout(function () {
                        toastr.<?php echo $this->session->toastr['type']; ?>('<?php echo $this->session->toastr['lineTwo']; ?>', '<?php echo $this->session->toastr['lineOne']; ?>');
                    }, 500);
                    <?php $this->session->unset_userdata('toastr'); ?>
                <?php endif; ?>

                

            }); 

            function updateCommunity() {
                // alert(1);
                $.post(siteUrl+'user/login/updateCommunity',{csrfToken: csrfToken},function(data){
                        
                });
                
               
            }

             function updateFavorite(id) {
               // alert(id);
                $.post(siteUrl+'course/favorite/updateFavorite',{csrfToken: csrfToken,courseId:id},function(data){
                    var res = JSON.parse(data);

                  if(res.info_txt == "success"){
                    //alert(2);
                     var wishlisted = document.getElementById("f-"+id);

                      
                        if (res.msg == "wishlisted") {
                          wishlisted.classList.add("wishlisted")
                          //alert(1);
                        } else {
                         // alert(0);
                          wishlisted.classList.remove("wishlisted");
                        }
                     
                  }
                        
                });
                
               
            }

            function setLoginAfterurl(url){
                $("#after_login_url").val(url);
            }

            

            jQuery(document).ready(function() {
                jQuery('.tabs .tab-links a').on('click', function(e) {
                    var currentAttrValue = jQuery(this).attr('href');

                    // Show/Hide Tabs
                    jQuery('.tabs ' + currentAttrValue).show().siblings().hide();

                    // Change/remove current tab to active
                    jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

                    e.preventDefault();
                });
            }); 

           


            function windowPopup(url, width, height) {
                // Calculate the position of the popup so
                // it’s centered on the screen.
                var left = (screen.width / 2) - (width / 2),
                    top = (screen.height / 2) - (height / 2);

                window.open(
                  url,
                  "",
                  "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=" + width + ",height=" + height + ",top=" + top + ",left=" + left
                );
              }

              //jQuery
              $(".js-social-share").on("click", function(e) {
                e.preventDefault();

                windowPopup($(this).attr("href"), 500, 300);
              });

              // Vanilla JavaScript
              var jsSocialShares = document.querySelectorAll(".js-social-share");
              if (jsSocialShares) {
                [].forEach.call(jsSocialShares, function(anchor) {
                  anchor.addEventListener("click", function(e) {
                    e.preventDefault();

                    windowPopup(this.href, 500, 300);
                  });
                });
              }

              $(document).ready(function(){
                    $("span").tooltip();
                     $("button").tooltip();
              });

              $(document).ready(function(){
                  $('[data-toggle="tooltip"]').tooltip();   

                  if($('.search_button').length)
                    {
                      $('.search_button').on('click', function()
                      {
                        if($('.header_search_container').length)
                        {
                          $('.header_search_container').toggleClass('active');
                        }
                      });
                    }
              });
        
        </script>
        

        <script src="<?php echo base_url(); ?>assets/scripts/front/login.js"></script>
        <script src="<?php echo base_url(); ?>assets/scripts/front/totop.js"></script>
      
        <!-- cookie -->
        <script src="<?=base_url();?>plugins/custom/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>plugins/custom/checkbox.js" type="text/javascript"></script>

        <!-- <script>
        $(document).ready(function(){
          if (Cookies.get('direction')==null) {
            $('#direction').trigger('click');			
          }

          $( "[data-fancybox-close]" ).click(function() {
            if ($('#no_show').is(':checked')) {
              Cookies.set('direction', '1');
              $( "#no_show" ).prop( "checked", false );			
            }
            $.fancybox.close(); 
          });
    
          $('[data-original-title]').click(function() {
            // alert(111);
            if ($('#no_show').is(':checked')) {
              $('button[data-fancybox-close]').show();
            }else{
              $('button[data-fancybox-close]').hide();
            }
          });
        });
        </script> -->
        <script>
        $("#direction").fancybox({
            openEffect: 'none',
            closeEffect: 'none',
            nextEffect: 'none',
            prevEffect: 'none',
            nextSpeed: 0,
            prevSpeed: 0,
            preload: 3,
            padding: 0,
            'closeBtn' : false,
            closeClick  : false,
            helpers   : { 
              overlay : {closeClick: false} // prevents closing when clicking OUTSIDE fancybox 
            }
            // 'speedIn'    :   0, // not a valid option for v2.x
            //'speedOut'    :   0, // not a valid option for v2.x
            //'overlayShow' :   true // not a valid option for v2.x
        });
        </script>
        
    </body>
    </html>
