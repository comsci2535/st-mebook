<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <base href="<?php echo site_url(); ?>">
    <title><?php echo $pageTitle; ?></title>
   
    <meta name="author" content="" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url('assets/website/') ?>images/favicon.ico" type="image/x-icon">



    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    
    <link href="<?php echo base_url("assets/website/") ?>fonts/font.css" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>template/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>template/vendors/linericon/style.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>template/css/font-awesome.min.css">

    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>template/vendors/lightbox/simpleLightbox.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>template/vendors/nice-select/css/nice-select.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>template/vendors/animate-css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>template/vendors/popup/magnific-popup.css">
    <!-- main css -->
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>template/css/style.css">
     <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>template/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>template/css/responsive.css">



</head>
<body >
    <div class="wrapper">
       

        <?php $this->load->view($contentView); ?>

       
        <input type="hidden" name="after_login_url" id="after_login_url" value="<?php echo site_url('');?>">
    </div>        
    


   <script src="<?php echo base_url("assets/website/") ?>template/js/jquery-3.3.1.min.js"></script>
        <script src="<?php echo base_url("assets/website/") ?>template/js/popper.js"></script>
        <script src="<?php echo base_url("assets/website/") ?>template/js/bootstrap.min.js"></script>

<?php echo $pageScript; ?>
<script>
            // swal({
            //   type: 'error',
            //   title: 'Oops...',
            //   text: 'Something went wrong!'
            // })

            // if(urlDd!=""){
            //     var after_login_url = urlDd;
            // }else{
                
            // }

            function get_cookie(name) {

                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ')
                        c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) == 0)
                        return c.substring(nameEQ.length, c.length);
                }
                return null;
            }
            var csrfToken = get_cookie('csrfCookie');
            var siteUrl = "<?php echo site_url(); ?>";
            var baseUrl = "<?php echo base_url(); ?>";
            var controller = "<?php echo $this->router->class ?>";
            var method = "<?php echo $this->router->method ?>";
            $(document).ready(function () {
                <?php if ($this->session->toastr) : ?>
                    setTimeout(function () {
                        toastr.<?php echo $this->session->toastr['type']; ?>('<?php echo $this->session->toastr['lineTwo']; ?>', '<?php echo $this->session->toastr['lineOne']; ?>');
                    }, 500);
                    <?php $this->session->unset_userdata('toastr'); ?>
                <?php endif; ?>

                

            }); 

            function setLoginAfterurl(url){
                $("#after_login_url").val(url);
            }


            

           

            

        
        </script>
        

        <script src="<?php echo base_url(); ?>assets/scripts/front/login.js"></script>

    </body>
    </html>
