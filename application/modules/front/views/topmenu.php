
        <header class="header_area " style="position: absolute;">
          
            <div class="main_menu">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <div class="container">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <a class="navbar-brand logo_h" href="<?php echo site_url() ?>"><img src="<?php echo base_url("assets/website/") ?>template/img/logo-2.png" alt="" style="height: 40px;"></a>
                        <span class="tablet-temp navbar-login">
                         <!--  <?php if ( !$isLogin) : ?>
                         <a class="menu-btn" href="#" data-toggle="modal" data-target="#login">เข้าสู่ระบบ</a>
                        
                          <?php else : ?>

                              <a class="b-name" href="<?php echo site_url("user/profile/") ?>">
                                  
                               <span><?php echo $this->session->member['name'] ?></span>
                              <img alt="K SME" src="<?php echo $fimage;?>" style="    border-radius: 50%; width: 27px;height: 27px; border: 1px solid #0071bb;padding: 1px;">
                           
                                    
                                </a>
                           <?php endif; ?> -->
                            <a class="search_button" style="color:#08c;"><i class="fa fa-search" aria-hidden="true"></i></a>
                        </span>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                            <ul class="nav navbar-nav menu_nav ml-auto">
                                <?php if ( $isLogin) : ?>
                                 <li class="nav-item tablet-temp profile">
                                     <div class="profile_item">
                                        <div class="profile_img">
                                            <img class="rounded-circle" src="<?php echo $fimage;?>" alt="" style="border: 2px solid #f1f1f1;padding: 5px;    object-fit: cover;">
                                            <!-- <div class="hover">
                                                <a href="#"><i class="fa fa-facebook"></i></a>
                                                <a href="#"><i class="fa fa-twitter"></i></a>
                                                <a href="#"><i class="fa fa-linkedin"></i></a>
                                            </div> -->
                                        </div>
                                        <div class="profile_name">
                                            <h4><?php echo $this->session->member['fullname'] ?></h4>
                                           <!--  <p>Managing Director (Sales)</p> -->
                                            <p><i class="fa fa-clock-o"></i> <?=$endStudyDate;?> วัน</p>
                                        </div>
                                    </div>
                                 </li>
                                 <?php endif; ?>
                               <!--  <li class="nav-item tablet-temp"><a class="nav-link" href="index.html"><i class="fa fa-user"></i> ทดสอสบ ทดสอบ</a></li>
                                <li class="nav-item tablet-temp"><a class="nav-link" href="index.html"><i class="fa fa-clock-o"></i> 365 วัน</a></li> -->
                               <?php if ( !$isLogin) : ?>
                                  <li class="nav-item tablet-temp"><a class="nav-link"  href="#" data-toggle="modal" data-target="#login"><i class="fas fa fa-sign-in"></i> เข้าสู่ระบบ</a></li>
                                 <?php else : ?>
                                    <li class="nav-item tablet-temp"><a class="nav-link" href="index.html"><i class="fas fa fa-edit"></i> แก้ไขข้อมูล</a></li>
                                <?php endif; ?>
                                <li class="nav-item <?php if(!empty($home_act)){ echo $home_act; } ?> "><a class="nav-link" href="<?php echo site_url(); ?>"><i class="fas fa fa-home"></i> หน้าหลัก</a></li> 
                                 <li class="nav-item"><a class="nav-link" href="<?php echo site_url('activity'); ?>"><i class="fas fa fa-calendar"></i> กิจกรรม</a></li> 
                                <!-- <li class="nav-item"><a class="nav-link" href="<?php echo site_url('home/payment_method'); ?>"><i class="fas fa fa-btc"></i> วิธีชำระเงิน</a></li>  -->
                                <li class="nav-item"><a class="nav-link" href="<?php echo site_url('package'); ?>"><i class="fas fa fa-btc"></i> วิธีชำระเงิน</a></li> 
                                <li class="nav-item <?php if(!empty($contact_act)){ echo $contact_act; } ?>"><a class="nav-link" href="<?php echo site_url('contact'); ?>"><i class="fas fa  fa-envelope"></i> ติดต่อเรา</a></li>
                                <li class="nav-item"><a class="nav-link" href="<?php echo site_url('home/help'); ?>"><i class="fas fa fa-lightbulb-o"></i> ช่วยเหลือ</a></li>
                                <!-- <li class="nav-item"><a class="nav-link" id="direction" href="#fancy_images" rel="gallery" href="javascript:;"><i class="fas fa-info-circle"></i> วิธีการใช้งาน</a></li> -->
                                <li class="nav-item pc-temp">
                                  <a class="nav-link search_button"><i class="fa fa-search" aria-hidden="true"></i></a>
                                </li>
                                <?php if ( !$isLogin) : ?>
                                   <li class="nav-item tablet-temp"> <a class="nav-link"  href="<?php echo site_url('user/register');?>"><i class="fas fa fa-user"></i> สมัครสมาชิก</a> </li>
                                <?php else : ?>
                                <li class="nav-item pc-temp"><a class="nav-link" href="<?php echo site_url('home/help'); ?>"><i class="fas fa fa-clock-o"></i> <?=$endStudyDate;?> วัน</a></li>
                                <li class="nav-item tablet-temp"> <a class="nav-link"  href="<?php echo site_url("logout") ?>"><i class="fas fa fa-sign-out"></i> ออกจากระบบ</a></li>
                                <?php endif; ?>
                            </ul>
                            
                             <div class="float-right pc-temp">
                                <?php if ( !$isLogin) : ?>
                                <a class="menu-btn" href="<?php echo site_url('user/register');?>">สมัครสมาชิก</a> 
                               
                                  <?php else : ?>

                                      <a href="<?php echo site_url("user/profile/") ?>" style="padding-right: 10px;">
                                         

                                      <img alt="K SME" src="<?php echo $fimage;?>" style="border-radius: 50%;width: 40px;height: 40px;border: 1px solid #25a9e0; padding: 2px;    object-fit: cover;">
                                   
                                            <span><?php echo $this->session->member['name'] ?></span> 
                                        </a>


                                   <?php endif; ?>

                                  <?php if ( !$isLogin) : ?>
                                    <a class="menu-btn2" href="#" data-toggle="modal" data-target="#login">เข้าสู่ระบบ</a>
                                  <?php else : ?>
                                    
                                    <a class="menu-btn2"  href="<?php echo site_url("logout") ?>">ออกจากระบบ</a>
                                 <?php endif; ?>
                            </div>
                        </div> 
                    </div>
                </nav>
                 <!-- Header Search Panel -->
              <div class="header_search_container">
                <div class="container">
                  <div class="row">
                    <div class="col">
                      <div class="header_search_content d-flex flex-row align-items-center justify-content-end">
                        <form action="#" class="header_search_form">
                          <input type="search" class="search_input" placeholder="ค้นหา..." required="required">
                          <button class="header_search_button d-flex flex-column align-items-center justify-content-center">
                            <i class="fa fa-search" aria-hidden="true"></i>
                          </button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>      
              </div>  
            </div>
           
        </header>
      