 <!--================ start footer Area  =================-->
 <footer class="footer-area ">
   <div class="container">

     <div class="row footer-bottom d-flex justify-content-between align-items-center">
       <p class="col-lg-8 col-md-8 footer-text m-0">
         <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
         Copyright &copy;<script>
           document.write(new Date().getFullYear());
         </script> All rights reserved | MeBook</a>
         <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
       </p>
       <?php echo Modules::run('banner/social') ?>
     </div>
   </div>
 </footer>

 <!-- Modal -->
 <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-login" role="document">
     <div class="modal-content">
       <!--  <div class="modal-header"></div> -->
       <div class="modal-body">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>

         </button>
         <div class="p-20 text-center">
           <img src="<?php echo base_url("assets/website/") ?>images/logo2.png" class="ls-bg" alt=""
             style="height: 65px;" />
         </div>

         <div class="login-box-body">
           <div class="loginmodal-container">
             <!-- <h1>เข้าสู่ระบบ</h1><br> -->
             <?php
								$form=array('accept-charset'=>'utf-8','class'=>'row login_form','id'=>'frm-login','name'=>'frm-login'); 
								echo form_open('/user/login/check', $form);
								echo form_hidden('base_url',base_url());
								echo form_hidden('site_url',site_url());

								?>
             <div class="col-md-12">
               <div class="form-group">
                 <input type="text" class="form-control" name="username" id="username_login"
                   placeholder="ชื่อผู้ใช้งาน/อีเมล์">

               </div>
             </div>

             <div class="col-md-12">
               <div class="form-group">
                 <input type="password" class="form-control" name="password" id="password_login" placeholder="รหัสผ่าน">

               </div>
             </div>
             <div class="col-md-12 text-center">
               <div id="form-success-div_login" class="text-success"></div>
               <div id="form-error-div_login" class="text-danger"></div>
             </div>

             <div class="col-md-12 p-10 text-center">
               <a href="<?php echo site_url("user/register/") ?>" class="login btn-register-l"><b>ลืมรหัสผ่าน ?</b></a>
             </div>

             <div class="col-md-12">
               <button type="submit" name="login" class="btn-custom btn-info">เข้าสู่ระบบ</button>

             </div>
             <div class="col-md-12">

               <div id="gSignInWrapper">
                 <div id="customBtn1" class="customGPlusSignIn">
                   <span class="buttonText" style="font-size: 16px;">เข้าสู่ระบบด้วย Google</span>
                 </div>
               </div>
               <script>
                  startApp();
              </script>

               <button onclick="window.location.href='<?=$authUrl?>'" class="login btn-facebook">
                 <!-- <i class="fa fa-facebook"></i>  -->เข้าสู่ระบบด้วย Facebook</button>
             </div>


             <div class="col-md-12  p-10 text-center">
               <a href="<?php echo site_url("user/register/") ?>" class="login btn-register-l "
                 style="text-decoration: underline;">ยังไม่มีบัญชี ? <b>สมัครสมาชิก!</b></a>
             </div>
             <?php echo form_close(); ?>


           </div>



         </div>

       </div>

     </div>
   </div>
 </div>

 <!-- Load Facebook SDK for JavaScript -->
 <!-- <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/th_TH/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

Your customer chat code
<div class="fb-customerchat"
  attribution=setup_tool
  page_id="122499241186246">
</div> -->

 <!-- cahat -->

 <!-- <div class="floating-chat enter">
	<a href="https://m.me/gorradesign" target="_blank" class="chat_a">
		<i class="fa fa-comments chat" aria-hidden="true"></i>
		<div class="chat">
			<div class="header">

			</div>
		</div>
	</a>
</div> -->

<style>
#fancy_images {
  max-width: 100%;
  padding: 0px !important;
}
@media only screen and (max-width: 767px) {
  #fancy_images {
    /* max-width: 80%; */
  }
}
#fancy_images img {
  width: auto;
  height: 75vh;
  max-width: 100%;
}
#fancy_images .checkbox {
  font-size: 25px;
  display: contents;
}
#fancy_images button {
  font-size: 15px;
  padding: 4px 10px;
  background: #e60000;
  border: 0;
  color: #fff;
}
#fancy_images button i {
  font-size: 16px;
}
#fancy_images button.chekbox {
  background: #1484bb;
}
#fancy_images button.chekbox:hover, #fancy_images button.chekbox:focus {
  background: #f96b6b;
}
#fancy_images button:hover {
  background: #f96b6b,;
}
</style>

  <!-- <a id="direction" href="#fancy_images" rel="gallery" href="javascript:;" style="display: none;">Open demo</a> -->
	<div id="fancy_images" style="display: none;">
		<h4 class="bold text-center" style="margin: 4px 0;">แนะนำวิธีการใช้งาน</h4>
    <div id="demo" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ul class="carousel-indicators">
        <li data-target="#demo" data-slide-to="0" class="active"></li>
        <li data-target="#demo" data-slide-to="1"></li>
        <li data-target="#demo" data-slide-to="2"></li>
      </ul>
      
      <!-- The slideshow -->
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img src="<?=base_url();?>images/page2.png" class="ImgFluid" alt="mebook">
        </div>
        <div class="carousel-item">
          <img src="<?=base_url();?>images/page3.png" class="ImgFluid" alt="mebook">
        </div>
        <div class="carousel-item">
          <img src="<?=base_url();?>images/page4.png" class="ImgFluid" alt="mebook">
        </div>
      </div>
      
      <!-- Left and right controls -->
      <a class="carousel-control-prev" href="#demo" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
      </a>
      <a class="carousel-control-next" href="#demo" data-slide="next">
        <span class="carousel-control-next-icon"></span>
      </a>
    </div>
		<div class="text-center" style="margin: 4px 0;">
			<span class="button-checkbox">
				<button type="button" class="btn chekbox" data-color="primary">ไม่ต้องแสดงอีก</button>
				<input id="no_show" type="checkbox" class="hidden" value="1" style="display: none;"/>
			</span>
			<button data-fancybox-close="" class="btn" style="display: none;">CLOSE</button>
		</div>
	</div>

