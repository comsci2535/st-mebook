<!-- Preloader Start -->
  <div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
  </div>

  <!-- Preloader Ends -->
  <div class="search-container topdropdown-container" style="display: none;">
    <span class="fa fa-times panel-close"></span>
    <form action="<?php echo site_url('search');?>" id="search_form"> 
      <h3 class="txt-search">Search</h3>
      <p>ค้นหาสิ่งที่คุณสนใจ</p>
      <div class="">
        <div class=" ">
          <span id="validateSearchFilter" class="txt-error" style="position: relative;display: block;">
            โปรดเลือก เงื่อนไข เพื่อค้นหา
          </span>
          
        </div>

      <div class="col s12 m7 l9 xl9 rightside">
      
     

        <div class="input-field second-font" >
          <select class="wide type-select" name="type" id="type">
            <option value="" selected>เลือกประเภทสินค้า</option>
            <option value="house">บ้านมือสอง</option>
            <option value="rental">บ้านเช่า</option>
            <option value="land">ที่ดิน</option>
          </select>
        </div>
        <div class="clearfix"></div>
        <div class="input-field second-font" >
          <select class="wide location-select" name="location" id="location">
            <option value="" selected>เลือกทำเลที่ตั้ง</option>
            <option value="ยะลา">ยะลา</option>
            <option value="ปัตตานี">ปัตตานี</option>
            <option value="นราธิวาส">นราธิวาส</option>
          </select>
        </div>
        <div class="clearfix"></div>
       <div style="padding-top: 15px;">
             
        </div>
        <div class="col s12 m6 l6 xl6 rightside">
             <span>ระดับราคา</span>
             <div class="slider-1">
             <input id="range" type="text" name="range" value="" >
           </div>
        </div> 

         

        <div style="margin-top: 50px;">
             
        </div>

       <button class="btn-search"><i></i> ค้นหา</button>
        
      
    
      </div>

         
      </div>

    </form>
  </div>