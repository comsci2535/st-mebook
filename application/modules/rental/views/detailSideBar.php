<!-- Sidebar Starts -->
				<div class="sidebar col s12 m4 l4 xl4" style="margin-bottom: 50px">
					<div class="row">
						<div class="col s6 m6 l6 xl6">
							<a href="<?php echo site_url('rental/list');?>" class="btn back"><i class="fa fa-arrow-left"></i> บ้านเช่า</a>
						</div>
						<div class="col s6 m6 l6 xl6">
							 <a href="<?php echo site_url('home');?>" class="btn back"><i class="fa fa-home"></i> หน้าหลัก</a>
						</div>
					</div>
					

					<?php echo Modules::run('rental/relate', $info['contentId']);?>
					
					
				</div>
				<!-- Sidebar Ends -->