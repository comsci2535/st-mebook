<html>

<head>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
    <script src="https://apis.google.com/js/api:client.js"></script>
    <script>
        var googleUser = {};
        var startApp = function () {
            gapi.load('auth2', function () {
                auth2 = gapi.auth2.init({
                    client_id: '648947645413-iecmn8j2oes78tf3m9mj7k0avmmp859u.apps.googleusercontent.com',
                    cookiepolicy: 'single_host_origin',
                });
                attachSignin(document.getElementById('customBtn'));
            });
        };

        function attachSignin(element) {
            console.log(element.id);
            auth2.attachClickHandler(element, {},
                function (googleUser) {
                    var profile = googleUser.getBasicProfile();
                    console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
                    console.log('Name: ' + profile.getName());
                    console.log('Image URL: ' + profile.getImageUrl());
                    console.log('Email: ' + profile.getEmail());
                },
                function (error) {
                    alert(JSON.stringify(error, undefined, 2));
                });
        }
    </script>
    <style type="text/css">
        #customBtn {
            cursor: pointer;
            background: #E44932;
            color: #fff;
            width: 100%;
            max-width: 190px;
            text-align: center;
            padding: 12px;
        }
    </style>
</head>

<body>
    <div id="gSignInWrapper">
        <div id="customBtn" class="customGPlusSignIn">
            <span class="buttonText">Google</span>
        </div>
    </div>
    <script>
        startApp();
    </script>
</body>

</html>