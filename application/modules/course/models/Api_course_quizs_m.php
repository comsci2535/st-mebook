<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api_course_quizs_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->from('course_quizs a')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('course_quizs a')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['keyword'])
                    ->or_like('a.excerpt', $param['keyword'])
                    ->or_like('a.detail', $param['keyword'])
                    // ->or_like('b.title', $param['keyword'])
                    // ->or_like('b.quizs_qty', $param['keyword'])
                    ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.createDate,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
        if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.updateDate,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
        }     
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }         
        // END form filter
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['search']['value'])
                    ->or_like('a.excerpt', $param['search']['value'])
                    ->or_like('a.detail', $param['search']['value'])
                    // ->or_like('b.title', $param['keyword'])
                    // ->or_like('b.quizs_qty', $param['keyword'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.title";
            if ($param['order'][0]['column'] == 2) $columnOrder = "a.excerpt";            
            if ( $this->router->method =="data_index" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.createDate";
                if ($param['order'][0]['column'] == 4) $columnOrder = "a.updateDate";
            } else if ( $this->router->method =="data_trash" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.recycleDate";
            }
            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        } 
        
        if ( isset($param['course_quizsId']) ) 
            $this->db->where('a.course_quizsId', $param['course_quizsId']);
        
        if ( isset($param['course_contentId']) ) 
            $this->db->where('a.course_contentId', $param['course_contentId']);
    
        // if ( isset($param['quizs_type_id']) ) 
        //     $this->db->where('a.quizs_type_id', $param['quizs_type_id']);
            
        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);

    }
    
    public function insert($value) {
        $this->db->insert('course_quizs', $value);
        return $this->db->insert_id();
    }

    public function insert_course_quizs_detail($value) {
        $query = $this->db->insert_batch('course_quizs_detail', $value);
        return $query;
    }
    
    public function update($id, $value)
    {
        $query = $this->db
                        ->where('course_quizsId', $id)
                        ->update('course_quizs', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('course_quizsId', $id)
                        ->update('course_quizs', $value);
        return $query;
    }   
    
    public function get_course_quizs_detail($id) 
    {
        $query = $this->db
                        ->select('a.*')
                        ->from('course_quizs_detail a')
                        ->where('a.course_quizsId', $id)
                        ->get();
        return $query;
    }

    public function get_quizs_type() 
    {
        $query = $this->db
                        ->select('a.*')
                        ->from('quizs_type a')
                        ->get();
        return $query;
    }

    public function get_quizs_type_id($id) 
    {
        $this->db->where('quizs_type_id', $id);
        $query = $this->db
                        ->select('a.*')
                        ->from('quizs_type a')
                        ->get();
        return $query;
    }

    //----------------------- Api -------------------------

    public function count_course_quizs_all() 
    {
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        return $this->db->count_all_results('course_quizs');
    }

    public function get_course_quizs_all()
    {
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->select('*');
        $query = $this->db->get('course_quizs');
        return $query;
    }

    public function get_course_quizs_by_course_contentId($id)
    {
        $this->db->where('course_contentId', $id);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('createDate', 'DESC');
        $this->db->select('course_contentId, course_quizsId, title, answer');
        $query = $this->db->get('course_quizs');
        
        return $query;
    }

    public function get_course_quizs_by_course_contentId_random($id)
    {
        $this->db->where('course_contentId', $id);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('rand()');
        $this->db->limit(1);
        $this->db->select('course_contentId, course_quizsId, title, answer');
        $query = $this->db->get('course_quizs');
        
        return $query;
    }

    public function get_detail_course_quizs($id)
    {
        $this->db->where('course_quizsId', $id);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->select('course_quizs_detailId, course_quizsId, file, point');
        $query = $this->db->get('course_quizs_detail');
        
        return $query;
    }

}
