<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Favorite extends MX_Controller {

    private $_grpContent = 'course';

    public function __construct() {
        parent::__construct();
        $this->load->model("course_m");
        $this->load->model("course_content_m");
        $this->load->model('front/front_m');
        $this->load->model("admin/upload_m");
        $this->load->library('image_moo');

         $this->load->model('category/category_m');
    }

    public function index() {

        $url="home";
        if(isset($this->session->urlreffer['url']) && $this->session->urlreffer['url']!=""){
            $url=$this->session->urlreffer['url'];
        }

        $this->session->unset_userdata('urlreffer');
        $urlreffer['url'] = site_url('course/favorite');
        $this->session->set_userdata('urlreffer', $urlreffer);

        $this->load->module('front');

        $input_c['categoryType'] = $this->_grpContent;
        $info = $this->category_m->get_rows($input_c)->result();
       
        
        
        if (empty($this->session->member['userId'])) redirect($url);
        
        
        foreach ($info as $key => $value) {
         
            $input['active']='1';
            $input['recycle']='0';
            $input['categoryId']=$value->categoryId;

            $course = $this->course_m->get_favorite_rows($input)->result();


            $value->countCourse=count($course);
            
            if ( !empty($course) ) {
                foreach ( $course as $key=>&$rs ) {
                    $rs->linkId = str_replace(" ","-",$rs->title_link);
                    $input_u['grpContent'] = $this->_grpContent;
                    $input_u['contentId'] = $rs->courseId;
                    $file_ = $this->course_m->get_uplode($input_u)->row();
                    if (!empty($file_) && is_file($file_->path.$file_->filename)) {
                        $rs->image = base_url($file_->path.$file_->filename);
                    
                    }

                    $rs->wishlisted="";

                    if($this->session->member['userId']!=""){
                         $userFavorite = $this->db
                            ->select('*')
                            ->from('course_favorite a')
                            ->where('a.userId',$this->session->member['userId'])
                            ->where('a.courseId',$rs->courseId)
                            ->get()->num_rows();

                         if($userFavorite > 0){
                            $rs->wishlisted="wishlisted";
                         }
                    }
                   

                    
                    $rs->stars=$this->reviews_stars($rs->courseId);
                }
               
                
                
            }

            $value->course=$course;

        }

           
         //arr($info);exit();
        $data['course_list'] = $info;
        $data['contentView'] = 'course/favorite/index';
        $data['pageScript'] = 'assets/scripts/course/favorite/index.js';
        $this->front->layout($data);
    }


    
   
    
    public function reviews_stars($courseId)
    {
       
        $info_r=$this->course_m->get_reviews_stars_($courseId)->result_array();
        $average=array();
        if ( !empty($info_r) ) {
            foreach ($info_r as $key => $info) {
                $average[]=$info['score'];
            }
        }

        if(!empty($average)){
            $v=round($this->average($average), 0, PHP_ROUND_HALF_UP);
        }else{
            $v='0';
        }

        if($v==1){
            $t='<span class="fa fa-star checked"></span>
<span class="fa fa-star "></span>
<span class="fa fa-star "></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>';
        }else if($v==2){
            $t='<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star "></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>';

        }else if($v==3){
            $t='<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>';

        }else if($v==4){
            $t='<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span>';

        }else if($v==5){
            $t='<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>';

        }else{
            $t='<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>';
        }

        $data['stars']=$t;
        $data['starsCount']=count($info_r);

        return $data;
       
    }
    

    

    public function average($arr) {
        $array_size = count($arr);

        $total = 0;
        for ($i = 0; $i < $array_size; $i++) {
            $total += $arr[$i];
        }

        $average = (float)($total / $array_size);
        return number_format($average,1);
    }

    public function updateFavorite(){
        $input=$this->input->post();

    
        $courseId=$input['courseId'];
        $userFavorite = $this->db
                            ->select('*')
                            ->from('course_favorite a')
                            ->where('a.userId',$this->session->member['userId'])
                            ->where('a.courseId',$courseId)
                            ->get()->num_rows();
        //arr($userFavorite);exit();
        if($userFavorite > 0){
            $this->db
                ->where('userId',$this->session->member['userId'])
                ->where('courseId',$courseId)
                ->delete('course_favorite');
            $resp_msg = array('info_txt'=>"success",'msg'=>'');
        }else{
            $value['userId']=$this->session->member['userId'];
            $value['courseId']=$courseId;
            $this->db->insert('course_favorite', $value);

            $resp_msg = array('info_txt'=>"success",'msg'=>'wishlisted');
        }

        echo json_encode($resp_msg);
         return false;
    }


}
