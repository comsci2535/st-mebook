<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api_course_quizs extends MX_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model("api_course_quizs_m");
    }

    public function index() {
        //Ex:api   http://st-mebook.test:82/course/api_course_quizs/index?CourseContentId=17

        $id     = $this->input->get('CourseContentId', true);
        $info   = $this->api_course_quizs_m->get_course_quizs_by_course_contentId($id)->result();
        if($info):
			foreach($info as $item):
				$item->list_course_quizs = $this->api_course_quizs_m->get_detail_course_quizs($item->course_quizsId)->result();
			endforeach;
        endif;
        
        $data['result'] = $info;
        echo json_encode($data);
    }


    public function exercise() {

        $id =  base64_decode($this->uri->segment(2));

        $page=array("page1","page2","page3","page4","page5");
        $random_page1=array_rand($page,1);
        $random_page2=array_rand($page,1);
        echo $page[$random_page1]."<br>";
        echo $page[$random_page2]."<br>";

        $info = $this->api_course_quizs_m->get_course_quizs_by_course_contentId_random($id)->result();
        if($info):
			foreach($info as $item):
				$item->list_course_quizs = $this->api_course_quizs_m->get_detail_course_quizs($item->course_quizsId)->result();
			endforeach;
        endif;
        arr($info);

        $this->session->unset_userdata('ses_exercise');
        $cart_data = $this->session->userdata('ses_exercise');
        if($cart_data == null || count($cart_data) == 0){
            $cart_data = array();
        }

        $course_quizsId = $info[0]->course_quizsId;

        $cart_data[] = $course_quizsId;
        $this->session->set_userdata('ses_exercise', $cart_data);
        print_r($this->session->userdata('ses_exercise'));

    }
    
    
}
