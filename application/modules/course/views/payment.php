
<section class="section blog-article">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-ms-12 col-12">
                    <div class="blog-posts">
                        <div class="title">
                          <!--   <h3>แจ้งชำระเงิน เลขที่อ้างอิง <?php echo $course_register->code;?></h3> -->
                            <!-- <div class="separator"></div> -->
                            <h3>ขั้นตอนที่ 1 โอนเงินผ่านธนาคาร</h3>
                        </div>
                        <div>
                           <table id="customers" >
							  <tr>
							    <th >ธนาคาร</th>
							    <th >สาขา</th>
							    <th >เลขที่บัญชี</th>
							    <th >ประเภทบันชี</th>
							    <th >ชื่อบัญชี</th>
							  </tr>
							  <?php foreach ($bank as $key => $value) { ?>
							  <tr>
							    <td >
							    	<img alt="K SME" src="<?php echo $value->image; ?>" style=" width: 30px"> <?php echo $value->title; ?> 
							    </td>
							    <td ><?php echo $value->branch; ?></td>
							    <td ><?php echo $value->accountNumber; ?></td>
							    <td ><?php echo $value->type; ?></td>
							    <td ><?php echo $value->name; ?></td>
							  </tr>
							  
							  <?php } ?>
							   
							   
							  
							</table>
							<br>
							<?php if(!empty($course_register->couponCode)){ 
							   		if($course_register->type==2){
							   			$rt=($course_register->price*$course_register->discount)/100;
							   			$total=$course_register->price-$rt;
							   		}else{
							   			$total=$course_register->price-$course_register->discount;
							   		}
							   	 }else{
							   	 	
							   	$total=$course_register->price;
							 } ?>
							<p>ยอดที่ท่านต้องชำระ  <span style="color: green; font-size: 24px;font-weight: 600; ">
								<?php echo number_format($total);?>
								<?php // echo number_format($course_register->price)?> บาท </span></p>
							
							
                        </div>
                           
                    </div><!-- blog-posts -->
                </div><!-- col-lg-4 -->
                <div class="clearfix">
                	
                </div>
                
            </div>  
        </div>

        
    </section><!-- section -->

	
<section class="section">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-12 col-md-12 col-12">
				<div class="blog-register">
					<div class="title">
						<h3>ขั้นตอนที่ 2 แจ้งโอนเงิน</h3>
						<p style="color: #000">เมื่อโอนเงินเรียบร้อยแล้ว โปรดแจ้งการชำระเงิน โดยกรอกแบบฟอร์มข้างล่าง</p>
					</div>
					
						<?php echo form_open_multipart($frmAction, array('class' => '', 'id'=>'frm-save' , 'method' => 'post')) ?>
						<input type="hidden" name="course_registerId" value="<?php echo $course_register->course_registerId;?>">
						<input type="hidden" name="userId" value="<?php echo $course_register->userId;?>">
						<input type="hidden" name="courseId" value="<?php echo $course_register->courseId;?>">
						<input type="hidden" name="code" value="<?php echo $course_register->code;?>">

						<div class="form-style-1 " style="border-radius: 3px;
    box-shadow: 0px 0px 0px 1px #ccc">
    					<li>

						    	<div class="row">
								 <span class="col-lg-4 col-md-4 col-12">		
										<span style="font-weight: 600;" >ธนาคารที่โอน <span class="required">*</span> </span> 
								 </span> 
								 <span class="col-lg-4 col-md-4 col-12">	
									<?php foreach ($bank as $key => $value) { ?>
										
										<span class="bank_i"><input class="bankId" value="<?php echo $value->bankId; ?>" type="radio" name="bankId">  <img alt="K SME" src="<?php echo $value->image; ?>" style=" width: 30px"> <?php echo $value->title; ?> (<?php echo $value->accountNumber; ?>)
										</span>
										
										<br> 

									<?php } ?>
			                        <div class="alert_bankId"></div>
							    </span>
						      </div>  
						       
						       
						    </li>
                         <li>

						    	<div class="row">
								 <span class="col-lg-4 col-md-4 col-12">		
										<span style="font-weight: 600;" >จำนวนเงินโอนเข้า <span class="required">*</span> </span> 
								 </span> 
								 <span class="col-lg-4 col-md-4 col-8">	
									
										 <input type="text" name="amount" id="amount" class="amount field-long" /> 
						        <div class="alert_amount"></div>
			                        
							    </span>
							     <span class="col-lg-4 col-md-4 col-4">
							     	บาท
							     </span>
						      </div>  
						       
						       
						    </li>
						    <li>

						      <div class="row">
								 <span class="col-lg-4 col-md-4 col-12">		
										<span style="font-weight: 600;" >วันและเวลาโอนเข้า <span class="required">*</span> </span> 
								 </span> 
								 <span class="col-lg-8 col-md-8 col-12">	
									
										<input type="text" name="transferDdate" id="transferDdate" class="field-divided" placeholder="วันที่" value="<?php echo date('d/m/Y');?>" />&nbsp;<span class="bootstrap-timepicker"><div class="input-group">
                                                    <input type="text" class="form-control timepicker" name="transferTime">
                                                  </div></span>
						    	         <div class="alert_date"></div>
			                        
							    </span>
						      </div>  
						    	
						    </li>
						    <li>	
                             <div class="row">
								 <span class="col-lg-4 col-md-4 col-12">		
										<span style="font-weight: 600;" >สลิปหลักฐานการโอน (รูป/PDF) <!-- <span class="required">*</span> --> </span> 
								 </span> 
								 <span class="col-lg-8 col-md-8 col-12">	
									
										<input type="file" name="fileUpload" id="fileUpload">
			                           <div class="alert_file"></div>
			                        
							    </span>
						      </div>   
						  </li>
						    
						    <li>

						    	<div class="row">
								 <span class="col-lg-4 col-md-4 col-12">		
										<span style="font-weight: 600;" >เบอร์โทรศัพท์ </span> 
								 </span> 
								 <span class="col-lg-4 col-md-4 col-12">	
									
										 <input type="text" name="phone" id="phone" class="field-long" />
						       
			                        
							    </span>
						      </div>  
						       
						       
						    </li>
						   <!--  <li>
						        <label>หมายเหตุ </label>
						        <textarea name="note" id="note" class="field-long"  rows="3"></textarea>
						        <div class="alert_massage"></div>
						    </li> -->
						     <li>
						     	<div class="row">
						     		<span class="col-lg-4 col-md-4 col-12">
						     		</span>
						     		 <span class="col-lg-4 col-md-4 col-12">
								     	
								          <!-- <div id="html_element" style="margin-bottom: 10px"></div>
						                  <input type="hidden" name="robot"  class="form-control"> -->
						                 <div id="form-success-div" class="text-success"></div> 
		                                <div id="form-error-div" class="text-danger"></div>

		                                <button type="submit" class="btn btn-flat btn-register"><span id="form-img-div"></span> แจ้งโอน</button>
		                            </span>
                            </div>
				             </li>
						    
							    
							</div>

						<?php echo form_close() ?>
					
				</div><!-- blog-posts -->
			</div><!-- col-lg-4 -->

			
		</div><!-- row -->
	</div><!-- container -->
</section><!-- section -->