<!-- 
<div class="top_site_main" >
<div class="page-title-wrapper">
	<div class="banner-wrapper container">
		<div class="text-title">
			<h2>คอร์สเรียนออนไลน์</h2>
			<h4>ไม่จำกัดเวลา เรียนจนกว่าเก่ง</h4>	
		</div>		
	</div>
</div>
</div> -->
<?php //echo Modules::run('banner/hero','course') ?>

<section class="course_details_area p-25">
   
	<div class="container">
		<div class="row course_details_inner">
			<div class="col-md-12 p-15 ">
                <h3><?php echo $info['title'] ?></h3>
            </div>
			<div class="col-lg-8">
				<div class="c_details_img">
				

					<?php if ($info['recommendVideo']!="") { ?>
						
							<div class="embed-container">
							    <div data-vimeo-id="<?php echo $info['recommendVideo']; ?>"  id="handstick-d"  ></div>
							</div>

						<?php }else{ ?>
							
							<img  src="<?php echo $info['image']; ?>" alt="Blog Image">
						<?php } ?>

						
						<div class="socila-link p-10">
                            <!-- <div class="sharethis-inline-share-buttons"></div> -->
                             <?php echo Modules::run('social/share_button'); ?>
                        </div>
				</div>
				<ul class="nav nav-tabs" id="myTab" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">รายละเอียด</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">วิธีชำระเงิน</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">รีวิว</a>
					</li>
					<li class="nav-item">
						<a class="nav-link " id="comments-tab" data-toggle="tab" href="#comments" role="tab" aria-controls="comments" aria-selected="false">ถาม-ตอบ</a>
					</li>
				</ul>
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade  show active" id="home" role="tabpanel" aria-labelledby="home-tab">
						<div class="objctive_text">
							<div style="max-height:386px;overflow-y: scroll;">
							<?php echo html_entity_decode($info['detail']); ?>
							</div>
							<?php echo Modules::run('course/couse_content',$info['courseId']) ?>
						</div>
						
					</div>
					<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
						<div class="objctive_text">
							<?php echo html_entity_decode($payment); ?>
						</div>
					</div>
					<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
						<div class="objctive_text">
							<?php echo Modules::run('course/reviews',$info['courseId']) ?>
							  <div class="row">
							  	<div class="col-sm-4 col-12">
							  		<div class="user-review-title" style="font-size: 20px; padding-bottom: 15px;">คะแนนเฉลี่ย</div>
							  	    <div class="progress-amount">
							  	    	<div id="p-value"></div>
							  	    </div>
							  	</div>
							  	<div class="col-sm-7 col-12">
							  		<div class="user-review-title" style="font-size: 16px; padding-bottom: 15px;">รายละเอียด</div>
							  		<div class="progress-group">
					                    <span class="progress-text">5 ดาว</span>
					                    <span class="progress-number"><b><div id="p-value5"></div></b></span>
					                    <div class="progress sm" id="p5">
					                      
					                    </div>
					                 </div>
					                 <div class="progress-group">
					                    <span class="progress-text">4 ดาว</span>
					                    <span class="progress-number"><b><div id="p-value4"></div></b></span>

					                    <div class="progress sm" id="p4">
					                     
					                    </div>
					                 </div>
					                 <div class="progress-group">
					                    <span class="progress-text">3 ดาว</span>
					                    <span class="progress-number"><b><div id="p-value3"></div></b></span>

					                    <div class="progress sm" id="p3">
					                      
					                    </div>
					                 </div>
					                 <div class="progress-group">
					                    <span class="progress-text">2 ดาว</span>
					                    <span class="progress-number"><b><div id="p-value2"></div></b></span>

					                    <div class="progress sm" id="p2">
					                     
					                    </div>
					                 </div>
					                 <div class="progress-group">
					                    <span class="progress-text">1 ดาว</span>
					                    <span class="progress-number"><b><div id="p-value1"></div></b></span>

					                    <div class="progress sm" id="p1">
					                      
					                    </div>
					                 </div>
							  	</div>
							   </div>
								<div class="user-review">
									 <div class="divider"></div>
									<?php if($isLogin){ ?>
								    <div class="user-review-title" style="font-size: 16px;">รีวิวบทเรียน</div>
								   
								    <div class="user-review-stars-container">
								        <div class="user-review-stars" >
											<fieldset class="rating">
											    <input type="radio" id="star5" name="rating" value="5"  /><label for="star5" title="gorgeous">5 stars</label>
											    <input type="radio" id="star4" name="rating" value="4" /><label for="star4" title="good">4 stars</label>
											    <input type="radio" id="star3" name="rating" value="3" /><label for="star3" title="regular">3 stars</label>
											    <input type="radio" id="star2" name="rating" value="2" /><label for="star2" title="poor">2 stars</label>
											    <input type="radio" id="star1" name="rating" value="1" /><label for="star1" title="bad">1 star</label>
											</fieldset>
								        <span style="margin: 8px;font-size: 16px;">กดเพื่อให้คะแนน</span>
								        </div>
								    </div>

								    <div class="review-form text-right">
								        
								       
								        <input type="hidden" id="review-form-title" class="form-control" placeholder="" value="">
								        <textarea id="review-form-comment" class="form-control" rows="3" cols="83" placeholder="แสดงความคิดเห็น"></textarea>
								        <div class="review-form-btns" style="padding-top: 10px;">
								          <button id="review-form-submit-btn" class="btn-smal btn-flat btn-info ">ตกลง</button>
								          <button id="review-form-cancel-btn" class="btn-smal btn-flat btn-default">ยกเลิก</button>
								        </div>
								    </div>
								    <?php } ?>
								  </div>
								 
								  <div id="reviews_stars_result"></div>
							</div>
					</div>
					<div class="tab-pane fade" id="comments" role="tabpanel" aria-labelledby="comments-tab">
						
						<div class="comments-area">
							
							<div id="comment-list"></div>
							                                        				
						</div>
						<?php if($isLogin){ ?>
						<div class="comment-form">
							<h4>ตั้งคำถามใหม่</h4>
							<form>
								<div class="form-group">
									<input type="text" class="form-control" id="c_subject" name="c_subject" placeholder="หัวข้อ" onfocus="this.placeholder = ''" onblur="this.placeholder = 'หัวข้อ'">
								</div>
								<div class="form-group">
									<textarea class="form-control mb-10" rows="5" id="c_message" name="c_message" placeholder="รายละเอียด" onfocus="this.placeholder = ''" onblur="this.placeholder = 'รายละเอียด'" required=""></textarea>
								</div>
								<button type="button" class="primary-btn submit_btn" id="btn-comment">ตั้งคำถาม</button> 
							</form>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<div class="col-lg-4">

				
				 
				  <div >
				  	<?php if($isLogin && $endStudyDate > 0){ ?>
				  		<a class="btn btn-green"  href="<?php echo site_url('course/list_video/'.$linkId);?>"  >คลิกเพื่อเข้าเรียน</a>
				  	<?php }else{ ?>
					  	<a class="btn btn-green"  href="<?php echo site_url('package');?>"  >ชำระเงินเพื่อเริ่มเรียน</a>
					<?php } ?>
				  </div>
				 <?php if($info['receipts']!=""){ ?>
				      <div class="sidebar-content">
				      <h3 class="sidebar-title">สิ่งที่ได้รับ</h3>
				        <div class="box">
				          
				          <?php echo $info['receipts']; ?>

				        </div>
				      </div>
				  <?php } ?>
				<?php echo Modules::run('course/instructor',$info['instructorId']) ?>
				<div class="p-25"><br></div>
		         <?php echo Modules::run('course/relate',$info['categoryId'],$info['courseId']) ?>
			</div>
		</div>
	</div>
</section>

<input type="hidden" name="courseId" id="courseId" value="<?php echo $info['courseId']; ?>">
<input type="hidden" name="linkId" id="linkId" value="<?php echo $linkId; ?>">
