<!doctype html>
<html lang="en">

<head>
<link rel="icon" href="https://cdn.plyr.io/static/icons/favicon.ico">
 <link href="<?php echo base_url("assets/website/") ?>fonts/font.css" rel="stylesheet">
<link href="<?php echo base_url("assets/website/") ?>common-css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url("assets/website/") ?>css/custom.css" rel="stylesheet">
</head>



<body style="font-family: Prompt;">
<div class="col-12 col-sm-12" style="width: 100%">
    <div style="text-align: left;">
    	<br>
    	<p><?php echo $content->title; ?></p>
    	<hr>
    </div>
	<div class="embed-container">
	     <div data-vimeo-id="<?php echo $content->videoLink; ?>"  id="handstick-2"></div>
	</div>
</div>
 
<script src="<?php echo base_url("assets/website/") ?>vimeo-player/dist/player.js"></script>

<script >
  
    var handstickPlayer = new Vimeo.Player('handstick-2');
    handstickPlayer.on('play', function() {
        console.log('played the handstick video!');
    });

</script>

</body>

</html>