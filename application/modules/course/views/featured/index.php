          <div class="tap-menu">
            <!--  <div class="container">
                <div class="row"> -->
                  <nav id="navbar-2" style="background: #eee;">
                    <div class="container-tap">
                        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                          <a class="nav-item nav-link active" href="<?php echo site_url('course/featured'); ?>"><i class="fa fa-star"></i> <br>น่าสนใจ</a>
                          <a class="nav-item nav-link" href="<?php echo site_url('course/top_chart'); ?>"  ><i class="fa fa-line-chart"></i> <br>ติดอันดับ</a>
                          <a class="nav-item nav-link " href="<?php echo site_url('course/category'); ?>"><i class="fa fa-sitemap"></i> <br>หมวดหมู่</a>
                           <a  <?php if(!$isLogin){ ?> href="javascript:void(0)" class="modalLogin nav-item nav-link "
                          <?php }else{ ?>
                            class="nav-item nav-link " href="<?php echo site_url('course/favorite'); ?>"
                          <?php } ?>><i class="fa fa-heart"></i> <br>รายการโปรด</a>
                           <a class="nav-item nav-link" href="<?php echo site_url('article'); ?>"><i class="fa fa-clock-o"></i> <br>ข่าววันนี้</a>
                        </div>
                    </div>
                  </nav>

                  
                
               <!--  </div>
              </div> -->
         <!--  </div>
          </div> -->
        </div>
    <div class="featured-courses vertical-column courses-wrap  p-20">
           <div class="container">
                  <div class="row" id="products">
                        
                         
                            <?php foreach ($course_list as $key => $rs) { ?>
                            <?php if($rs->countCourse > 0){ ?>
                            
                            <div class="product-tile col-12 col-xs-12 col-lg-12">
                                <div class="text-title"><?php echo $rs->name; ?></div>
                                <div class="row">
                                 <span class="col-8 col-xs-6 col-lg-6 font-16"><?php echo $rs->excerpt; ?></span>
                                 <span class="col-4 col-xs-6 col-lg-6 text-right" ><a href="<?php echo site_url("course/category/type/{$rs->nameLink}");?>">ดูทั้งหมด</a></span>
                                </div>
                                <hr>
                            </div>
                           
                            <div  class="owl-course owl-carousel owl-theme" >
                           
                            <?php foreach ($rs->course as $key => $value) { ?>

                            <div class="item ">
                                <div class="course-content">
                                     <div class="course-thumbnail-2">
                                         <a href="<?php echo site_url("course/detail/{$value->linkId}");?>"><img src="<?php echo $value->image ?>" alt=""></a>
                                    </div>

                                    <div class="course-content-wrap">
                                        <header class="entry-header">
                                            

                                            <div class="entry-meta  align-items-center">
                                                <div class="course-author"><a href="<?php echo site_url("course/detail/{$value->linkId}");?>"><?php echo $value->title ?></a></div>
                                            </div>
                                             <div class="entry-title"><?php echo $value->instructorName ?></div>
                                           <!--    <div class="entry-title"> <a href="<?php echo site_url("course/detail/{$value->linkId}");?>"><?php echo  iconv_substr($value->excerpt,0,100, "UTF-8");?></a></div> -->

                                        </header>

                                        <footer class="entry-footer text-right">
                                           
                                            <div class="course-ratings flex justify-content-end text-right ">
                                                 <span class="rating4"><?php echo $value->stars['stars'] ?></span>

                                                <span class="course-ratings-count">(<?php echo $value->stars['starsCount'] ?> votes)</span>
                                            </div>
                                        </footer>
                                    </div>
                                    <div class="course-wishlist">
                                       <a val="<?php echo $value->courseId ?>" id="f-<?php echo $value->courseId ?>"   data-toggle="tooltip" data-placement="bottom" title="สนใจ" <?php if(!$isLogin){ ?> href="javascript:void(0)" class="wishlist-heart  modalLogin"
                                                <?php }else{ ?>
                                        class="wishlist-heart <?php echo $value->wishlisted ?>" onclick="javascript:updateFavorite(<?php echo $value->courseId ?>)"
                                                <?php } ?>>
                                            <svg height="18" preserveAspectRatio="xMidYMid" viewBox="0 0 20 18" width="18" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
                                            <defs>
                                            <style>
                                              .heart {
                                                fill: #ffffff;
                                                stroke: #none;
                                                stroke-linejoin: round;
                                                stroke-width: 1px;
                                                fill-rule: evenodd;
                                              }
                                              .heart:hover {
                                                fill: #ffffff
                                              }
                                              .wishlist {
                                                fill: #ffffff
                                              }
                                            </style>
                                            </defs>
                                            <path class="heart" d="M10.000,17.000 L8.740,15.783 C4.060,11.783 1.000,9.087 1.000,5.783 C1.000,3.087 3.160,1.000 5.950,1.000 C7.480,1.000 9.010,1.696 10.000,2.826 C10.990,1.696 12.520,1.000 14.050,1.000 C16.840,1.000 19.000,3.087 19.000,5.783 C19.000,9.087 15.940,11.783 11.260,15.783 L10.000,17.000 Z"></path>
                                            </svg>
                                            
                                        </a>
                                    </div>
                                </div>
                            </div>
                             
                           
                            <?php } ?>
                            
                          
                           </div>
                           <!-- </div> -->
                           
                           <?php } ?>
                           <?php } ?>

                            
                        </div> 
        </div>
</div>
