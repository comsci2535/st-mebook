          <div class="tap-menu">
            <!--  <div class="container">
                <div class="row"> -->
                  <nav id="navbar-2" style="background: #eee;">
                    <div class="container-tap">
                        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                          <a class="nav-item nav-link " href="<?php echo site_url('course/featured'); ?>"><i class="fa fa-star"></i> <br>น่าสนใจ</a>
                          <a class="nav-item nav-link" href="<?php echo site_url('course/top_chart'); ?>"  ><i class="fa fa-line-chart"></i> <br>ติดอันดับ</a>
                          <a class="nav-item nav-link active" href="<?php echo site_url('course/category'); ?>"><i class="fa fa-sitemap"></i> <br>หมวดหมู่</a>
                           <a  <?php if(!$isLogin){ ?> href="javascript:void(0)" class="modalLogin nav-item nav-link "
                          <?php }else{ ?>
                            class="nav-item nav-link " href="<?php echo site_url('course/favorite'); ?>"
                          <?php } ?>><i class="fa fa-heart"></i> <br>รายการโปรด</a>
                           <a class="nav-item nav-link" href="<?php echo site_url('article'); ?>"><i class="fa fa-clock-o"></i> <br>ข่าววันนี้</a>
                        </div>
                    </div>
                  </nav>

                  
                
               <!--  </div>
              </div> -->
         <!--  </div>
          </div> -->
        </div>
    <!--================Blog Categorie Area =================-->
        <section class="blog_categorie_area p-20">
            <div class="container">
                <div class="row">

                    <div class="product-tile col-12 col-xs-12 col-lg-12">
                        <div class="text-title">Categories</div>
                        <div class="row">
                         <span class="col-8 col-xs-6 col-lg-6 font-16">หมวดหมู่</span>
                         <span class="col-4 col-xs-6 col-lg-6 text-right" ></span>
                        </div>
                        <hr>
                    </div>

                     <?php foreach ($category_list as $key => $rs) { ?>
                     <?php if($rs->countCourse > 0){ ?>

                    <div class="col-12 col-md-4 col-xs-3 col-lg-3" style="padding-bottom: 25px">
                        <div class="categories_post">
                            <img src="<?php echo $rs->image; ?>" alt="post">
                            <div class="categories_details">
                                <div class="categories_text">
                                    <a href="<?php echo site_url("course/category/type/{$rs->nameLink}");?>"><h5><?php echo $rs->name; ?></h5></a>
                                    <div class="border_line"></div>
                                    <p><?php echo $rs->excerpt; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </section>
