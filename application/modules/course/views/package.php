<!--================Pagkages Area =================-->
        <section class="packages_area p_120">
        	<div class="container">
        		<div class="row packages_inner">
        			<div class="col-lg-4">
                        <div class="packages_item">
                            <div class="pack_head">
                                <i class="lnr lnr-graduation-hat"></i>
                                <h3>3 เดือน</h3>
                                <p>For the individuals</p>
                            </div>
                            <div class="pack_body">
                                <ul class="list">
                                    <li><a href="#">Secure Online Transfer</a></li>
                                    <li><a href="#">Unlimited Styles for interface</a></li>
                                    <li><a href="#">Reliable Customer Service</a></li>
                                </ul>
                            </div>
                            <div class="pack_footer">
                                <h4>1,000 ฿</h4>
                                <a class="main_btn" href="#">กดสั่งซื้อ</a>
                            </div>
                        </div>
                    </div>
        			<div class="col-lg-4">
        				<div class="packages_item">
        					<div class="pack_head">
        						<i class="lnr lnr-graduation-hat"></i>
        						<h3>6 เดือน</h3>
        						<p>For the individuals</p>
        					</div>
        					<div class="pack_body">
        						<ul class="list">
        							<li><a href="#">Secure Online Transfer</a></li>
        							<li><a href="#">Unlimited Styles for interface</a></li>
        							<li><a href="#">Reliable Customer Service</a></li>
        						</ul>
        					</div>
        					<div class="pack_footer">
        						<h4>1,500 ฿</h4>
        						<a class="main_btn" href="#">กดสั่งซื้อ</a>
        					</div>
        				</div>
        			</div>
        			<div class="col-lg-4">
        				<div class="packages_item">
        					<div class="pack_head">
        						<i class="lnr lnr-diamond"></i>
        						<h3>1 ปี</h3>
        						<p>For the individuals</p>
        					</div>
        					<div class="pack_body">
        						<ul class="list">
        							<li><a href="#">Secure Online Transfer</a></li>
        							<li><a href="#">Unlimited Styles for interface</a></li>
        							<li><a href="#">Reliable Customer Service</a></li>
        						</ul>
        					</div>
        					<div class="pack_footer">
        						<h4>3,500 ฿</h4>
        						<a class="main_btn" href="#">กดสั่งซื้อ</a>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Pagkages Area =================-->