<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Repo extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("Repo_m");
    }

    public function index($id = "") {
        $this->load->module('front');
        $data['contentView'] = 'empty';
        $this->front->layout($data);
    }

}
