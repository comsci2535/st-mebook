  <section class="packages_area p-25">
            <div class="container">
                <div class="col-md-12 p-15 btn-register-l">
                        <h3>เลือกแพ็กเกจ</h3>

                </div>
                <div class="row packages_inner p-25">
                     <?php if(!empty($info)){ foreach ($info as $key => $rs) {  ?>
                    <div class="col-lg-4" style="align-self: auto;">
                        <div class="packages_item text-left" <?php if($rs->isPopular==1){ ?> style="background: #EDEDED;" <?php } ?>>
                            <?php if($rs->isPopular==1){ ?> <span class="badge badge-warning" style="position: absolute;left: 5px;top: 10px;">นิยมสูงสุด</span> <?php } ?>
                       
                            <div class="pack_head">
                                <h3><?=$rs->title?></h3>
                                <span><?=$rs->excerpt?></span><br>
                               <price class=" plan_cost pro">
                                        <span class="superscript">฿</span>
                                        <span class="priceWithoutSymbolAndCents"><?=number_format($rs->price)?></span>
                                       <!--  <span class="superscript cents">00</span> -->
                                        <span class="per_month ">/ <?=$rs->length?> <?=$rs->length_m?></span>
                                </price>
                            </div>
                            <div class="pack_body" style="height: 245px;">
                                 <p><?=$rs->detail?></p>
                                <!-- <ul class="list">
                                    <li><a href="#">Secure Online Transfer</a></li>
                                    <li><a href="#">Unlimited Styles for interface</a></li>
                                    <li><a href="#">Reliable Customer Service</a></li>
                                </ul> -->
                            </div>
                            <span><a href="">อ่านเพิ่มเติม</a></span>
                            <div class="pack_footer text-center p-15">
                                 <a  <?php if(!$isLogin){ echo 'class="pay-btn modalLogin" href="javascript:void(0)"'; }else if($isLogin && $endStudyDate > 7){ ?> class="pay-btn" onclick="functionAlert()" href="javascript:void(0)" <?php }else{ ?> class="pay-btn  various5 fancybox.iframe"  href="<?php echo site_url('package/payment/index/'.$rs->packageId);?>" <?php } ?>>สั่งซื้อ</a>
                            </div>
                        </div>
                    </div>
                    <?php } } ?>
                </div>
            </div>
        </section>

<script>
function functionAlert() {
  alert("แพ็กเกจของท่านยังไม่ถึงเวลากำหนดหมดเขต ");
}
</script>