<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Package_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    

    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);

        
        
        $query = $this->db
                        ->select('a.*')
                        ->from('package a')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('package a')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['keyword'])
                    ->or_like('a.excerpt', $param['keyword'])
                    ->or_like('a.detail', $param['keyword'])
                    ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.createDate,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
        if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.updateDate,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
        }     
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }         
        // END form filter
        
         if ( isset($param['packageId']) && $param['packageId'] != "" ) {
            $this->db->where('a.packageId', $param['packageId']);
        }


        if ( isset($param['recycle']) ){
            if (is_array($param['recycle'])) {
                $this->db->where_in('a.recycle', $param['recycle']);
            } else {
                $this->db->where('a.recycle', $param['recycle']);
            }
           
        }

        //$this->db->order_by('a.recommend', 'DESC');
       
        $this->db->order_by('a.updateDate', 'DESC');
        $this->db->order_by('a.createDate', 'DESC');
        


    }

    
     public function get_promotion_rows($param) 
    {
        $this->_condition_promotion($param); 
          
        $query = $this->db
                        ->select('a.*')
                        ->from('promotion a')
                        ->get();
        return $query;
    }

    private function _condition_promotion($param) 

    {   
   
        $this->db->where("((a.startDate ='0000-00-00' AND a.endDate ='0000-00-00') OR  (a.startDate <='".date('Y-m-d')."' AND a.endDate >='".date('Y-m-d')."'))");
       
       if ( isset($param['packageId']) ) 
            $this->db->where('a.packageId', $param['packageId']);


        $this->db->where('a.active', 1);
               
        $this->db->where('a.recycle',0);

       

    }

    public function insertPackageRegis($value) 
    {
        // $this->db->insert('course_register', $value);
        // return $this->db->insert_id();

        if($this->db->insert('course_register', $value)){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

     public function get_PackageRegis($id) 
    {
        
        $this->db->where('a.course_registerId', $id);
        $query = $this->db
                        ->select('a.*')
                        ->select('b.firstname,b.lastname,b.email')
                        //->select('c.*')
                        ->from('course_register a')
                        ->join('user b', 'a.userId = b.userId', 'left')
                        //->join('promotion c', 'a.promotionId = c.promotionId', 'left')
                        ->get();
        return $query;
    }

}
