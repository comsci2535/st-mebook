<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$active_group = 'default';
$query_builder = TRUE;

$db['default'] = array(
	'dsn'	=> '',
	'hostname' => 'localhost',
	'username' => 'root',
	'password' => 'mebook!@#$1234',
	'database' => 'mebook',
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => 'uploads/cache/',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);

if ( in_array(gethostname(), array('CSI-RUSLEE', 'DESKTOP-S160BUC','DESKTOP-RLBN8AB')) ) { 

    $db['default']['hostname'] = '150.95.25.104';
    $db['default']['username'] = 'root';
    $db['default']['password'] = 'mebook!@#$1234';
    $db['default']['database'] = 'mebook';    
        
}

