/*
Navicat MySQL Data Transfer

Source Server         : MySQL Local
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : db_mebook

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2019-07-04 20:50:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for activity
-- ----------------------------
DROP TABLE IF EXISTS `activity`;
CREATE TABLE `activity` (
  `activityId` int(11) NOT NULL AUTO_INCREMENT,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `detail` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `recommend` int(1) DEFAULT '0' COMMENT '0=ไม่แนะนำ,1=แนะนำ',
  `view` int(15) DEFAULT '0',
  `price` varchar(25) DEFAULT '',
  `promotion` varchar(25) DEFAULT '',
  `metaTitle` text,
  `metaDescription` text,
  `metaKeyword` text,
  `title_link` varchar(250) DEFAULT '',
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `startTime` time DEFAULT NULL,
  `endTime` time DEFAULT NULL,
  `location` varchar(255) DEFAULT '',
  `title_gallery` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`activityId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of activity
-- ----------------------------
INSERT INTO `activity` VALUES ('1', null, 'Sales Training: Practical Sales Techniques', 'fasdfa', 'fasdf', '1', '0', '2019-02-17 12:34:37', '3', '2019-02-17 12:35:22', '3', '0', null, null, '0', '0', '1200', '1000', 'Sales Training: Practical Sales Techniques', 'fasdfa', 'Sales Training: Practical Sales Techniques', 'Sales Training: Practical Sales Techniques', '2019-02-17', '2019-02-28', '08:00:00', '17:00:00', 'ทดสอบ', '');

-- ----------------------------
-- Table structure for bank
-- ----------------------------
DROP TABLE IF EXISTS `bank`;
CREATE TABLE `bank` (
  `bankId` int(11) NOT NULL AUTO_INCREMENT,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `branch` varchar(250) DEFAULT '',
  `accountNumber` varchar(150) DEFAULT '',
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`bankId`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bank
-- ----------------------------
INSERT INTO `bank` VALUES ('1', 'th', 'งานประชุมประจำปี2017', null, null, '0', '0', '2018-03-10 14:25:13', '0', '2018-07-03 04:27:00', '1', '2', '2018-04-25 16:59:31', '1', null, null);
INSERT INTO `bank` VALUES ('2', 'th', 'บริหารจัดการหมู่บ้านจัดสรรและอาคารชุด', null, null, '0', '0', '2018-03-09 20:57:07', '3', '2018-10-21 20:17:29', '3', '2', '2018-10-21 19:54:00', '3', null, null);
INSERT INTO `bank` VALUES ('3', 'th', 'ดูแลสาธารณูปโภคของหมู่บ้านจัดสรรและอาคารชุด', null, null, '0', '0', '2018-03-09 21:16:50', '3', '2018-10-21 20:17:29', '3', '2', '2018-10-21 19:54:00', '3', null, null);
INSERT INTO `bank` VALUES ('4', 'th', 'รับจดทะเบียนจัดตั้งนิติบุคคลหมู่บ้านจัดสรรและอาคารชุด', null, null, '0', '0', '2018-03-09 21:19:06', '3', '2018-10-21 20:17:29', '3', '2', '2018-10-21 19:54:00', '3', null, null);
INSERT INTO `bank` VALUES ('5', 'th', 'test', null, null, '0', '0', '2018-03-09 21:23:54', '3', '2018-03-16 09:16:00', '1', '2', '2018-03-16 09:15:47', '1', null, null);
INSERT INTO `bank` VALUES ('6', 'th', 'test', null, null, '0', '0', '2018-03-09 21:24:16', '3', '2018-03-16 09:15:57', '1', '2', '2018-03-16 09:15:47', '1', null, null);
INSERT INTO `bank` VALUES ('7', 'th', 'test', null, null, '0', '0', '2018-03-09 21:37:20', '3', '2018-03-16 09:16:03', '1', '2', '2018-03-16 09:15:47', '1', null, null);
INSERT INTO `bank` VALUES ('8', 'th', 'โครงการที่ 1', null, null, '0', '0', '2018-03-10 11:39:19', '1', '2018-10-21 20:17:29', '3', '2', '2018-10-21 19:54:00', '3', null, null);
INSERT INTO `bank` VALUES ('9', 'th', 'โครงการที่ 2', null, null, '0', '0', '2018-03-10 12:03:36', '1', '2018-10-21 20:17:29', '3', '2', '2018-10-21 19:54:28', '3', null, null);
INSERT INTO `bank` VALUES ('10', 'th', 'โครงการที่ 3', null, null, '0', '0', '2018-03-10 12:06:06', '1', '2018-10-21 20:17:36', '3', '2', '2018-10-21 19:54:10', '3', null, null);
INSERT INTO `bank` VALUES ('11', 'th', 'จัดตั้งบริษัท', null, null, '0', '0', '2018-03-10 14:04:01', '1', '2018-10-21 20:17:29', '3', '2', '2018-10-21 19:54:00', '3', null, null);
INSERT INTO `bank` VALUES ('12', 'th', 'ให้บริการด้านจัดสวน', null, null, '0', '0', '2018-03-10 15:36:02', '1', '2018-10-21 20:17:36', '3', '2', '2018-10-21 19:54:10', '3', null, null);
INSERT INTO `bank` VALUES ('14', 'th', null, null, null, '0', '0', '2018-03-10 16:49:34', '1', '2018-10-21 20:17:29', '3', '2', '2018-10-21 19:54:00', '3', null, null);
INSERT INTO `bank` VALUES ('15', 'th', null, null, null, '0', '0', '2018-03-10 20:27:49', '1', '2018-10-21 20:17:29', '3', '2', '2018-10-21 19:54:00', '3', null, null);
INSERT INTO `bank` VALUES ('16', 'th', null, null, null, '0', '0', '2018-03-10 20:28:50', '1', '2018-10-21 20:17:29', '3', '2', '2018-10-21 19:54:00', '3', null, null);
INSERT INTO `bank` VALUES ('17', 'th', 'ตกแต่งครัวแบบไทยๆ', null, null, '0', '0', '2018-03-11 15:17:28', '1', '2018-10-21 20:17:29', '3', '2', '2018-10-21 19:54:00', '3', null, null);
INSERT INTO `bank` VALUES ('18', 'th', 'การจัดตั้งนิติบุคคลหมู่บ้านจัดสรร', '', '', '0', '0', '2018-03-11 15:26:16', '1', '2018-07-03 04:27:00', '1', '2', '2018-04-25 16:58:33', '1', null, null);
INSERT INTO `bank` VALUES ('19', null, 'test repo', 'test is test', 'test is test test is test', '0', '0', '2018-03-16 08:34:32', '1', '2018-03-23 11:21:35', '1', '2', '2018-03-23 10:41:36', '1', null, null);
INSERT INTO `bank` VALUES ('20', null, 'กสิกรไทย', '-', '868-2-08221-7', '1', '0', '2018-10-21 20:01:24', '3', '2019-02-20 15:50:28', '3', '0', null, null, 'ออมทรัพย์', 'นาย กรกฎ พรลักษณพิมล');
INSERT INTO `bank` VALUES ('21', null, 'กรุงเทพ', '-', '106-5-40644-7', '1', '0', '2018-10-21 20:16:01', '3', '2019-02-20 15:50:22', '3', '0', null, null, 'ออมทรัพย์', 'กรกฎ พรลักษณพิมล');
INSERT INTO `bank` VALUES ('22', null, 'กรุงศรีอยุธยา', '-', '494-1-10515-7', '1', '0', '2018-10-21 20:16:51', '3', '2019-02-20 15:50:13', '3', '0', null, null, 'ออมทรัพย์', 'กรกฎ พรลักษณพิมล');

-- ----------------------------
-- Table structure for bank_list
-- ----------------------------
DROP TABLE IF EXISTS `bank_list`;
CREATE TABLE `bank_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT '',
  `title` varchar(250) DEFAULT NULL,
  `imgFile` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bank_list
-- ----------------------------
INSERT INTO `bank_list` VALUES ('23', 'BKKBTHBK', 'ธนาคารกรุงเทพ', '002.gif');
INSERT INTO `bank_list` VALUES ('24', 'KASITHBK', 'ธนาคารกสิกรไทย', '004.gif');
INSERT INTO `bank_list` VALUES ('25', 'KRTHTHBK', 'ธนาคารกรุงไทย', '006.gif');
INSERT INTO `bank_list` VALUES ('26', 'TMBKTHB', 'ธนาคารทหารไทย', '011.gif');
INSERT INTO `bank_list` VALUES ('27', 'SICOTHBK', 'ธนาคารไทยพาณิชย์', '014.gif');
INSERT INTO `bank_list` VALUES ('28', 'AYUDTHBK', 'ธนาคารกรุงศรีอยุธยา', '025.gif');
INSERT INTO `bank_list` VALUES ('29', 'KIFITHB1', 'ธนาคารเกียรตินาคิน', '069.gif');
INSERT INTO `bank_list` VALUES ('30', 'UBOBTHBK', 'ธนาคารซีไอเอ็มบีไทย', '022.gif');
INSERT INTO `bank_list` VALUES ('31', 'TFPCTHB1', 'ธนาคารทิสโก้', '067Newlogotisco.jpg');
INSERT INTO `bank_list` VALUES ('32', 'THBKTHBK', 'ธนาคารธนชาต', '065.gif');
INSERT INTO `bank_list` VALUES ('33', 'UOVBTHBK', 'ธนาคารยูโอบี', 'Logo-UOB.jpg');
INSERT INTO `bank_list` VALUES ('34', 'SCBLTHBX', 'ธนาคารสแตนดาร์ดชาร์เตอร์ด (ไทย)', '020.gif');
INSERT INTO `bank_list` VALUES ('35', 'LAHRTHB1', 'ธนาคารแลนด์ แอนด์ เฮาส์', '073.GIF');
INSERT INTO `bank_list` VALUES ('36', 'ICBKTHBK', 'ธนาคารไอซีบีซี (ไทย)', '070.gif');
INSERT INTO `bank_list` VALUES ('37', 'BAABTHBK', 'ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร', 'Baac2.png');
INSERT INTO `bank_list` VALUES ('38', 'GSBATHBK', 'ธนาคารออมสิน', '030.png');
INSERT INTO `bank_list` VALUES ('39', 'TIBTTHBK', 'ธนาคารอิสลามแห่งประเทศไทย', '066.jpg');

-- ----------------------------
-- Table structure for banner
-- ----------------------------
DROP TABLE IF EXISTS `banner`;
CREATE TABLE `banner` (
  `bannerId` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `detail` text,
  `link` varchar(255) DEFAULT '',
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `type` varchar(30) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `page` varchar(150) DEFAULT NULL,
  `order` int(11) DEFAULT '0',
  PRIMARY KEY (`bannerId`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of banner
-- ----------------------------
INSERT INTO `banner` VALUES ('13', 'หน้าปก', 'We Ensure better education  ', 'In the history of modern astronomy, there is probably', 'http://google.com', '0000-00-00', '0000-00-00', '1', 'hero', '2018-09-23 22:27:36', '3', '2019-01-24 12:44:41', '3', '0', null, null, 'home', '1');
INSERT INTO `banner` VALUES ('14', 'ทดสอบ', '', null, '', '0000-00-00', '0000-00-00', '0', 'hero', '2018-09-23 22:43:02', '3', '2018-09-23 22:45:34', '3', '1', '2018-09-23 22:45:34', '3', null, '2');
INSERT INTO `banner` VALUES ('15', 'ทดสอบ', '', '', '', '2019-01-18', '2019-01-26', '1', 'hero', '2018-09-25 19:26:49', '3', '2019-01-24 12:56:52', '3', '0', null, null, 'home', '3');
INSERT INTO `banner` VALUES ('16', 'facebook', 'facebook', null, 'https://facebook.com', '0000-00-00', '0000-00-00', '1', 'social', '2018-10-06 23:18:32', '3', '2019-01-10 00:00:35', '3', '0', null, null, null, '4');
INSERT INTO `banner` VALUES ('17', 'ยูทูบ', '', null, 'http://youtube.com', '0000-00-00', '0000-00-00', '1', 'social', '2018-10-06 23:26:45', '3', '2019-01-10 00:04:17', '3', '0', null, null, null, '5');
INSERT INTO `banner` VALUES ('18', 'aa', '', null, 'http://google.com', '0000-00-00', '0000-00-00', '0', 'hero', '2018-12-01 18:43:07', '3', '2018-12-30 22:38:19', '3', '1', '2018-12-30 22:38:19', '3', 'home', '6');
INSERT INTO `banner` VALUES ('19', 'Messenger ', '', null, 'http://facebook.com', '0000-00-00', '0000-00-00', '1', 'social', '2019-01-09 23:51:48', '3', '2019-01-09 23:51:55', '3', '0', null, null, null, '7');
INSERT INTO `banner` VALUES ('20', 'twitter', '', null, 'http://twitter.com', '0000-00-00', '0000-00-00', '1', 'social', '2019-01-10 00:02:18', '3', '2019-01-10 00:02:20', '3', '0', null, null, null, '8');
INSERT INTO `banner` VALUES ('21', 'instagram', '', null, 'http://instagram.com', '0000-00-00', '0000-00-00', '1', 'social', '2019-01-10 00:07:39', '3', '2019-01-10 00:08:20', '3', '0', null, null, null, '9');

-- ----------------------------
-- Table structure for banner_image
-- ----------------------------
DROP TABLE IF EXISTS `banner_image`;
CREATE TABLE `banner_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bannerId` int(11) DEFAULT NULL,
  `uploadId` int(11) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `titleBanner` varchar(250) DEFAULT '',
  `excerpt` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=193 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of banner_image
-- ----------------------------
INSERT INTO `banner_image` VALUES ('93', '2', '428', 'aa', '2018-07-05', '2018-07-06', 'th', '', null);
INSERT INTO `banner_image` VALUES ('111', '3', '448', '', null, null, 'th', '', null);
INSERT INTO `banner_image` VALUES ('138', '4', '510', '', null, null, 'th', '', '');
INSERT INTO `banner_image` VALUES ('139', '1', '511', '', null, null, 'th', '', '');
INSERT INTO `banner_image` VALUES ('140', '1', '512', '', null, null, 'th', '', '');
INSERT INTO `banner_image` VALUES ('141', '1', '513', '', null, null, 'th', '', '');
INSERT INTO `banner_image` VALUES ('142', '5', '514', '', null, null, 'th', '', null);
INSERT INTO `banner_image` VALUES ('143', '5', '515', '', null, null, 'th', '', null);
INSERT INTO `banner_image` VALUES ('144', '5', '517', '', null, null, 'th', '', null);
INSERT INTO `banner_image` VALUES ('145', '5', '518', '', null, null, 'th', '', null);
INSERT INTO `banner_image` VALUES ('146', '5', '516', '', null, null, 'th', '', null);
INSERT INTO `banner_image` VALUES ('149', '8', '547', null, '2018-08-10', '2018-08-13', 'th', 'บ้าน 1', null);
INSERT INTO `banner_image` VALUES ('150', '8', '549', null, null, null, 'th', 'บ้าน 2', null);
INSERT INTO `banner_image` VALUES ('166', '9', '628', null, null, null, 'th', '', null);
INSERT INTO `banner_image` VALUES ('167', '9', '629', null, null, null, 'th', '', null);
INSERT INTO `banner_image` VALUES ('168', '9', '630', null, null, null, 'th', '', null);
INSERT INTO `banner_image` VALUES ('169', '10', '631', null, null, null, 'th', '', null);
INSERT INTO `banner_image` VALUES ('170', '10', '632', null, null, null, 'th', '', null);
INSERT INTO `banner_image` VALUES ('171', '10', '633', null, null, null, 'th', '', null);
INSERT INTO `banner_image` VALUES ('172', '11', '634', null, null, null, 'th', '', null);
INSERT INTO `banner_image` VALUES ('173', '11', '635', null, null, null, 'th', '', null);
INSERT INTO `banner_image` VALUES ('174', '11', '636', null, null, null, 'th', '', null);
INSERT INTO `banner_image` VALUES ('190', '12', '637', 'https://adminlte.io/themes/AdminLTE/index.html', null, null, 'th', 'เว็บทรัพย์ดีดี', 'เว็บทรัพย์ดีดี เว็บทรัพย์ดีดี เว็บทรัพย์ดีดี เว็บทรัพย์ดีดี');
INSERT INTO `banner_image` VALUES ('191', '12', '640', '', null, null, 'th', 'เว็บทรัพย์ดีดี', '');
INSERT INTO `banner_image` VALUES ('192', '12', '641', '', null, null, 'th', 'เว็บทรัพย์ดีดี', '');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `categoryId` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(11) DEFAULT '0',
  `active` tinyint(1) DEFAULT '1' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `type` varchar(50) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`categoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('14', null, '1', '1', 'news', '2018-07-03 07:06:14', '3', '2018-07-03 09:34:08', '3', '0', null, null);
INSERT INTO `category` VALUES ('15', '14', '0', '2', 'news', '2018-07-03 07:07:26', '3', '2018-07-03 07:08:13', '3', '1', '2018-07-03 07:08:13', '3');
INSERT INTO `category` VALUES ('16', '15', '0', '3', 'news', '2018-07-03 07:07:49', '3', '2018-07-03 07:08:13', '3', '1', '2018-07-03 07:08:13', '3');
INSERT INTO `category` VALUES ('17', null, '1', '4', 'media', '2018-07-06 10:26:55', '3', null, '0', '0', null, null);
INSERT INTO `category` VALUES ('18', null, '1', '5', 'media', '2018-07-06 10:27:31', '3', null, '0', '0', null, null);
INSERT INTO `category` VALUES ('19', null, '1', '6', 'news', '2018-07-11 06:20:24', '3', null, '0', '0', null, null);
INSERT INTO `category` VALUES ('20', null, '0', '7', 'article', '2018-08-04 16:44:29', '3', '2018-11-06 12:49:58', '3', '2', '2018-08-04 19:42:54', '3');
INSERT INTO `category` VALUES ('21', null, '0', '8', '', '2018-08-04 19:41:01', '3', '2018-08-04 19:42:50', '3', '1', '2018-08-04 19:42:50', '3');
INSERT INTO `category` VALUES ('22', null, '1', '11', 'article', '2018-08-04 21:25:09', '3', '2018-11-06 12:50:10', '3', '0', null, null);
INSERT INTO `category` VALUES ('23', null, '0', '10', 'article', '2018-08-04 23:47:15', '3', '2018-11-06 12:49:58', '3', '2', '2018-09-18 01:10:50', '3');
INSERT INTO `category` VALUES ('24', null, '1', '9', 'article', '2018-11-05 13:58:37', '3', '2018-11-06 12:59:28', '3', '0', null, null);
INSERT INTO `category` VALUES ('25', null, '0', '12', 'article', '2018-11-05 14:09:12', '3', '2018-11-06 12:49:58', '3', '2', '2018-11-05 14:09:20', '3');
INSERT INTO `category` VALUES ('26', null, '0', '13', 'article', '2018-11-05 14:30:56', '3', '2018-11-06 12:51:10', '3', '1', '2018-11-06 12:51:10', '3');
INSERT INTO `category` VALUES ('27', null, '1', '15', 'course', '2018-12-11 22:40:37', '3', '2019-01-22 23:41:23', '3', '0', null, null);
INSERT INTO `category` VALUES ('28', null, '1', '14', 'course', '2018-12-11 22:41:04', '3', '2019-01-22 23:41:30', '3', '0', null, null);
INSERT INTO `category` VALUES ('29', null, '0', '16', 'course', '2019-01-26 16:06:11', '3', '2019-01-26 16:06:28', '3', '1', '2019-01-26 16:06:28', '3');

-- ----------------------------
-- Table structure for category_lang
-- ----------------------------
DROP TABLE IF EXISTS `category_lang`;
CREATE TABLE `category_lang` (
  `categoryId` int(11) NOT NULL DEFAULT '0',
  `lang` varchar(2) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `excerpt` text,
  `content` text,
  `nameLink` varchar(255) DEFAULT '',
  `metaTitle` text,
  `metaDescription` text,
  `metaKeyword` text,
  PRIMARY KEY (`categoryId`,`lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of category_lang
-- ----------------------------
INSERT INTO `category_lang` VALUES ('1', 'th', 'คณิตศาสตร์', 'คณิตศาสตร์ระดับชั้นประถม', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('2', 'th', 'บวกเลข', null, null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('3', 'th', 'พุทธศาสนา', 'พุทธศาสนาเพื่อชีวติประจำวัน', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('4', 'th', 'คูณเลข', null, null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('5', 'th', 'พระสูตร', 'ปปปปป', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('6', 'th', 'ธรรมดำ', '-', '-', '', null, null, null);
INSERT INTO `category_lang` VALUES ('7', 'en', 'ABC', 'ABC Excerpt', '', '', null, null, null);
INSERT INTO `category_lang` VALUES ('8', 'en', 'DEFG', '', '', '', null, null, null);
INSERT INTO `category_lang` VALUES ('9', 'en', 'HIJ', '', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('10', 'en', 'KLMN', '', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('11', 'th', 'วิธีคิดลัด', '', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('12', 'th', 'xxx', 'xx', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('13', 'th', 'aa', '', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('14', 'th', 'ข่าวประชาสัมพันธ์', 'ข่าวประชาสัมพันธ์', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('15', 'th', 'aa1', 'aa1', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('16', 'th', 'aaa1', '', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('17', 'th', 'หลักสูตรทั่วไป', 'หลักสูตรทั่วไป', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('18', 'th', 'หลักสูตรภาคบังคับ (วิชาชีพ/เฉพาะทาง)', 'หลักสูตรภาคบังคับ (วิชาชีพ/เฉพาะทาง)', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('19', 'th', 'ข่าวแนะนำ', '', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('20', 'th', 'ฟฟ', 'ฟฟ', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('21', 'th', 'aaaa2', 'aa2', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('22', 'th', 'บทความทั่วไป', 'บทความทั่วไป', null, 'บทความทั่วไป', 'บทความทั่วไป', 'บทความทั่วไป', 'บทความทั่วไป');
INSERT INTO `category_lang` VALUES ('23', 'th', 'บทความเกี่ยวกับบ้าน 2', 'บทความเกี่ยวกับบ้าน 2', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('24', 'th', 'บทความสอนสด', 'บทความสอนสด', null, 'บทความสอนสด', 'บทความสอนสด', 'บทความสอนสด', 'บทความสอนสด');
INSERT INTO `category_lang` VALUES ('25', 'th', 'บทความทั่วไป', '', null, '', null, null, null);
INSERT INTO `category_lang` VALUES ('26', 'th', 'บทความทั่วไป 2', '', null, 'บทความทั่วไป-2', null, null, null);
INSERT INTO `category_lang` VALUES ('27', 'th', 'การตลาด', 'การตลาด', null, 'การตลาด', 'การตลาด', 'การตลาด', 'การตลาด');
INSERT INTO `category_lang` VALUES ('28', 'th', 'กราฟฟิกดีไซน์', 'กราฟฟิกดีไซน์', null, 'กราฟฟิกดีไซน์', 'กราฟฟิกดีไซน์', 'กราฟฟิกดีไซน์', 'กราฟฟิกดีไซน์');
INSERT INTO `category_lang` VALUES ('29', 'th', 'Brio', 'บรีโอ', null, 'Brio', 'Brio', 'บรีโอ', 'Brio');

-- ----------------------------
-- Table structure for ci_sessions
-- ----------------------------
DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions` (
  `id` varchar(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `data` longtext,
  PRIMARY KEY (`id`,`ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ci_sessions
-- ----------------------------
INSERT INTO `ci_sessions` VALUES ('16a8cb7f66763a00d7e92a585e4b59992debf4e6', '::1', '1550719841', '__ci_last_regenerate|i:1550719839;urlreffer|a:1:{s:3:\"url\";s:28:\"http://localhost:81/_mebook/\";}FBRLH_state|s:32:\"1e297f5c062106f9e995d1a27b6737a8\";');
INSERT INTO `ci_sessions` VALUES ('25c04dc0d157ff48311bd682bfe3e55984d227c1', '::1', '1550663534', '__ci_last_regenerate|i:1550663534;FBRLH_state|s:32:\"286eb525b83353a4ee711e7e5f63faef\";condition|a:1:{s:9:\"promotion\";a:17:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"createDate\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"updateDate\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"25\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:9:\"packageId\";s:1:\"5\";s:9:\"frmFilter\";s:114:\"createDateRange=&createStartDate=&createEndDate=&updateDateRange=&updateStartDate=&updateEndDate=&active=&keyword=\";s:15:\"createDateRange\";s:0:\"\";s:15:\"createStartDate\";s:0:\"\";s:13:\"createEndDate\";s:0:\"\";s:15:\"updateDateRange\";s:0:\"\";s:15:\"updateStartDate\";s:0:\"\";s:13:\"updateEndDate\";s:0:\"\";s:6:\"active\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:7:\"recycle\";i:0;}}user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:44:\"http://localhost:81/_mebook/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}member|a:13:{s:6:\"userId\";s:2:\"71\";s:4:\"name\";s:6:\"satari\";s:8:\"fullname\";s:13:\"satari maseng\";s:5:\"image\";s:85:\"http://localhost:81/_mebook/uploads/2019/02/user/98692c9e47d44f4141d6a1938e4560de.jpg\";s:5:\"email\";s:20:\"satari.dop@gmail.com\";s:9:\"sectionId\";N;s:7:\"partyId\";N;s:10:\"positionId\";N;s:6:\"degree\";N;s:4:\"type\";s:6:\"member\";s:8:\"policyId\";s:1:\"0\";s:10:\"couponCode\";s:9:\"7K6EWAH71\";s:9:\"isBackend\";b:1;}urlreffer|a:1:{s:3:\"url\";s:41:\"http://localhost:81/_mebook/package/index\";}');
INSERT INTO `ci_sessions` VALUES ('5ae8e648cba23dbbebdc21f5d025060ceab546e8', '::1', '1550662917', '__ci_last_regenerate|i:1550662917;FBRLH_state|s:32:\"286eb525b83353a4ee711e7e5f63faef\";condition|a:1:{s:10:\"order_list\";a:14:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:11:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:4:\"code\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:6:\"member\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:5:\"price\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:10:\"couponCode\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:8:\"discount\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:7;a:5:{s:4:\"data\";s:5:\"total\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:8;a:5:{s:4:\"data\";s:4:\"slip\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:9;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:10;a:5:{s:4:\"data\";s:10:\"createDate\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:2:\"10\";s:3:\"dir\";s:4:\"desc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"25\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:9:\"frmFilter\";s:76:\"createDateRange=&createStartDate=&createEndDate=&packageId=&status=&keyword=\";s:15:\"createDateRange\";s:0:\"\";s:15:\"createStartDate\";s:0:\"\";s:13:\"createEndDate\";s:0:\"\";s:9:\"packageId\";s:0:\"\";s:6:\"status\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:7:\"recycle\";i:0;}}user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:44:\"http://localhost:81/_mebook/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}member|a:13:{s:6:\"userId\";s:2:\"71\";s:4:\"name\";s:6:\"satari\";s:8:\"fullname\";s:13:\"satari maseng\";s:5:\"image\";s:85:\"http://localhost:81/_mebook/uploads/2019/02/user/98692c9e47d44f4141d6a1938e4560de.jpg\";s:5:\"email\";s:20:\"satari.dop@gmail.com\";s:9:\"sectionId\";N;s:7:\"partyId\";N;s:10:\"positionId\";N;s:6:\"degree\";N;s:4:\"type\";s:6:\"member\";s:8:\"policyId\";s:1:\"0\";s:10:\"couponCode\";s:9:\"7K6EWAH71\";s:9:\"isBackend\";b:1;}urlreffer|a:1:{s:3:\"url\";s:41:\"http://localhost:81/_mebook/package/index\";}');
INSERT INTO `ci_sessions` VALUES ('5fa26da93964392deda999a9c0ce2631f110dd68', '::1', '1550663219', '__ci_last_regenerate|i:1550663219;FBRLH_state|s:32:\"286eb525b83353a4ee711e7e5f63faef\";condition|a:1:{s:10:\"order_list\";a:14:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:11:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:4:\"code\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:6:\"member\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:5:\"price\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:10:\"couponCode\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:8:\"discount\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:7;a:5:{s:4:\"data\";s:5:\"total\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:8;a:5:{s:4:\"data\";s:4:\"slip\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:9;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:10;a:5:{s:4:\"data\";s:10:\"createDate\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:2:\"10\";s:3:\"dir\";s:4:\"desc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"25\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:9:\"frmFilter\";s:76:\"createDateRange=&createStartDate=&createEndDate=&packageId=&status=&keyword=\";s:15:\"createDateRange\";s:0:\"\";s:15:\"createStartDate\";s:0:\"\";s:13:\"createEndDate\";s:0:\"\";s:9:\"packageId\";s:0:\"\";s:6:\"status\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:7:\"recycle\";i:0;}}user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:44:\"http://localhost:81/_mebook/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}member|a:13:{s:6:\"userId\";s:2:\"71\";s:4:\"name\";s:6:\"satari\";s:8:\"fullname\";s:13:\"satari maseng\";s:5:\"image\";s:85:\"http://localhost:81/_mebook/uploads/2019/02/user/98692c9e47d44f4141d6a1938e4560de.jpg\";s:5:\"email\";s:20:\"satari.dop@gmail.com\";s:9:\"sectionId\";N;s:7:\"partyId\";N;s:10:\"positionId\";N;s:6:\"degree\";N;s:4:\"type\";s:6:\"member\";s:8:\"policyId\";s:1:\"0\";s:10:\"couponCode\";s:9:\"7K6EWAH71\";s:9:\"isBackend\";b:1;}urlreffer|a:1:{s:3:\"url\";s:41:\"http://localhost:81/_mebook/package/index\";}');
INSERT INTO `ci_sessions` VALUES ('7fee9e514377d4f63bd31f230c07c7a2f50f6b8f', '::1', '1550850604', '__ci_last_regenerate|i:1550850566;urlreffer|a:1:{s:3:\"url\";s:28:\"http://localhost:81/_mebook/\";}FBRLH_state|s:32:\"1e2a1bf909089bf54c2e9883ee798164\";user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:44:\"http://localhost:81/_mebook/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}condition|a:1:{s:6:\"course\";a:14:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:8:\"courseNo\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:8:\"category\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:6:\"manage\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"2\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"25\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:9:\"frmFilter\";s:77:\"createDateRange=&createStartDate=&createEndDate=&categoryId=&active=&keyword=\";s:15:\"createDateRange\";s:0:\"\";s:15:\"createStartDate\";s:0:\"\";s:13:\"createEndDate\";s:0:\"\";s:10:\"categoryId\";s:0:\"\";s:6:\"active\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:7:\"recycle\";i:0;}}');
INSERT INTO `ci_sessions` VALUES ('b108dbe19caa8a0de2dbb2392d3facc0cc21bafd', '::1', '1551801826', '__ci_last_regenerate|i:1551801796;user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:44:\"http://localhost:81/_mebook/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}condition|a:1:{s:6:\"coupon\";a:16:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"createDate\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"updateDate\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"25\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:9:\"frmFilter\";s:114:\"createDateRange=&createStartDate=&createEndDate=&updateDateRange=&updateStartDate=&updateEndDate=&active=&keyword=\";s:15:\"createDateRange\";s:0:\"\";s:15:\"createStartDate\";s:0:\"\";s:13:\"createEndDate\";s:0:\"\";s:15:\"updateDateRange\";s:0:\"\";s:15:\"updateStartDate\";s:0:\"\";s:13:\"updateEndDate\";s:0:\"\";s:6:\"active\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:7:\"recycle\";i:0;}}');
INSERT INTO `ci_sessions` VALUES ('bb7d8eaa56344943170e9665dd0caf1ebc74d578', '::1', '1554369709', '__ci_last_regenerate|i:1554369698;user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:44:\"http://localhost:81/_mebook/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}condition|a:1:{s:7:\"package\";a:16:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:8:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:5:\"price\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:6:\"length\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"manage\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:7;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"25\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:9:\"frmFilter\";s:114:\"createDateRange=&createStartDate=&createEndDate=&updateDateRange=&updateStartDate=&updateEndDate=&active=&keyword=\";s:15:\"createDateRange\";s:0:\"\";s:15:\"createStartDate\";s:0:\"\";s:13:\"createEndDate\";s:0:\"\";s:15:\"updateDateRange\";s:0:\"\";s:15:\"updateStartDate\";s:0:\"\";s:13:\"updateEndDate\";s:0:\"\";s:6:\"active\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:7:\"recycle\";i:0;}}');
INSERT INTO `ci_sessions` VALUES ('d4a86b1f88f85a83d572dd5dfc3ee6dd975d16b4', '::1', '1551801797', '__ci_last_regenerate|i:1551801794;urlreffer|a:1:{s:3:\"url\";s:28:\"http://localhost:81/_mebook/\";}FBRLH_state|s:32:\"bc192a250addee0400b88f4ae6c627e4\";');
INSERT INTO `ci_sessions` VALUES ('e86678c5d3fb20302f49b18ad58d80df9aaecfe5', '::1', '1550663978', '__ci_last_regenerate|i:1550663940;FBRLH_state|s:32:\"286eb525b83353a4ee711e7e5f63faef\";condition|a:1:{s:9:\"promotion\";a:17:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"createDate\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"updateDate\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"25\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:9:\"packageId\";s:1:\"5\";s:9:\"frmFilter\";s:114:\"createDateRange=&createStartDate=&createEndDate=&updateDateRange=&updateStartDate=&updateEndDate=&active=&keyword=\";s:15:\"createDateRange\";s:0:\"\";s:15:\"createStartDate\";s:0:\"\";s:13:\"createEndDate\";s:0:\"\";s:15:\"updateDateRange\";s:0:\"\";s:15:\"updateStartDate\";s:0:\"\";s:13:\"updateEndDate\";s:0:\"\";s:6:\"active\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:7:\"recycle\";i:0;}}user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:44:\"http://localhost:81/_mebook/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}member|a:13:{s:6:\"userId\";s:2:\"71\";s:4:\"name\";s:6:\"satari\";s:8:\"fullname\";s:13:\"satari maseng\";s:5:\"image\";s:85:\"http://localhost:81/_mebook/uploads/2019/02/user/98692c9e47d44f4141d6a1938e4560de.jpg\";s:5:\"email\";s:20:\"satari.dop@gmail.com\";s:9:\"sectionId\";N;s:7:\"partyId\";N;s:10:\"positionId\";N;s:6:\"degree\";N;s:4:\"type\";s:6:\"member\";s:8:\"policyId\";s:1:\"0\";s:10:\"couponCode\";s:9:\"7K6EWAH71\";s:9:\"isBackend\";b:1;}urlreffer|a:1:{s:3:\"url\";s:41:\"http://localhost:81/_mebook/package/index\";}');
INSERT INTO `ci_sessions` VALUES ('f10d1a4d74fdd2fa3ee1c94d1aeef1531588139e', '::1', '1554369699', '__ci_last_regenerate|i:1554369696;urlreffer|a:1:{s:3:\"url\";s:28:\"http://localhost:81/_mebook/\";}FBRLH_state|s:32:\"0bb5f057e4b9f76ec9bbecd0ed97839a\";');
INSERT INTO `ci_sessions` VALUES ('f6e0622095e2b4e3bebc0ea6c609945519cda40b', '::1', '1550662578', '__ci_last_regenerate|i:1550662578;FBRLH_state|s:32:\"286eb525b83353a4ee711e7e5f63faef\";condition|a:1:{s:10:\"order_list\";a:14:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:11:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:4:\"code\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:6:\"member\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:5:\"price\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:10:\"couponCode\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:8:\"discount\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:7;a:5:{s:4:\"data\";s:5:\"total\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:8;a:5:{s:4:\"data\";s:4:\"slip\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:9;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:10;a:5:{s:4:\"data\";s:10:\"createDate\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:2:\"10\";s:3:\"dir\";s:4:\"desc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"25\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:9:\"frmFilter\";s:76:\"createDateRange=&createStartDate=&createEndDate=&packageId=&status=&keyword=\";s:15:\"createDateRange\";s:0:\"\";s:15:\"createStartDate\";s:0:\"\";s:13:\"createEndDate\";s:0:\"\";s:9:\"packageId\";s:0:\"\";s:6:\"status\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:7:\"recycle\";i:0;}}user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:44:\"http://localhost:81/_mebook/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}member|a:13:{s:6:\"userId\";s:2:\"71\";s:4:\"name\";s:6:\"satari\";s:8:\"fullname\";s:13:\"satari maseng\";s:5:\"image\";s:85:\"http://localhost:81/_mebook/uploads/2019/02/user/98692c9e47d44f4141d6a1938e4560de.jpg\";s:5:\"email\";s:20:\"satari.dop@gmail.com\";s:9:\"sectionId\";N;s:7:\"partyId\";N;s:10:\"positionId\";N;s:6:\"degree\";N;s:4:\"type\";s:6:\"member\";s:8:\"policyId\";s:1:\"0\";s:10:\"couponCode\";s:9:\"7K6EWAH71\";s:9:\"isBackend\";b:1;}urlreffer|a:1:{s:3:\"url\";s:97:\"http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography\";}');
INSERT INTO `ci_sessions` VALUES ('f784135dcc2318e8222608978f673de6d39fbb58', '::1', '1550663940', '__ci_last_regenerate|i:1550663940;FBRLH_state|s:32:\"286eb525b83353a4ee711e7e5f63faef\";condition|a:1:{s:9:\"promotion\";a:17:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"createDate\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"updateDate\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"25\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:9:\"packageId\";s:1:\"5\";s:9:\"frmFilter\";s:114:\"createDateRange=&createStartDate=&createEndDate=&updateDateRange=&updateStartDate=&updateEndDate=&active=&keyword=\";s:15:\"createDateRange\";s:0:\"\";s:15:\"createStartDate\";s:0:\"\";s:13:\"createEndDate\";s:0:\"\";s:15:\"updateDateRange\";s:0:\"\";s:15:\"updateStartDate\";s:0:\"\";s:13:\"updateEndDate\";s:0:\"\";s:6:\"active\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:7:\"recycle\";i:0;}}user|a:8:{s:6:\"userId\";s:1:\"3\";s:4:\"name\";s:46:\"โปรแกรมเมอร์ เดฟ\";s:5:\"image\";s:44:\"http://localhost:81/_mebook/uploads/user.png\";s:5:\"email\";s:18:\"programmer@ait.com\";s:4:\"type\";s:9:\"developer\";s:8:\"policyId\";s:1:\"1\";s:9:\"isBackend\";b:1;s:6:\"policy\";O:8:\"stdClass\":17:{s:3:\"270\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:1:\"1\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"299\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"320\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"319\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"321\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"324\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"323\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"309\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"322\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"294\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:0;}s:2:\"20\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"8\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:1:\"9\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"310\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:2:\"10\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}s:3:\"195\";O:8:\"stdClass\":2:{s:1:\"1\";i:1;s:1:\"2\";i:1;}}}member|a:13:{s:6:\"userId\";s:2:\"71\";s:4:\"name\";s:6:\"satari\";s:8:\"fullname\";s:13:\"satari maseng\";s:5:\"image\";s:85:\"http://localhost:81/_mebook/uploads/2019/02/user/98692c9e47d44f4141d6a1938e4560de.jpg\";s:5:\"email\";s:20:\"satari.dop@gmail.com\";s:9:\"sectionId\";N;s:7:\"partyId\";N;s:10:\"positionId\";N;s:6:\"degree\";N;s:4:\"type\";s:6:\"member\";s:8:\"policyId\";s:1:\"0\";s:10:\"couponCode\";s:9:\"7K6EWAH71\";s:9:\"isBackend\";b:1;}urlreffer|a:1:{s:3:\"url\";s:41:\"http://localhost:81/_mebook/package/index\";}');

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `commentsId` int(11) NOT NULL AUTO_INCREMENT,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `excerpt` text,
  `detail` text NOT NULL,
  `active` tinyint(1) DEFAULT '1' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `courseId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `parentId` int(11) DEFAULT '0',
  PRIMARY KEY (`commentsId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of comments
-- ----------------------------
INSERT INTO `comments` VALUES ('1', null, 'aa', null, 'aa', '1', '0', '2019-02-20 12:45:26', '0', '2019-02-20 12:45:26', '0', '0', null, null, '9', '71', '0');
INSERT INTO `comments` VALUES ('2', null, 'aa', null, 'aaaaa', '1', '0', '2019-02-20 13:06:16', '0', '2019-02-20 13:06:16', '0', '0', null, null, '9', '71', '1');
INSERT INTO `comments` VALUES ('3', null, 'aa', null, 'fasdfasd', '1', '0', '2019-02-20 13:06:23', '0', '2019-02-20 13:06:23', '0', '0', null, null, '9', '71', '1');
INSERT INTO `comments` VALUES ('4', null, 'sdfasd', null, 'fasdfa', '1', '0', '2019-02-20 13:06:32', '0', '2019-02-20 13:06:32', '0', '0', null, null, '9', '71', '0');
INSERT INTO `comments` VALUES ('5', null, 'sdfasd', null, 'ffaff', '1', '0', '2019-02-20 13:07:47', '0', '2019-02-20 13:07:47', '0', '0', null, null, '9', '71', '4');
INSERT INTO `comments` VALUES ('6', null, 'sdfasd', null, 'asdD', '1', '0', '2019-02-20 13:08:06', '0', '2019-02-20 13:08:06', '0', '0', null, null, '9', '71', '4');

-- ----------------------------
-- Table structure for config
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `variable` varchar(50) NOT NULL DEFAULT '',
  `value` text,
  `lang` varchar(2) NOT NULL DEFAULT 'th',
  `description` varchar(250) DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`variable`,`lang`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of config
-- ----------------------------
INSERT INTO `config` VALUES ('content', 'บริษัทฯ ก่อตั้งโดยคณะผู้บริหารมืออาชีพที่มากด้วยประสบการณ์และความเชี่ยวชาญมาเป็นเวลานาน โดยมีวัตถุประสงค์เพื่อเสริมสร้างคุณภาพชีวิตที่ดีของผู้พักอาศัย และเสริมสร้างภาพลักษณ์ให้กับโครงการ และสร้างมูลค่าเพิ่มของสินทรัพย์ให้แก่เจ้าของ', 'th', null, '2018-03-15 08:23:05', 'about');
INSERT INTO `config` VALUES ('content', 'Chrn Property Management ถนนประชาอุทิศ ตำบลบางแม่นาง อำเภอบางใหญ่ จังหวัดนนทบุรี 11140\r\nโทรศัพท์: 02-225-3654 โทรสาร: 02-225-3654\r\nอีเมล์: admin@admin.com\r\n', 'th', null, '2018-03-15 09:03:35', 'contact');
INSERT INTO `config` VALUES ('content', 'ข่าวสาร บทความ และกิจกรรมของบริษัทฯ', 'th', null, '2018-03-11 14:17:02', 'news');
INSERT INTO `config` VALUES ('content', 'บริหารงานโดยบุคลากรที่มีประสบการณ์ มีความรู้ความชำนาญและนักวิชาการให้คำแนะนำวางแผนและจัดระบบ จึงทำให้การทำงานมีประสิทธิภาพ  โดยคัดสรรบุคลากรที่มีคุณภาพ  รักงานบริการมีการอบรมบุคลากรทั้งภาคทฤษฏีและภาคปฏิบัติ  มีเจ้าหน้าที่ตรวจสอบ  การปฏิบัติงานจากส่วนกลางเข้าทำการตรวจสอบเพื่อตรวจสอบการปฏิบัติงานของพนักงาน ให้ได้คุณภาพมาตรฐาน  มีหน่วยปฏิบัติการเข้า พบลูกค้าเพื่อแก้ไขปัญหาต่าง ๆ ของระบบงาน', 'th', null, '2018-03-15 08:47:18', 'portfolio');
INSERT INTO `config` VALUES ('content', 'บริหารงานโดยบุคลากรที่มีประสบการณ์ มีความรู้ความชำนาญและนักวิชาการให้คำแนะนำวางแผนและจัดระบบ จึงทำให้การทำงานมีประสิทธิภาพ  โดยคัดสรรบุคลากรที่มีคุณภาพ  รักงานบริการมีการอบรมบุคลากรทั้งภาคทฤษฏีและภาคปฏิบัติ  มีเจ้าหน้าที่ตรวจสอบ  การปฏิบัติงานจากส่วนกลางเข้าทำการตรวจสอบเพื่อตรวจสอบการปฏิบัติงานของพนักงาน ให้ได้คุณภาพมาตรฐาน  มีหน่วยปฏิบัติการเข้า พบลูกค้าเพื่อแก้ไขปัญหาต่าง ๆ ของระบบงาน', 'th', null, '2018-03-15 09:18:38', 'service');
INSERT INTO `config` VALUES ('content', 'ส่วนหนึ่งของทีมงานบริหารที่มีประสบการณ์การทำงานที่เชี่ยวชาญ', 'th', null, '2018-03-10 16:42:36', 'team');
INSERT INTO `config` VALUES ('facebook', 'mebook', 'th', null, '2019-01-09 23:31:54', 'general');
INSERT INTO `config` VALUES ('help', '&lt;h4&gt;ช่วยเหลือ&lt;/h4&gt;\r\n', 'th', null, '2019-01-09 23:31:54', 'general');
INSERT INTO `config` VALUES ('idLine', '@mebook', 'th', null, '2019-01-09 23:31:54', 'general');
INSERT INTO `config` VALUES ('mailAuthenticate', '1', 'th', null, '2019-01-04 09:20:05', 'mail');
INSERT INTO `config` VALUES ('mailDefault', 'gorradesign.info@gmail.com', 'th', null, '2019-01-04 09:20:05', 'mail');
INSERT INTO `config` VALUES ('mailMethod', '1', 'th', null, '2019-01-04 09:20:05', 'mail');
INSERT INTO `config` VALUES ('metaDescription', 'MeBook', 'th', null, '2019-01-09 23:31:54', 'general');
INSERT INTO `config` VALUES ('metaKeyword', 'MeBook', 'th', null, '2019-01-09 23:31:54', 'general');
INSERT INTO `config` VALUES ('payment', '&lt;span style=&quot;font-size:16px;&quot;&gt;กดสั่งแผนสมาชิกตามที่ต้องการและชำระเงินเข้าบัญชี 888-888-888&lt;/span&gt;\r\n&lt;table border=&quot;0&quot; cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; class=&quot;t-payment&quot; style=&quot;width:100%;&quot;&gt;\r\n	&lt;tbody&gt;\r\n		&lt;tr&gt;\r\n			&lt;td style=&quot;padding: 8px; width: 60%; background: rgb(241, 251, 253); border: 1px solid rgb(69, 176, 244); text-align: center;&quot;&gt;&lt;span style=&quot;color:#3498db;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;7,000 บาท / 1 ปี&lt;/span&gt;&lt;/span&gt;&lt;/td&gt;\r\n			&lt;td style=&quot;padding: 8px; width: 60%; background: rgb(115, 204, 248); border: 1px solid rgb(69, 176, 244); text-align: center;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;font color=&quot;#ffffff&quot;&gt;STANDARD&lt;/font&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td style=&quot;padding: 8px; width: 60%; background: rgb(241, 251, 253); border: 1px solid rgb(69, 176, 244); text-align: center;&quot;&gt;&lt;span style=&quot;color:#3498db;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;12,000 บาท / 2 ปี&lt;/span&gt;&lt;/span&gt;&lt;/td&gt;\r\n			&lt;td style=&quot;padding: 8px; width: 60%; background: rgb(69, 176, 244); border: 1px solid rgb(69, 176, 244); text-align: center;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;color:#ffffff;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;PRO&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td style=&quot;padding: 8px; width: 60%; background: rgb(241, 251, 253); border: 1px solid rgb(69, 176, 244); text-align: center;&quot;&gt;&lt;span style=&quot;color:#3498db;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;15,000 บาท / 3 ปี&lt;/span&gt;&lt;/span&gt;&lt;/td&gt;\r\n			&lt;td style=&quot;padding: 8px; width: 60%; background: rgb(42, 149, 229); border: 1px solid rgb(69, 176, 244); text-align: center;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;color:#ffffff;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;PREMIUM&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n	&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n\r\n&lt;div style=&quot;text-align: center;&quot;&gt;&lt;br /&gt;\r\n&lt;span style=&quot;color:#3498db;&quot;&gt;&lt;span style=&quot;font-size:16px;&quot;&gt;หลังจากแจ้งชำระเงิน ระบบจะอนุมัติภายใน 24 ชม. เก็บหลักฐานการชำระไว้ในกรณีไม่สามารถเข้าเรียนได้&lt;/span&gt;&lt;/span&gt;&lt;/div&gt;\r\n', 'th', null, '2019-01-09 23:31:54', 'general');
INSERT INTO `config` VALUES ('phoneContact', '(ทุกวัน 10:00 - 04:00 น.)', 'th', null, '2019-01-09 23:31:54', 'general');
INSERT INTO `config` VALUES ('phoneNumber', '0971456536', 'th', null, '2019-01-09 23:31:54', 'general');
INSERT INTO `config` VALUES ('senderEmail', 'gorradesign.info@gmail.com', 'th', null, '2019-01-04 09:20:05', 'mail');
INSERT INTO `config` VALUES ('senderName', 'MeBook', 'th', null, '2019-01-04 09:20:05', 'mail');
INSERT INTO `config` VALUES ('siteTitle', 'MeBook', 'th', null, '2019-01-09 23:31:54', 'general');
INSERT INTO `config` VALUES ('SMTPpassword', 'P@ssw0rdGorra', 'th', null, '2019-01-04 09:20:05', 'mail');
INSERT INTO `config` VALUES ('SMTPport', '465', 'th', null, '2019-01-04 09:20:05', 'mail');
INSERT INTO `config` VALUES ('SMTPserver', 'ssl://smtp.gmail.com', 'th', null, '2019-01-04 09:20:05', 'mail');
INSERT INTO `config` VALUES ('SMTPusername', 'gorradesign.info@gmail.com', 'th', null, '2019-01-04 09:20:05', 'mail');

-- ----------------------------
-- Table structure for contact
-- ----------------------------
DROP TABLE IF EXISTS `contact`;
CREATE TABLE `contact` (
  `contactId` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(250) DEFAULT '',
  `lname` varchar(250) DEFAULT '',
  `email` varchar(150) DEFAULT '',
  `phone` varchar(25) DEFAULT NULL,
  `detail` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`contactId`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of contact
-- ----------------------------
INSERT INTO `contact` VALUES ('1', 'aa', '', 'admin@ait.com', '1', 'fasdfa', '0', '0', '2018-10-07 23:08:03', '0', '2018-10-19 23:58:05', '3', '2', '2018-10-19 20:47:58', '3');
INSERT INTO `contact` VALUES ('2', 'aa', 'aa', 'satari.dop@gmail.com', 'aaa', 'aa', '0', '0', '2018-10-10 20:18:34', '0', '2018-10-19 23:58:13', '3', '2', '2018-10-19 20:47:58', '3');
INSERT INTO `contact` VALUES ('3', 'สาทาริ', 'ดดหก', 'satari.dop@gmail.com', '0857997445', 'เหก', '0', '0', '2018-10-12 21:35:56', '0', '2018-10-19 20:50:40', '3', '1', '2018-10-19 20:50:40', '3');
INSERT INTO `contact` VALUES ('4', 'สาทาริ', 'ดดหก', 'satari.dop@gmail.com', '0857997445', 'เหก', '0', '0', '2018-10-12 21:36:34', '0', '2018-12-12 14:40:47', '3', '1', '2018-12-12 14:40:47', '3');
INSERT INTO `contact` VALUES ('5', 'aa', 'aa', 'programmer@oem.com', '', 'fdasf', '0', '0', '2018-10-12 21:41:04', '0', '2018-10-19 20:51:30', '3', '1', '2018-10-19 20:51:30', '3');
INSERT INTO `contact` VALUES ('6', 'สาทาริ', 'มะเซ็ง', 'programmer@ait.com', '', 'gffds', '0', '0', '2018-10-12 21:47:07', '0', '2018-12-12 14:40:47', '3', '1', '2018-12-12 14:40:47', '3');
INSERT INTO `contact` VALUES ('7', 'aa', 'aa', 'admin1@ait.com', '', 'fsadfa', '0', '0', '2018-10-12 21:52:36', '0', '2018-10-19 23:57:56', '3', '1', '2018-10-19 23:57:56', '3');
INSERT INTO `contact` VALUES ('8', 'aa', 'aa', 'admin1@ait.com', '', 'fsadfa', '0', '0', '2018-10-12 21:53:16', '0', '2018-12-12 14:40:47', '3', '1', '2018-12-12 14:40:47', '3');
INSERT INTO `contact` VALUES ('9', 'สาทาริ', 'มะเซ็ง', 'satari.dop@gmail.com', '', 'aaaa', '0', '0', '2018-10-19 21:15:10', '0', '2018-12-12 14:40:47', '3', '1', '2018-12-12 14:40:47', '3');
INSERT INTO `contact` VALUES ('10', 'สาทาริ', 'มะเซ็ง', 'satari.dop@gmail.com', '0857997445', 'ทดสอบส่งเมล์', '0', '0', '2018-10-19 21:28:35', '0', '2018-12-12 14:40:47', '3', '1', '2018-12-12 14:40:47', '3');
INSERT INTO `contact` VALUES ('11', 'สาทาริ', 'มะเซ็ง', 'satari.dop@gmail.com', '0857997445', 'ทดสอบส่งเมล์', '0', '0', '2018-10-19 21:31:13', '0', '2018-12-12 14:40:47', '3', '1', '2018-12-12 14:40:47', '3');
INSERT INTO `contact` VALUES ('12', 'dSDD', 'fdasf', 'satari.dop@gmail.com', 'ddasf', 'fasdfasd', '0', '0', '2019-01-03 22:24:36', '0', '2019-01-03 22:24:36', '0', '0', null, null);
INSERT INTO `contact` VALUES ('13', 'dADS', 'Das', 'satari.dop@gmail.com', '1111', 'dASDsd', '0', '0', '2019-01-03 22:31:42', '0', '2019-01-03 22:31:42', '0', '0', null, null);
INSERT INTO `contact` VALUES ('14', 'satari', 'maseng', 'satari.dop@gmail.com', '0857997445', 'aaa', '0', '0', '2019-02-20 16:42:08', '0', '2019-02-20 16:42:08', '0', '0', null, null);

-- ----------------------------
-- Table structure for content
-- ----------------------------
DROP TABLE IF EXISTS `content`;
CREATE TABLE `content` (
  `contentId` int(11) NOT NULL AUTO_INCREMENT,
  `grpContent` varchar(200) DEFAULT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `detail` text,
  `period` varchar(250) DEFAULT NULL,
  `client` varchar(250) DEFAULT NULL,
  `service` varchar(250) DEFAULT NULL,
  `contact` varchar(250) DEFAULT NULL,
  `position` varchar(250) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `categoryId` int(11) DEFAULT NULL,
  `recommend` int(1) DEFAULT '0' COMMENT '0=ไม่แนะนำ,1=แนะนำ',
  `view` int(15) DEFAULT '0',
  `tagsId` varchar(255) DEFAULT '',
  `statusBuy` int(1) DEFAULT '0' COMMENT '0=ยังไม่ขาย,1=ขายแล้ว',
  `location` varchar(255) DEFAULT NULL,
  `price` int(25) DEFAULT NULL,
  `metaTitle` text,
  `metaDescription` text,
  `metaKeyword` text,
  `title_link` varchar(250) DEFAULT '',
  PRIMARY KEY (`contentId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of content
-- ----------------------------
INSERT INTO `content` VALUES ('1', 'article', null, 'Astronomy Binoculars A Great Alternative', 'MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction.', '<p>Boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower to actually sit through a self-imposed MCSE training. who has the willpower to actually sit through a self-imposed</p>\r\n\r\n<p>Boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower to actually sit through a self-imposed MCSE training. who has the willpower to actually sit through a self-imposed</p>\r\n', null, null, null, null, null, '1', '0', '2019-01-21 22:44:18', '3', '2019-01-22 01:47:18', '3', '0', null, null, '22', '1', '46', '4', '0', null, null, 'Astronomy Binoculars A Great Alternative', 'MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction.', 'Astronomy Binoculars A Great Alternative', 'Astronomy-Binoculars-A-Great-Alternative');
INSERT INTO `content` VALUES ('2', 'article', null, 'We Ensure better education  for a better world', 'We Ensure better education for a better world', 'In the history of modern astronomy, there is probably no one greater leap forward than the building and launch of the space telescope known as the Hubble.&nbsp;<img alt=\"\" src=\"http://localhost:81/_mebook/uploads/ck/data/test/83513_2018_mazda_CX-5.jpg\" style=\"width: 1920px; height: 1200px;\" />', null, null, null, null, null, '1', '0', '2019-01-22 01:56:22', '3', null, '0', '0', null, null, '22', '0', '17', '4,5', '0', null, null, 'We Ensure better education  for a better world', 'We Ensure better education for a better world', 'We Ensure better education  for a better world', 'We-Ensure-better-education--for-a-better-world');

-- ----------------------------
-- Table structure for coupon
-- ----------------------------
DROP TABLE IF EXISTS `coupon`;
CREATE TABLE `coupon` (
  `couponId` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `detail` text,
  `type` varchar(30) DEFAULT '' COMMENT '1=ราคา,2=%',
  `discount` int(15) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `excerpt` text,
  `titleLink` varchar(255) DEFAULT NULL,
  `metaTitle` text,
  `metaDescription` text,
  `metaKeyword` text,
  `couponCode` varchar(15) DEFAULT NULL,
  `isCourse` int(1) DEFAULT '0' COMMENT '0=ไม่จำกัดคอร์ส,1=จำกัดจำนวนคอร์ส',
  `isCourseNum` int(10) DEFAULT NULL COMMENT 'ระบุจำนวน',
  `isMember` int(1) DEFAULT '0' COMMENT '0=ไม่จำกัดจำนวนคน,1=จำกัดจำนวนคน',
  `isMemberNum` int(10) DEFAULT NULL COMMENT 'ระบุจำนวน',
  PRIMARY KEY (`couponId`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of coupon
-- ----------------------------
INSERT INTO `coupon` VALUES ('13', 'จำกัดคอร์สเรียน/คน 1 คอร์ส แต่ไม่จำกัดคนลงทะเบียน', null, '2', '10', '1', '2018-10-12 13:06:39', '3', '2018-12-08 14:40:49', '3', '0', '2018-10-12 13:12:06', '3', '2018-12-08', '2019-01-31', null, null, null, null, null, 'MGKC54R', '1', '1', '0', '0');
INSERT INTO `coupon` VALUES ('14', 'ไม่จำกัดคอร์สเรียน/คน  แต่จำกัดคนลงทะเบียนเพียง 2 คน', null, '2', '5', '1', '2018-10-12 13:16:02', '3', '2018-10-12 14:34:28', '3', '0', null, null, '2018-10-12', '2018-10-20', null, null, null, null, null, 'CMV63L3', '0', '0', '1', '2');
INSERT INTO `coupon` VALUES ('15', 'จำกัดคอร์สเรียน/คน 1 คอร์ส และ จำกัดคนลงทะเบียนแค่ 2 คน', null, '1', '100', '1', '2018-10-12 13:17:36', '3', '2018-10-12 15:15:53', '3', '0', null, null, '2018-10-12', '2018-10-20', null, null, null, null, null, '9GVBBWR', '1', '1', '1', '2');

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `courseId` int(11) NOT NULL AUTO_INCREMENT,
  `categoryId` int(11) DEFAULT NULL,
  `instructor` varchar(250) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `detail` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `recommend` int(1) DEFAULT '0' COMMENT '0=ไม่แนะนำ,1=แนะนำ',
  `view` int(15) DEFAULT '0',
  `price` int(25) DEFAULT NULL,
  `metaTitle` text,
  `metaDescription` text,
  `metaKeyword` text,
  `title_link` varchar(250) DEFAULT '',
  `learner` int(11) DEFAULT NULL,
  `recommendVideo` text,
  `receipts` text COMMENT 'สิ่งที่ได้รับ',
  `courseNo` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`courseId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES ('4', '28', '1', 'm', '', '', '1', '0', '2018-12-12 14:38:02', '3', '2019-01-22 21:05:21', '3', '0', null, null, '0', '51', '0', 'm', '', 'm', 'm', '0', '', '', '1');
INSERT INTO `course` VALUES ('5', '27', '1', 'Photography Masterclass: A Complete Guide to Photography', 'The Best Online Professional Photography Class: How to Take Amazing Photos for Beginners & Advanced Photographers', '<p><strong><em>This online photography course will teach you how to take amazing images and even sell them, whether you use a smartphone, mirrorless or DSLR camera.</em></strong></p>\r\n\r\n<p><em>Over 173,292 students enrolled in the previous version of this course, with over 14,000 top ratings! </em><strong><em>This new version is even better!</em></strong></p>\r\n\r\n<p>This photography course is designed to teach you the ins and outs of photography, even if you have little to no experience with it, to help create profitable images that help you stand out from the crowd and sell.</p>\r\n\r\n<p><strong>Master Photography Techniques to Create Extraordinary Images!</strong></p>\r\n\r\n<p>While there are plenty of digital photography courses that focus on specific styles or how to use gear, it&#39;s hard to find a comprehensive course like this one, which is for beginner to advanced photographers.</p>\r\n\r\n<p>This course is designed for all levels of photographers who want to improve their skills, take stellar images, and make money with their photos - especially great for any DSLR or mirrorless camera user.</p>\r\n\r\n<p><strong>What will you learn:</strong></p>\r\n\r\n<ul>\r\n <li>\r\n <p>Understand how cameras work and what gear you need</p>\r\n </li>\r\n <li>\r\n <p>Master shooting in manual mode and understanding your camera</p>\r\n </li>\r\n <li>\r\n <p>Know what equipment you should buy no matter what your budget is</p>\r\n </li>\r\n <li>\r\n <p>Follow our practical demonstrations to see how we shoot in real-world scenarios</p>\r\n </li>\r\n <li>\r\n <p>Use composition, lighting, and proper settings to take better photos</p>\r\n </li>\r\n <li>\r\n <p>Edit photos professionally to make them look even better</p>\r\n </li>\r\n <li>\r\n <p>Learn start a photography business and make money from your new skills</p>\r\n </li>\r\n</ul>\r\n\r\n<p><strong>Styles of photography you will learn:</strong></p>\r\n\r\n<ul>\r\n <li>\r\n <p>Portrait photography</p>\r\n </li>\r\n <li>\r\n <p>Long exposure photography</p>\r\n </li>\r\n <li>\r\n <p>Landscape photography</p>\r\n </li>\r\n <li>\r\n <p>Product photography</p>\r\n </li>\r\n <li>\r\n <p>Low light photography</p>\r\n </li>\r\n <li>\r\n <p>Sports and action photography</p>\r\n </li>\r\n <li>\r\n <p>Street photography</p>\r\n </li>\r\n <li>\r\n <p>Architecture photography</p>\r\n </li>\r\n <li>\r\n <p>Event and wedding photography</p>\r\n </li>\r\n <li>\r\n <p>Aerial and drone photography</p>\r\n </li>\r\n <li>\r\n <p>Wildlife photography</p>\r\n </li>\r\n <li>\r\n <p>Night photography</p>\r\n </li>\r\n <li>\r\n <p>DSLR photography</p>\r\n </li>\r\n <li>\r\n <p>Mirrorless photography</p>\r\n </li>\r\n <li>\r\n <p>Smartphone photography</p>\r\n </li>\r\n <li>\r\n <p>and more!</p>\r\n </li>\r\n</ul>\r\n\r\n<p><strong>Improve Your Photography Techniques, Acquire Clients, And Make More Money</strong></p>\r\n\r\n<p>Regardless of what your level of experience is or what type of camera you use, this in-depth course is designed to provide you with everything you need to take your photography skills to the next level.</p>\r\n\r\n<p>Whether you prefer taking photos of nature, animals, or people, a great photographer knows how to compose a shot, light it, and edit it. By honing these skills, you can sell your photos so you can turn your passion into a career. This course shows you how.</p>\r\n\r\n<p>Unlike other photography classes that are more limited in scope, this complete course teaches you how to take amazing photos and how to make money by selling them.</p>\r\n\r\n<p><strong>Contents and Overview</strong></p>\r\n\r\n<p>This course is aimed at teaching photographers what it takes to improve your techniques to earn more money.</p>\r\n\r\n<p>You&#39;ll start with the basics and tackle how a camera operates, the types of cameras and lenses available, and equipment you&#39;ll need for accomplishing your goals. You&#39;ll then dive into the different styles of photography you can focus on.</p>\r\n\r\n<p>You&#39;ll learn about your camera settings (DSLR, mirrorless, or other), including how to shoot in manual mode. You&#39;ll use stabilization tools and master how to properly compose and light a scene. You&#39;ll even learn how to take great photos with a smartphone or tablet and how to edit images.</p>\r\n\r\n<p>So you can sell your photos, you&#39;ll learn how to brand yourself, create a portfolio and website, and find freelance work or a full-time position. Gain insight into licensing, fair use, and more. And if you want to get into wedding photography, you&#39;ll learn how to start your own business.</p>\r\n\r\n<p>By the end of this master course, your confidence as a photographer will soar. You&#39;ll have a thorough understanding of your camera and gear so you can use them to their fullest potential to take unforgettable photos and start a profitable photography career.</p>\r\n\r\n<p><strong>What camera should you use?</strong></p>\r\n\r\n<p>A DSLR camera or mirrorless camera is a great option for taking this course. It doesn&#39;t have to be an expensive or fancy model. Any interchangeable lens camera like a DSLR or mirrorless is fine. You can even use a smartphone or point and shoot camera, although you may not have all the manual settings and options that we cover in this course.</p>\r\n\r\n<p><strong>Who are the Instructors?</strong></p>\r\n\r\n<p>Phil Ebiner, Sam Shimizu-Jones, and Will Carnahan - all professionals making a living from their photographic skills, have come together to create this amazing new course. </p>\r\n\r\n<p>Phil & Sam, creators of the original Photography Masterclass, took the feedback left from students to make this course even better. Will has come on board to share his passion and in-depth knowledge of photography with you.</p>\r\n\r\n<p><strong>With 3 instructors, you&#39;ll get premium support and feedback to help you become a better photographer!</strong></p>\r\n\r\n<p>Our happiness guarantee...</p>\r\n\r\n<p>We have a <strong>30-day 100% money back guarantee, </strong>so if you aren&#39;t happy with your purchase, we will refund your course - <em>no questions asked!</em></p>\r\n\r\n<p><strong>We can&#39;t wait to see you in the course!</strong></p>\r\n\r\n<p>Enroll now, and we&#39;ll help you take better photos than ever before!</p>\r\n\r\n<p>Phil, Sam, & Will</p>\r\nหลักสูตรนี้เหมาะกับ\r\n\r\n<ul>\r\n <li>Anyone who wants to take better photos</li>\r\n <li>Absolute beginners who want to become skilled photographers</li>\r\n <li>Moms, dads, kids, grandparents, and anyone else who wants to document their lives in a beautiful way</li>\r\n <li>Amateur photographers wanting to improve their skills</li>\r\n <li>Photographers looking to make money with their skills</li>\r\n</ul>\r\n', '1', '0', '2019-01-06 19:30:50', '3', '2019-01-22 21:05:29', '3', '0', null, null, '0', '176', '0', 'Photography Masterclass: A Complete Guide to Photography', 'The Best Online Professional Photography Class: How to Take Amazing Photos for Beginners & Advanced Photographers', 'Photography Masterclass: A Complete Guide to Photography', 'Photography-Masterclass-A-Complete-Guide-to-Photography', '8', '8697260', '<ul>\r\n <li>You will know how to take amazing photos that impress your family and friends</li>\r\n <li>You will know how to compose images beautifully with basic photography rules</li>\r\n <li>You will know how to light your subjects with flash and natural lighting</li>\r\n <li>You will know how to edit your photos like a professional</li>\r\n</ul>\r\n', '2');
INSERT INTO `course` VALUES ('6', '27', '1', 'Sales Training: Practical Sales Techniques 1', 'Sales Training: Practical Sales Techniques 1', 'Sales Training: Practical Sales Techniques 1', '1', '0', '2019-01-15 21:36:28', '3', '2019-01-22 21:05:41', '3', '0', null, null, '1', '4', '0', 'Sales Training: Practical Sales Techniques 1', 'Sales Training: Practical Sales Techniques 1', 'Sales Training: Practical Sales Techniques 1', 'Sales-Training-Practical-Sales-Techniques-1', '0', '', 'Sales Training: Practical Sales Techniques 1', '3');
INSERT INTO `course` VALUES ('7', '27', '1', 'Sales Training: Practical Sales Techniques 2', 'Sales Training: Practical Sales Techniques 2', 'Sales Training: Practical Sales Techniques 2', '1', '0', '2019-01-15 21:36:56', '3', '2019-01-22 21:05:49', '3', '0', null, null, '1', '2', '0', 'Sales Training: Practical Sales Techniques 2', 'Sales Training: Practical Sales Techniques 2', 'Sales Training: Practical Sales Techniques 2', 'Sales-Training-Practical-Sales-Techniques-2', '0', '', 'Sales Training: Practical Sales Techniques 2', '4');
INSERT INTO `course` VALUES ('8', '27', '1', 'Sales Training: Practical Sales Techniques 3', 'Sales Training: Practical Sales Techniques 3', 'Sales Training: Practical Sales Techniques 3', '1', '0', '2019-01-15 21:37:28', '3', '2019-01-22 21:05:57', '3', '0', null, null, '1', '8', '0', 'Sales Training: Practical Sales Techniques 3', 'Sales Training: Practical Sales Techniques 3', 'Sales Training: Practical Sales Techniques 3', 'Sales-Training-Practical-Sales-Techniques-3', '0', '', 'Sales Training: Practical Sales Techniques 3', '5');
INSERT INTO `course` VALUES ('9', '27', '1', 'Sales Training: Practical Sales Techniques 4', 'Sales Training: Practical Sales Techniques 4', 'Sales Training: Practical Sales Techniques 4', '1', '0', '2019-01-15 21:38:24', '3', '2019-01-24 21:54:59', '3', '0', null, null, '0', '108', '0', 'Sales Training: Practical Sales Techniques 4', 'Sales Training: Practical Sales Techniques 4', 'Sales Training: Practical Sales Techniques 4', 'Sales-Training-Practical-Sales-Techniques-4', '0', '8697260', 'Sales Training: Practical Sales Techniques 4', '6');

-- ----------------------------
-- Table structure for course_content
-- ----------------------------
DROP TABLE IF EXISTS `course_content`;
CREATE TABLE `course_content` (
  `course_contentId` int(11) NOT NULL AUTO_INCREMENT,
  `courseId` int(11) DEFAULT NULL,
  `parentId` smallint(6) DEFAULT NULL,
  `type` int(11) DEFAULT '1' COMMENT '1menu, 2separate',
  `active` tinyint(1) DEFAULT '0' COMMENT '0Disabled, 1Enabled',
  `title` varchar(150) DEFAULT '',
  `order` int(10) DEFAULT '0',
  `param` varchar(150) DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateBy` int(11) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `modify` int(11) DEFAULT '0',
  `view` int(11) DEFAULT '0',
  `print` int(11) DEFAULT '0',
  `import` int(11) DEFAULT '0',
  `export` int(11) DEFAULT '0',
  `descript` text,
  `recycle` int(11) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `videoLink` varchar(255) DEFAULT '',
  `videoLength` varchar(10) DEFAULT NULL,
  `fileTypeUploadType` int(1) DEFAULT '1' COMMENT '1=ผ่านเว็บ,2=url',
  `fileUrl` text,
  PRIMARY KEY (`course_contentId`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of course_content
-- ----------------------------
INSERT INTO `course_content` VALUES ('13', '4', '0', '0', '1', 'บทที่ 1', '1', null, '3', '0', '2018-12-12 14:39:06', null, '0', '0', '0', '0', '0', '', '0', null, null, '', '', '1', '');
INSERT INTO `course_content` VALUES ('14', '4', '13', '0', '1', 'ติดตั้งโปรแกรม', '1', null, '3', '0', '2018-12-12 14:39:52', null, '0', '0', '0', '0', '0', '', '0', null, null, 'a', '12', '1', '');
INSERT INTO `course_content` VALUES ('15', '5', '0', '0', '1', 'Introduction', '1', null, '3', '3', '2019-01-06 19:31:16', '2019-01-11 00:19:51', '0', '0', '0', '0', '0', '', '0', null, null, '', '', '1', '');
INSERT INTO `course_content` VALUES ('16', '5', '15', '0', '1', 'Welcome to the Course  ', '1', null, '3', '3', '2019-01-06 19:32:01', '2019-01-11 00:20:33', '0', '0', '0', '0', '0', 'Welcome to the Course &amp;nbsp;', '0', null, null, '112866269', '10', '1', '');
INSERT INTO `course_content` VALUES ('17', '5', '15', '1', '1', 'How Does a Camera Work?  ', '2', null, '3', '0', '2019-01-11 00:21:16', null, '0', '0', '0', '0', '0', 'How Does a Camera Work? &amp;nbsp;', '0', null, null, '141142088', '10', '1', '');
INSERT INTO `course_content` VALUES ('18', '5', '0', '0', '1', 'Exposure 20', '2', null, '3', '0', '2019-01-11 00:48:41', null, '0', '0', '0', '0', '0', '', '0', null, null, '', '', '1', '');
INSERT INTO `course_content` VALUES ('19', '5', '18', '1', '1', 'What is Exposure? ', '0', null, '3', '0', '2019-01-11 00:49:26', null, '0', '0', '0', '0', '0', '&lt;a data-purpose=&quot;clp-landing-page-url&quot; href=&quot;https://www.udemy.com/photography-masterclass-complete-guide-to-photography/tutorial/what-is-exposure/&quot;&gt;What is Exposure?&lt;/a&gt;&amp;nbsp;&lt;a href=&quot;javascript:void(0)&quot;&gt; &lt;/a&gt;', '0', null, null, '141142088', '10', '1', '');
INSERT INTO `course_content` VALUES ('20', '4', '0', '0', '1', 'บทที่ 2', '2', null, '3', '0', '2019-02-08 22:46:33', null, '0', '0', '0', '0', '0', '', '0', null, null, '', '', '1', '');
INSERT INTO `course_content` VALUES ('21', '4', '20', '1', '1', 'ทดสอบ 2', '1', null, '3', '0', '2019-02-08 22:47:27', null, '0', '0', '0', '0', '0', '', '0', null, null, '8697260', '12:01 ', '1', '');

-- ----------------------------
-- Table structure for course_content_member
-- ----------------------------
DROP TABLE IF EXISTS `course_content_member`;
CREATE TABLE `course_content_member` (
  `course_contentId` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `status` int(1) DEFAULT '0' COMMENT '0=ดูแล้วยังไม่จบ ,1=ดูจบแล้ว'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of course_content_member
-- ----------------------------
INSERT INTO `course_content_member` VALUES ('16', '71', '0', '2019-02-20 18:10:10', '1');
INSERT INTO `course_content_member` VALUES ('17', '71', '0', '2019-02-20 18:05:46', '0');
INSERT INTO `course_content_member` VALUES ('19', '71', '0', '2019-02-20 18:05:52', '0');

-- ----------------------------
-- Table structure for course_favorite
-- ----------------------------
DROP TABLE IF EXISTS `course_favorite`;
CREATE TABLE `course_favorite` (
  `courseId` int(11) NOT NULL,
  `userId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of course_favorite
-- ----------------------------
INSERT INTO `course_favorite` VALUES ('9', '70');
INSERT INTO `course_favorite` VALUES ('8', '70');
INSERT INTO `course_favorite` VALUES ('5', '70');
INSERT INTO `course_favorite` VALUES ('8', '23');
INSERT INTO `course_favorite` VALUES ('7', '23');
INSERT INTO `course_favorite` VALUES ('4', '23');
INSERT INTO `course_favorite` VALUES ('8', '71');

-- ----------------------------
-- Table structure for course_member
-- ----------------------------
DROP TABLE IF EXISTS `course_member`;
CREATE TABLE `course_member` (
  `course_memberId` int(11) NOT NULL AUTO_INCREMENT,
  `courseId` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `code` varchar(150) DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT '1',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) unsigned DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) unsigned DEFAULT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` varchar(255) DEFAULT NULL,
  `packageId` int(11) DEFAULT NULL,
  `endStudyDate` date DEFAULT NULL,
  PRIMARY KEY (`course_memberId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of course_member
-- ----------------------------

-- ----------------------------
-- Table structure for course_payment
-- ----------------------------
DROP TABLE IF EXISTS `course_payment`;
CREATE TABLE `course_payment` (
  `course_paymentId` int(11) NOT NULL AUTO_INCREMENT,
  `course_registerId` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `courseId` int(11) DEFAULT NULL,
  `code` varchar(15) DEFAULT '',
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `transferDdate` date DEFAULT NULL,
  `transferTime` time DEFAULT NULL,
  `amount` varchar(25) DEFAULT NULL,
  `note` text,
  `phone` varchar(25) DEFAULT NULL,
  `bankId` int(11) DEFAULT NULL,
  PRIMARY KEY (`course_paymentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of course_payment
-- ----------------------------

-- ----------------------------
-- Table structure for course_register
-- ----------------------------
DROP TABLE IF EXISTS `course_register`;
CREATE TABLE `course_register` (
  `course_registerId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `courseId` int(11) NOT NULL,
  `promotionId` int(11) DEFAULT NULL,
  `price` varchar(15) DEFAULT '',
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '0' COMMENT '0=ยังไม่ชำระเงิน,1=ชำระเงินแล้ว',
  `code` varchar(150) DEFAULT NULL,
  `couponCode` varchar(15) DEFAULT '',
  `type` varchar(30) DEFAULT '' COMMENT '1=ราคา,2=%',
  `discount` int(15) DEFAULT NULL,
  `isPaypal` int(1) DEFAULT NULL,
  `codePaypal` varchar(255) DEFAULT NULL,
  `packageId` int(11) DEFAULT NULL,
  `approveDate` datetime DEFAULT NULL,
  PRIMARY KEY (`course_registerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of course_register
-- ----------------------------

-- ----------------------------
-- Table structure for instructor
-- ----------------------------
DROP TABLE IF EXISTS `instructor`;
CREATE TABLE `instructor` (
  `instructorId` int(11) NOT NULL AUTO_INCREMENT,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `detail` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `email` varchar(250) DEFAULT '',
  `phoneNumber` varchar(250) DEFAULT '',
  `phoneContact` varchar(250) DEFAULT '',
  `idLine` varchar(250) DEFAULT '',
  `facebook` varchar(250) DEFAULT '',
  PRIMARY KEY (`instructorId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of instructor
-- ----------------------------
INSERT INTO `instructor` VALUES ('1', null, 'Phil Ebiner', null, 'Best Selling Instructor, 700,000+ Students', '1', '0', '2018-10-01 20:25:58', '3', '2019-01-11 00:59:35', '3', '0', null, null, 'mebook.info@gmail.com', '8888888', 'ทุกวัน 24 ชม.', '@mebook', 'mebook');

-- ----------------------------
-- Table structure for language
-- ----------------------------
DROP TABLE IF EXISTS `language`;
CREATE TABLE `language` (
  `languageId` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `filename` varchar(64) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `default` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=No,1=Yes',
  `order` int(3) NOT NULL DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  PRIMARY KEY (`languageId`),
  KEY `fk_language_idx` (`default`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of language
-- ----------------------------
INSERT INTO `language` VALUES ('1', 'ไทย', 'th', 'ไทย', null, '1', '1', '1', '2015-06-10 09:57:05', null, '2015-09-23 14:58:11', null);
INSERT INTO `language` VALUES ('2', 'อังกฤษ', 'en', 'English', null, '1', '0', '2', '2015-06-10 09:48:36', null, '2015-09-23 14:58:11', null);

-- ----------------------------
-- Table structure for module
-- ----------------------------
DROP TABLE IF EXISTS `module`;
CREATE TABLE `module` (
  `moduleId` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` smallint(6) DEFAULT NULL,
  `type` int(11) DEFAULT '1' COMMENT '1menu, 2separate',
  `active` tinyint(1) DEFAULT '0' COMMENT '0Disabled, 1Enabled',
  `title` varchar(150) DEFAULT NULL,
  `directory` varchar(200) DEFAULT NULL,
  `class` varchar(50) DEFAULT NULL,
  `order` int(10) DEFAULT '0',
  `method` varchar(50) DEFAULT NULL,
  `param` varchar(150) DEFAULT NULL,
  `isSidebar` int(11) DEFAULT '1' COMMENT '0:hide,1show',
  `isDev` tinyint(1) DEFAULT '0',
  `icon` varchar(50) DEFAULT 'fa fa-angle-double-right',
  `createBy` int(11) DEFAULT '0',
  `updateBy` int(11) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `modify` int(11) DEFAULT '0',
  `view` int(11) DEFAULT '0',
  `print` int(11) DEFAULT '0',
  `import` int(11) DEFAULT '0',
  `export` int(11) DEFAULT '0',
  `descript` text,
  `recycle` int(11) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`moduleId`)
) ENGINE=InnoDB AUTO_INCREMENT=339 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of module
-- ----------------------------
INSERT INTO `module` VALUES ('1', '0', '1', '1', 'แผงควบคุม', 'admin', 'dashboard', '2', '', null, '1', '0', 'fa fa-dashboard', '0', '3', '0000-00-00 00:00:00', '2018-08-05 00:03:14', '0', '1', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('2', '0', '1', '1', 'ตั้งค่า', 'admin', '', '16', '', '', '1', '0', 'fa fa-cogs', '0', '7', '0000-00-00 00:00:00', '2016-12-10 10:37:56', '0', '1', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('8', '2', '1', '1', 'ผู้ใช้งาน', 'admin', 'user', '3', '', '', '1', '0', 'fa fa-user', '0', '1', '0000-00-00 00:00:00', '2018-03-23 16:14:13', '1', '1', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('9', '2', '1', '1', 'พื้นฐาน', 'admin', 'config_general', '4', null, '', '1', '0', 'fa fa-circle-o', '0', '7', '0000-00-00 00:00:00', '2016-07-17 09:37:06', '1', '1', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('10', '2', '1', '1', 'อีเมล์', 'admin', 'config_mail', '6', '', '', '1', '0', 'fa fa-circle-o', '0', '1', '0000-00-00 00:00:00', '2018-03-16 10:22:10', '1', '1', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('20', '2', '1', '1', 'กลุ่มผู้ใช้งาน', 'admin', 'policy', '2', '', '', '1', '0', 'fa fa-users', '0', '1', '0000-00-00 00:00:00', '2018-03-23 16:14:02', '1', '1', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('30', '0', '1', '1', 'โมดูล', 'admin', 'module', '19', '', '', '1', '1', 'fa fa-cog', '0', '7', '0000-00-00 00:00:00', '2016-07-16 13:04:45', '0', '0', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('166', '0', '1', '1', 'คลังไฟล์', 'admin', 'library', '18', '', '', '0', '0', 'fa fa-briefcase', '7', '1', '2016-07-23 08:54:42', '2018-03-23 14:48:52', '0', '0', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('193', '0', '1', '0', 'รายงาน', 'admin', '', '7', '', '', '1', '0', 'fa fa-bar-chart', '7', '3', '2017-01-19 13:06:36', '2018-09-23 22:48:27', '0', '0', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('195', '0', '1', '0', 'ประวัติการใช้งาน', 'admin', 'stat_admin', '14', '', '', '1', '0', 'fa fa-calendar-o', '7', '3', '2017-01-19 13:11:57', '2018-10-13 00:20:58', '0', '0', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('267', '0', '1', '0', 'Repo', 'admin', 'repo', '21', null, null, '1', '1', 'fa fa-circle-o', '1', '3', '2018-03-16 21:32:34', '2018-07-03 04:28:33', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับrepo', '0', null, null);
INSERT INTO `module` VALUES ('270', '0', '2', '1', 'รายการเมนู', 'admin', '', '1', null, null, '1', '0', '', '1', '3', '2018-03-23 13:17:03', '2018-08-05 00:07:24', '0', '0', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('271', '0', '2', '1', 'นักพัฒนา', 'admin', '', '17', null, null, '1', '1', '', '1', '1', '2018-03-23 16:15:05', '2018-03-23 16:15:16', '0', '0', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('272', '0', '1', '0', 'สำรองข้อมูล', 'admin', 'backup', '16', null, null, '1', '0', 'fa fa-database', '1', '3', '2018-03-23 17:12:35', '2018-07-03 04:29:21', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับสำรองข้อมูล', '0', null, null);
INSERT INTO `module` VALUES ('294', '0', '2', '1', 'สำหรับผู้ดูแลระบบ', 'admin', '', '14', null, null, '1', '0', '', '3', '0', '2018-07-03 03:19:39', null, '0', '0', '0', '0', '0', '', '0', null, null);
INSERT INTO `module` VALUES ('299', '0', '1', '1', 'แบนเนอร์', 'admin', 'banner', '3', null, null, '1', '0', 'fa fa-map-signs', '3', '3', '2018-07-05 08:08:59', '2018-08-04 22:07:27', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับป้ายโฆษณาหน้าแรก', '0', null, null);
INSERT INTO `module` VALUES ('304', '193', '1', '0', 'รายงานสรุปการเข้าชม', 'admin', 'report_website', '1', null, null, '1', '0', 'fa fa-bar-chart', '3', '3', '2018-07-15 20:09:02', '2018-09-23 22:48:29', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับรายงานสรุปการเข้าชม', '0', null, null);
INSERT INTO `module` VALUES ('305', '193', '1', '0', 'รายงานประวัติการเข้าชม', 'admin', 'report_history', '2', null, null, '1', '0', 'fa fa-line-chart', '3', '3', '2018-07-16 00:24:34', '2018-09-23 22:48:31', '0', '0', '0', '0', '0', 'รายงานประวัติการเข้าชม', '0', null, null);
INSERT INTO `module` VALUES ('307', '308', '1', '1', 'หมวดหมู่บทความ', 'admin', 'category', '1', 'type/article', null, '1', '0', 'fa fa-circle-o', '3', '3', '2018-08-04 16:36:05', '2018-11-05 13:56:03', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับหมวดหมู่บทความ', '0', '2018-08-04 16:46:46', '3');
INSERT INTO `module` VALUES ('308', '0', '1', '1', 'บทความ', '', '', '9', null, null, '1', '0', 'fa fa-newspaper-o', '3', '3', '2018-08-04 21:24:12', '2018-11-05 13:56:46', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับบทความ', '0', null, null);
INSERT INTO `module` VALUES ('309', '308', '1', '1', 'บทความ', 'admin', 'article', '2', null, null, '1', '0', 'fa fa-newspaper-o', '3', '3', '2018-08-04 21:26:39', '2018-09-22 20:47:31', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับรายการบทความ', '0', null, null);
INSERT INTO `module` VALUES ('310', '2', '1', '1', 'โซเชียล', 'admin', 'social', '5', null, null, '1', '0', '', '3', '3', '2018-08-04 23:01:17', '2018-10-06 23:09:56', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับโซเชียล', '0', null, null);
INSERT INTO `module` VALUES ('311', '0', '1', '0', 'จัดการข้อมูลบ้านมือสอง', 'admin', 'house', '5', null, null, '1', '0', 'fa fa-home', '3', '3', '2018-08-04 23:26:02', '2018-09-14 17:09:13', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับบ้าน', '0', null, null);
INSERT INTO `module` VALUES ('312', '0', '1', '0', 'จัดการข้อมูลบ้านเช่า', 'admin', 'rental', '6', null, null, '1', '0', 'fa fa-home', '3', '3', '2018-08-04 23:58:11', '2018-09-14 17:09:15', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับบ้านเช่า', '0', null, null);
INSERT INTO `module` VALUES ('313', '0', '1', '0', 'จัดการข้อมูลที่ดิน', 'admin', 'land', '7', null, null, '1', '0', 'fa fa-navicon', '3', '3', '2018-08-05 00:06:24', '2018-09-14 17:09:16', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับที่ดิน', '0', null, null);
INSERT INTO `module` VALUES ('314', '0', '1', '0', 'จัดการข้อมูลบ้านใหม่', 'admin', 'new_house', '4', null, null, '1', '0', 'fa fa-home', '3', '3', '2018-08-28 19:11:07', '2018-09-14 17:09:12', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับบ้านมือหนึ่ง', '0', null, null);
INSERT INTO `module` VALUES ('316', '0', '1', '0', 'จัดการข้อมูลโปรโมชั่น', 'admin', 'promotion', '4', null, null, '1', '0', 'fa fa-map-signs', '3', '3', '2018-08-29 10:37:33', '2018-09-23 22:47:59', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับโปรโมชั่น', '0', null, null);
INSERT INTO `module` VALUES ('317', '0', '1', '1', 'จัดการข้อมูลคอร์สเรียน', 'admin', '', '4', null, null, '1', '0', 'fa fa-book', '3', '3', '2018-09-25 19:48:15', '2018-10-05 02:20:28', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับข้อมูลคอร์สเรียน', '0', null, null);
INSERT INTO `module` VALUES ('318', '317', '1', '1', 'จัดการเนื้อหาคอร์สเรียน', 'admin', 'course_content', '4', null, null, '0', '0', 'fa fa-circle-o', '3', '0', '2018-09-26 23:25:25', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับ ___', '0', null, null);
INSERT INTO `module` VALUES ('319', '317', '1', '1', 'คอร์สเรียน', 'admin', 'course', '3', null, null, '1', '0', 'fa fa-circle-o', '3', '3', '2018-10-01 13:25:55', '2018-10-03 14:45:42', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับคอร์สเรียน', '0', null, null);
INSERT INTO `module` VALUES ('320', '317', '1', '1', 'ผู้สอน', 'admin', 'instructor', '1', null, null, '1', '0', 'fa fa-circle-o', '3', '3', '2018-10-01 13:27:16', '2018-10-01 20:41:16', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับผู้สอน', '0', null, null);
INSERT INTO `module` VALUES ('321', '317', '1', '1', 'รีวิว', 'admin', 'reviews', '5', null, null, '0', '0', 'fa fa-circle-o', '3', '3', '2018-10-01 13:29:27', '2018-10-10 21:24:26', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับรีวิวคอร์สเรียน', '0', null, null);
INSERT INTO `module` VALUES ('322', '0', '1', '1', 'จัดการข้อมูลสมาชิก', 'admin', 'member', '11', null, null, '1', '0', 'fa fa-user', '3', '0', '2018-10-02 22:50:04', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับสมาชิก', '0', null, null);
INSERT INTO `module` VALUES ('323', '0', '1', '1', 'กิจกรรม', 'admin', 'activity', '8', null, null, '1', '0', 'fa fa-calendar', '3', '0', '2018-10-03 10:44:58', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกิจกรรม', '0', null, null);
INSERT INTO `module` VALUES ('324', '0', '1', '1', 'รายการสั่งซื้อ', 'admin', 'order_list', '7', null, null, '1', '0', 'fa fa-list', '3', '0', '2018-10-04 20:56:36', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับรายการสั่งซื้อคอร์สเรียน', '0', null, null);
INSERT INTO `module` VALUES ('325', '0', '1', '1', 'จดหมาย ', 'admin', 'contact', '12', null, null, '1', '0', 'fa fa-hand-paper-o', '3', '3', '2018-10-07 23:14:06', '2019-01-16 22:08:47', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับจดหมาย ', '0', null, null);
INSERT INTO `module` VALUES ('326', '0', '1', '1', 'จัดการ Tags', 'admin', 'tags', '10', null, null, '1', '0', 'fa fa-tags', '3', '0', '2018-10-11 15:57:05', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับ Tags', '0', null, null);
INSERT INTO `module` VALUES ('327', '0', '1', '1', 'คูปองส่วนลด', 'admin', 'coupon', '6', null, null, '1', '0', 'fa fa-circle-o', '3', '0', '2018-10-11 22:26:51', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับคูปองส่วนลด', '0', null, null);
INSERT INTO `module` VALUES ('328', '0', '1', '1', 'จัดการข้อมูล SEO', '', '', '15', null, null, '1', '0', 'fa fa-tags', '3', '0', '2018-10-13 02:40:47', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับ ___', '0', null, null);
INSERT INTO `module` VALUES ('329', '328', '1', '1', 'SEO บทความ', 'admin', 'seo_article', '4', 'edit', null, '1', '0', 'fa fa-circle-o', '3', '0', '2018-10-13 02:41:21', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับ ___', '0', null, null);
INSERT INTO `module` VALUES ('330', '0', '1', '1', 'จัดการ Video', '', 'https://www.gorradesign.com/', '13', null, null, '1', '0', 'fa fa-circle-o', '3', '3', '2018-10-13 02:50:48', '2019-01-16 22:10:42', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับ ___', '0', null, null);
INSERT INTO `module` VALUES ('331', '328', '1', '1', 'SEO หน้าหลักเว็บไซต์', 'admin', 'seo_home', '1', 'edit', null, '1', '0', 'fa fa-circle-o', '3', '0', '2018-10-13 03:30:01', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับ ___', '0', null, null);
INSERT INTO `module` VALUES ('332', '328', '1', '1', 'SEO คอร์สเรียน', 'admin', 'seo_course', '2', 'edit', null, '1', '0', 'fa fa-circle-o', '3', '0', '2018-10-13 03:31:34', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับ ___', '0', null, null);
INSERT INTO `module` VALUES ('333', '328', '1', '1', 'SEO สอนสด', 'admin', 'seo_activity', '3', 'edit', null, '1', '0', 'fa fa-circle-o', '3', '0', '2018-10-13 03:33:21', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับ ___', '0', null, null);
INSERT INTO `module` VALUES ('334', '2', '1', '1', 'ธนาคาร', 'admin', 'bank', '1', null, null, '1', '0', 'fa fa-bank ', '3', '0', '2018-10-21 19:48:21', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับธนาคารโอนเงิน', '0', null, null);
INSERT INTO `module` VALUES ('335', '0', '1', '0', 'รายการถอนเงินสะสมสมาชิก', 'admin', 'withdraw_list', '7', null, null, '1', '0', 'fa fa-money', '3', '3', '2018-11-28 14:54:19', '2019-01-06 00:32:19', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับรายการถอนเงินสะสมสมาชิก', '0', null, null);
INSERT INTO `module` VALUES ('336', '317', '1', '1', 'หมวดหมู่คอร์สเรียน', 'admin', 'category', '2', 'type/course', null, '1', '0', 'fa fa-circle-o', '3', '0', '2018-12-11 20:38:42', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับหมวดหมู่คอร์สเรียน', '0', null, null);
INSERT INTO `module` VALUES ('337', '0', '1', '1', 'แพ็กเกจ', 'admin', 'package', '5', null, null, '1', '0', 'fa fa-map', '3', '0', '2019-01-06 14:58:34', null, '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับแพ็กเกจ', '0', null, null);
INSERT INTO `module` VALUES ('338', '0', '1', '0', 'ทดสอบ', 'admin', 'Test', '0', null, null, '1', '0', 'fa fa-circle-o', '3', '3', '2019-01-22 19:57:57', '2019-01-22 20:32:51', '0', '0', '0', '0', '0', 'การจัดการข้อมูลเกี่ยวกับ ___', '1', '2019-01-22 20:32:51', '3');

-- ----------------------------
-- Table structure for package
-- ----------------------------
DROP TABLE IF EXISTS `package`;
CREATE TABLE `package` (
  `packageId` int(11) NOT NULL AUTO_INCREMENT,
  `categoryId` int(11) DEFAULT NULL,
  `instructorId` int(11) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `detail` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `recommend` int(1) DEFAULT '0' COMMENT '0=ไม่แนะนำ,1=แนะนำ',
  `view` int(15) DEFAULT '0',
  `price` int(25) DEFAULT NULL,
  `metaTitle` text,
  `metaDescription` text,
  `metaKeyword` text,
  `title_link` varchar(250) DEFAULT '',
  `length` int(11) DEFAULT NULL,
  `recommendVideo` text,
  `receipts` text COMMENT 'สิ่งที่ได้รับ',
  `length_m` int(11) DEFAULT NULL,
  PRIMARY KEY (`packageId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of package
-- ----------------------------
INSERT INTO `package` VALUES ('5', null, null, 'STANDARD', 'ทั่วไป', 'เเพ็กเกจทั่วไป ระยะเวลา 1 ปี', '1', '0', '2019-01-06 19:19:10', '3', '2019-02-17 21:15:18', '3', '0', null, null, '0', '0', '7000', 'เเพ็กเกจ 1 ปี', 'เเพ็กเกจ 1 ปี', 'เเพ็กเกจ 1 ปี', 'STANDARD', '1', null, null, '6');
INSERT INTO `package` VALUES ('6', null, null, 'PRO', 'โปร', 'แพ็กเกจโปร ระยะเวลา 2 ปี', '1', '0', '2019-01-09 22:58:31', '3', '2019-02-17 21:15:04', '3', '0', null, null, '0', '0', '12000', 'PRO', 'โปร', 'PRO', 'PRO', '2', null, null, null);

-- ----------------------------
-- Table structure for point_log
-- ----------------------------
DROP TABLE IF EXISTS `point_log`;
CREATE TABLE `point_log` (
  `point_logId` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) NOT NULL,
  `point` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `course_registerId` varchar(255) DEFAULT '',
  `courseId` int(11) DEFAULT NULL,
  `note` text NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `point_withdrawId` int(11) DEFAULT NULL,
  PRIMARY KEY (`point_logId`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of point_log
-- ----------------------------
INSERT INTO `point_log` VALUES ('1', 'add', '280', '23', '1', '1', 'ลงทะเบียนเรียน', '2018-11-28 16:20:59', '33', null);
INSERT INTO `point_log` VALUES ('2', 'add', '280', '23', '1', '1', 'ลงทะเบียนเรียน', '2018-11-28 16:21:02', '33', null);
INSERT INTO `point_log` VALUES ('3', 'add', '280', '23', '1', '1', 'ลงทะเบียนเรียน', '2018-11-28 16:21:04', '33', null);
INSERT INTO `point_log` VALUES ('4', 'add', '280', '23', '1', '1', 'ลงทะเบียนเรียน', '2018-11-28 16:21:09', '33', null);
INSERT INTO `point_log` VALUES ('5', 'add', '280', '23', '1', '1', 'ลงทะเบียนเรียน', '2018-11-28 16:21:11', '33', null);
INSERT INTO `point_log` VALUES ('6', 'add', '280', '23', '1', '1', 'ลงทะเบียนเรียน', '2018-11-28 16:21:12', '33', null);
INSERT INTO `point_log` VALUES ('7', 'add', '280', '23', '1', '1', 'ลงทะเบียนเรียน', '2018-11-28 16:21:14', '33', null);
INSERT INTO `point_log` VALUES ('8', 'add', '280', '23', '1', '1', 'ลงทะเบียนเรียน', '2018-11-28 16:21:15', '33', null);
INSERT INTO `point_log` VALUES ('9', 'add', '280', '23', '1', '1', 'ลงทะเบียนเรียน', '2018-11-28 16:21:17', '33', null);
INSERT INTO `point_log` VALUES ('10', 'add', '280', '23', '1', '1', 'ลงทะเบียนเรียน', '2018-11-28 16:21:18', '33', null);
INSERT INTO `point_log` VALUES ('11', 'add', '280', '23', '1', '1', 'ลงทะเบียนเรียน', '2018-11-28 16:21:23', '33', null);
INSERT INTO `point_log` VALUES ('12', 'add', '860', '23', '2', '2', 'ลงทะเบียนเรียน', '2018-11-28 16:39:50', '33', null);

-- ----------------------------
-- Table structure for point_withdraw
-- ----------------------------
DROP TABLE IF EXISTS `point_withdraw`;
CREATE TABLE `point_withdraw` (
  `point_withdrawId` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(150) DEFAULT '',
  `userId` int(11) DEFAULT NULL,
  `bankCode` varchar(100) DEFAULT '',
  `bankName` varchar(250) DEFAULT '',
  `bankNo` varchar(150) DEFAULT '',
  `price` varchar(15) DEFAULT '',
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '0' COMMENT '0=ยังไม่ชำระเงิน,1=ชำระเงินแล้ว',
  `filePath` varchar(255) DEFAULT NULL,
  `fileBookbank` varchar(255) DEFAULT NULL,
  `fileEvidence` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`point_withdrawId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of point_withdraw
-- ----------------------------

-- ----------------------------
-- Table structure for policy
-- ----------------------------
DROP TABLE IF EXISTS `policy`;
CREATE TABLE `policy` (
  `policyId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `remark` text,
  `active` tinyint(1) unsigned DEFAULT '0',
  `policy` text,
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) unsigned DEFAULT NULL,
  `updateDate` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updateBy` int(11) unsigned DEFAULT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `visible` int(11) DEFAULT '1',
  PRIMARY KEY (`policyId`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of policy
-- ----------------------------
INSERT INTO `policy` VALUES ('1', 'ผู้ดูแลระบบ', 'บริหารจัดการระบบ', '1', '{\"270\":{\"1\":1,\"2\":0},\"1\":{\"1\":1,\"2\":1},\"299\":{\"1\":1,\"2\":1},\"320\":{\"1\":1,\"2\":1},\"319\":{\"1\":1,\"2\":1},\"321\":{\"1\":1,\"2\":1},\"324\":{\"1\":1,\"2\":1},\"323\":{\"1\":1,\"2\":1},\"309\":{\"1\":1,\"2\":1},\"322\":{\"1\":1,\"2\":1},\"294\":{\"1\":1,\"2\":0},\"20\":{\"1\":1,\"2\":1},\"8\":{\"1\":1,\"2\":1},\"9\":{\"1\":1,\"2\":1},\"310\":{\"1\":1,\"2\":1},\"10\":{\"1\":1,\"2\":1},\"195\":{\"1\":1,\"2\":1}}', '2016-07-13 10:55:21', null, '2018-10-05 02:19:05', '22', '0', null, null, '0');
INSERT INTO `policy` VALUES ('2', 'Exclusive', 'เข้าถึงรายงานได้ทุกรายงาน', '0', '{\"269\":{\"1\":1,\"2\":1},\"1\":{\"1\":1,\"2\":1},\"267\":{\"1\":1,\"2\":0}}', '2016-07-13 10:55:24', null, '2018-08-04 23:18:55', '3', '1', null, null, '1');
INSERT INTO `policy` VALUES ('4', 'นักศึกษาฝึกงาน', '', '0', null, '2017-01-03 08:23:17', '7', '2018-04-26 09:40:12', '1', '1', null, null, '1');
INSERT INTO `policy` VALUES ('5', 'Data Manager', 'บริหารจัดการข้อมูล', '0', '{\"1\":{\"1\":1,\"2\":1},\"270\":{\"1\":1,\"2\":0},\"299\":{\"1\":1,\"2\":1},\"186\":{\"1\":1,\"2\":1},\"185\":{\"1\":1,\"2\":1},\"301\":{\"1\":1,\"2\":1}}', '2018-01-19 06:17:41', '1', '2018-08-04 23:18:55', '3', '1', null, null, '1');
INSERT INTO `policy` VALUES ('6', 'test', 'test', '0', '{\"1\":{\"1\":1,\"2\":0},\"185\":{\"1\":1,\"2\":0},\"186\":{\"1\":1,\"2\":0},\"199\":{\"1\":1,\"2\":1},\"200\":{\"1\":1,\"2\":1}}', '2018-01-19 07:01:10', '1', '2018-04-25 16:53:04', '1', '2', null, null, '1');
INSERT INTO `policy` VALUES ('7', 'asdfa', 'adfadf', '0', '{\"1\":{\"1\":1,\"2\":1}}', '2018-01-19 07:01:32', '1', '2018-03-23 11:27:56', '1', '2', null, null, '1');
INSERT INTO `policy` VALUES ('8', 'adfadf', '', '0', '{\"201\":{\"1\":1,\"2\":1}}', '2018-01-19 07:03:59', '1', '2018-03-23 11:27:56', '1', '2', null, null, '1');
INSERT INTO `policy` VALUES ('9', 'aaaa', '', '0', '{\"30\":{\"1\":1,\"2\":1}}', '2018-01-19 07:05:14', '1', '2018-01-26 16:45:28', '3', '2', null, null, '1');
INSERT INTO `policy` VALUES ('10', 'aaaa', '', '0', '{\"30\":{\"1\":1,\"2\":1}}', '2018-01-19 07:05:45', '1', '2018-01-26 16:45:28', '3', '2', null, null, '1');
INSERT INTO `policy` VALUES ('11', 'aaaa', '', '0', '{\"30\":{\"1\":1,\"2\":1}}', '2018-01-19 07:06:49', '1', '2018-01-26 16:45:28', '3', '2', null, null, '1');
INSERT INTO `policy` VALUES ('12', 'aaaa', '', '0', '{\"30\":{\"1\":1,\"2\":1}}', '2018-01-19 07:07:25', '1', '2018-01-26 16:45:28', '3', '2', null, null, '1');
INSERT INTO `policy` VALUES ('13', 'aaaa', '', '0', '{\"30\":{\"1\":1,\"2\":1}}', '2018-01-19 07:07:33', '1', '2018-04-25 16:53:04', '1', '2', null, null, '1');
INSERT INTO `policy` VALUES ('14', 'aaaa', '', '0', '{\"30\":{\"1\":1,\"2\":1}}', '2018-01-19 07:07:43', '1', '2018-04-25 16:53:04', '1', '2', null, null, '1');
INSERT INTO `policy` VALUES ('15', 'Guest', 'ผู้ใช้ทั่วไปที่เข้าถึงรายงานที่กำหนดให้อ่านได้', '0', '{\"1\":{\"1\":1,\"2\":1}}', '2018-03-26 14:39:28', '1', '2018-08-04 23:18:55', '3', '1', null, null, '1');
INSERT INTO `policy` VALUES ('16', 'ผู้อัพเดทข้อมูลเว็บไซต์', 'บริหารจัดการข้อมูลเว็บไซต์', '1', '{\"270\":{\"1\":1,\"2\":0},\"1\":{\"1\":1,\"2\":1},\"299\":{\"1\":1,\"2\":1},\"311\":{\"1\":1,\"2\":1},\"312\":{\"1\":1,\"2\":1},\"313\":{\"1\":1,\"2\":1},\"307\":{\"1\":1,\"2\":1},\"309\":{\"1\":1,\"2\":1},\"304\":{\"1\":1,\"2\":1},\"305\":{\"1\":1,\"2\":1}}', '2018-08-05 00:20:00', '22', '2018-08-05 00:20:09', '22', '0', null, null, '1');

-- ----------------------------
-- Table structure for promotion
-- ----------------------------
DROP TABLE IF EXISTS `promotion`;
CREATE TABLE `promotion` (
  `promotionId` int(11) NOT NULL AUTO_INCREMENT,
  `courseId` int(11) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `detail` text,
  `type` varchar(30) DEFAULT '' COMMENT '1=ปกติ,2=จ่ายเท่าไรก็ได้',
  `discount` int(15) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `excerpt` text,
  `titleLink` varchar(255) DEFAULT NULL,
  `metaTitle` text,
  `metaDescription` text,
  `metaKeyword` text,
  `packageId` int(11) DEFAULT NULL,
  PRIMARY KEY (`promotionId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of promotion
-- ----------------------------
INSERT INTO `promotion` VALUES ('1', '5', 'โปรปีใหม่', '', '2', '0', '0', '2019-01-06 19:27:19', '3', '2019-02-20 16:12:28', '3', '0', null, null, '2019-02-20', '2019-02-28', null, 'โปรปีใหม่', 'โปรปีใหม่', null, 'โปรปีใหม่', '5');

-- ----------------------------
-- Table structure for promotion_content
-- ----------------------------
DROP TABLE IF EXISTS `promotion_content`;
CREATE TABLE `promotion_content` (
  `promotionId` int(11) DEFAULT NULL,
  `courseId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of promotion_content
-- ----------------------------

-- ----------------------------
-- Table structure for repo
-- ----------------------------
DROP TABLE IF EXISTS `repo`;
CREATE TABLE `repo` (
  `repoId` int(11) NOT NULL AUTO_INCREMENT,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `detail` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`repoId`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of repo
-- ----------------------------
INSERT INTO `repo` VALUES ('1', 'th', 'งานประชุมประจำปี2017', null, null, '0', '0', '2018-03-10 14:25:13', '0', '2018-07-03 04:27:00', '1', '2', '2018-04-25 16:59:31', '1');
INSERT INTO `repo` VALUES ('2', 'th', 'บริหารจัดการหมู่บ้านจัดสรรและอาคารชุด', null, null, '1', '0', '2018-03-09 20:57:07', '3', '2018-03-19 09:03:36', '1', '0', null, null);
INSERT INTO `repo` VALUES ('3', 'th', 'ดูแลสาธารณูปโภคของหมู่บ้านจัดสรรและอาคารชุด', null, null, '1', '0', '2018-03-09 21:16:50', '3', '2018-03-23 21:29:19', '3', '0', null, null);
INSERT INTO `repo` VALUES ('4', 'th', 'รับจดทะเบียนจัดตั้งนิติบุคคลหมู่บ้านจัดสรรและอาคารชุด', null, null, '1', '0', '2018-03-09 21:19:06', '3', '2018-03-10 17:26:39', '1', '0', null, null);
INSERT INTO `repo` VALUES ('5', 'th', 'test', null, null, '0', '0', '2018-03-09 21:23:54', '3', '2018-03-16 09:16:00', '1', '2', '2018-03-16 09:15:47', '1');
INSERT INTO `repo` VALUES ('6', 'th', 'test', null, null, '0', '0', '2018-03-09 21:24:16', '3', '2018-03-16 09:15:57', '1', '2', '2018-03-16 09:15:47', '1');
INSERT INTO `repo` VALUES ('7', 'th', 'test', null, null, '0', '0', '2018-03-09 21:37:20', '3', '2018-03-16 09:16:03', '1', '2', '2018-03-16 09:15:47', '1');
INSERT INTO `repo` VALUES ('8', 'th', 'โครงการที่ 1', null, null, '1', '0', '2018-03-10 11:39:19', '1', '2018-03-11 09:09:58', '1', '0', null, null);
INSERT INTO `repo` VALUES ('9', 'th', 'โครงการที่ 2', null, null, '1', '0', '2018-03-10 12:03:36', '1', '2018-03-11 09:12:47', '1', '0', null, null);
INSERT INTO `repo` VALUES ('10', 'th', 'โครงการที่ 3', null, null, '1', '0', '2018-03-10 12:06:06', '1', '2018-03-11 09:06:32', '1', '0', null, null);
INSERT INTO `repo` VALUES ('11', 'th', 'จัดตั้งบริษัท', null, null, '1', '0', '2018-03-10 14:04:01', '1', '2018-03-22 09:58:00', '1', '0', null, null);
INSERT INTO `repo` VALUES ('12', 'th', 'ให้บริการด้านจัดสวน', null, null, '1', '0', '2018-03-10 15:36:02', '1', '2018-03-15 08:24:54', '1', '0', null, null);
INSERT INTO `repo` VALUES ('14', 'th', null, null, null, '1', '0', '2018-03-10 16:49:34', '1', '2018-03-10 17:06:45', '1', '0', null, null);
INSERT INTO `repo` VALUES ('15', 'th', null, null, null, '1', '0', '2018-03-10 20:27:49', '1', '2018-03-11 15:58:24', '1', '0', null, null);
INSERT INTO `repo` VALUES ('16', 'th', null, null, null, '1', '0', '2018-03-10 20:28:50', '1', '2018-03-10 20:29:34', '1', '0', null, null);
INSERT INTO `repo` VALUES ('17', 'th', 'ตกแต่งครัวแบบไทยๆ', null, null, '1', '0', '2018-03-11 15:17:28', '1', '2018-03-22 09:57:58', '1', '0', null, null);
INSERT INTO `repo` VALUES ('18', 'th', 'การจัดตั้งนิติบุคคลหมู่บ้านจัดสรร', '', '', '0', '0', '2018-03-11 15:26:16', '1', '2018-07-03 04:27:00', '1', '2', '2018-04-25 16:58:33', '1');
INSERT INTO `repo` VALUES ('19', null, 'test repo', 'test is test', 'test is test test is test', '0', '0', '2018-03-16 08:34:32', '1', '2018-03-23 11:21:35', '1', '2', '2018-03-23 10:41:36', '1');

-- ----------------------------
-- Table structure for repo_copy
-- ----------------------------
DROP TABLE IF EXISTS `repo_copy`;
CREATE TABLE `repo_copy` (
  `repoId` int(11) NOT NULL AUTO_INCREMENT,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `detail` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`repoId`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of repo_copy
-- ----------------------------
INSERT INTO `repo_copy` VALUES ('1', 'th', 'งานประชุมประจำปี2017', null, null, '0', '0', '2018-03-10 14:25:13', '0', '2018-07-03 04:27:00', '1', '2', '2018-04-25 16:59:31', '1');
INSERT INTO `repo_copy` VALUES ('2', 'th', 'บริหารจัดการหมู่บ้านจัดสรรและอาคารชุด', null, null, '1', '0', '2018-03-09 20:57:07', '3', '2018-03-19 09:03:36', '1', '0', null, null);
INSERT INTO `repo_copy` VALUES ('3', 'th', 'ดูแลสาธารณูปโภคของหมู่บ้านจัดสรรและอาคารชุด', null, null, '1', '0', '2018-03-09 21:16:50', '3', '2018-03-23 21:29:19', '3', '0', null, null);
INSERT INTO `repo_copy` VALUES ('4', 'th', 'รับจดทะเบียนจัดตั้งนิติบุคคลหมู่บ้านจัดสรรและอาคารชุด', null, null, '1', '0', '2018-03-09 21:19:06', '3', '2018-03-10 17:26:39', '1', '0', null, null);
INSERT INTO `repo_copy` VALUES ('5', 'th', 'test', null, null, '0', '0', '2018-03-09 21:23:54', '3', '2018-03-16 09:16:00', '1', '2', '2018-03-16 09:15:47', '1');
INSERT INTO `repo_copy` VALUES ('6', 'th', 'test', null, null, '0', '0', '2018-03-09 21:24:16', '3', '2018-03-16 09:15:57', '1', '2', '2018-03-16 09:15:47', '1');
INSERT INTO `repo_copy` VALUES ('7', 'th', 'test', null, null, '0', '0', '2018-03-09 21:37:20', '3', '2018-03-16 09:16:03', '1', '2', '2018-03-16 09:15:47', '1');
INSERT INTO `repo_copy` VALUES ('8', 'th', 'โครงการที่ 1', null, null, '1', '0', '2018-03-10 11:39:19', '1', '2018-03-11 09:09:58', '1', '0', null, null);
INSERT INTO `repo_copy` VALUES ('9', 'th', 'โครงการที่ 2', null, null, '1', '0', '2018-03-10 12:03:36', '1', '2018-03-11 09:12:47', '1', '0', null, null);
INSERT INTO `repo_copy` VALUES ('10', 'th', 'โครงการที่ 3', null, null, '1', '0', '2018-03-10 12:06:06', '1', '2018-03-11 09:06:32', '1', '0', null, null);
INSERT INTO `repo_copy` VALUES ('11', 'th', 'จัดตั้งบริษัท', null, null, '1', '0', '2018-03-10 14:04:01', '1', '2018-03-22 09:58:00', '1', '0', null, null);
INSERT INTO `repo_copy` VALUES ('12', 'th', 'ให้บริการด้านจัดสวน', null, null, '1', '0', '2018-03-10 15:36:02', '1', '2018-03-15 08:24:54', '1', '0', null, null);
INSERT INTO `repo_copy` VALUES ('14', 'th', null, null, null, '1', '0', '2018-03-10 16:49:34', '1', '2018-03-10 17:06:45', '1', '0', null, null);
INSERT INTO `repo_copy` VALUES ('15', 'th', null, null, null, '1', '0', '2018-03-10 20:27:49', '1', '2018-03-11 15:58:24', '1', '0', null, null);
INSERT INTO `repo_copy` VALUES ('16', 'th', null, null, null, '1', '0', '2018-03-10 20:28:50', '1', '2018-03-10 20:29:34', '1', '0', null, null);
INSERT INTO `repo_copy` VALUES ('17', 'th', 'ตกแต่งครัวแบบไทยๆ', null, null, '1', '0', '2018-03-11 15:17:28', '1', '2018-03-22 09:57:58', '1', '0', null, null);
INSERT INTO `repo_copy` VALUES ('18', 'th', 'การจัดตั้งนิติบุคคลหมู่บ้านจัดสรร', '', '', '0', '0', '2018-03-11 15:26:16', '1', '2018-07-03 04:27:00', '1', '2', '2018-04-25 16:58:33', '1');
INSERT INTO `repo_copy` VALUES ('19', null, 'test repo', 'test is test', 'test is test test is test', '0', '0', '2018-03-16 08:34:32', '1', '2018-03-23 11:21:35', '1', '2', '2018-03-23 10:41:36', '1');

-- ----------------------------
-- Table structure for reviews
-- ----------------------------
DROP TABLE IF EXISTS `reviews`;
CREATE TABLE `reviews` (
  `reviewsId` int(11) NOT NULL AUTO_INCREMENT,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `detail` text,
  `active` tinyint(1) DEFAULT '1' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  `courseId` int(11) DEFAULT NULL,
  PRIMARY KEY (`reviewsId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of reviews
-- ----------------------------

-- ----------------------------
-- Table structure for reviews_stars
-- ----------------------------
DROP TABLE IF EXISTS `reviews_stars`;
CREATE TABLE `reviews_stars` (
  `courseId` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `score` int(1) DEFAULT NULL,
  `comment` text,
  `createDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of reviews_stars
-- ----------------------------
INSERT INTO `reviews_stars` VALUES ('5', '23', '4', ' ', '2019-01-07 23:40:40', '2019-01-07 23:40:40');
INSERT INTO `reviews_stars` VALUES ('4', '23', '3', ' ', '2019-01-07 23:47:17', '2019-01-07 23:47:17');
INSERT INTO `reviews_stars` VALUES ('5', '26', '5', ' ', '2019-01-11 02:16:43', '2019-01-11 02:16:43');
INSERT INTO `reviews_stars` VALUES ('9', '71', '5', 'v', '2019-02-20 11:05:06', '2019-02-20 11:05:06');

-- ----------------------------
-- Table structure for seo
-- ----------------------------
DROP TABLE IF EXISTS `seo`;
CREATE TABLE `seo` (
  `seoId` int(11) NOT NULL AUTO_INCREMENT,
  `grpContent` varchar(250) DEFAULT '',
  `metaTitle` text,
  `metaDescription` text,
  `metaKeyword` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT '0',
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`seoId`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of seo
-- ----------------------------
INSERT INTO `seo` VALUES ('1', 'seo_article', 'บทความ 1', 'บทความ 1', 'บทความ 1', '0', '0', '2018-10-13 03:25:36', '3', '2018-10-13 03:25:36', '3', '0', null, null);
INSERT INTO `seo` VALUES ('2', 'seo_promotin', 'โปร', 'โปร', 'โปร', '0', '0', '2018-09-17 22:16:37', '3', '2018-09-17 22:16:37', '3', '0', null, null);
INSERT INTO `seo` VALUES ('3', 'seo_new_house', 'a', 'a', 'a', '0', '0', '2018-09-17 22:21:54', '3', '2018-09-17 22:21:54', '3', '0', null, null);
INSERT INTO `seo` VALUES ('4', 'seo_house', 'aaa', 'aaaaa', 'aaaa', '0', '0', '2018-09-17 22:22:24', '3', '2018-09-17 22:22:24', '3', '0', null, null);
INSERT INTO `seo` VALUES ('5', 'seo_rental', 'gg', 'gg', 'gg', '0', '0', '2018-09-17 22:22:53', '3', '2018-09-17 22:22:53', '3', '0', null, null);
INSERT INTO `seo` VALUES ('6', 'seo_land', 'fsd', 'fasfa', 'fsadfa', '0', '0', '2018-09-17 22:23:19', '3', '2018-09-17 22:23:19', '3', '0', null, null);
INSERT INTO `seo` VALUES ('7', 'seo_promotion', 'โปรโมชั่น', 'โปรโมชั่น', 'โปรโมชั่น', '0', '0', '2018-09-18 00:09:45', '3', '2018-09-18 00:09:45', '3', '0', null, null);
INSERT INTO `seo` VALUES ('8', 'seo_home', 'aaaa', 'aaa', 'aaa', '0', '0', '2018-10-13 03:30:50', '3', '2018-10-13 03:30:50', '3', '0', null, null);
INSERT INTO `seo` VALUES ('9', 'seo_course', 'bbb', 'bb', 'bb', '0', '0', '2018-10-13 03:32:19', '3', '2018-10-13 03:32:19', '3', '0', null, null);
INSERT INTO `seo` VALUES ('10', 'seo_activity', 'สอนสด', 'สอนสด', 'สอนสด', '0', '0', '2018-10-13 03:34:06', '3', '2018-10-13 03:34:06', '3', '0', null, null);

-- ----------------------------
-- Table structure for tags
-- ----------------------------
DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `tagsID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'รหัสแท็ก',
  `tagsName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tagsType` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ประเภทแท็ก video,sound,picture',
  `tagsActive` int(11) DEFAULT '1' COMMENT 'สถานะ 1เปิด,0ปิด',
  `tagsDateCreate` datetime DEFAULT NULL COMMENT 'วันที่สร้าง',
  `tagsDateUpdate` datetime DEFAULT NULL COMMENT 'วันที่แก้ไข',
  `tagsAuthor` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ผู้สร้าง',
  `tagsEditor` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ผู้แก้ไข',
  `tagsRecycle` int(11) DEFAULT '0' COMMENT 'สถานะการลบ 1ลบ,0ปกติ',
  `tagsRecycleDel` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ผู้ลบ',
  `tagsRecycleDate` datetime DEFAULT NULL COMMENT 'วันที่ลบ',
  `tagsParent` int(11) NOT NULL,
  `tagsLang` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ภาษา th,en',
  `tagsExcerpt` text COLLATE utf8_unicode_ci,
  `metaTitle` text CHARACTER SET utf8,
  `metaDescription` text CHARACTER SET utf8,
  `metaKeyword` text CHARACTER SET utf8,
  PRIMARY KEY (`tagsID`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tags
-- ----------------------------
INSERT INTO `tags` VALUES ('4', 'Astronomy', 'article', '1', '2019-01-22 01:47:18', null, '3', '', '0', null, null, '0', '', null, null, null, null);
INSERT INTO `tags` VALUES ('5', 'Test', 'article', '1', '2019-01-22 01:56:22', null, '3', '', '0', null, null, '0', '', null, null, null, null);

-- ----------------------------
-- Table structure for track
-- ----------------------------
DROP TABLE IF EXISTS `track`;
CREATE TABLE `track` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(30) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `controller` varchar(100) DEFAULT NULL,
  `method` varchar(100) DEFAULT NULL,
  `contentId` int(11) DEFAULT NULL,
  `memberId` int(11) DEFAULT NULL,
  `isMobile` int(11) DEFAULT NULL,
  `referrer` varchar(255) DEFAULT NULL,
  `platform` varchar(100) DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `sessionId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21315 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of track
-- ----------------------------
INSERT INTO `track` VALUES ('19117', 'front', '2018-12-12 13:55:42', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '6b59a878ca1a69a39e78ca2686c91e20aa67dae7');
INSERT INTO `track` VALUES ('19118', 'front', '2018-12-13 17:22:28', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'f2653978512f77c3fc22e92eb2128ff2bdfa6fb3');
INSERT INTO `track` VALUES ('19119', 'front', '2018-12-20 22:00:43', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '7750213c48d5fe46d2e12dbf51f2d170de8dd82c');
INSERT INTO `track` VALUES ('19120', 'front', '2018-12-20 22:07:43', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'c303d0e0b0102017ecfd6527e86ceff41d355149');
INSERT INTO `track` VALUES ('19121', 'front', '2018-12-20 22:35:58', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '43b10bc8f5db67f24278f79f852692bf796a4570');
INSERT INTO `track` VALUES ('19122', 'front', '2018-12-30 22:15:00', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '906e906a39869a181cab61d64ee7a89c968df545');
INSERT INTO `track` VALUES ('19123', 'front', '2018-12-30 22:33:39', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '200f49db173be24782ac0fc8113999e030f2a97c');
INSERT INTO `track` VALUES ('19124', 'front', '2018-12-30 22:33:44', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '200f49db173be24782ac0fc8113999e030f2a97c');
INSERT INTO `track` VALUES ('19125', 'front', '2018-12-30 22:34:24', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '200f49db173be24782ac0fc8113999e030f2a97c');
INSERT INTO `track` VALUES ('19126', 'front', '2018-12-30 22:35:04', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '200f49db173be24782ac0fc8113999e030f2a97c');
INSERT INTO `track` VALUES ('19127', 'front', '2018-12-30 22:35:26', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '200f49db173be24782ac0fc8113999e030f2a97c');
INSERT INTO `track` VALUES ('19128', 'front', '2018-12-30 22:36:11', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '200f49db173be24782ac0fc8113999e030f2a97c');
INSERT INTO `track` VALUES ('19129', 'front', '2018-12-30 22:36:21', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '200f49db173be24782ac0fc8113999e030f2a97c');
INSERT INTO `track` VALUES ('19130', 'front', '2018-12-30 22:36:28', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '200f49db173be24782ac0fc8113999e030f2a97c');
INSERT INTO `track` VALUES ('19131', 'front', '2018-12-30 22:36:43', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '200f49db173be24782ac0fc8113999e030f2a97c');
INSERT INTO `track` VALUES ('19132', 'front', '2018-12-30 22:37:04', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '200f49db173be24782ac0fc8113999e030f2a97c');
INSERT INTO `track` VALUES ('19133', 'front', '2018-12-30 22:38:23', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '200f49db173be24782ac0fc8113999e030f2a97c');
INSERT INTO `track` VALUES ('19134', 'front', '2018-12-30 22:38:27', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '200f49db173be24782ac0fc8113999e030f2a97c');
INSERT INTO `track` VALUES ('19135', 'front', '2018-12-30 22:38:38', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '200f49db173be24782ac0fc8113999e030f2a97c');
INSERT INTO `track` VALUES ('19136', 'front', '2018-12-30 22:38:39', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '200f49db173be24782ac0fc8113999e030f2a97c');
INSERT INTO `track` VALUES ('19137', 'front', '2018-12-30 22:38:57', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'ec8b89d1e310823cac088ad82b38a58c93c3fcb4');
INSERT INTO `track` VALUES ('19138', 'front', '2018-12-30 22:39:21', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'ec8b89d1e310823cac088ad82b38a58c93c3fcb4');
INSERT INTO `track` VALUES ('19139', 'front', '2018-12-30 22:39:33', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'ec8b89d1e310823cac088ad82b38a58c93c3fcb4');
INSERT INTO `track` VALUES ('19140', 'front', '2018-12-30 22:40:44', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'ec8b89d1e310823cac088ad82b38a58c93c3fcb4');
INSERT INTO `track` VALUES ('19141', 'front', '2018-12-30 22:41:07', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'ec8b89d1e310823cac088ad82b38a58c93c3fcb4');
INSERT INTO `track` VALUES ('19142', 'front', '2018-12-30 22:41:19', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'ec8b89d1e310823cac088ad82b38a58c93c3fcb4');
INSERT INTO `track` VALUES ('19143', 'front', '2018-12-30 22:41:30', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'ec8b89d1e310823cac088ad82b38a58c93c3fcb4');
INSERT INTO `track` VALUES ('19144', 'front', '2018-12-30 22:41:32', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'ec8b89d1e310823cac088ad82b38a58c93c3fcb4');
INSERT INTO `track` VALUES ('19145', 'front', '2018-12-30 22:42:34', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'ec8b89d1e310823cac088ad82b38a58c93c3fcb4');
INSERT INTO `track` VALUES ('19146', 'front', '2018-12-30 22:43:17', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'ec8b89d1e310823cac088ad82b38a58c93c3fcb4');
INSERT INTO `track` VALUES ('19147', 'front', '2018-12-30 22:43:26', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'ec8b89d1e310823cac088ad82b38a58c93c3fcb4');
INSERT INTO `track` VALUES ('19148', 'front', '2018-12-30 22:43:29', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'ec8b89d1e310823cac088ad82b38a58c93c3fcb4');
INSERT INTO `track` VALUES ('19149', 'front', '2018-12-30 22:43:45', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'ec8b89d1e310823cac088ad82b38a58c93c3fcb4');
INSERT INTO `track` VALUES ('19150', 'front', '2018-12-30 22:44:14', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '982d3cfeacac474a202457d1722caf6e853c9250');
INSERT INTO `track` VALUES ('19151', 'front', '2018-12-30 22:48:15', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '982d3cfeacac474a202457d1722caf6e853c9250');
INSERT INTO `track` VALUES ('19152', 'front', '2018-12-30 22:48:19', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '982d3cfeacac474a202457d1722caf6e853c9250');
INSERT INTO `track` VALUES ('19153', 'front', '2018-12-30 22:48:25', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '982d3cfeacac474a202457d1722caf6e853c9250');
INSERT INTO `track` VALUES ('19154', 'front', '2018-12-30 22:51:03', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'b72345d099963055aab68162da528712fc4f69af');
INSERT INTO `track` VALUES ('19155', 'front', '2018-12-30 22:51:06', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'b72345d099963055aab68162da528712fc4f69af');
INSERT INTO `track` VALUES ('19156', 'front', '2018-12-30 22:51:17', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'b72345d099963055aab68162da528712fc4f69af');
INSERT INTO `track` VALUES ('19157', 'front', '2018-12-30 22:52:33', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'b72345d099963055aab68162da528712fc4f69af');
INSERT INTO `track` VALUES ('19158', 'front', '2018-12-30 22:53:07', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'b72345d099963055aab68162da528712fc4f69af');
INSERT INTO `track` VALUES ('19159', 'front', '2018-12-30 22:59:54', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'deffbb893c0e1d6743818d3f771a667a2e4cb430');
INSERT INTO `track` VALUES ('19160', 'front', '2018-12-30 23:00:20', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'deffbb893c0e1d6743818d3f771a667a2e4cb430');
INSERT INTO `track` VALUES ('19161', 'front', '2018-12-30 23:08:39', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '7b232426637428fd127af4f9f6590959116cdb41');
INSERT INTO `track` VALUES ('19162', 'front', '2018-12-30 23:11:30', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '7b232426637428fd127af4f9f6590959116cdb41');
INSERT INTO `track` VALUES ('19163', 'front', '2018-12-30 23:12:52', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '7b232426637428fd127af4f9f6590959116cdb41');
INSERT INTO `track` VALUES ('19164', 'front', '2018-12-30 23:13:29', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '7b232426637428fd127af4f9f6590959116cdb41');
INSERT INTO `track` VALUES ('19165', 'front', '2018-12-30 23:13:34', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '7b232426637428fd127af4f9f6590959116cdb41');
INSERT INTO `track` VALUES ('19166', 'front', '2018-12-30 23:13:48', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'e7b33fbfcd9b10c4326349a32ad84cc3f224b3f2');
INSERT INTO `track` VALUES ('19167', 'front', '2018-12-30 23:14:08', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'e7b33fbfcd9b10c4326349a32ad84cc3f224b3f2');
INSERT INTO `track` VALUES ('19168', 'front', '2018-12-30 23:14:46', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'e7b33fbfcd9b10c4326349a32ad84cc3f224b3f2');
INSERT INTO `track` VALUES ('19169', 'front', '2018-12-30 23:16:04', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'e7b33fbfcd9b10c4326349a32ad84cc3f224b3f2');
INSERT INTO `track` VALUES ('19170', 'front', '2018-12-30 23:17:37', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'e7b33fbfcd9b10c4326349a32ad84cc3f224b3f2');
INSERT INTO `track` VALUES ('19171', 'front', '2018-12-30 23:17:41', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'e7b33fbfcd9b10c4326349a32ad84cc3f224b3f2');
INSERT INTO `track` VALUES ('19172', 'front', '2018-12-30 23:18:48', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'e7b33fbfcd9b10c4326349a32ad84cc3f224b3f2');
INSERT INTO `track` VALUES ('19173', 'front', '2018-12-30 23:19:00', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '8d2d8ff1c4b27cbac24dd42d42d7efd35d11246e');
INSERT INTO `track` VALUES ('19174', 'front', '2018-12-30 23:19:01', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '8d2d8ff1c4b27cbac24dd42d42d7efd35d11246e');
INSERT INTO `track` VALUES ('19175', 'front', '2018-12-30 23:19:13', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '8d2d8ff1c4b27cbac24dd42d42d7efd35d11246e');
INSERT INTO `track` VALUES ('19176', 'front', '2018-12-30 23:19:20', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '8d2d8ff1c4b27cbac24dd42d42d7efd35d11246e');
INSERT INTO `track` VALUES ('19177', 'front', '2018-12-30 23:19:58', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '8d2d8ff1c4b27cbac24dd42d42d7efd35d11246e');
INSERT INTO `track` VALUES ('19178', 'front', '2018-12-30 23:20:14', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '8d2d8ff1c4b27cbac24dd42d42d7efd35d11246e');
INSERT INTO `track` VALUES ('19179', 'front', '2018-12-30 23:20:36', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '8d2d8ff1c4b27cbac24dd42d42d7efd35d11246e');
INSERT INTO `track` VALUES ('19180', 'front', '2018-12-30 23:20:54', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '8d2d8ff1c4b27cbac24dd42d42d7efd35d11246e');
INSERT INTO `track` VALUES ('19181', 'front', '2018-12-30 23:20:56', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '8d2d8ff1c4b27cbac24dd42d42d7efd35d11246e');
INSERT INTO `track` VALUES ('19182', 'front', '2018-12-30 23:22:11', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner', 'Android', '::1', '/_mebook/', '8d2d8ff1c4b27cbac24dd42d42d7efd35d11246e');
INSERT INTO `track` VALUES ('19183', 'front', '2018-12-30 23:27:10', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '2f04afeafa681fe3c32dfe07f40c54bbaaf437af');
INSERT INTO `track` VALUES ('19184', 'front', '2018-12-30 23:31:20', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner', 'Android', '::1', '/_mebook/', '2f04afeafa681fe3c32dfe07f40c54bbaaf437af');
INSERT INTO `track` VALUES ('19185', 'front', '2018-12-30 23:32:49', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner', 'Android', '::1', '/_mebook/', '6082c117171cbb90f974171f243fb7c120be7c4f');
INSERT INTO `track` VALUES ('19186', 'front', '2018-12-30 23:33:22', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner', 'Android', '::1', '/_mebook/', '6082c117171cbb90f974171f243fb7c120be7c4f');
INSERT INTO `track` VALUES ('19187', 'front', '2018-12-30 23:33:57', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '6082c117171cbb90f974171f243fb7c120be7c4f');
INSERT INTO `track` VALUES ('19188', 'front', '2018-12-30 23:33:59', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '6082c117171cbb90f974171f243fb7c120be7c4f');
INSERT INTO `track` VALUES ('19189', 'front', '2018-12-30 23:35:11', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner', 'Android', '::1', '/_mebook/', '6082c117171cbb90f974171f243fb7c120be7c4f');
INSERT INTO `track` VALUES ('19190', 'front', '2018-12-30 23:37:39', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '6082c117171cbb90f974171f243fb7c120be7c4f');
INSERT INTO `track` VALUES ('19191', 'front', '2018-12-30 23:37:52', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '15770aad4844d664e3b41491d15994e0e35f33f6');
INSERT INTO `track` VALUES ('19192', 'front', '2018-12-30 23:38:15', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '15770aad4844d664e3b41491d15994e0e35f33f6');
INSERT INTO `track` VALUES ('19193', 'front', '2018-12-30 23:39:11', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '15770aad4844d664e3b41491d15994e0e35f33f6');
INSERT INTO `track` VALUES ('19194', 'front', '2018-12-30 23:39:49', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '15770aad4844d664e3b41491d15994e0e35f33f6');
INSERT INTO `track` VALUES ('19195', 'front', '2018-12-30 23:40:13', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '15770aad4844d664e3b41491d15994e0e35f33f6');
INSERT INTO `track` VALUES ('19196', 'front', '2018-12-30 23:40:52', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '15770aad4844d664e3b41491d15994e0e35f33f6');
INSERT INTO `track` VALUES ('19197', 'front', '2018-12-30 23:40:54', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '15770aad4844d664e3b41491d15994e0e35f33f6');
INSERT INTO `track` VALUES ('19198', 'front', '2018-12-30 23:41:02', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '15770aad4844d664e3b41491d15994e0e35f33f6');
INSERT INTO `track` VALUES ('19199', 'front', '2018-12-30 23:42:59', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '27e7cdcc451d4f1064bb056331b1547c1bd43754');
INSERT INTO `track` VALUES ('19200', 'front', '2018-12-30 23:43:16', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '27e7cdcc451d4f1064bb056331b1547c1bd43754');
INSERT INTO `track` VALUES ('19201', 'front', '2018-12-30 23:44:26', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '27e7cdcc451d4f1064bb056331b1547c1bd43754');
INSERT INTO `track` VALUES ('19202', 'front', '2018-12-30 23:44:50', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '27e7cdcc451d4f1064bb056331b1547c1bd43754');
INSERT INTO `track` VALUES ('19203', 'front', '2018-12-30 23:45:18', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner', 'Android', '::1', '/_mebook/', '27e7cdcc451d4f1064bb056331b1547c1bd43754');
INSERT INTO `track` VALUES ('19204', 'front', '2018-12-30 23:45:46', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner', 'Android', '::1', '/_mebook/', '27e7cdcc451d4f1064bb056331b1547c1bd43754');
INSERT INTO `track` VALUES ('19205', 'front', '2018-12-30 23:47:57', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '27e7cdcc451d4f1064bb056331b1547c1bd43754');
INSERT INTO `track` VALUES ('19206', 'front', '2018-12-31 00:31:26', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '0523136641c3d8c34f19cc73f1f6953381873fdd');
INSERT INTO `track` VALUES ('19207', 'front', '2018-12-31 00:32:49', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '0523136641c3d8c34f19cc73f1f6953381873fdd');
INSERT INTO `track` VALUES ('19208', 'front', '2018-12-31 00:35:04', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '0523136641c3d8c34f19cc73f1f6953381873fdd');
INSERT INTO `track` VALUES ('19209', 'front', '2018-12-31 00:35:59', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '0523136641c3d8c34f19cc73f1f6953381873fdd');
INSERT INTO `track` VALUES ('19210', 'front', '2018-12-31 00:36:36', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '897efa931c0a4b6bafb93ade57c6090e75ddd28e');
INSERT INTO `track` VALUES ('19211', 'front', '2018-12-31 00:36:54', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '897efa931c0a4b6bafb93ade57c6090e75ddd28e');
INSERT INTO `track` VALUES ('19212', 'front', '2018-12-31 00:37:15', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '897efa931c0a4b6bafb93ade57c6090e75ddd28e');
INSERT INTO `track` VALUES ('19213', 'front', '2018-12-31 00:37:47', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner', 'Android', '::1', '/_mebook/', '897efa931c0a4b6bafb93ade57c6090e75ddd28e');
INSERT INTO `track` VALUES ('19214', 'front', '2018-12-31 00:37:58', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '897efa931c0a4b6bafb93ade57c6090e75ddd28e');
INSERT INTO `track` VALUES ('19215', 'front', '2018-12-31 00:39:07', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '897efa931c0a4b6bafb93ade57c6090e75ddd28e');
INSERT INTO `track` VALUES ('19216', 'front', '2018-12-31 00:39:07', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '897efa931c0a4b6bafb93ade57c6090e75ddd28e');
INSERT INTO `track` VALUES ('19217', 'front', '2018-12-31 00:39:08', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '897efa931c0a4b6bafb93ade57c6090e75ddd28e');
INSERT INTO `track` VALUES ('19218', 'front', '2018-12-31 00:39:08', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '897efa931c0a4b6bafb93ade57c6090e75ddd28e');
INSERT INTO `track` VALUES ('19219', 'front', '2018-12-31 00:39:08', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '897efa931c0a4b6bafb93ade57c6090e75ddd28e');
INSERT INTO `track` VALUES ('19220', 'front', '2018-12-31 00:39:09', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '897efa931c0a4b6bafb93ade57c6090e75ddd28e');
INSERT INTO `track` VALUES ('19221', 'front', '2018-12-31 00:40:15', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '897efa931c0a4b6bafb93ade57c6090e75ddd28e');
INSERT INTO `track` VALUES ('19222', 'front', '2018-12-31 00:41:26', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '897efa931c0a4b6bafb93ade57c6090e75ddd28e');
INSERT INTO `track` VALUES ('19223', 'front', '2018-12-31 00:45:22', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '099fd31c76e69c8fa992be5c90ea1c886a5b7ac8');
INSERT INTO `track` VALUES ('19224', 'front', '2018-12-31 00:45:23', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '099fd31c76e69c8fa992be5c90ea1c886a5b7ac8');
INSERT INTO `track` VALUES ('19225', 'front', '2018-12-31 00:45:23', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '099fd31c76e69c8fa992be5c90ea1c886a5b7ac8');
INSERT INTO `track` VALUES ('19226', 'front', '2018-12-31 00:45:23', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '099fd31c76e69c8fa992be5c90ea1c886a5b7ac8');
INSERT INTO `track` VALUES ('19227', 'front', '2018-12-31 00:45:23', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '099fd31c76e69c8fa992be5c90ea1c886a5b7ac8');
INSERT INTO `track` VALUES ('19228', 'front', '2018-12-31 00:45:24', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '099fd31c76e69c8fa992be5c90ea1c886a5b7ac8');
INSERT INTO `track` VALUES ('19229', 'front', '2018-12-31 00:45:24', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '099fd31c76e69c8fa992be5c90ea1c886a5b7ac8');
INSERT INTO `track` VALUES ('19230', 'front', '2018-12-31 00:45:24', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '099fd31c76e69c8fa992be5c90ea1c886a5b7ac8');
INSERT INTO `track` VALUES ('19231', 'front', '2018-12-31 00:45:24', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '099fd31c76e69c8fa992be5c90ea1c886a5b7ac8');
INSERT INTO `track` VALUES ('19232', 'front', '2018-12-31 00:45:24', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '099fd31c76e69c8fa992be5c90ea1c886a5b7ac8');
INSERT INTO `track` VALUES ('19233', 'front', '2018-12-31 00:45:24', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '099fd31c76e69c8fa992be5c90ea1c886a5b7ac8');
INSERT INTO `track` VALUES ('19234', 'front', '2018-12-31 00:45:24', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '099fd31c76e69c8fa992be5c90ea1c886a5b7ac8');
INSERT INTO `track` VALUES ('19235', 'front', '2018-12-31 00:45:25', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '099fd31c76e69c8fa992be5c90ea1c886a5b7ac8');
INSERT INTO `track` VALUES ('19236', 'front', '2018-12-31 00:45:25', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '099fd31c76e69c8fa992be5c90ea1c886a5b7ac8');
INSERT INTO `track` VALUES ('19237', 'front', '2018-12-31 00:46:53', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '099fd31c76e69c8fa992be5c90ea1c886a5b7ac8');
INSERT INTO `track` VALUES ('19238', 'front', '2018-12-31 00:47:06', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '099fd31c76e69c8fa992be5c90ea1c886a5b7ac8');
INSERT INTO `track` VALUES ('19239', 'front', '2018-12-31 00:47:25', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '099fd31c76e69c8fa992be5c90ea1c886a5b7ac8');
INSERT INTO `track` VALUES ('19240', 'front', '2018-12-31 00:48:57', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', '4b92a10b03abb182cdaf14271f1857d9636147a8');
INSERT INTO `track` VALUES ('19241', 'front', '2018-12-31 22:28:04', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '1116a0fa16c29757df72e34484f50a235dce122e');
INSERT INTO `track` VALUES ('19242', 'front', '2018-12-31 22:33:57', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '5faf281a2f6388ddd00cd5aa0e91e6a389235c32');
INSERT INTO `track` VALUES ('19243', 'front', '2018-12-31 22:34:49', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '5faf281a2f6388ddd00cd5aa0e91e6a389235c32');
INSERT INTO `track` VALUES ('19244', 'front', '2018-12-31 22:39:00', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'd1439b7e2e0ca5d0d5f91e1c9a81a865ccf6a15f');
INSERT INTO `track` VALUES ('19245', 'front', '2018-12-31 22:41:27', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'd1439b7e2e0ca5d0d5f91e1c9a81a865ccf6a15f');
INSERT INTO `track` VALUES ('19246', 'front', '2018-12-31 22:42:27', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'd1439b7e2e0ca5d0d5f91e1c9a81a865ccf6a15f');
INSERT INTO `track` VALUES ('19247', 'front', '2018-12-31 22:44:10', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '02352a447eba24fc3004869777e9161ce963ad53');
INSERT INTO `track` VALUES ('19248', 'front', '2018-12-31 22:45:39', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '02352a447eba24fc3004869777e9161ce963ad53');
INSERT INTO `track` VALUES ('19249', 'front', '2018-12-31 22:45:40', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '02352a447eba24fc3004869777e9161ce963ad53');
INSERT INTO `track` VALUES ('19250', 'front', '2018-12-31 22:45:40', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '02352a447eba24fc3004869777e9161ce963ad53');
INSERT INTO `track` VALUES ('19251', 'front', '2018-12-31 22:45:40', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '02352a447eba24fc3004869777e9161ce963ad53');
INSERT INTO `track` VALUES ('19252', 'front', '2018-12-31 22:45:40', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '02352a447eba24fc3004869777e9161ce963ad53');
INSERT INTO `track` VALUES ('19253', 'front', '2018-12-31 22:45:41', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '02352a447eba24fc3004869777e9161ce963ad53');
INSERT INTO `track` VALUES ('19254', 'front', '2018-12-31 22:46:33', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '02352a447eba24fc3004869777e9161ce963ad53');
INSERT INTO `track` VALUES ('19255', 'front', '2019-01-02 09:19:05', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'b3d80ce865e49121f9c3d46fdf1118315725a88a');
INSERT INTO `track` VALUES ('19256', 'front', '2019-01-02 14:38:02', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '11a55b6c2515fd58f93c2b05d15ab3e34992420d');
INSERT INTO `track` VALUES ('19257', 'front', '2019-01-02 14:38:06', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/about-us.html', 'Windows 10', '::1', '/_mebook/', '11a55b6c2515fd58f93c2b05d15ab3e34992420d');
INSERT INTO `track` VALUES ('19258', 'front', '2019-01-02 14:38:11', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/about-us.html', 'Windows 10', '::1', '/_mebook/', '11a55b6c2515fd58f93c2b05d15ab3e34992420d');
INSERT INTO `track` VALUES ('19259', 'front', '2019-01-02 14:38:14', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact.html', 'Windows 10', '::1', '/_mebook/', '11a55b6c2515fd58f93c2b05d15ab3e34992420d');
INSERT INTO `track` VALUES ('19260', 'front', '2019-01-02 14:38:18', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact.html', 'Windows 10', '::1', '/_mebook/', '11a55b6c2515fd58f93c2b05d15ab3e34992420d');
INSERT INTO `track` VALUES ('19261', 'front', '2019-01-03 21:48:26', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '99439fb8065009b2e3d1f46e3e59985176065bb8');
INSERT INTO `track` VALUES ('19262', 'front', '2019-01-03 21:49:08', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '99439fb8065009b2e3d1f46e3e59985176065bb8');
INSERT INTO `track` VALUES ('19263', 'front', '2019-01-03 21:49:20', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '99439fb8065009b2e3d1f46e3e59985176065bb8');
INSERT INTO `track` VALUES ('19264', 'front', '2019-01-03 21:53:18', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '99439fb8065009b2e3d1f46e3e59985176065bb8');
INSERT INTO `track` VALUES ('19265', 'front', '2019-01-03 21:54:42', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '99839cd81995c75743c9bba5144703f0fa65c1e9');
INSERT INTO `track` VALUES ('19266', 'front', '2019-01-03 21:58:34', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '99839cd81995c75743c9bba5144703f0fa65c1e9');
INSERT INTO `track` VALUES ('19267', 'front', '2019-01-03 21:58:38', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '99839cd81995c75743c9bba5144703f0fa65c1e9');
INSERT INTO `track` VALUES ('19268', 'front', '2019-01-03 21:58:41', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '99839cd81995c75743c9bba5144703f0fa65c1e9');
INSERT INTO `track` VALUES ('19269', 'front', '2019-01-03 22:00:11', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', 'b8a7201c09739941e2697a160f987ee84069b5ce');
INSERT INTO `track` VALUES ('19270', 'front', '2019-01-03 22:02:41', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', 'b8a7201c09739941e2697a160f987ee84069b5ce');
INSERT INTO `track` VALUES ('19271', 'front', '2019-01-03 22:02:55', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', 'b8a7201c09739941e2697a160f987ee84069b5ce');
INSERT INTO `track` VALUES ('19272', 'front', '2019-01-03 22:24:46', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', 'e9b174f1bed4ffafe8646682210ffb7d9f73f85f');
INSERT INTO `track` VALUES ('19273', 'front', '2019-01-03 22:28:40', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '00063180ef466f40967b7d2d48d143d63dd1bbb4');
INSERT INTO `track` VALUES ('19274', 'front', '2019-01-03 22:28:46', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '00063180ef466f40967b7d2d48d143d63dd1bbb4');
INSERT INTO `track` VALUES ('19275', 'front', '2019-01-03 22:31:51', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '00063180ef466f40967b7d2d48d143d63dd1bbb4');
INSERT INTO `track` VALUES ('19276', 'front', '2019-01-03 22:36:53', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '8d7e87209a5fb29aad25ecbdaa7c6f3b58fcc2aa');
INSERT INTO `track` VALUES ('19277', 'front', '2019-01-03 22:37:09', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '8d7e87209a5fb29aad25ecbdaa7c6f3b58fcc2aa');
INSERT INTO `track` VALUES ('19278', 'front', '2019-01-03 22:37:24', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '8d7e87209a5fb29aad25ecbdaa7c6f3b58fcc2aa');
INSERT INTO `track` VALUES ('19279', 'front', '2019-01-03 22:37:28', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '8d7e87209a5fb29aad25ecbdaa7c6f3b58fcc2aa');
INSERT INTO `track` VALUES ('19280', 'front', '2019-01-03 22:37:33', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '8d7e87209a5fb29aad25ecbdaa7c6f3b58fcc2aa');
INSERT INTO `track` VALUES ('19281', 'front', '2019-01-03 22:38:28', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '8d7e87209a5fb29aad25ecbdaa7c6f3b58fcc2aa');
INSERT INTO `track` VALUES ('19282', 'front', '2019-01-03 22:39:28', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'c2ddf92af360511d6ed48453c03535c1a1e0dea9');
INSERT INTO `track` VALUES ('19283', 'front', '2019-01-03 22:40:05', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'c2ddf92af360511d6ed48453c03535c1a1e0dea9');
INSERT INTO `track` VALUES ('19284', 'front', '2019-01-03 22:40:21', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'c2ddf92af360511d6ed48453c03535c1a1e0dea9');
INSERT INTO `track` VALUES ('19285', 'front', '2019-01-03 22:43:44', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'c2ddf92af360511d6ed48453c03535c1a1e0dea9');
INSERT INTO `track` VALUES ('19286', 'front', '2019-01-03 22:43:56', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'c2ddf92af360511d6ed48453c03535c1a1e0dea9');
INSERT INTO `track` VALUES ('19287', 'front', '2019-01-03 22:44:11', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'c2ddf92af360511d6ed48453c03535c1a1e0dea9');
INSERT INTO `track` VALUES ('19288', 'front', '2019-01-03 22:45:41', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '58cca35d9be31edd6695df1db2c9fe2d70d808d7');
INSERT INTO `track` VALUES ('19289', 'front', '2019-01-03 22:46:04', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '58cca35d9be31edd6695df1db2c9fe2d70d808d7');
INSERT INTO `track` VALUES ('19290', 'front', '2019-01-03 22:56:18', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '24f8836a3fd3bcb91c8afafe5c358dd29d2a8ce3');
INSERT INTO `track` VALUES ('19291', 'front', '2019-01-03 22:58:00', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '24f8836a3fd3bcb91c8afafe5c358dd29d2a8ce3');
INSERT INTO `track` VALUES ('19292', 'front', '2019-01-03 22:58:12', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '24f8836a3fd3bcb91c8afafe5c358dd29d2a8ce3');
INSERT INTO `track` VALUES ('19293', 'front', '2019-01-03 22:58:16', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '24f8836a3fd3bcb91c8afafe5c358dd29d2a8ce3');
INSERT INTO `track` VALUES ('19294', 'front', '2019-01-03 22:59:00', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '24f8836a3fd3bcb91c8afafe5c358dd29d2a8ce3');
INSERT INTO `track` VALUES ('19295', 'front', '2019-01-04 09:18:37', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '403af2e7848707fca6c00a4b84ad5cfd749eb9e1');
INSERT INTO `track` VALUES ('19296', 'front', '2019-01-04 09:18:58', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '403af2e7848707fca6c00a4b84ad5cfd749eb9e1');
INSERT INTO `track` VALUES ('19297', 'front', '2019-01-05 11:29:02', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'f9d4b7c1d12d351c9e58f0f7b8adcf9752498a01');
INSERT INTO `track` VALUES ('19298', 'front', '2019-01-05 11:37:09', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'Android', '::1', '/_mebook/', 'a5cec196da1b545bf56b015b10cee4d7b72d2d6e');
INSERT INTO `track` VALUES ('19299', 'front', '2019-01-05 11:40:21', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'Android', '::1', '/_mebook/', 'a5cec196da1b545bf56b015b10cee4d7b72d2d6e');
INSERT INTO `track` VALUES ('19300', 'front', '2019-01-05 11:46:49', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'Android', '::1', '/_mebook/', 'a54ae4e9bf8b3e5d2b2f301818a4a17c90cde28b');
INSERT INTO `track` VALUES ('19301', 'front', '2019-01-05 11:46:51', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'Android', '::1', '/_mebook/', 'a54ae4e9bf8b3e5d2b2f301818a4a17c90cde28b');
INSERT INTO `track` VALUES ('19302', 'front', '2019-01-05 11:52:45', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'iOS', '::1', '/_mebook/', '22e2ec301b47341a3b3336206dfecb976e4c45f2');
INSERT INTO `track` VALUES ('19303', 'front', '2019-01-05 11:53:27', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'iOS', '::1', '/_mebook/', '22e2ec301b47341a3b3336206dfecb976e4c45f2');
INSERT INTO `track` VALUES ('19304', 'front', '2019-01-05 11:54:30', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'Android', '::1', '/_mebook/', '22e2ec301b47341a3b3336206dfecb976e4c45f2');
INSERT INTO `track` VALUES ('19305', 'front', '2019-01-05 11:54:36', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'iOS', '::1', '/_mebook/', '22e2ec301b47341a3b3336206dfecb976e4c45f2');
INSERT INTO `track` VALUES ('19306', 'front', '2019-01-05 11:55:04', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'iOS', '::1', '/_mebook/', '22e2ec301b47341a3b3336206dfecb976e4c45f2');
INSERT INTO `track` VALUES ('19307', 'front', '2019-01-05 11:55:44', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'iOS', '::1', '/_mebook/', '22e2ec301b47341a3b3336206dfecb976e4c45f2');
INSERT INTO `track` VALUES ('19308', 'front', '2019-01-05 11:59:18', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'iOS', '::1', '/_mebook/', 'b1651aa0111d52bf88a9e09e82d02dc0e51efe7d');
INSERT INTO `track` VALUES ('19309', 'front', '2019-01-05 12:01:05', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'iOS', '::1', '/_mebook/', 'b1651aa0111d52bf88a9e09e82d02dc0e51efe7d');
INSERT INTO `track` VALUES ('19310', 'front', '2019-01-05 12:01:22', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'iOS', '::1', '/_mebook/', 'b1651aa0111d52bf88a9e09e82d02dc0e51efe7d');
INSERT INTO `track` VALUES ('19311', 'front', '2019-01-05 12:02:00', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'iOS', '::1', '/_mebook/', 'b1651aa0111d52bf88a9e09e82d02dc0e51efe7d');
INSERT INTO `track` VALUES ('19312', 'front', '2019-01-05 12:27:59', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'Android', '::1', '/_mebook/', 'a82a17becacf4344324319739678a82f53f56461');
INSERT INTO `track` VALUES ('19313', 'front', '2019-01-05 12:30:45', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'a82a17becacf4344324319739678a82f53f56461');
INSERT INTO `track` VALUES ('19314', 'front', '2019-01-05 13:04:56', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'Android', '::1', '/_mebook/', '64445d62834f065665f1b963ea2998f7b400d9a9');
INSERT INTO `track` VALUES ('19315', 'front', '2019-01-05 13:11:21', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'Android', '::1', '/_mebook/', '8b878e49e3734240e8f09175cca74eca37b4a420');
INSERT INTO `track` VALUES ('19316', 'front', '2019-01-05 13:11:23', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'Android', '::1', '/_mebook/', '8b878e49e3734240e8f09175cca74eca37b4a420');
INSERT INTO `track` VALUES ('19317', 'front', '2019-01-05 13:12:15', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'Android', '::1', '/_mebook/', '8b878e49e3734240e8f09175cca74eca37b4a420');
INSERT INTO `track` VALUES ('19318', 'front', '2019-01-05 13:12:20', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'Android', '::1', '/_mebook/', '8b878e49e3734240e8f09175cca74eca37b4a420');
INSERT INTO `track` VALUES ('19319', 'front', '2019-01-05 13:12:29', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'Android', '::1', '/_mebook/', '8b878e49e3734240e8f09175cca74eca37b4a420');
INSERT INTO `track` VALUES ('19320', 'front', '2019-01-05 13:12:51', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'Android', '::1', '/_mebook/', '8b878e49e3734240e8f09175cca74eca37b4a420');
INSERT INTO `track` VALUES ('19321', 'front', '2019-01-05 13:14:39', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'iOS', '::1', '/_mebook/', '8b878e49e3734240e8f09175cca74eca37b4a420');
INSERT INTO `track` VALUES ('19322', 'front', '2019-01-05 13:14:42', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'iOS', '::1', '/_mebook/', '8b878e49e3734240e8f09175cca74eca37b4a420');
INSERT INTO `track` VALUES ('19323', 'front', '2019-01-05 13:15:22', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'iOS', '::1', '/_mebook/', '8b878e49e3734240e8f09175cca74eca37b4a420');
INSERT INTO `track` VALUES ('19324', 'front', '2019-01-05 13:15:52', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'iOS', '::1', '/_mebook/', '8b878e49e3734240e8f09175cca74eca37b4a420');
INSERT INTO `track` VALUES ('19325', 'front', '2019-01-05 13:16:31', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'iOS', '::1', '/_mebook/', '8baa96e3cc257fd45ab659c4760e11458b36629e');
INSERT INTO `track` VALUES ('19326', 'front', '2019-01-05 13:16:35', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'iOS', '::1', '/_mebook/', '8baa96e3cc257fd45ab659c4760e11458b36629e');
INSERT INTO `track` VALUES ('19327', 'front', '2019-01-05 13:16:38', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'iOS', '::1', '/_mebook/', '8baa96e3cc257fd45ab659c4760e11458b36629e');
INSERT INTO `track` VALUES ('19328', 'front', '2019-01-05 13:16:52', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'iOS', '::1', '/_mebook/', '8baa96e3cc257fd45ab659c4760e11458b36629e');
INSERT INTO `track` VALUES ('19329', 'front', '2019-01-05 13:22:23', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'Android', '::1', '/_mebook/', '69328ab00777dee01fa8e2941bee5950a98c6193');
INSERT INTO `track` VALUES ('19330', 'front', '2019-01-05 13:22:45', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/', '69328ab00777dee01fa8e2941bee5950a98c6193');
INSERT INTO `track` VALUES ('19331', 'front', '2019-01-05 13:25:03', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'Android', '::1', '/_mebook/', '69328ab00777dee01fa8e2941bee5950a98c6193');
INSERT INTO `track` VALUES ('19332', 'front', '2019-01-05 13:49:26', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'fa0834f57b382a7463dbbfdbf19df97347659d0f');
INSERT INTO `track` VALUES ('19333', 'front', '2019-01-05 13:51:41', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'Android', '::1', '/_mebook/', 'fa0834f57b382a7463dbbfdbf19df97347659d0f');
INSERT INTO `track` VALUES ('19334', 'front', '2019-01-05 19:20:21', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/', '9cb5623bc3d6b830f37e8468f7aef829d2097355');
INSERT INTO `track` VALUES ('19335', 'front', '2019-01-05 19:21:18', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/', '9cb5623bc3d6b830f37e8468f7aef829d2097355');
INSERT INTO `track` VALUES ('19336', 'front', '2019-01-05 19:21:44', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/', '9cb5623bc3d6b830f37e8468f7aef829d2097355');
INSERT INTO `track` VALUES ('19337', 'front', '2019-01-05 19:34:36', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/', 'a1ec93a02ce7102ef9be2650fe3e5e974b91c6a9');
INSERT INTO `track` VALUES ('19338', 'front', '2019-01-05 19:35:00', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/', 'a1ec93a02ce7102ef9be2650fe3e5e974b91c6a9');
INSERT INTO `track` VALUES ('19339', 'front', '2019-01-05 19:58:00', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/', '320f5fa250cd9b4409269efcb202efefaedfa30e');
INSERT INTO `track` VALUES ('19340', 'front', '2019-01-05 19:58:08', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/', '320f5fa250cd9b4409269efcb202efefaedfa30e');
INSERT INTO `track` VALUES ('19341', 'front', '2019-01-05 19:59:08', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/', '320f5fa250cd9b4409269efcb202efefaedfa30e');
INSERT INTO `track` VALUES ('19342', 'front', '2019-01-05 19:59:11', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/', '320f5fa250cd9b4409269efcb202efefaedfa30e');
INSERT INTO `track` VALUES ('19343', 'front', '2019-01-05 19:59:22', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/', '320f5fa250cd9b4409269efcb202efefaedfa30e');
INSERT INTO `track` VALUES ('19344', 'front', '2019-01-05 19:59:25', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/', '320f5fa250cd9b4409269efcb202efefaedfa30e');
INSERT INTO `track` VALUES ('19345', 'front', '2019-01-05 20:00:01', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/', '320f5fa250cd9b4409269efcb202efefaedfa30e');
INSERT INTO `track` VALUES ('19346', 'front', '2019-01-05 20:00:24', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/', '320f5fa250cd9b4409269efcb202efefaedfa30e');
INSERT INTO `track` VALUES ('19347', 'front', '2019-01-05 20:00:55', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/', '320f5fa250cd9b4409269efcb202efefaedfa30e');
INSERT INTO `track` VALUES ('19348', 'front', '2019-01-05 20:02:44', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/', '320f5fa250cd9b4409269efcb202efefaedfa30e');
INSERT INTO `track` VALUES ('19349', 'front', '2019-01-05 20:09:28', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'c838e4575c446fc8e5f6f2df6bf592452f48e67a');
INSERT INTO `track` VALUES ('19350', 'front', '2019-01-05 20:11:57', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'c838e4575c446fc8e5f6f2df6bf592452f48e67a');
INSERT INTO `track` VALUES ('19351', 'front', '2019-01-05 20:11:59', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'c838e4575c446fc8e5f6f2df6bf592452f48e67a');
INSERT INTO `track` VALUES ('19352', 'front', '2019-01-05 20:15:38', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'e53b29b93f9562d3979ba3aa018ff1cbb3119931');
INSERT INTO `track` VALUES ('19353', 'front', '2019-01-05 20:22:06', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '4ff4e349493188a914458a378595d5f3cb4ba55c');
INSERT INTO `track` VALUES ('19354', 'front', '2019-01-05 20:23:15', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '4ff4e349493188a914458a378595d5f3cb4ba55c');
INSERT INTO `track` VALUES ('19355', 'front', '2019-01-05 20:23:17', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/home', '4ff4e349493188a914458a378595d5f3cb4ba55c');
INSERT INTO `track` VALUES ('19356', 'front', '2019-01-05 20:23:53', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/home', '4ff4e349493188a914458a378595d5f3cb4ba55c');
INSERT INTO `track` VALUES ('19357', 'front', '2019-01-05 20:24:33', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/home', '4ff4e349493188a914458a378595d5f3cb4ba55c');
INSERT INTO `track` VALUES ('19358', 'front', '2019-01-05 20:28:59', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '27df7b0f0e1a3299a3e46e573931e1f9bcffc123');
INSERT INTO `track` VALUES ('19359', 'front', '2019-01-05 20:30:19', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '27df7b0f0e1a3299a3e46e573931e1f9bcffc123');
INSERT INTO `track` VALUES ('19360', 'front', '2019-01-05 20:30:29', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '27df7b0f0e1a3299a3e46e573931e1f9bcffc123');
INSERT INTO `track` VALUES ('19361', 'front', '2019-01-05 20:31:38', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '27df7b0f0e1a3299a3e46e573931e1f9bcffc123');
INSERT INTO `track` VALUES ('19362', 'front', '2019-01-05 20:31:39', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '27df7b0f0e1a3299a3e46e573931e1f9bcffc123');
INSERT INTO `track` VALUES ('19363', 'front', '2019-01-05 20:31:42', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '27df7b0f0e1a3299a3e46e573931e1f9bcffc123');
INSERT INTO `track` VALUES ('19364', 'front', '2019-01-05 20:31:42', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '27df7b0f0e1a3299a3e46e573931e1f9bcffc123');
INSERT INTO `track` VALUES ('19365', 'front', '2019-01-05 20:32:40', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '27df7b0f0e1a3299a3e46e573931e1f9bcffc123');
INSERT INTO `track` VALUES ('19366', 'front', '2019-01-05 20:33:25', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '27df7b0f0e1a3299a3e46e573931e1f9bcffc123');
INSERT INTO `track` VALUES ('19367', 'front', '2019-01-05 20:33:54', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '27df7b0f0e1a3299a3e46e573931e1f9bcffc123');
INSERT INTO `track` VALUES ('19368', 'front', '2019-01-05 20:35:03', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', 'e953623144832575e1bda9d0d3ea34446a107826');
INSERT INTO `track` VALUES ('19369', 'front', '2019-01-05 20:35:34', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', 'e953623144832575e1bda9d0d3ea34446a107826');
INSERT INTO `track` VALUES ('19370', 'front', '2019-01-05 20:37:25', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', 'e953623144832575e1bda9d0d3ea34446a107826');
INSERT INTO `track` VALUES ('19371', 'front', '2019-01-05 20:37:29', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', 'e953623144832575e1bda9d0d3ea34446a107826');
INSERT INTO `track` VALUES ('19372', 'front', '2019-01-05 20:37:43', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', 'e953623144832575e1bda9d0d3ea34446a107826');
INSERT INTO `track` VALUES ('19373', 'front', '2019-01-05 20:38:40', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', 'e953623144832575e1bda9d0d3ea34446a107826');
INSERT INTO `track` VALUES ('19374', 'front', '2019-01-05 20:38:49', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', 'e953623144832575e1bda9d0d3ea34446a107826');
INSERT INTO `track` VALUES ('19375', 'front', '2019-01-05 20:41:55', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', 'dac36a412a878d550335936b37ec2c5a2672cff8');
INSERT INTO `track` VALUES ('19376', 'front', '2019-01-05 20:43:19', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', 'dac36a412a878d550335936b37ec2c5a2672cff8');
INSERT INTO `track` VALUES ('19377', 'front', '2019-01-05 20:43:45', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', 'dac36a412a878d550335936b37ec2c5a2672cff8');
INSERT INTO `track` VALUES ('19378', 'front', '2019-01-05 20:44:38', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', 'dac36a412a878d550335936b37ec2c5a2672cff8');
INSERT INTO `track` VALUES ('19379', 'front', '2019-01-05 20:45:57', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', 'dac36a412a878d550335936b37ec2c5a2672cff8');
INSERT INTO `track` VALUES ('19380', 'front', '2019-01-05 20:46:51', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', 'dac36a412a878d550335936b37ec2c5a2672cff8');
INSERT INTO `track` VALUES ('19381', 'front', '2019-01-05 20:47:15', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '33b5db9901483039790bb20529230ba331861fd3');
INSERT INTO `track` VALUES ('19382', 'front', '2019-01-05 20:48:33', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '33b5db9901483039790bb20529230ba331861fd3');
INSERT INTO `track` VALUES ('19383', 'front', '2019-01-05 20:48:46', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '33b5db9901483039790bb20529230ba331861fd3');
INSERT INTO `track` VALUES ('19384', 'front', '2019-01-05 20:49:22', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '33b5db9901483039790bb20529230ba331861fd3');
INSERT INTO `track` VALUES ('19385', 'front', '2019-01-05 20:51:28', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '33b5db9901483039790bb20529230ba331861fd3');
INSERT INTO `track` VALUES ('19386', 'front', '2019-01-05 20:51:52', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '33b5db9901483039790bb20529230ba331861fd3');
INSERT INTO `track` VALUES ('19387', 'front', '2019-01-05 20:57:33', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register/', 'Android', '::1', '/_mebook/', '58b3756c5cc28095ea22822cc59516750aaecb66');
INSERT INTO `track` VALUES ('19388', 'front', '2019-01-05 20:58:13', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register/', 'Windows 10', '::1', '/_mebook/', 'aa46d7503cff22ca6eb9e2060a9a6938a8346ebe');
INSERT INTO `track` VALUES ('19389', 'front', '2019-01-05 21:01:12', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register/', 'Android', '::1', '/_mebook/', 'aa46d7503cff22ca6eb9e2060a9a6938a8346ebe');
INSERT INTO `track` VALUES ('19390', 'front', '2019-01-05 21:08:00', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register/', 'Android', '::1', '/_mebook/', 'f9adcc1ecfdc400dbcd5f61a3c25c3864819bf72');
INSERT INTO `track` VALUES ('19391', 'front', '2019-01-05 21:08:13', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register/', 'Android', '::1', '/_mebook/', 'f9adcc1ecfdc400dbcd5f61a3c25c3864819bf72');
INSERT INTO `track` VALUES ('19392', 'front', '2019-01-05 21:08:53', 'home', 'index', '0', null, '1', '', 'Android', '::1', '/_mebook/', 'f9adcc1ecfdc400dbcd5f61a3c25c3864819bf72');
INSERT INTO `track` VALUES ('19393', 'front', '2019-01-05 21:09:08', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'f9adcc1ecfdc400dbcd5f61a3c25c3864819bf72');
INSERT INTO `track` VALUES ('19394', 'front', '2019-01-05 21:09:34', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'f9adcc1ecfdc400dbcd5f61a3c25c3864819bf72');
INSERT INTO `track` VALUES ('19395', 'front', '2019-01-05 21:10:30', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'f9adcc1ecfdc400dbcd5f61a3c25c3864819bf72');
INSERT INTO `track` VALUES ('19396', 'front', '2019-01-05 21:10:59', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'Android', '::1', '/_mebook/', 'f9adcc1ecfdc400dbcd5f61a3c25c3864819bf72');
INSERT INTO `track` VALUES ('19397', 'front', '2019-01-05 21:11:15', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/', 'f9adcc1ecfdc400dbcd5f61a3c25c3864819bf72');
INSERT INTO `track` VALUES ('19398', 'front', '2019-01-05 21:12:16', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/', 'f9adcc1ecfdc400dbcd5f61a3c25c3864819bf72');
INSERT INTO `track` VALUES ('19399', 'front', '2019-01-05 21:13:56', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/', '44b6ddc0473648656e05c63788d1d74446a70061');
INSERT INTO `track` VALUES ('19400', 'front', '2019-01-05 21:14:07', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/', '44b6ddc0473648656e05c63788d1d74446a70061');
INSERT INTO `track` VALUES ('19401', 'front', '2019-01-05 21:14:16', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '44b6ddc0473648656e05c63788d1d74446a70061');
INSERT INTO `track` VALUES ('19402', 'front', '2019-01-05 21:14:29', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/home', 'Android', '::1', '/_mebook/', '44b6ddc0473648656e05c63788d1d74446a70061');
INSERT INTO `track` VALUES ('19403', 'front', '2019-01-05 21:15:27', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '44b6ddc0473648656e05c63788d1d74446a70061');
INSERT INTO `track` VALUES ('19404', 'front', '2019-01-05 21:15:36', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/home', 'Android', '::1', '/_mebook/', '44b6ddc0473648656e05c63788d1d74446a70061');
INSERT INTO `track` VALUES ('19405', 'front', '2019-01-05 21:16:17', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/home', 'Android', '::1', '/_mebook/', '44b6ddc0473648656e05c63788d1d74446a70061');
INSERT INTO `track` VALUES ('19406', 'front', '2019-01-05 21:16:44', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/home', 'Android', '::1', '/_mebook/', '44b6ddc0473648656e05c63788d1d74446a70061');
INSERT INTO `track` VALUES ('19407', 'front', '2019-01-05 21:17:03', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/home', 'iOS', '::1', '/_mebook/', '44b6ddc0473648656e05c63788d1d74446a70061');
INSERT INTO `track` VALUES ('19408', 'front', '2019-01-05 21:18:55', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/home', 'iOS', '::1', '/_mebook/', '44b6ddc0473648656e05c63788d1d74446a70061');
INSERT INTO `track` VALUES ('19409', 'front', '2019-01-05 21:19:30', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/home', 'iOS', '::1', '/_mebook/', '34c29f2764f74c5681051aa8152c595ad6510132');
INSERT INTO `track` VALUES ('19410', 'front', '2019-01-05 21:20:37', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/home', 'iOS', '::1', '/_mebook/', '34c29f2764f74c5681051aa8152c595ad6510132');
INSERT INTO `track` VALUES ('19411', 'front', '2019-01-05 21:22:33', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/home', 'iOS', '::1', '/_mebook/', '34c29f2764f74c5681051aa8152c595ad6510132');
INSERT INTO `track` VALUES ('19412', 'front', '2019-01-05 21:22:40', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '34c29f2764f74c5681051aa8152c595ad6510132');
INSERT INTO `track` VALUES ('19413', 'front', '2019-01-05 21:22:43', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '34c29f2764f74c5681051aa8152c595ad6510132');
INSERT INTO `track` VALUES ('19414', 'front', '2019-01-05 21:23:27', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '34c29f2764f74c5681051aa8152c595ad6510132');
INSERT INTO `track` VALUES ('19415', 'front', '2019-01-05 21:23:29', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '34c29f2764f74c5681051aa8152c595ad6510132');
INSERT INTO `track` VALUES ('19416', 'front', '2019-01-05 21:24:26', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/home', '34c29f2764f74c5681051aa8152c595ad6510132');
INSERT INTO `track` VALUES ('19417', 'front', '2019-01-05 21:24:36', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '12a4700fe73d490ebdd222fdd95f43464d6c3848');
INSERT INTO `track` VALUES ('19418', 'front', '2019-01-05 21:25:45', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/home', '12a4700fe73d490ebdd222fdd95f43464d6c3848');
INSERT INTO `track` VALUES ('19419', 'front', '2019-01-05 21:28:23', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '12a4700fe73d490ebdd222fdd95f43464d6c3848');
INSERT INTO `track` VALUES ('19420', 'front', '2019-01-05 21:28:42', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '12a4700fe73d490ebdd222fdd95f43464d6c3848');
INSERT INTO `track` VALUES ('19421', 'front', '2019-01-05 21:28:51', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '12a4700fe73d490ebdd222fdd95f43464d6c3848');
INSERT INTO `track` VALUES ('19422', 'front', '2019-01-05 21:29:19', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '12a4700fe73d490ebdd222fdd95f43464d6c3848');
INSERT INTO `track` VALUES ('19423', 'front', '2019-01-05 21:30:15', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/home', '134040dd755c2b21ac96a3a03c88ac83d57716ca');
INSERT INTO `track` VALUES ('19424', 'front', '2019-01-05 21:30:17', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/home', '134040dd755c2b21ac96a3a03c88ac83d57716ca');
INSERT INTO `track` VALUES ('19425', 'front', '2019-01-05 21:30:25', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '134040dd755c2b21ac96a3a03c88ac83d57716ca');
INSERT INTO `track` VALUES ('19426', 'front', '2019-01-05 21:30:53', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '134040dd755c2b21ac96a3a03c88ac83d57716ca');
INSERT INTO `track` VALUES ('19427', 'front', '2019-01-05 21:31:32', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '134040dd755c2b21ac96a3a03c88ac83d57716ca');
INSERT INTO `track` VALUES ('19428', 'front', '2019-01-05 21:32:09', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '134040dd755c2b21ac96a3a03c88ac83d57716ca');
INSERT INTO `track` VALUES ('19429', 'front', '2019-01-05 21:32:10', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '134040dd755c2b21ac96a3a03c88ac83d57716ca');
INSERT INTO `track` VALUES ('19430', 'front', '2019-01-05 21:32:27', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/home', '134040dd755c2b21ac96a3a03c88ac83d57716ca');
INSERT INTO `track` VALUES ('19431', 'front', '2019-01-05 21:32:54', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', '134040dd755c2b21ac96a3a03c88ac83d57716ca');
INSERT INTO `track` VALUES ('19432', 'front', '2019-01-05 21:33:07', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/home', 'Android', '::1', '/_mebook/', '134040dd755c2b21ac96a3a03c88ac83d57716ca');
INSERT INTO `track` VALUES ('19433', 'front', '2019-01-05 21:33:25', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/home', 'iOS', '::1', '/_mebook/', '134040dd755c2b21ac96a3a03c88ac83d57716ca');
INSERT INTO `track` VALUES ('19434', 'front', '2019-01-05 21:33:52', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/home', 'iOS', '::1', '/_mebook/', '134040dd755c2b21ac96a3a03c88ac83d57716ca');
INSERT INTO `track` VALUES ('19435', 'front', '2019-01-05 21:33:53', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/home', 'iOS', '::1', '/_mebook/', '134040dd755c2b21ac96a3a03c88ac83d57716ca');
INSERT INTO `track` VALUES ('19436', 'front', '2019-01-05 21:33:56', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/', '134040dd755c2b21ac96a3a03c88ac83d57716ca');
INSERT INTO `track` VALUES ('19437', 'front', '2019-01-05 21:33:59', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/', '134040dd755c2b21ac96a3a03c88ac83d57716ca');
INSERT INTO `track` VALUES ('19438', 'front', '2019-01-05 21:56:03', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/', '1ad99a14322df8d2fbb758e9b14ee52dd1017866');
INSERT INTO `track` VALUES ('19439', 'front', '2019-01-05 21:56:07', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/home', '1ad99a14322df8d2fbb758e9b14ee52dd1017866');
INSERT INTO `track` VALUES ('19440', 'front', '2019-01-05 22:25:51', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register/', 'Android', '::1', '/_mebook/', 'fd907c1debcbcb638a9970e60ccb95e21e9840f0');
INSERT INTO `track` VALUES ('19441', 'front', '2019-01-05 22:30:05', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register/', 'Android', '::1', '/_mebook/', 'ec0a56f80f8b29dbd1427e2bc9e3b71c1be15691');
INSERT INTO `track` VALUES ('19442', 'front', '2019-01-05 22:30:22', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/', 'ec0a56f80f8b29dbd1427e2bc9e3b71c1be15691');
INSERT INTO `track` VALUES ('19443', 'front', '2019-01-05 22:31:17', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', 'ec0a56f80f8b29dbd1427e2bc9e3b71c1be15691');
INSERT INTO `track` VALUES ('19444', 'front', '2019-01-05 22:31:42', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register/', 'Android', '::1', '/_mebook/', 'ec0a56f80f8b29dbd1427e2bc9e3b71c1be15691');
INSERT INTO `track` VALUES ('19445', 'front', '2019-01-05 22:38:12', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register/', 'Windows 10', '::1', '/_mebook/', 'fa9420ad5565e0c75f555077201031bd70bc3adf');
INSERT INTO `track` VALUES ('19446', 'front', '2019-01-05 22:41:47', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register/', 'Android', '::1', '/_mebook/', 'fa9420ad5565e0c75f555077201031bd70bc3adf');
INSERT INTO `track` VALUES ('19447', 'front', '2019-01-05 22:44:00', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register/', 'Android', '::1', '/_mebook/', 'ee37542ea3be0f95d0f16b93eadb4097f2601dd6');
INSERT INTO `track` VALUES ('19448', 'front', '2019-01-05 22:45:12', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register/', 'Windows 10', '::1', '/_mebook/', 'ee37542ea3be0f95d0f16b93eadb4097f2601dd6');
INSERT INTO `track` VALUES ('19449', 'front', '2019-01-05 22:46:09', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register/', 'Windows 10', '::1', '/_mebook/', 'ee37542ea3be0f95d0f16b93eadb4097f2601dd6');
INSERT INTO `track` VALUES ('19450', 'front', '2019-01-05 22:46:41', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register/', 'Windows 10', '::1', '/_mebook/', 'ee37542ea3be0f95d0f16b93eadb4097f2601dd6');
INSERT INTO `track` VALUES ('19451', 'front', '2019-01-05 22:46:50', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register/', 'Windows 10', '::1', '/_mebook/', 'ee37542ea3be0f95d0f16b93eadb4097f2601dd6');
INSERT INTO `track` VALUES ('19452', 'front', '2019-01-05 22:47:14', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/', 'ee37542ea3be0f95d0f16b93eadb4097f2601dd6');
INSERT INTO `track` VALUES ('19453', 'front', '2019-01-05 22:48:15', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/', 'ee37542ea3be0f95d0f16b93eadb4097f2601dd6');
INSERT INTO `track` VALUES ('19454', 'front', '2019-01-05 22:48:44', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/', 'ee37542ea3be0f95d0f16b93eadb4097f2601dd6');
INSERT INTO `track` VALUES ('19455', 'front', '2019-01-05 22:50:03', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/', 'de081a0def41fef01ffb088b3dca496602de7d65');
INSERT INTO `track` VALUES ('19456', 'front', '2019-01-05 22:50:22', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/', 'de081a0def41fef01ffb088b3dca496602de7d65');
INSERT INTO `track` VALUES ('19457', 'front', '2019-01-05 22:50:50', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', 'de081a0def41fef01ffb088b3dca496602de7d65');
INSERT INTO `track` VALUES ('19458', 'front', '2019-01-05 22:51:16', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', 'de081a0def41fef01ffb088b3dca496602de7d65');
INSERT INTO `track` VALUES ('19459', 'front', '2019-01-05 22:51:28', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/home', 'Android', '::1', '/_mebook/', 'de081a0def41fef01ffb088b3dca496602de7d65');
INSERT INTO `track` VALUES ('19460', 'front', '2019-01-05 22:52:31', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/profile/', 'Android', '::1', '/_mebook/home', 'de081a0def41fef01ffb088b3dca496602de7d65');
INSERT INTO `track` VALUES ('19461', 'front', '2019-01-05 22:57:49', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/contact', 'Android', '::1', '/_mebook/', '1aa7dae272a3ee6e85703c4fab64a213e111ffd9');
INSERT INTO `track` VALUES ('19462', 'front', '2019-01-05 23:00:01', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/contact', 'Android', '::1', '/_mebook/', '1aa7dae272a3ee6e85703c4fab64a213e111ffd9');
INSERT INTO `track` VALUES ('19463', 'front', '2019-01-05 23:00:29', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/contact', 'Android', '::1', '/_mebook/', '1aa7dae272a3ee6e85703c4fab64a213e111ffd9');
INSERT INTO `track` VALUES ('19464', 'front', '2019-01-05 23:06:44', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/contact', 'Android', '::1', '/_mebook/', '340ac8a851bd8bcb8f9961016a2d9d68fb1a8cdc');
INSERT INTO `track` VALUES ('19465', 'front', '2019-01-05 23:07:18', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/contact', 'Android', '::1', '/_mebook/', '340ac8a851bd8bcb8f9961016a2d9d68fb1a8cdc');
INSERT INTO `track` VALUES ('19466', 'front', '2019-01-05 23:08:12', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/contact', 'Android', '::1', '/_mebook/', '340ac8a851bd8bcb8f9961016a2d9d68fb1a8cdc');
INSERT INTO `track` VALUES ('19467', 'front', '2019-01-05 23:12:41', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/contact', 'Android', '::1', '/_mebook/', 'a967c1a44eeca02d5712a951ad87770b62dd7c7e');
INSERT INTO `track` VALUES ('19468', 'front', '2019-01-05 23:12:43', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/contact', 'Android', '::1', '/_mebook/', 'a967c1a44eeca02d5712a951ad87770b62dd7c7e');
INSERT INTO `track` VALUES ('19469', 'front', '2019-01-05 23:13:38', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/contact', 'Android', '::1', '/_mebook/', 'a967c1a44eeca02d5712a951ad87770b62dd7c7e');
INSERT INTO `track` VALUES ('19470', 'front', '2019-01-05 23:13:44', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', 'a967c1a44eeca02d5712a951ad87770b62dd7c7e');
INSERT INTO `track` VALUES ('19471', 'front', '2019-01-05 23:15:30', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', 'a967c1a44eeca02d5712a951ad87770b62dd7c7e');
INSERT INTO `track` VALUES ('19472', 'front', '2019-01-05 23:15:32', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', 'a967c1a44eeca02d5712a951ad87770b62dd7c7e');
INSERT INTO `track` VALUES ('19473', 'front', '2019-01-05 23:18:18', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', 'ec9f9d382c9029075555f0e7df2d0c9811144de1');
INSERT INTO `track` VALUES ('19474', 'front', '2019-01-05 23:19:00', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', 'ec9f9d382c9029075555f0e7df2d0c9811144de1');
INSERT INTO `track` VALUES ('19475', 'front', '2019-01-05 23:19:42', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', 'ec9f9d382c9029075555f0e7df2d0c9811144de1');
INSERT INTO `track` VALUES ('19476', 'front', '2019-01-05 23:20:58', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', 'ec9f9d382c9029075555f0e7df2d0c9811144de1');
INSERT INTO `track` VALUES ('19477', 'front', '2019-01-05 23:22:13', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', 'ec9f9d382c9029075555f0e7df2d0c9811144de1');
INSERT INTO `track` VALUES ('19478', 'front', '2019-01-05 23:23:34', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '22573fcd7b4a65bccd40fd88848c120385cda551');
INSERT INTO `track` VALUES ('19479', 'front', '2019-01-05 23:24:23', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '22573fcd7b4a65bccd40fd88848c120385cda551');
INSERT INTO `track` VALUES ('19480', 'front', '2019-01-05 23:37:09', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '253eb78aa0c459779a9ac6497afdb1b535fbd370');
INSERT INTO `track` VALUES ('19481', 'front', '2019-01-05 23:38:07', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '253eb78aa0c459779a9ac6497afdb1b535fbd370');
INSERT INTO `track` VALUES ('19482', 'front', '2019-01-05 23:39:07', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '253eb78aa0c459779a9ac6497afdb1b535fbd370');
INSERT INTO `track` VALUES ('19483', 'front', '2019-01-05 23:43:25', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '9348dea12400dfbfbd81a37e4f611c96f95378cc');
INSERT INTO `track` VALUES ('19484', 'front', '2019-01-05 23:43:34', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '9348dea12400dfbfbd81a37e4f611c96f95378cc');
INSERT INTO `track` VALUES ('19485', 'front', '2019-01-05 23:43:37', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '9348dea12400dfbfbd81a37e4f611c96f95378cc');
INSERT INTO `track` VALUES ('19486', 'front', '2019-01-05 23:45:12', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'a9b902b96eec44d5056591a12d38ae4f0251994f');
INSERT INTO `track` VALUES ('19487', 'front', '2019-01-05 23:45:56', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'a9b902b96eec44d5056591a12d38ae4f0251994f');
INSERT INTO `track` VALUES ('19488', 'front', '2019-01-05 23:46:43', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'a9b902b96eec44d5056591a12d38ae4f0251994f');
INSERT INTO `track` VALUES ('19489', 'front', '2019-01-05 23:49:52', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', 'a9b902b96eec44d5056591a12d38ae4f0251994f');
INSERT INTO `track` VALUES ('19490', 'front', '2019-01-05 23:50:25', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'a81edeba24b1e7256122b90c979a4f54b04e3cde');
INSERT INTO `track` VALUES ('19491', 'front', '2019-01-05 23:51:23', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'a81edeba24b1e7256122b90c979a4f54b04e3cde');
INSERT INTO `track` VALUES ('19492', 'front', '2019-01-05 23:52:39', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'a81edeba24b1e7256122b90c979a4f54b04e3cde');
INSERT INTO `track` VALUES ('19493', 'front', '2019-01-06 00:02:58', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/profile/', 'Android', '::1', '/_mebook/home', 'acd9549ff326693cd3c14c326ac853482c9cc349');
INSERT INTO `track` VALUES ('19494', 'front', '2019-01-06 00:04:02', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register/', 'Android', '::1', '/_mebook/', 'acd9549ff326693cd3c14c326ac853482c9cc349');
INSERT INTO `track` VALUES ('19495', 'front', '2019-01-06 00:04:08', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register/', 'Android', '::1', '/_mebook/', 'acd9549ff326693cd3c14c326ac853482c9cc349');
INSERT INTO `track` VALUES ('19496', 'front', '2019-01-06 00:04:12', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register/', 'Android', '::1', '/_mebook/', 'acd9549ff326693cd3c14c326ac853482c9cc349');
INSERT INTO `track` VALUES ('19497', 'front', '2019-01-06 00:04:33', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/contact', 'Android', '::1', '/_mebook/', 'acd9549ff326693cd3c14c326ac853482c9cc349');
INSERT INTO `track` VALUES ('19498', 'front', '2019-01-06 00:05:15', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/contact', 'Android', '::1', '/_mebook/', 'acd9549ff326693cd3c14c326ac853482c9cc349');
INSERT INTO `track` VALUES ('19499', 'front', '2019-01-06 00:09:02', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/contact', 'iOS', '::1', '/_mebook/', 'cdea373474a9fa99d7e5e434ab3510900b329bab');
INSERT INTO `track` VALUES ('19500', 'front', '2019-01-06 00:09:06', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/contact', 'iOS', '::1', '/_mebook/', 'cdea373474a9fa99d7e5e434ab3510900b329bab');
INSERT INTO `track` VALUES ('19501', 'front', '2019-01-06 00:09:33', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/contact', 'iOS', '::1', '/_mebook/', 'cdea373474a9fa99d7e5e434ab3510900b329bab');
INSERT INTO `track` VALUES ('19502', 'front', '2019-01-06 00:10:22', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/contact', 'iOS', '::1', '/_mebook/', 'cdea373474a9fa99d7e5e434ab3510900b329bab');
INSERT INTO `track` VALUES ('19503', 'front', '2019-01-06 00:10:31', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/contact', 'Android', '::1', '/_mebook/', 'cdea373474a9fa99d7e5e434ab3510900b329bab');
INSERT INTO `track` VALUES ('19504', 'front', '2019-01-06 00:11:43', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/contact', 'Android', '::1', '/_mebook/', 'cdea373474a9fa99d7e5e434ab3510900b329bab');
INSERT INTO `track` VALUES ('19505', 'front', '2019-01-06 00:11:47', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/contact', 'Android', '::1', '/_mebook/', 'cdea373474a9fa99d7e5e434ab3510900b329bab');
INSERT INTO `track` VALUES ('19506', 'front', '2019-01-06 00:13:33', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/contact', 'Android', '::1', '/_mebook/', 'cdea373474a9fa99d7e5e434ab3510900b329bab');
INSERT INTO `track` VALUES ('19507', 'front', '2019-01-06 00:13:50', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', 'cdea373474a9fa99d7e5e434ab3510900b329bab');
INSERT INTO `track` VALUES ('19508', 'front', '2019-01-06 00:14:36', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', 'ef7d34408fd550c2f2f1b97c5f1ace530cee9688');
INSERT INTO `track` VALUES ('19509', 'front', '2019-01-06 00:15:10', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', 'ef7d34408fd550c2f2f1b97c5f1ace530cee9688');
INSERT INTO `track` VALUES ('19510', 'front', '2019-01-06 00:18:29', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/home', 'ef7d34408fd550c2f2f1b97c5f1ace530cee9688');
INSERT INTO `track` VALUES ('19511', 'front', '2019-01-06 00:19:01', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', 'ef7d34408fd550c2f2f1b97c5f1ace530cee9688');
INSERT INTO `track` VALUES ('19512', 'front', '2019-01-06 00:25:37', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '783fa50e64928cf323ba6b3c12d84992b04a64ad');
INSERT INTO `track` VALUES ('19513', 'front', '2019-01-06 00:26:47', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '783fa50e64928cf323ba6b3c12d84992b04a64ad');
INSERT INTO `track` VALUES ('19514', 'front', '2019-01-06 00:27:31', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register/', 'iOS', '::1', '/_mebook/', '783fa50e64928cf323ba6b3c12d84992b04a64ad');
INSERT INTO `track` VALUES ('19515', 'front', '2019-01-06 00:27:55', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register/', 'Windows 10', '::1', '/_mebook/', '783fa50e64928cf323ba6b3c12d84992b04a64ad');
INSERT INTO `track` VALUES ('19516', 'front', '2019-01-06 00:29:12', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register/', 'Windows 10', '::1', '/_mebook/', '783fa50e64928cf323ba6b3c12d84992b04a64ad');
INSERT INTO `track` VALUES ('19517', 'front', '2019-01-06 00:29:50', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '783fa50e64928cf323ba6b3c12d84992b04a64ad');
INSERT INTO `track` VALUES ('19518', 'front', '2019-01-06 00:33:15', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'fa864a356f9eb958644d0d6b0ed653ad872ed27a');
INSERT INTO `track` VALUES ('19519', 'front', '2019-01-06 00:33:19', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'fa864a356f9eb958644d0d6b0ed653ad872ed27a');
INSERT INTO `track` VALUES ('19520', 'front', '2019-01-06 00:39:32', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/user/register/', 'Android', '::1', '/_mebook/', '8b1f0052ee03d43ef5f9eabfc6a419b6599c01af');
INSERT INTO `track` VALUES ('19521', 'front', '2019-01-06 00:40:24', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/user/register/', 'Windows 10', '::1', '/_mebook/', '4b4c880751eb31be18f7a04cfc1321f3dbefa71d');
INSERT INTO `track` VALUES ('19522', 'front', '2019-01-06 00:41:16', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/user/register/', 'iOS', '::1', '/_mebook/', '4b4c880751eb31be18f7a04cfc1321f3dbefa71d');
INSERT INTO `track` VALUES ('19523', 'front', '2019-01-06 00:41:20', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/home', '4b4c880751eb31be18f7a04cfc1321f3dbefa71d');
INSERT INTO `track` VALUES ('19524', 'front', '2019-01-06 00:41:26', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/home', '4b4c880751eb31be18f7a04cfc1321f3dbefa71d');
INSERT INTO `track` VALUES ('19525', 'front', '2019-01-06 00:41:38', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/', '4b4c880751eb31be18f7a04cfc1321f3dbefa71d');
INSERT INTO `track` VALUES ('19526', 'front', '2019-01-06 00:42:02', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/', '4b4c880751eb31be18f7a04cfc1321f3dbefa71d');
INSERT INTO `track` VALUES ('19527', 'front', '2019-01-06 00:42:12', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/', '4b4c880751eb31be18f7a04cfc1321f3dbefa71d');
INSERT INTO `track` VALUES ('19528', 'front', '2019-01-06 00:42:43', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/', '4b4c880751eb31be18f7a04cfc1321f3dbefa71d');
INSERT INTO `track` VALUES ('19529', 'front', '2019-01-06 00:44:49', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/user/profile/', 'Windows 10', '::1', '/_mebook/', '4b4c880751eb31be18f7a04cfc1321f3dbefa71d');
INSERT INTO `track` VALUES ('19530', 'front', '2019-01-06 00:47:17', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/user/profile/', 'Android', '::1', '/_mebook/', 'b54dafb48e143509dd60a12550ca331767e0541b');
INSERT INTO `track` VALUES ('19531', 'front', '2019-01-06 00:48:07', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/user/profile/', 'Android', '::1', '/_mebook/', 'b54dafb48e143509dd60a12550ca331767e0541b');
INSERT INTO `track` VALUES ('19532', 'front', '2019-01-06 00:48:48', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home', 'b54dafb48e143509dd60a12550ca331767e0541b');
INSERT INTO `track` VALUES ('19533', 'front', '2019-01-06 00:49:59', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/contact', 'Android', '::1', '/_mebook/', 'b54dafb48e143509dd60a12550ca331767e0541b');
INSERT INTO `track` VALUES ('19534', 'front', '2019-01-06 00:54:15', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/contact', 'Android', '::1', '/_mebook/', '22b95f3e001930598da5b1226f062d47a412810c');
INSERT INTO `track` VALUES ('19535', 'front', '2019-01-06 00:54:24', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '22b95f3e001930598da5b1226f062d47a412810c');
INSERT INTO `track` VALUES ('19536', 'front', '2019-01-06 00:55:03', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '22b95f3e001930598da5b1226f062d47a412810c');
INSERT INTO `track` VALUES ('19537', 'front', '2019-01-06 00:57:51', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '22b95f3e001930598da5b1226f062d47a412810c');
INSERT INTO `track` VALUES ('19538', 'front', '2019-01-06 00:58:41', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '22b95f3e001930598da5b1226f062d47a412810c');
INSERT INTO `track` VALUES ('19539', 'front', '2019-01-06 00:58:44', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '22b95f3e001930598da5b1226f062d47a412810c');
INSERT INTO `track` VALUES ('19540', 'front', '2019-01-06 01:00:08', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '0f47c46669817c1c68c20aebbe370d8dcb556d92');
INSERT INTO `track` VALUES ('19541', 'front', '2019-01-06 01:11:02', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '110e954a1f5e08551751655f64ffd9abf59d0b69');
INSERT INTO `track` VALUES ('19542', 'front', '2019-01-06 01:11:41', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '110e954a1f5e08551751655f64ffd9abf59d0b69');
INSERT INTO `track` VALUES ('19543', 'front', '2019-01-06 01:13:00', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '110e954a1f5e08551751655f64ffd9abf59d0b69');
INSERT INTO `track` VALUES ('19544', 'front', '2019-01-06 01:13:03', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/home', '110e954a1f5e08551751655f64ffd9abf59d0b69');
INSERT INTO `track` VALUES ('19545', 'front', '2019-01-06 01:13:51', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '110e954a1f5e08551751655f64ffd9abf59d0b69');
INSERT INTO `track` VALUES ('19546', 'front', '2019-01-06 01:15:27', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '110e954a1f5e08551751655f64ffd9abf59d0b69');
INSERT INTO `track` VALUES ('19547', 'front', '2019-01-06 01:15:47', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '110e954a1f5e08551751655f64ffd9abf59d0b69');
INSERT INTO `track` VALUES ('19548', 'front', '2019-01-06 01:18:40', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'bf83718c9376c66b256f57e403a204d7375fbd13');
INSERT INTO `track` VALUES ('19549', 'front', '2019-01-06 01:19:04', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'bf83718c9376c66b256f57e403a204d7375fbd13');
INSERT INTO `track` VALUES ('19550', 'front', '2019-01-06 01:19:07', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'bf83718c9376c66b256f57e403a204d7375fbd13');
INSERT INTO `track` VALUES ('19551', 'front', '2019-01-06 01:19:22', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'bf83718c9376c66b256f57e403a204d7375fbd13');
INSERT INTO `track` VALUES ('19552', 'front', '2019-01-06 01:20:03', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'bf83718c9376c66b256f57e403a204d7375fbd13');
INSERT INTO `track` VALUES ('19553', 'front', '2019-01-06 01:20:05', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'bf83718c9376c66b256f57e403a204d7375fbd13');
INSERT INTO `track` VALUES ('19554', 'front', '2019-01-06 01:20:22', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'bf83718c9376c66b256f57e403a204d7375fbd13');
INSERT INTO `track` VALUES ('19555', 'front', '2019-01-06 01:20:24', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'bf83718c9376c66b256f57e403a204d7375fbd13');
INSERT INTO `track` VALUES ('19556', 'front', '2019-01-06 01:20:37', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'bf83718c9376c66b256f57e403a204d7375fbd13');
INSERT INTO `track` VALUES ('19557', 'front', '2019-01-06 01:21:07', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'bf83718c9376c66b256f57e403a204d7375fbd13');
INSERT INTO `track` VALUES ('19558', 'front', '2019-01-06 01:21:38', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'bf83718c9376c66b256f57e403a204d7375fbd13');
INSERT INTO `track` VALUES ('19559', 'front', '2019-01-06 01:22:19', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'bf83718c9376c66b256f57e403a204d7375fbd13');
INSERT INTO `track` VALUES ('19560', 'front', '2019-01-06 01:23:11', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'bf83718c9376c66b256f57e403a204d7375fbd13');
INSERT INTO `track` VALUES ('19561', 'front', '2019-01-06 01:23:13', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/home', 'bf83718c9376c66b256f57e403a204d7375fbd13');
INSERT INTO `track` VALUES ('19562', 'front', '2019-01-06 01:23:31', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'bf83718c9376c66b256f57e403a204d7375fbd13');
INSERT INTO `track` VALUES ('19563', 'front', '2019-01-06 01:25:30', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '15c978e3d42d9a10f5d120bc38167c5c89ce65d4');
INSERT INTO `track` VALUES ('19564', 'front', '2019-01-06 01:25:33', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '15c978e3d42d9a10f5d120bc38167c5c89ce65d4');
INSERT INTO `track` VALUES ('19565', 'front', '2019-01-06 01:25:55', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '15c978e3d42d9a10f5d120bc38167c5c89ce65d4');
INSERT INTO `track` VALUES ('19566', 'front', '2019-01-06 01:26:14', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '15c978e3d42d9a10f5d120bc38167c5c89ce65d4');
INSERT INTO `track` VALUES ('19567', 'front', '2019-01-06 01:26:36', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '15c978e3d42d9a10f5d120bc38167c5c89ce65d4');
INSERT INTO `track` VALUES ('19568', 'front', '2019-01-06 01:30:29', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '15c978e3d42d9a10f5d120bc38167c5c89ce65d4');
INSERT INTO `track` VALUES ('19569', 'front', '2019-01-06 01:31:35', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '5b9801687faa868f9a53d67b45e331c891a22522');
INSERT INTO `track` VALUES ('19570', 'front', '2019-01-06 01:31:37', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '5b9801687faa868f9a53d67b45e331c891a22522');
INSERT INTO `track` VALUES ('19571', 'front', '2019-01-06 01:31:53', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '5b9801687faa868f9a53d67b45e331c891a22522');
INSERT INTO `track` VALUES ('19572', 'front', '2019-01-06 01:31:56', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '5b9801687faa868f9a53d67b45e331c891a22522');
INSERT INTO `track` VALUES ('19573', 'front', '2019-01-06 01:32:08', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '5b9801687faa868f9a53d67b45e331c891a22522');
INSERT INTO `track` VALUES ('19574', 'front', '2019-01-06 01:32:10', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '5b9801687faa868f9a53d67b45e331c891a22522');
INSERT INTO `track` VALUES ('19575', 'front', '2019-01-06 01:32:14', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '5b9801687faa868f9a53d67b45e331c891a22522');
INSERT INTO `track` VALUES ('19576', 'front', '2019-01-06 01:32:26', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '5b9801687faa868f9a53d67b45e331c891a22522');
INSERT INTO `track` VALUES ('19577', 'front', '2019-01-06 01:32:49', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '5b9801687faa868f9a53d67b45e331c891a22522');
INSERT INTO `track` VALUES ('19578', 'front', '2019-01-06 01:32:51', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '5b9801687faa868f9a53d67b45e331c891a22522');
INSERT INTO `track` VALUES ('19579', 'front', '2019-01-06 01:33:21', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '5b9801687faa868f9a53d67b45e331c891a22522');
INSERT INTO `track` VALUES ('19580', 'front', '2019-01-06 01:37:28', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'a8fc7ebb9e6cc9f3d40188e1e6daf20875c6f0d6');
INSERT INTO `track` VALUES ('19581', 'front', '2019-01-06 01:37:43', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'a8fc7ebb9e6cc9f3d40188e1e6daf20875c6f0d6');
INSERT INTO `track` VALUES ('19582', 'front', '2019-01-06 01:38:49', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'a8fc7ebb9e6cc9f3d40188e1e6daf20875c6f0d6');
INSERT INTO `track` VALUES ('19583', 'front', '2019-01-06 01:39:30', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/user/register', 'iOS', '::1', '/_mebook/', 'a8fc7ebb9e6cc9f3d40188e1e6daf20875c6f0d6');
INSERT INTO `track` VALUES ('19584', 'front', '2019-01-06 01:39:43', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/user/register', 'iOS', '::1', '/_mebook/', 'a8fc7ebb9e6cc9f3d40188e1e6daf20875c6f0d6');
INSERT INTO `track` VALUES ('19585', 'front', '2019-01-06 01:40:15', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/user/register', 'iOS', '::1', '/_mebook/', 'a8fc7ebb9e6cc9f3d40188e1e6daf20875c6f0d6');
INSERT INTO `track` VALUES ('19586', 'front', '2019-01-06 01:40:34', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'a8fc7ebb9e6cc9f3d40188e1e6daf20875c6f0d6');
INSERT INTO `track` VALUES ('19587', 'front', '2019-01-06 01:41:41', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/user/profile/', 'Windows 10', '::1', '/_mebook/', 'a8fc7ebb9e6cc9f3d40188e1e6daf20875c6f0d6');
INSERT INTO `track` VALUES ('19588', 'front', '2019-01-06 01:59:40', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/home', 'd9a136f169021bee4767c1e264b6819d1df44de3');
INSERT INTO `track` VALUES ('19589', 'front', '2019-01-06 09:01:59', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'c868c5907c3fc1600e651fe1d31b6d009fe7c542');
INSERT INTO `track` VALUES ('19590', 'front', '2019-01-06 09:02:50', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/', 'c868c5907c3fc1600e651fe1d31b6d009fe7c542');
INSERT INTO `track` VALUES ('19591', 'front', '2019-01-06 09:03:27', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'c868c5907c3fc1600e651fe1d31b6d009fe7c542');
INSERT INTO `track` VALUES ('19592', 'front', '2019-01-06 09:23:09', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/user/profile/', 'Windows 10', '::1', '/_mebook/', 'f1513a4959f30e9a0396f2e54404c4feb63f4617');
INSERT INTO `track` VALUES ('19593', 'front', '2019-01-06 09:23:24', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'f1513a4959f30e9a0396f2e54404c4feb63f4617');
INSERT INTO `track` VALUES ('19594', 'front', '2019-01-06 09:50:25', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/home', 'b8f574b94fdc730b5c2ec32cf0d762508dc4a977');
INSERT INTO `track` VALUES ('19595', 'front', '2019-01-06 09:53:02', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/home', 'b8f574b94fdc730b5c2ec32cf0d762508dc4a977');
INSERT INTO `track` VALUES ('19596', 'front', '2019-01-06 09:53:05', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/home', 'b8f574b94fdc730b5c2ec32cf0d762508dc4a977');
INSERT INTO `track` VALUES ('19597', 'front', '2019-01-06 09:53:25', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'b8f574b94fdc730b5c2ec32cf0d762508dc4a977');
INSERT INTO `track` VALUES ('19598', 'front', '2019-01-06 09:55:31', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '9b7038f3824fde03d21536f01df3bc527a89f340');
INSERT INTO `track` VALUES ('19599', 'front', '2019-01-06 09:57:43', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/home', '9b7038f3824fde03d21536f01df3bc527a89f340');
INSERT INTO `track` VALUES ('19600', 'front', '2019-01-06 11:28:06', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '26aa8d0735830633b8410e12f28aa9c0a402b28a');
INSERT INTO `track` VALUES ('19601', 'front', '2019-01-06 11:30:45', 'home', 'index', '0', '23', '1', 'http://localhost:81/_mebook/user/register', 'iOS', '::1', '/_mebook/', '26aa8d0735830633b8410e12f28aa9c0a402b28a');
INSERT INTO `track` VALUES ('19602', 'front', '2019-01-06 11:39:37', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '64f3b72c344966b6af7b3dd17d9c5a026255e1da');
INSERT INTO `track` VALUES ('19603', 'front', '2019-01-06 11:53:50', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '4f55ee959b2734c9143512cc00a259705997dba9');
INSERT INTO `track` VALUES ('19604', 'front', '2019-01-06 11:53:58', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/home', '4f55ee959b2734c9143512cc00a259705997dba9');
INSERT INTO `track` VALUES ('19605', 'front', '2019-01-06 11:55:24', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/home', '4f55ee959b2734c9143512cc00a259705997dba9');
INSERT INTO `track` VALUES ('19606', 'front', '2019-01-06 11:55:30', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/home', '4f55ee959b2734c9143512cc00a259705997dba9');
INSERT INTO `track` VALUES ('19607', 'front', '2019-01-06 11:56:16', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/home', '4f55ee959b2734c9143512cc00a259705997dba9');
INSERT INTO `track` VALUES ('19608', 'front', '2019-01-06 12:34:52', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '683b405fe99117c30c4f4a69d3de86a32b37dfae');
INSERT INTO `track` VALUES ('19609', 'front', '2019-01-06 12:37:51', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/home', '683b405fe99117c30c4f4a69d3de86a32b37dfae');
INSERT INTO `track` VALUES ('19610', 'front', '2019-01-06 13:24:14', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/home', '71f918673d9ec97a3dbd5eb2402b97f30ed13aac');
INSERT INTO `track` VALUES ('19611', 'front', '2019-01-06 13:24:16', 'home', 'payment_method', '0', null, '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/home/payment_method', '71f918673d9ec97a3dbd5eb2402b97f30ed13aac');
INSERT INTO `track` VALUES ('19612', 'front', '2019-01-06 13:24:21', 'home', 'help', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/home/help', '71f918673d9ec97a3dbd5eb2402b97f30ed13aac');
INSERT INTO `track` VALUES ('19613', 'front', '2019-01-06 13:24:36', 'home', 'payment_method', '0', null, '0', 'http://localhost:81/_mebook/home/help', 'Windows 10', '::1', '/_mebook/home/payment_method', '71f918673d9ec97a3dbd5eb2402b97f30ed13aac');
INSERT INTO `track` VALUES ('19614', 'front', '2019-01-06 13:25:07', 'home', 'payment_method', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/home/payment_method', '71f918673d9ec97a3dbd5eb2402b97f30ed13aac');
INSERT INTO `track` VALUES ('19615', 'front', '2019-01-06 13:25:11', 'home', 'help', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/home/help', '71f918673d9ec97a3dbd5eb2402b97f30ed13aac');
INSERT INTO `track` VALUES ('19616', 'front', '2019-01-06 13:29:48', 'home', 'help', '0', null, '0', 'http://localhost:81/_mebook/home/help', 'Windows 10', '::1', '/_mebook/home/help', 'a074a941d85c2b38ead9eb5dbe58bd452b526dc7');
INSERT INTO `track` VALUES ('19617', 'front', '2019-01-06 13:33:42', 'home', 'help', '0', null, '0', 'http://localhost:81/_mebook/home/help', 'Windows 10', '::1', '/_mebook/home/help', '71f918673d9ec97a3dbd5eb2402b97f30ed13aac');
INSERT INTO `track` VALUES ('19618', 'front', '2019-01-06 13:34:27', 'home', 'payment_method', '0', null, '0', 'http://localhost:81/_mebook/home/help', 'Windows 10', '::1', '/_mebook/home/payment_method', '71f918673d9ec97a3dbd5eb2402b97f30ed13aac');
INSERT INTO `track` VALUES ('19619', 'front', '2019-01-06 13:34:32', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '71f918673d9ec97a3dbd5eb2402b97f30ed13aac');
INSERT INTO `track` VALUES ('19620', 'front', '2019-01-06 13:48:42', 'home', 'payment_method', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/home/payment_method', '88c03a113baed99fd2fc6d9a416adbf2281642df');
INSERT INTO `track` VALUES ('19621', 'front', '2019-01-06 13:48:45', 'home', 'help', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/home/help', '88c03a113baed99fd2fc6d9a416adbf2281642df');
INSERT INTO `track` VALUES ('19622', 'front', '2019-01-06 13:48:47', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/help', 'Windows 10', '::1', '/_mebook/', '88c03a113baed99fd2fc6d9a416adbf2281642df');
INSERT INTO `track` VALUES ('19623', 'front', '2019-01-06 13:52:16', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/help', 'Windows 10', '::1', '/_mebook/', '88c03a113baed99fd2fc6d9a416adbf2281642df');
INSERT INTO `track` VALUES ('19624', 'front', '2019-01-06 13:52:17', 'home', 'payment_method', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/home/payment_method', '88c03a113baed99fd2fc6d9a416adbf2281642df');
INSERT INTO `track` VALUES ('19625', 'front', '2019-01-06 13:52:20', 'home', 'help', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/home/help', '88c03a113baed99fd2fc6d9a416adbf2281642df');
INSERT INTO `track` VALUES ('19626', 'front', '2019-01-06 13:52:22', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/help', 'Windows 10', '::1', '/_mebook/', '88c03a113baed99fd2fc6d9a416adbf2281642df');
INSERT INTO `track` VALUES ('19627', 'front', '2019-01-06 14:09:46', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'c9a4baebed5028fd9435ce02468b44fd536b80c9');
INSERT INTO `track` VALUES ('19628', 'front', '2019-01-06 14:53:24', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/category/type/course', 'Windows 10', '::1', '/_mebook/', 'b40c76703498d4954c1e39367f2d5198a2f80a5f');
INSERT INTO `track` VALUES ('19629', 'front', '2019-01-06 19:37:10', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/category/type/course', 'Windows 10', '::1', '/_mebook/', '19caec9aea6a0a4844f925a940860b259a688ae9');
INSERT INTO `track` VALUES ('19630', 'front', '2019-01-06 19:37:22', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/category/type/course', 'Windows 10', '::1', '/_mebook/', '19caec9aea6a0a4844f925a940860b259a688ae9');
INSERT INTO `track` VALUES ('19631', 'front', '2019-01-06 19:38:00', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/category/type/course', 'Windows 10', '::1', '/_mebook/', '19caec9aea6a0a4844f925a940860b259a688ae9');
INSERT INTO `track` VALUES ('19632', 'front', '2019-01-06 19:38:43', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/category/type/course', 'Windows 10', '::1', '/_mebook/', '19caec9aea6a0a4844f925a940860b259a688ae9');
INSERT INTO `track` VALUES ('19633', 'front', '2019-01-06 19:50:25', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'b6a2ec22fed4e96fd22de7ca17c9093ae210b285');
INSERT INTO `track` VALUES ('19634', 'front', '2019-01-06 19:52:16', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'b6a2ec22fed4e96fd22de7ca17c9093ae210b285');
INSERT INTO `track` VALUES ('19635', 'front', '2019-01-06 20:03:26', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '47b55844c6468444aadb2d2fd6ea8aff98e5518c');
INSERT INTO `track` VALUES ('19636', 'front', '2019-01-06 20:22:41', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '47b55844c6468444aadb2d2fd6ea8aff98e5518c');
INSERT INTO `track` VALUES ('19637', 'front', '2019-01-06 20:25:51', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '47b55844c6468444aadb2d2fd6ea8aff98e5518c');
INSERT INTO `track` VALUES ('19638', 'front', '2019-01-06 20:25:58', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', '47b55844c6468444aadb2d2fd6ea8aff98e5518c');
INSERT INTO `track` VALUES ('19639', 'front', '2019-01-06 20:27:27', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', '47b55844c6468444aadb2d2fd6ea8aff98e5518c');
INSERT INTO `track` VALUES ('19640', 'front', '2019-01-06 20:29:09', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'c1f7f9074bb26100eb328d4407a70cb9614f0b41');
INSERT INTO `track` VALUES ('19641', 'front', '2019-01-06 20:31:24', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'c1f7f9074bb26100eb328d4407a70cb9614f0b41');
INSERT INTO `track` VALUES ('19642', 'front', '2019-01-06 20:31:27', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'c1f7f9074bb26100eb328d4407a70cb9614f0b41');
INSERT INTO `track` VALUES ('19643', 'front', '2019-01-06 20:31:50', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'c1f7f9074bb26100eb328d4407a70cb9614f0b41');
INSERT INTO `track` VALUES ('19644', 'front', '2019-01-06 20:32:30', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'c1f7f9074bb26100eb328d4407a70cb9614f0b41');
INSERT INTO `track` VALUES ('19645', 'front', '2019-01-06 20:32:36', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'c1f7f9074bb26100eb328d4407a70cb9614f0b41');
INSERT INTO `track` VALUES ('19646', 'front', '2019-01-06 20:41:55', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'f52d4c7005c715b1aa21509c099a6e34f17bdc13');
INSERT INTO `track` VALUES ('19647', 'front', '2019-01-06 20:42:41', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'f52d4c7005c715b1aa21509c099a6e34f17bdc13');
INSERT INTO `track` VALUES ('19648', 'front', '2019-01-06 20:43:02', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'f52d4c7005c715b1aa21509c099a6e34f17bdc13');
INSERT INTO `track` VALUES ('19649', 'front', '2019-01-06 20:48:01', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', '60d25676711ffbef93d6958d2bd869fbca1eed29');
INSERT INTO `track` VALUES ('19650', 'front', '2019-01-06 20:48:15', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', '60d25676711ffbef93d6958d2bd869fbca1eed29');
INSERT INTO `track` VALUES ('19651', 'front', '2019-01-06 20:49:18', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', '60d25676711ffbef93d6958d2bd869fbca1eed29');
INSERT INTO `track` VALUES ('19652', 'front', '2019-01-06 20:50:29', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', '60d25676711ffbef93d6958d2bd869fbca1eed29');
INSERT INTO `track` VALUES ('19653', 'front', '2019-01-06 21:07:10', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'b5e2ab3812f05871e243fc733a63bcce25bb26a3');
INSERT INTO `track` VALUES ('19654', 'front', '2019-01-06 21:07:44', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'b5e2ab3812f05871e243fc733a63bcce25bb26a3');
INSERT INTO `track` VALUES ('19655', 'front', '2019-01-06 21:07:54', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/m', 'Windows 10', '::1', '/_mebook/', 'b5e2ab3812f05871e243fc733a63bcce25bb26a3');
INSERT INTO `track` VALUES ('19656', 'front', '2019-01-06 21:07:59', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/aaaa', 'b5e2ab3812f05871e243fc733a63bcce25bb26a3');
INSERT INTO `track` VALUES ('19657', 'front', '2019-01-06 21:08:09', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/aaaa', 'Windows 10', '::1', '/_mebook/', 'b5e2ab3812f05871e243fc733a63bcce25bb26a3');
INSERT INTO `track` VALUES ('19658', 'front', '2019-01-06 21:08:13', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'b5e2ab3812f05871e243fc733a63bcce25bb26a3');
INSERT INTO `track` VALUES ('19659', 'front', '2019-01-06 21:10:24', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'b5e2ab3812f05871e243fc733a63bcce25bb26a3');
INSERT INTO `track` VALUES ('19660', 'front', '2019-01-06 21:10:30', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'b5e2ab3812f05871e243fc733a63bcce25bb26a3');
INSERT INTO `track` VALUES ('19661', 'front', '2019-01-06 21:10:33', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'b5e2ab3812f05871e243fc733a63bcce25bb26a3');
INSERT INTO `track` VALUES ('19662', 'front', '2019-01-06 21:11:20', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'b5e2ab3812f05871e243fc733a63bcce25bb26a3');
INSERT INTO `track` VALUES ('19663', 'front', '2019-01-06 21:12:49', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'ed2e9fb169413ac882ee5ad5430537df5da04ddb');
INSERT INTO `track` VALUES ('19664', 'front', '2019-01-06 21:13:06', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'ed2e9fb169413ac882ee5ad5430537df5da04ddb');
INSERT INTO `track` VALUES ('19665', 'front', '2019-01-06 21:13:23', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'ed2e9fb169413ac882ee5ad5430537df5da04ddb');
INSERT INTO `track` VALUES ('19666', 'front', '2019-01-06 21:13:30', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/m', 'Windows 10', '::1', '/_mebook/', 'ed2e9fb169413ac882ee5ad5430537df5da04ddb');
INSERT INTO `track` VALUES ('19667', 'front', '2019-01-06 21:13:48', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'ed2e9fb169413ac882ee5ad5430537df5da04ddb');
INSERT INTO `track` VALUES ('19668', 'front', '2019-01-06 21:15:30', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'ed2e9fb169413ac882ee5ad5430537df5da04ddb');
INSERT INTO `track` VALUES ('19669', 'front', '2019-01-06 21:16:14', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'ed2e9fb169413ac882ee5ad5430537df5da04ddb');
INSERT INTO `track` VALUES ('19670', 'front', '2019-01-06 21:16:58', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/help', 'Windows 10', '::1', '/_mebook/', 'ed2e9fb169413ac882ee5ad5430537df5da04ddb');
INSERT INTO `track` VALUES ('19671', 'front', '2019-01-06 21:17:00', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'ed2e9fb169413ac882ee5ad5430537df5da04ddb');
INSERT INTO `track` VALUES ('19672', 'front', '2019-01-06 21:18:43', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'c5060956543d3629f9b1624dc67ce14664d29851');
INSERT INTO `track` VALUES ('19673', 'front', '2019-01-06 21:19:12', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'c5060956543d3629f9b1624dc67ce14664d29851');
INSERT INTO `track` VALUES ('19674', 'front', '2019-01-06 21:19:26', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'c5060956543d3629f9b1624dc67ce14664d29851');
INSERT INTO `track` VALUES ('19675', 'front', '2019-01-06 21:19:39', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'c5060956543d3629f9b1624dc67ce14664d29851');
INSERT INTO `track` VALUES ('19676', 'front', '2019-01-06 21:19:52', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'c5060956543d3629f9b1624dc67ce14664d29851');
INSERT INTO `track` VALUES ('19677', 'front', '2019-01-06 21:21:05', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'c5060956543d3629f9b1624dc67ce14664d29851');
INSERT INTO `track` VALUES ('19678', 'front', '2019-01-06 21:21:14', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/m', 'Windows 10', '::1', '/_mebook/', 'c5060956543d3629f9b1624dc67ce14664d29851');
INSERT INTO `track` VALUES ('19679', 'front', '2019-01-07 09:59:59', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/help', 'Windows 10', '::1', '/_mebook/', '7a5550bbe6775246b053fce69a34d726db44abea');
INSERT INTO `track` VALUES ('19680', 'front', '2019-01-07 10:40:54', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/help', 'Windows 10', '::1', '/_mebook/', 'dcdc1971878d3945c03b6e3bf54e17b5d63559b2');
INSERT INTO `track` VALUES ('19681', 'front', '2019-01-07 10:40:56', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/m', 'Windows 10', '::1', '/_mebook/', 'dcdc1971878d3945c03b6e3bf54e17b5d63559b2');
INSERT INTO `track` VALUES ('19682', 'front', '2019-01-07 14:55:38', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3c788af6d7c311658c7198eb0e457cebd3b7edd0');
INSERT INTO `track` VALUES ('19683', 'front', '2019-01-07 14:57:55', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3c788af6d7c311658c7198eb0e457cebd3b7edd0');
INSERT INTO `track` VALUES ('19684', 'front', '2019-01-07 14:58:06', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3c788af6d7c311658c7198eb0e457cebd3b7edd0');
INSERT INTO `track` VALUES ('19685', 'front', '2019-01-07 14:58:25', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3c788af6d7c311658c7198eb0e457cebd3b7edd0');
INSERT INTO `track` VALUES ('19686', 'front', '2019-01-07 14:58:29', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3c788af6d7c311658c7198eb0e457cebd3b7edd0');
INSERT INTO `track` VALUES ('19687', 'front', '2019-01-07 14:59:00', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3c788af6d7c311658c7198eb0e457cebd3b7edd0');
INSERT INTO `track` VALUES ('19688', 'front', '2019-01-07 14:59:49', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3c788af6d7c311658c7198eb0e457cebd3b7edd0');
INSERT INTO `track` VALUES ('19689', 'front', '2019-01-07 14:59:52', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3c788af6d7c311658c7198eb0e457cebd3b7edd0');
INSERT INTO `track` VALUES ('19690', 'front', '2019-01-07 15:00:31', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3c788af6d7c311658c7198eb0e457cebd3b7edd0');
INSERT INTO `track` VALUES ('19691', 'front', '2019-01-07 15:00:42', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '5121748406db81230b66a840e2e8b991af83fe6c');
INSERT INTO `track` VALUES ('19692', 'front', '2019-01-07 23:34:50', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '1f8c097b7f9e68439289ef676f6e1b1366540c5b');
INSERT INTO `track` VALUES ('19693', 'front', '2019-01-07 23:35:10', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '1f8c097b7f9e68439289ef676f6e1b1366540c5b');
INSERT INTO `track` VALUES ('19694', 'front', '2019-01-07 23:35:13', 'course', 'detail', '5', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/aaaa', '1f8c097b7f9e68439289ef676f6e1b1366540c5b');
INSERT INTO `track` VALUES ('19695', 'front', '2019-01-07 23:35:48', 'course', 'detail', '5', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/aaaa', '1f8c097b7f9e68439289ef676f6e1b1366540c5b');
INSERT INTO `track` VALUES ('19696', 'front', '2019-01-07 23:36:24', 'course', 'detail', '5', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/aaaa', '1f8c097b7f9e68439289ef676f6e1b1366540c5b');
INSERT INTO `track` VALUES ('19697', 'front', '2019-01-07 23:36:55', 'course', 'detail', '5', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/aaaa', '1f8c097b7f9e68439289ef676f6e1b1366540c5b');
INSERT INTO `track` VALUES ('19698', 'front', '2019-01-07 23:39:01', 'course', 'detail', '5', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/aaaa', '1f8c097b7f9e68439289ef676f6e1b1366540c5b');
INSERT INTO `track` VALUES ('19699', 'front', '2019-01-07 23:39:33', 'course', 'detail', '5', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/aaaa', '1f8c097b7f9e68439289ef676f6e1b1366540c5b');
INSERT INTO `track` VALUES ('19700', 'front', '2019-01-07 23:40:31', 'course', 'detail', '5', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/aaaa', '0d341b52899209b2cec41e70eabe5c0e75983ac3');
INSERT INTO `track` VALUES ('19701', 'front', '2019-01-07 23:41:56', 'course', 'detail', '5', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/aaaa', '0d341b52899209b2cec41e70eabe5c0e75983ac3');
INSERT INTO `track` VALUES ('19702', 'front', '2019-01-07 23:42:05', 'course', 'detail', '5', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/aaaa', '0d341b52899209b2cec41e70eabe5c0e75983ac3');
INSERT INTO `track` VALUES ('19703', 'front', '2019-01-07 23:43:23', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/aaaa', 'Windows 10', '::1', '/_mebook/home', '0d341b52899209b2cec41e70eabe5c0e75983ac3');
INSERT INTO `track` VALUES ('19704', 'front', '2019-01-07 23:45:05', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/', '0d341b52899209b2cec41e70eabe5c0e75983ac3');
INSERT INTO `track` VALUES ('19705', 'front', '2019-01-07 23:45:07', 'course', 'detail', '4', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', '0d341b52899209b2cec41e70eabe5c0e75983ac3');
INSERT INTO `track` VALUES ('19706', 'front', '2019-01-07 23:45:15', 'course', 'detail', '4', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', '0d341b52899209b2cec41e70eabe5c0e75983ac3');
INSERT INTO `track` VALUES ('19707', 'front', '2019-01-07 23:45:37', 'course', 'detail', '4', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'e72b672e99aa407e1c62635e644aade2e4d935b8');
INSERT INTO `track` VALUES ('19708', 'front', '2019-01-07 23:45:39', 'course', 'detail', '4', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'e72b672e99aa407e1c62635e644aade2e4d935b8');
INSERT INTO `track` VALUES ('19709', 'front', '2019-01-07 23:45:56', 'course', 'detail', '4', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'e72b672e99aa407e1c62635e644aade2e4d935b8');
INSERT INTO `track` VALUES ('19710', 'front', '2019-01-07 23:45:58', 'course', 'detail', '4', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'e72b672e99aa407e1c62635e644aade2e4d935b8');
INSERT INTO `track` VALUES ('19711', 'front', '2019-01-07 23:46:44', 'course', 'detail', '4', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'e72b672e99aa407e1c62635e644aade2e4d935b8');
INSERT INTO `track` VALUES ('19712', 'front', '2019-01-07 23:47:00', 'course', 'detail', '4', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'e72b672e99aa407e1c62635e644aade2e4d935b8');
INSERT INTO `track` VALUES ('19713', 'front', '2019-01-07 23:48:18', 'course', 'detail', '4', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'e72b672e99aa407e1c62635e644aade2e4d935b8');
INSERT INTO `track` VALUES ('19714', 'front', '2019-01-07 23:48:29', 'course', 'detail', '4', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'e72b672e99aa407e1c62635e644aade2e4d935b8');
INSERT INTO `track` VALUES ('19715', 'front', '2019-01-07 23:48:40', 'course', 'detail', '4', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'e72b672e99aa407e1c62635e644aade2e4d935b8');
INSERT INTO `track` VALUES ('19716', 'front', '2019-01-07 23:51:05', 'course', 'detail', '4', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', '632b4c5a86903f79e69a1a9c5b869f3081cc37be');
INSERT INTO `track` VALUES ('19717', 'front', '2019-01-07 23:53:19', 'course', 'detail', '4', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', '632b4c5a86903f79e69a1a9c5b869f3081cc37be');
INSERT INTO `track` VALUES ('19718', 'front', '2019-01-07 23:54:13', 'course', 'detail', '4', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', '632b4c5a86903f79e69a1a9c5b869f3081cc37be');
INSERT INTO `track` VALUES ('19719', 'front', '2019-01-07 23:54:58', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/course/detail/m', 'Windows 10', '::1', '/_mebook/', '632b4c5a86903f79e69a1a9c5b869f3081cc37be');
INSERT INTO `track` VALUES ('19720', 'front', '2019-01-07 23:56:16', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '5731ef95415df291d7e2c9a5af09054cdffca73e');
INSERT INTO `track` VALUES ('19721', 'front', '2019-01-07 23:59:15', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '5731ef95415df291d7e2c9a5af09054cdffca73e');
INSERT INTO `track` VALUES ('19722', 'front', '2019-01-08 00:21:48', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'b7d7443a266abe118519ef1f5e00e2fafc2ec397');
INSERT INTO `track` VALUES ('19723', 'front', '2019-01-08 00:23:36', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'b7d7443a266abe118519ef1f5e00e2fafc2ec397');
INSERT INTO `track` VALUES ('19724', 'front', '2019-01-08 00:24:51', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'b7d7443a266abe118519ef1f5e00e2fafc2ec397');
INSERT INTO `track` VALUES ('19725', 'front', '2019-01-08 00:25:55', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'b7d7443a266abe118519ef1f5e00e2fafc2ec397');
INSERT INTO `track` VALUES ('19726', 'front', '2019-01-08 00:26:01', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'b7d7443a266abe118519ef1f5e00e2fafc2ec397');
INSERT INTO `track` VALUES ('19727', 'front', '2019-01-08 00:26:07', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'b7d7443a266abe118519ef1f5e00e2fafc2ec397');
INSERT INTO `track` VALUES ('19728', 'front', '2019-01-08 00:26:30', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'b7d7443a266abe118519ef1f5e00e2fafc2ec397');
INSERT INTO `track` VALUES ('19729', 'front', '2019-01-08 00:27:11', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '72e7749947ee45c0f61e1d4749bce3f950857fa7');
INSERT INTO `track` VALUES ('19730', 'front', '2019-01-08 00:27:58', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '72e7749947ee45c0f61e1d4749bce3f950857fa7');
INSERT INTO `track` VALUES ('19731', 'front', '2019-01-08 00:28:33', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '72e7749947ee45c0f61e1d4749bce3f950857fa7');
INSERT INTO `track` VALUES ('19732', 'front', '2019-01-08 00:30:23', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '72e7749947ee45c0f61e1d4749bce3f950857fa7');
INSERT INTO `track` VALUES ('19733', 'front', '2019-01-08 00:31:35', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '72e7749947ee45c0f61e1d4749bce3f950857fa7');
INSERT INTO `track` VALUES ('19734', 'front', '2019-01-08 00:32:06', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '72e7749947ee45c0f61e1d4749bce3f950857fa7');
INSERT INTO `track` VALUES ('19735', 'front', '2019-01-08 00:33:42', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'efce97dc00a795c5cdf29a32574963e82255cb9d');
INSERT INTO `track` VALUES ('19736', 'front', '2019-01-08 00:35:12', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'efce97dc00a795c5cdf29a32574963e82255cb9d');
INSERT INTO `track` VALUES ('19737', 'front', '2019-01-08 00:35:32', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'efce97dc00a795c5cdf29a32574963e82255cb9d');
INSERT INTO `track` VALUES ('19738', 'front', '2019-01-08 00:37:29', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'efce97dc00a795c5cdf29a32574963e82255cb9d');
INSERT INTO `track` VALUES ('19739', 'front', '2019-01-08 00:37:54', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'efce97dc00a795c5cdf29a32574963e82255cb9d');
INSERT INTO `track` VALUES ('19740', 'front', '2019-01-08 00:40:25', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'c4fcc669bf03948186d1b32c638f54e7f0a0ac46');
INSERT INTO `track` VALUES ('19741', 'front', '2019-01-08 00:40:48', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'c4fcc669bf03948186d1b32c638f54e7f0a0ac46');
INSERT INTO `track` VALUES ('19742', 'front', '2019-01-08 00:41:24', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'c4fcc669bf03948186d1b32c638f54e7f0a0ac46');
INSERT INTO `track` VALUES ('19743', 'front', '2019-01-08 00:42:55', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'c4fcc669bf03948186d1b32c638f54e7f0a0ac46');
INSERT INTO `track` VALUES ('19744', 'front', '2019-01-08 00:43:18', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'c4fcc669bf03948186d1b32c638f54e7f0a0ac46');
INSERT INTO `track` VALUES ('19745', 'front', '2019-01-08 00:43:44', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'c4fcc669bf03948186d1b32c638f54e7f0a0ac46');
INSERT INTO `track` VALUES ('19746', 'front', '2019-01-08 00:44:01', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'c4fcc669bf03948186d1b32c638f54e7f0a0ac46');
INSERT INTO `track` VALUES ('19747', 'front', '2019-01-08 00:47:28', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '4507bbcba42427d287d456d266d4be7b4749ef61');
INSERT INTO `track` VALUES ('19748', 'front', '2019-01-08 00:48:14', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '4507bbcba42427d287d456d266d4be7b4749ef61');
INSERT INTO `track` VALUES ('19749', 'front', '2019-01-08 00:49:55', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '4507bbcba42427d287d456d266d4be7b4749ef61');
INSERT INTO `track` VALUES ('19750', 'front', '2019-01-08 00:51:13', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '4507bbcba42427d287d456d266d4be7b4749ef61');
INSERT INTO `track` VALUES ('19751', 'front', '2019-01-08 00:51:52', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '4507bbcba42427d287d456d266d4be7b4749ef61');
INSERT INTO `track` VALUES ('19752', 'front', '2019-01-08 00:52:00', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '4507bbcba42427d287d456d266d4be7b4749ef61');
INSERT INTO `track` VALUES ('19753', 'front', '2019-01-08 00:53:00', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '6a5fc470a0b722845eda24ab38b40e4f4c028b8f');
INSERT INTO `track` VALUES ('19754', 'front', '2019-01-08 00:53:19', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '6a5fc470a0b722845eda24ab38b40e4f4c028b8f');
INSERT INTO `track` VALUES ('19755', 'front', '2019-01-08 00:53:55', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '6a5fc470a0b722845eda24ab38b40e4f4c028b8f');
INSERT INTO `track` VALUES ('19756', 'front', '2019-01-08 00:54:00', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '6a5fc470a0b722845eda24ab38b40e4f4c028b8f');
INSERT INTO `track` VALUES ('19757', 'front', '2019-01-08 00:54:27', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '6a5fc470a0b722845eda24ab38b40e4f4c028b8f');
INSERT INTO `track` VALUES ('19758', 'front', '2019-01-08 00:54:46', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '6a5fc470a0b722845eda24ab38b40e4f4c028b8f');
INSERT INTO `track` VALUES ('19759', 'front', '2019-01-08 00:55:01', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '6a5fc470a0b722845eda24ab38b40e4f4c028b8f');
INSERT INTO `track` VALUES ('19760', 'front', '2019-01-08 00:55:22', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '6a5fc470a0b722845eda24ab38b40e4f4c028b8f');
INSERT INTO `track` VALUES ('19761', 'front', '2019-01-08 00:55:46', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '6a5fc470a0b722845eda24ab38b40e4f4c028b8f');
INSERT INTO `track` VALUES ('19762', 'front', '2019-01-08 00:56:36', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '6a5fc470a0b722845eda24ab38b40e4f4c028b8f');
INSERT INTO `track` VALUES ('19763', 'front', '2019-01-08 00:59:42', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'e3577d011f3211b2dbb28fecefad8f3d17aa2395');
INSERT INTO `track` VALUES ('19764', 'front', '2019-01-08 00:59:57', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'e3577d011f3211b2dbb28fecefad8f3d17aa2395');
INSERT INTO `track` VALUES ('19765', 'front', '2019-01-08 01:00:10', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'e3577d011f3211b2dbb28fecefad8f3d17aa2395');
INSERT INTO `track` VALUES ('19766', 'front', '2019-01-08 01:00:13', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'e3577d011f3211b2dbb28fecefad8f3d17aa2395');
INSERT INTO `track` VALUES ('19767', 'front', '2019-01-08 01:00:16', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'e3577d011f3211b2dbb28fecefad8f3d17aa2395');
INSERT INTO `track` VALUES ('19768', 'front', '2019-01-08 01:00:49', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'e3577d011f3211b2dbb28fecefad8f3d17aa2395');
INSERT INTO `track` VALUES ('19769', 'front', '2019-01-08 01:00:52', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'e3577d011f3211b2dbb28fecefad8f3d17aa2395');
INSERT INTO `track` VALUES ('19770', 'front', '2019-01-08 01:02:05', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'e3577d011f3211b2dbb28fecefad8f3d17aa2395');
INSERT INTO `track` VALUES ('19771', 'front', '2019-01-08 01:02:07', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'e3577d011f3211b2dbb28fecefad8f3d17aa2395');
INSERT INTO `track` VALUES ('19772', 'front', '2019-01-08 01:03:25', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'e3577d011f3211b2dbb28fecefad8f3d17aa2395');
INSERT INTO `track` VALUES ('19773', 'front', '2019-01-08 01:03:53', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'e3577d011f3211b2dbb28fecefad8f3d17aa2395');
INSERT INTO `track` VALUES ('19774', 'front', '2019-01-08 01:04:08', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'e3577d011f3211b2dbb28fecefad8f3d17aa2395');
INSERT INTO `track` VALUES ('19775', 'front', '2019-01-08 01:04:24', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'e3577d011f3211b2dbb28fecefad8f3d17aa2395');
INSERT INTO `track` VALUES ('19776', 'front', '2019-01-08 01:05:02', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '0b703eb79632e90a8582aa81df0c158537a1b1be');
INSERT INTO `track` VALUES ('19777', 'front', '2019-01-08 01:05:43', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '0b703eb79632e90a8582aa81df0c158537a1b1be');
INSERT INTO `track` VALUES ('19778', 'front', '2019-01-08 01:08:50', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '0b703eb79632e90a8582aa81df0c158537a1b1be');
INSERT INTO `track` VALUES ('19779', 'front', '2019-01-08 01:09:22', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '0b703eb79632e90a8582aa81df0c158537a1b1be');
INSERT INTO `track` VALUES ('19780', 'front', '2019-01-08 01:09:45', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '0b703eb79632e90a8582aa81df0c158537a1b1be');
INSERT INTO `track` VALUES ('19781', 'front', '2019-01-08 01:10:10', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '856c18376f8fcb898685747eacac4540696815e0');
INSERT INTO `track` VALUES ('19782', 'front', '2019-01-08 01:11:14', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '856c18376f8fcb898685747eacac4540696815e0');
INSERT INTO `track` VALUES ('19783', 'front', '2019-01-08 01:12:25', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '856c18376f8fcb898685747eacac4540696815e0');
INSERT INTO `track` VALUES ('19784', 'front', '2019-01-08 01:13:05', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '856c18376f8fcb898685747eacac4540696815e0');
INSERT INTO `track` VALUES ('19785', 'front', '2019-01-08 01:13:30', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '856c18376f8fcb898685747eacac4540696815e0');
INSERT INTO `track` VALUES ('19786', 'front', '2019-01-08 01:14:02', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '856c18376f8fcb898685747eacac4540696815e0');
INSERT INTO `track` VALUES ('19787', 'front', '2019-01-08 01:14:19', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '856c18376f8fcb898685747eacac4540696815e0');
INSERT INTO `track` VALUES ('19788', 'front', '2019-01-08 01:23:15', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '1c20e86d79094f7fc1acd31199dccb208255fe3e');
INSERT INTO `track` VALUES ('19789', 'front', '2019-01-08 01:23:55', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '1c20e86d79094f7fc1acd31199dccb208255fe3e');
INSERT INTO `track` VALUES ('19790', 'front', '2019-01-08 01:24:07', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '1c20e86d79094f7fc1acd31199dccb208255fe3e');
INSERT INTO `track` VALUES ('19791', 'front', '2019-01-08 01:24:14', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '1c20e86d79094f7fc1acd31199dccb208255fe3e');
INSERT INTO `track` VALUES ('19792', 'front', '2019-01-08 01:24:25', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '1c20e86d79094f7fc1acd31199dccb208255fe3e');
INSERT INTO `track` VALUES ('19793', 'front', '2019-01-08 01:25:24', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '1c20e86d79094f7fc1acd31199dccb208255fe3e');
INSERT INTO `track` VALUES ('19794', 'front', '2019-01-08 01:28:14', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'ec1f0abb0b0c29068f6012e54fb39b8e303d4da7');
INSERT INTO `track` VALUES ('19795', 'front', '2019-01-08 01:36:33', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'f487baab43dbebac1f24cf0eb38114e67106b0a8');
INSERT INTO `track` VALUES ('19796', 'front', '2019-01-08 01:37:21', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'f487baab43dbebac1f24cf0eb38114e67106b0a8');
INSERT INTO `track` VALUES ('19797', 'front', '2019-01-08 01:37:42', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'f487baab43dbebac1f24cf0eb38114e67106b0a8');
INSERT INTO `track` VALUES ('19798', 'front', '2019-01-08 01:38:41', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'f487baab43dbebac1f24cf0eb38114e67106b0a8');
INSERT INTO `track` VALUES ('19799', 'front', '2019-01-08 01:55:05', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '5419fa0a6cf49efd8927ba725dc7381532e5aebb');
INSERT INTO `track` VALUES ('19800', 'front', '2019-01-08 01:55:58', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '5419fa0a6cf49efd8927ba725dc7381532e5aebb');
INSERT INTO `track` VALUES ('19801', 'front', '2019-01-08 02:13:31', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'f80c99a7ce30e6267b59a70fd5c506a3d94b0e31');
INSERT INTO `track` VALUES ('19802', 'front', '2019-01-08 02:13:35', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'f80c99a7ce30e6267b59a70fd5c506a3d94b0e31');
INSERT INTO `track` VALUES ('19803', 'front', '2019-01-08 02:13:42', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'f80c99a7ce30e6267b59a70fd5c506a3d94b0e31');
INSERT INTO `track` VALUES ('19804', 'front', '2019-01-08 02:14:42', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'f80c99a7ce30e6267b59a70fd5c506a3d94b0e31');
INSERT INTO `track` VALUES ('19805', 'front', '2019-01-08 02:14:46', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'f80c99a7ce30e6267b59a70fd5c506a3d94b0e31');
INSERT INTO `track` VALUES ('19806', 'front', '2019-01-08 02:14:51', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'f80c99a7ce30e6267b59a70fd5c506a3d94b0e31');
INSERT INTO `track` VALUES ('19807', 'front', '2019-01-08 02:14:53', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'f80c99a7ce30e6267b59a70fd5c506a3d94b0e31');
INSERT INTO `track` VALUES ('19808', 'front', '2019-01-08 02:15:54', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'f80c99a7ce30e6267b59a70fd5c506a3d94b0e31');
INSERT INTO `track` VALUES ('19809', 'front', '2019-01-08 02:15:55', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'f80c99a7ce30e6267b59a70fd5c506a3d94b0e31');
INSERT INTO `track` VALUES ('19810', 'front', '2019-01-08 02:17:03', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'f80c99a7ce30e6267b59a70fd5c506a3d94b0e31');
INSERT INTO `track` VALUES ('19811', 'front', '2019-01-08 02:17:29', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'f80c99a7ce30e6267b59a70fd5c506a3d94b0e31');
INSERT INTO `track` VALUES ('19812', 'front', '2019-01-08 02:17:45', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'f80c99a7ce30e6267b59a70fd5c506a3d94b0e31');
INSERT INTO `track` VALUES ('19813', 'front', '2019-01-08 02:18:26', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', 'f80c99a7ce30e6267b59a70fd5c506a3d94b0e31');
INSERT INTO `track` VALUES ('19814', 'front', '2019-01-08 02:19:05', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '5a9c7f5d78ba5468dfa7de27525456716474bed3');
INSERT INTO `track` VALUES ('19815', 'front', '2019-01-08 02:19:13', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '5a9c7f5d78ba5468dfa7de27525456716474bed3');
INSERT INTO `track` VALUES ('19816', 'front', '2019-01-08 02:19:17', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '5a9c7f5d78ba5468dfa7de27525456716474bed3');
INSERT INTO `track` VALUES ('19817', 'front', '2019-01-08 02:19:20', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '5a9c7f5d78ba5468dfa7de27525456716474bed3');
INSERT INTO `track` VALUES ('19818', 'front', '2019-01-08 02:20:17', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '5a9c7f5d78ba5468dfa7de27525456716474bed3');
INSERT INTO `track` VALUES ('19819', 'front', '2019-01-08 02:20:21', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '5a9c7f5d78ba5468dfa7de27525456716474bed3');
INSERT INTO `track` VALUES ('19820', 'front', '2019-01-08 02:21:30', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '5a9c7f5d78ba5468dfa7de27525456716474bed3');
INSERT INTO `track` VALUES ('19821', 'front', '2019-01-08 02:22:35', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '5a9c7f5d78ba5468dfa7de27525456716474bed3');
INSERT INTO `track` VALUES ('19822', 'front', '2019-01-08 02:23:02', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '5a9c7f5d78ba5468dfa7de27525456716474bed3');
INSERT INTO `track` VALUES ('19823', 'front', '2019-01-08 02:23:04', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '5a9c7f5d78ba5468dfa7de27525456716474bed3');
INSERT INTO `track` VALUES ('19824', 'front', '2019-01-08 02:24:26', 'home', 'index', '0', '23', '1', '', 'Android', '::1', '/_mebook/', '7f83712e8cf9a4fc827f20d93200374bd04093d2');
INSERT INTO `track` VALUES ('19825', 'front', '2019-01-08 02:24:41', 'home', 'index', '0', '23', '1', '', 'Android', '::1', '/_mebook/', '7f83712e8cf9a4fc827f20d93200374bd04093d2');
INSERT INTO `track` VALUES ('19826', 'front', '2019-01-08 02:24:58', 'home', 'index', '0', '23', '1', '', 'Android', '::1', '/_mebook/', '7f83712e8cf9a4fc827f20d93200374bd04093d2');
INSERT INTO `track` VALUES ('19827', 'front', '2019-01-08 02:25:19', 'home', 'index', '0', '23', '1', '', 'Android', '::1', '/_mebook/', '7f83712e8cf9a4fc827f20d93200374bd04093d2');
INSERT INTO `track` VALUES ('19828', 'front', '2019-01-08 02:26:14', 'home', 'index', '0', '23', '1', '', 'Android', '::1', '/_mebook/', '7f83712e8cf9a4fc827f20d93200374bd04093d2');
INSERT INTO `track` VALUES ('19829', 'front', '2019-01-08 02:26:17', 'home', 'index', '0', '23', '1', '', 'Android', '::1', '/_mebook/', '7f83712e8cf9a4fc827f20d93200374bd04093d2');
INSERT INTO `track` VALUES ('19830', 'front', '2019-01-08 02:27:28', 'home', 'index', '0', '23', '1', '', 'iOS', '::1', '/_mebook/', '7f83712e8cf9a4fc827f20d93200374bd04093d2');
INSERT INTO `track` VALUES ('19831', 'front', '2019-01-08 02:27:36', 'home', 'index', '0', '23', '1', '', 'Android', '::1', '/_mebook/', '7f83712e8cf9a4fc827f20d93200374bd04093d2');
INSERT INTO `track` VALUES ('19832', 'front', '2019-01-08 02:28:00', 'home', 'index', '0', '23', '1', '', 'Android', '::1', '/_mebook/', '7f83712e8cf9a4fc827f20d93200374bd04093d2');
INSERT INTO `track` VALUES ('19833', 'front', '2019-01-08 02:28:20', 'home', 'index', '0', '23', '1', '', 'Android', '::1', '/_mebook/', '7f83712e8cf9a4fc827f20d93200374bd04093d2');
INSERT INTO `track` VALUES ('19834', 'front', '2019-01-08 02:28:25', 'home', 'index', '0', '23', '1', '', 'Android', '::1', '/_mebook/', '7f83712e8cf9a4fc827f20d93200374bd04093d2');
INSERT INTO `track` VALUES ('19835', 'front', '2019-01-08 02:28:39', 'home', 'index', '0', '23', '1', '', 'Android', '::1', '/_mebook/', '7f83712e8cf9a4fc827f20d93200374bd04093d2');
INSERT INTO `track` VALUES ('19836', 'front', '2019-01-08 02:28:57', 'home', 'index', '0', '23', '1', '', 'Android', '::1', '/_mebook/', '7f83712e8cf9a4fc827f20d93200374bd04093d2');
INSERT INTO `track` VALUES ('19837', 'front', '2019-01-08 02:29:02', 'home', 'index', '0', '23', '1', '', 'Android', '::1', '/_mebook/', '7f83712e8cf9a4fc827f20d93200374bd04093d2');
INSERT INTO `track` VALUES ('19838', 'front', '2019-01-08 02:35:36', 'home', 'index', '0', '23', '1', '', 'Android', '::1', '/_mebook/', '1276ff1169df3c074bdbdadc67a5c8029083dbaa');
INSERT INTO `track` VALUES ('19839', 'front', '2019-01-08 02:36:56', 'home', 'index', '0', '23', '1', '', 'Android', '::1', '/_mebook/', '1276ff1169df3c074bdbdadc67a5c8029083dbaa');
INSERT INTO `track` VALUES ('19840', 'front', '2019-01-08 02:36:59', 'home', 'index', '0', '23', '1', '', 'Android', '::1', '/_mebook/', '1276ff1169df3c074bdbdadc67a5c8029083dbaa');
INSERT INTO `track` VALUES ('19841', 'front', '2019-01-08 02:37:04', 'home', 'index', '0', '23', '1', '', 'Android', '::1', '/_mebook/', '1276ff1169df3c074bdbdadc67a5c8029083dbaa');
INSERT INTO `track` VALUES ('19842', 'front', '2019-01-08 02:37:19', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '1276ff1169df3c074bdbdadc67a5c8029083dbaa');
INSERT INTO `track` VALUES ('19843', 'front', '2019-01-08 02:41:53', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '0ef31f0d6d120fc7ada74cc7010ae2d9f2a43e85');
INSERT INTO `track` VALUES ('19844', 'front', '2019-01-08 03:12:02', 'home', 'index', '0', '23', '0', '', 'Windows 10', '::1', '/_mebook/', '357fc389f6124c794a565bde730d058911b839f9');
INSERT INTO `track` VALUES ('19845', 'front', '2019-01-09 22:11:40', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '65bef64b762be97359188e4c584b19a93b92cb44');
INSERT INTO `track` VALUES ('19846', 'front', '2019-01-09 22:23:40', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '792ebf6c613f617553d67778a8c76be13e348dc3');
INSERT INTO `track` VALUES ('19847', 'front', '2019-01-09 22:24:50', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '792ebf6c613f617553d67778a8c76be13e348dc3');
INSERT INTO `track` VALUES ('19848', 'front', '2019-01-09 22:31:32', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'ec3c17ab07c5efc8e45de607f05c30f6b34c3816');
INSERT INTO `track` VALUES ('19849', 'front', '2019-01-09 22:31:45', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'ec3c17ab07c5efc8e45de607f05c30f6b34c3816');
INSERT INTO `track` VALUES ('19850', 'front', '2019-01-09 22:32:16', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'ec3c17ab07c5efc8e45de607f05c30f6b34c3816');
INSERT INTO `track` VALUES ('19851', 'front', '2019-01-09 22:32:23', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'ec3c17ab07c5efc8e45de607f05c30f6b34c3816');
INSERT INTO `track` VALUES ('19852', 'front', '2019-01-09 22:33:09', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'ec3c17ab07c5efc8e45de607f05c30f6b34c3816');
INSERT INTO `track` VALUES ('19853', 'front', '2019-01-09 22:33:31', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'ec3c17ab07c5efc8e45de607f05c30f6b34c3816');
INSERT INTO `track` VALUES ('19854', 'front', '2019-01-09 22:33:41', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'ec3c17ab07c5efc8e45de607f05c30f6b34c3816');
INSERT INTO `track` VALUES ('19855', 'front', '2019-01-09 23:15:57', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/config_general', 'Windows 10', '::1', '/_mebook/', 'de8290d86cbe86b92a886314572673723f929e3d');
INSERT INTO `track` VALUES ('19856', 'front', '2019-01-09 23:16:01', 'home', 'payment_method', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/home/payment_method', 'de8290d86cbe86b92a886314572673723f929e3d');
INSERT INTO `track` VALUES ('19857', 'front', '2019-01-09 23:16:23', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'de8290d86cbe86b92a886314572673723f929e3d');
INSERT INTO `track` VALUES ('19858', 'front', '2019-01-09 23:16:25', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', 'de8290d86cbe86b92a886314572673723f929e3d');
INSERT INTO `track` VALUES ('19859', 'front', '2019-01-09 23:16:43', 'home', 'payment_method', '0', null, '0', 'http://localhost:81/_mebook/course/detail/m', 'Windows 10', '::1', '/_mebook/home/payment_method', 'de8290d86cbe86b92a886314572673723f929e3d');
INSERT INTO `track` VALUES ('19860', 'front', '2019-01-09 23:31:57', 'home', 'payment_method', '0', null, '0', 'http://localhost:81/_mebook/course/detail/m', 'Windows 10', '::1', '/_mebook/home/payment_method', 'e0408b69d8fc325f94c6618918181459535642f2');
INSERT INTO `track` VALUES ('19861', 'front', '2019-01-09 23:37:57', 'home', 'payment_method', '0', null, '0', 'http://localhost:81/_mebook/course/detail/m', 'Windows 10', '::1', '/_mebook/home/payment_method', '0fd352a34e58fcfed995c9f9c1564a130364f44e');
INSERT INTO `track` VALUES ('19862', 'front', '2019-01-09 23:38:53', 'home', 'payment_method', '0', null, '0', 'http://localhost:81/_mebook/course/detail/m', 'Windows 10', '::1', '/_mebook/home/payment_method', '0fd352a34e58fcfed995c9f9c1564a130364f44e');
INSERT INTO `track` VALUES ('19863', 'front', '2019-01-09 23:40:01', 'home', 'payment_method', '0', null, '0', 'http://localhost:81/_mebook/course/detail/m', 'Windows 10', '::1', '/_mebook/home/payment_method', '0fd352a34e58fcfed995c9f9c1564a130364f44e');
INSERT INTO `track` VALUES ('19864', 'front', '2019-01-09 23:40:05', 'home', 'payment_method', '0', null, '0', 'http://localhost:81/_mebook/course/detail/m', 'Windows 10', '::1', '/_mebook/home/payment_method', '0fd352a34e58fcfed995c9f9c1564a130364f44e');
INSERT INTO `track` VALUES ('19865', 'front', '2019-01-09 23:41:13', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'd080a705a4daeb0511e7ca655c536d5cdd935b8e');
INSERT INTO `track` VALUES ('19866', 'front', '2019-01-09 23:46:28', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'd4766806b5d946dd1a2d95f82d5813f8b71e9aad');
INSERT INTO `track` VALUES ('19867', 'front', '2019-01-09 23:47:17', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'd4766806b5d946dd1a2d95f82d5813f8b71e9aad');
INSERT INTO `track` VALUES ('19868', 'front', '2019-01-09 23:48:07', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'd4766806b5d946dd1a2d95f82d5813f8b71e9aad');
INSERT INTO `track` VALUES ('19869', 'front', '2019-01-09 23:48:43', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'd4766806b5d946dd1a2d95f82d5813f8b71e9aad');
INSERT INTO `track` VALUES ('19870', 'front', '2019-01-09 23:48:46', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'd4766806b5d946dd1a2d95f82d5813f8b71e9aad');
INSERT INTO `track` VALUES ('19871', 'front', '2019-01-09 23:50:11', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'd4766806b5d946dd1a2d95f82d5813f8b71e9aad');
INSERT INTO `track` VALUES ('19872', 'front', '2019-01-09 23:50:53', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'd4766806b5d946dd1a2d95f82d5813f8b71e9aad');
INSERT INTO `track` VALUES ('19873', 'front', '2019-01-09 23:51:51', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '2a8796bf501ffa30a0610cb2ac15d173170e2cd7');
INSERT INTO `track` VALUES ('19874', 'front', '2019-01-09 23:51:56', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '2a8796bf501ffa30a0610cb2ac15d173170e2cd7');
INSERT INTO `track` VALUES ('19875', 'front', '2019-01-09 23:52:15', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '2a8796bf501ffa30a0610cb2ac15d173170e2cd7');
INSERT INTO `track` VALUES ('19876', 'front', '2019-01-09 23:53:15', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '2a8796bf501ffa30a0610cb2ac15d173170e2cd7');
INSERT INTO `track` VALUES ('19877', 'front', '2019-01-09 23:56:01', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '2a8796bf501ffa30a0610cb2ac15d173170e2cd7');
INSERT INTO `track` VALUES ('19878', 'front', '2019-01-10 00:00:38', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '01d384ba851991a6673574eb3b79c2c158f51dc9');
INSERT INTO `track` VALUES ('19879', 'front', '2019-01-10 00:00:47', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '01d384ba851991a6673574eb3b79c2c158f51dc9');
INSERT INTO `track` VALUES ('19880', 'front', '2019-01-10 00:02:23', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '01d384ba851991a6673574eb3b79c2c158f51dc9');
INSERT INTO `track` VALUES ('19881', 'front', '2019-01-10 00:04:19', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '01d384ba851991a6673574eb3b79c2c158f51dc9');
INSERT INTO `track` VALUES ('19882', 'front', '2019-01-10 00:05:04', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '01d384ba851991a6673574eb3b79c2c158f51dc9');
INSERT INTO `track` VALUES ('19883', 'front', '2019-01-10 00:05:09', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '01d384ba851991a6673574eb3b79c2c158f51dc9');
INSERT INTO `track` VALUES ('19884', 'front', '2019-01-10 00:05:18', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '01d384ba851991a6673574eb3b79c2c158f51dc9');
INSERT INTO `track` VALUES ('19885', 'front', '2019-01-10 00:06:30', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '1408a046c877fa94b16b3ce307d3506963ca97bf');
INSERT INTO `track` VALUES ('19886', 'front', '2019-01-10 00:06:32', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '1408a046c877fa94b16b3ce307d3506963ca97bf');
INSERT INTO `track` VALUES ('19887', 'front', '2019-01-10 00:07:43', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '1408a046c877fa94b16b3ce307d3506963ca97bf');
INSERT INTO `track` VALUES ('19888', 'front', '2019-01-10 00:08:22', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '1408a046c877fa94b16b3ce307d3506963ca97bf');
INSERT INTO `track` VALUES ('19889', 'front', '2019-01-10 00:31:33', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '5bc7453e0f07dcbac8e95184b15becbd6f10ceec');
INSERT INTO `track` VALUES ('19890', 'front', '2019-01-10 00:38:01', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'b23aacf40fa02b654c9199122f788be72dcc8ea0');
INSERT INTO `track` VALUES ('19891', 'front', '2019-01-10 00:38:22', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'b23aacf40fa02b654c9199122f788be72dcc8ea0');
INSERT INTO `track` VALUES ('19892', 'front', '2019-01-10 00:38:25', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'b23aacf40fa02b654c9199122f788be72dcc8ea0');
INSERT INTO `track` VALUES ('19893', 'front', '2019-01-10 00:38:38', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'b23aacf40fa02b654c9199122f788be72dcc8ea0');
INSERT INTO `track` VALUES ('19894', 'front', '2019-01-10 00:38:59', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'b23aacf40fa02b654c9199122f788be72dcc8ea0');
INSERT INTO `track` VALUES ('19895', 'front', '2019-01-10 00:39:03', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'b23aacf40fa02b654c9199122f788be72dcc8ea0');
INSERT INTO `track` VALUES ('19896', 'front', '2019-01-10 00:39:56', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'b23aacf40fa02b654c9199122f788be72dcc8ea0');
INSERT INTO `track` VALUES ('19897', 'front', '2019-01-10 00:41:11', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'b23aacf40fa02b654c9199122f788be72dcc8ea0');
INSERT INTO `track` VALUES ('19898', 'front', '2019-01-10 00:42:51', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'b23aacf40fa02b654c9199122f788be72dcc8ea0');
INSERT INTO `track` VALUES ('19899', 'front', '2019-01-10 11:14:31', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'bd4b9128b4e765118f142c051071d6d0273f2df5');
INSERT INTO `track` VALUES ('19900', 'front', '2019-01-10 11:15:45', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'bd4b9128b4e765118f142c051071d6d0273f2df5');
INSERT INTO `track` VALUES ('19901', 'front', '2019-01-10 11:39:16', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '10bb6407ada86acd8ce5ae7f8e1178e1aac095e5');
INSERT INTO `track` VALUES ('19902', 'front', '2019-01-10 16:08:06', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '5f1134751c39e7ebbc0dc22ec65621c47842bb9e');
INSERT INTO `track` VALUES ('19903', 'front', '2019-01-10 16:08:14', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '5f1134751c39e7ebbc0dc22ec65621c47842bb9e');
INSERT INTO `track` VALUES ('19904', 'front', '2019-01-10 16:09:27', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '5f1134751c39e7ebbc0dc22ec65621c47842bb9e');
INSERT INTO `track` VALUES ('19905', 'front', '2019-01-10 16:33:57', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'eaa493d155aaaf9e634a24fb3f2ae4aa1023de20');
INSERT INTO `track` VALUES ('19906', 'front', '2019-01-10 16:35:01', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'eaa493d155aaaf9e634a24fb3f2ae4aa1023de20');
INSERT INTO `track` VALUES ('19907', 'front', '2019-01-10 16:36:49', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'eaa493d155aaaf9e634a24fb3f2ae4aa1023de20');
INSERT INTO `track` VALUES ('19908', 'front', '2019-01-10 16:38:11', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'eaa493d155aaaf9e634a24fb3f2ae4aa1023de20');
INSERT INTO `track` VALUES ('19909', 'front', '2019-01-10 16:38:14', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'eaa493d155aaaf9e634a24fb3f2ae4aa1023de20');
INSERT INTO `track` VALUES ('19910', 'front', '2019-01-10 16:38:17', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'eaa493d155aaaf9e634a24fb3f2ae4aa1023de20');
INSERT INTO `track` VALUES ('19911', 'front', '2019-01-10 16:39:23', 'home', 'index', '0', null, '1', '', 'Android', '::1', '/_mebook/', '82eef9174280b40e66d2bf7442d6ae80a7a839fb');
INSERT INTO `track` VALUES ('19912', 'front', '2019-01-10 16:43:29', 'home', 'index', '0', null, '1', '', 'Android', '::1', '/_mebook/', '82eef9174280b40e66d2bf7442d6ae80a7a839fb');
INSERT INTO `track` VALUES ('19913', 'front', '2019-01-10 16:46:16', 'home', 'index', '0', null, '1', '', 'Android', '::1', '/_mebook/', '77c3bf5f46a5903cfe0d38afea55df6d99bd90f0');
INSERT INTO `track` VALUES ('19914', 'front', '2019-01-10 16:46:29', 'home', 'index', '0', null, '1', '', 'Android', '::1', '/_mebook/', '77c3bf5f46a5903cfe0d38afea55df6d99bd90f0');
INSERT INTO `track` VALUES ('19915', 'front', '2019-01-10 16:47:19', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '77c3bf5f46a5903cfe0d38afea55df6d99bd90f0');
INSERT INTO `track` VALUES ('19916', 'front', '2019-01-10 16:50:29', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '77c3bf5f46a5903cfe0d38afea55df6d99bd90f0');
INSERT INTO `track` VALUES ('19917', 'front', '2019-01-11 00:01:20', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'bf8583db9756923662eb482956207dfd2d82f8be');
INSERT INTO `track` VALUES ('19918', 'front', '2019-01-11 00:07:06', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/course/edit/YjY0NUA3Nw==', 'Windows 10', '::1', '/_mebook/', '1c8cc864429e0f74f01f30ce4aaa097d08def340');
INSERT INTO `track` VALUES ('19919', 'front', '2019-01-11 00:09:41', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/course/edit/YjY0NUA3Nw==', 'Windows 10', '::1', '/_mebook/', '1c8cc864429e0f74f01f30ce4aaa097d08def340');
INSERT INTO `track` VALUES ('19920', 'front', '2019-01-11 00:09:44', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/aaaa', '1c8cc864429e0f74f01f30ce4aaa097d08def340');
INSERT INTO `track` VALUES ('19921', 'front', '2019-01-11 00:10:59', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/aaaa', '1c8cc864429e0f74f01f30ce4aaa097d08def340');
INSERT INTO `track` VALUES ('19922', 'front', '2019-01-11 00:13:08', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/aaaa', 'Windows 10', '::1', '/_mebook/', '9c8cdf7a5c891228a98c1d936b71d4b0caef8e54');
INSERT INTO `track` VALUES ('19923', 'front', '2019-01-11 00:15:36', 'course', 'detail', '4', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/m', '9c8cdf7a5c891228a98c1d936b71d4b0caef8e54');
INSERT INTO `track` VALUES ('19924', 'front', '2019-01-11 00:15:58', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/m', 'Windows 10', '::1', '/_mebook/', '9c8cdf7a5c891228a98c1d936b71d4b0caef8e54');
INSERT INTO `track` VALUES ('19925', 'front', '2019-01-11 00:16:03', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/aaaa', '9c8cdf7a5c891228a98c1d936b71d4b0caef8e54');
INSERT INTO `track` VALUES ('19926', 'front', '2019-01-11 00:21:38', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/aaaa', 'Windows 10', '::1', '/_mebook/', 'df1cb30f82d136e69e5a0afda464d5f241b1e57d');
INSERT INTO `track` VALUES ('19927', 'front', '2019-01-11 00:22:02', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'df1cb30f82d136e69e5a0afda464d5f241b1e57d');
INSERT INTO `track` VALUES ('19928', 'front', '2019-01-11 00:24:05', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '1ce398aea25b867a977811c197a802eb3d1d04a9');
INSERT INTO `track` VALUES ('19929', 'front', '2019-01-11 00:26:43', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '1ce398aea25b867a977811c197a802eb3d1d04a9');
INSERT INTO `track` VALUES ('19930', 'front', '2019-01-11 00:27:09', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '1ce398aea25b867a977811c197a802eb3d1d04a9');
INSERT INTO `track` VALUES ('19931', 'front', '2019-01-11 00:27:54', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '1ce398aea25b867a977811c197a802eb3d1d04a9');
INSERT INTO `track` VALUES ('19932', 'front', '2019-01-11 00:29:17', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '9f4f5b5c56fde014fa67ad7d7936d321c58af824');
INSERT INTO `track` VALUES ('19933', 'front', '2019-01-11 00:30:39', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '9f4f5b5c56fde014fa67ad7d7936d321c58af824');
INSERT INTO `track` VALUES ('19934', 'front', '2019-01-11 00:31:07', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '9f4f5b5c56fde014fa67ad7d7936d321c58af824');
INSERT INTO `track` VALUES ('19935', 'front', '2019-01-11 00:31:09', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '9f4f5b5c56fde014fa67ad7d7936d321c58af824');
INSERT INTO `track` VALUES ('19936', 'front', '2019-01-11 00:31:30', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '9f4f5b5c56fde014fa67ad7d7936d321c58af824');
INSERT INTO `track` VALUES ('19937', 'front', '2019-01-11 00:31:35', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '9f4f5b5c56fde014fa67ad7d7936d321c58af824');
INSERT INTO `track` VALUES ('19938', 'front', '2019-01-11 00:33:31', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '9f4f5b5c56fde014fa67ad7d7936d321c58af824');
INSERT INTO `track` VALUES ('19939', 'front', '2019-01-11 00:37:07', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '1c5caa11c4d6e15a71b55cea17c2de7cdda02851');
INSERT INTO `track` VALUES ('19940', 'front', '2019-01-11 00:37:10', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '1c5caa11c4d6e15a71b55cea17c2de7cdda02851');
INSERT INTO `track` VALUES ('19941', 'front', '2019-01-11 00:37:13', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '1c5caa11c4d6e15a71b55cea17c2de7cdda02851');
INSERT INTO `track` VALUES ('19942', 'front', '2019-01-11 00:37:15', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '1c5caa11c4d6e15a71b55cea17c2de7cdda02851');
INSERT INTO `track` VALUES ('19943', 'front', '2019-01-11 00:37:18', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '1c5caa11c4d6e15a71b55cea17c2de7cdda02851');
INSERT INTO `track` VALUES ('19944', 'front', '2019-01-11 00:40:09', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '1c5caa11c4d6e15a71b55cea17c2de7cdda02851');
INSERT INTO `track` VALUES ('19945', 'front', '2019-01-11 00:40:11', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '1c5caa11c4d6e15a71b55cea17c2de7cdda02851');
INSERT INTO `track` VALUES ('19946', 'front', '2019-01-11 00:40:20', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '1c5caa11c4d6e15a71b55cea17c2de7cdda02851');
INSERT INTO `track` VALUES ('19947', 'front', '2019-01-11 00:42:07', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '4d2521b64933bd61f127c8379b5555b1824ad61f');
INSERT INTO `track` VALUES ('19948', 'front', '2019-01-11 00:42:51', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '4d2521b64933bd61f127c8379b5555b1824ad61f');
INSERT INTO `track` VALUES ('19949', 'front', '2019-01-11 00:43:10', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '4d2521b64933bd61f127c8379b5555b1824ad61f');
INSERT INTO `track` VALUES ('19950', 'front', '2019-01-11 00:43:28', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '4d2521b64933bd61f127c8379b5555b1824ad61f');
INSERT INTO `track` VALUES ('19951', 'front', '2019-01-11 00:43:41', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '4d2521b64933bd61f127c8379b5555b1824ad61f');
INSERT INTO `track` VALUES ('19952', 'front', '2019-01-11 00:44:04', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '4d2521b64933bd61f127c8379b5555b1824ad61f');
INSERT INTO `track` VALUES ('19953', 'front', '2019-01-11 00:44:44', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '4d2521b64933bd61f127c8379b5555b1824ad61f');
INSERT INTO `track` VALUES ('19954', 'front', '2019-01-11 00:47:37', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '5cf6ba374ea4401b841376573d6ba626081767aa');
INSERT INTO `track` VALUES ('19955', 'front', '2019-01-11 00:49:31', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '5cf6ba374ea4401b841376573d6ba626081767aa');
INSERT INTO `track` VALUES ('19956', 'front', '2019-01-11 00:50:16', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '5cf6ba374ea4401b841376573d6ba626081767aa');
INSERT INTO `track` VALUES ('19957', 'front', '2019-01-11 00:50:45', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '5cf6ba374ea4401b841376573d6ba626081767aa');
INSERT INTO `track` VALUES ('19958', 'front', '2019-01-11 00:50:48', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '5cf6ba374ea4401b841376573d6ba626081767aa');
INSERT INTO `track` VALUES ('19959', 'front', '2019-01-11 00:52:23', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '5cf6ba374ea4401b841376573d6ba626081767aa');
INSERT INTO `track` VALUES ('19960', 'front', '2019-01-11 00:55:02', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '47159bbd2b80ef98a0d5992088fc359043168a9b');
INSERT INTO `track` VALUES ('19961', 'front', '2019-01-11 00:56:40', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '47159bbd2b80ef98a0d5992088fc359043168a9b');
INSERT INTO `track` VALUES ('19962', 'front', '2019-01-11 00:57:00', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '47159bbd2b80ef98a0d5992088fc359043168a9b');
INSERT INTO `track` VALUES ('19963', 'front', '2019-01-11 00:57:57', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '47159bbd2b80ef98a0d5992088fc359043168a9b');
INSERT INTO `track` VALUES ('19964', 'front', '2019-01-11 00:58:40', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '47159bbd2b80ef98a0d5992088fc359043168a9b');
INSERT INTO `track` VALUES ('19965', 'front', '2019-01-11 00:59:13', 'course', 'detail', '5', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '47159bbd2b80ef98a0d5992088fc359043168a9b');
INSERT INTO `track` VALUES ('19966', 'front', '2019-01-11 00:59:38', 'course', 'detail', '5', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '47159bbd2b80ef98a0d5992088fc359043168a9b');
INSERT INTO `track` VALUES ('19967', 'front', '2019-01-11 01:00:33', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '7d89a7b434c28fb16a271fb5f87c03d69935025e');
INSERT INTO `track` VALUES ('19968', 'front', '2019-01-11 01:01:34', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', '7d89a7b434c28fb16a271fb5f87c03d69935025e');
INSERT INTO `track` VALUES ('19969', 'front', '2019-01-11 01:05:18', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', '7d89a7b434c28fb16a271fb5f87c03d69935025e');
INSERT INTO `track` VALUES ('19970', 'front', '2019-01-11 01:06:25', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'd5f0fcdf6806cb982ff9d487c8538e5a8a3a737b');
INSERT INTO `track` VALUES ('19971', 'front', '2019-01-11 01:06:37', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'd5f0fcdf6806cb982ff9d487c8538e5a8a3a737b');
INSERT INTO `track` VALUES ('19972', 'front', '2019-01-11 01:07:23', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'd5f0fcdf6806cb982ff9d487c8538e5a8a3a737b');
INSERT INTO `track` VALUES ('19973', 'front', '2019-01-11 01:07:26', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'd5f0fcdf6806cb982ff9d487c8538e5a8a3a737b');
INSERT INTO `track` VALUES ('19974', 'front', '2019-01-11 01:08:14', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'd5f0fcdf6806cb982ff9d487c8538e5a8a3a737b');
INSERT INTO `track` VALUES ('19975', 'front', '2019-01-11 01:08:27', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'd5f0fcdf6806cb982ff9d487c8538e5a8a3a737b');
INSERT INTO `track` VALUES ('19976', 'front', '2019-01-11 01:08:53', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'd5f0fcdf6806cb982ff9d487c8538e5a8a3a737b');
INSERT INTO `track` VALUES ('19977', 'front', '2019-01-11 01:10:05', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'd5f0fcdf6806cb982ff9d487c8538e5a8a3a737b');
INSERT INTO `track` VALUES ('19978', 'front', '2019-01-11 01:10:39', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'd5f0fcdf6806cb982ff9d487c8538e5a8a3a737b');
INSERT INTO `track` VALUES ('19979', 'front', '2019-01-11 01:12:05', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'e6ccfe393133417e0479e8199a53fd6d01ca3d20');
INSERT INTO `track` VALUES ('19980', 'front', '2019-01-11 01:12:47', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'e6ccfe393133417e0479e8199a53fd6d01ca3d20');
INSERT INTO `track` VALUES ('19981', 'front', '2019-01-11 01:12:56', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'e6ccfe393133417e0479e8199a53fd6d01ca3d20');
INSERT INTO `track` VALUES ('19982', 'front', '2019-01-11 01:13:42', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'e6ccfe393133417e0479e8199a53fd6d01ca3d20');
INSERT INTO `track` VALUES ('19983', 'front', '2019-01-11 01:13:54', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'e6ccfe393133417e0479e8199a53fd6d01ca3d20');
INSERT INTO `track` VALUES ('19984', 'front', '2019-01-11 01:15:11', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'e6ccfe393133417e0479e8199a53fd6d01ca3d20');
INSERT INTO `track` VALUES ('19985', 'front', '2019-01-11 01:16:36', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'e6ccfe393133417e0479e8199a53fd6d01ca3d20');
INSERT INTO `track` VALUES ('19986', 'front', '2019-01-11 01:17:07', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '7bbd94223dfbf9a95930066d000b24400cd2a80f');
INSERT INTO `track` VALUES ('19987', 'front', '2019-01-11 01:20:02', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '7bbd94223dfbf9a95930066d000b24400cd2a80f');
INSERT INTO `track` VALUES ('19988', 'front', '2019-01-11 01:20:48', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '7bbd94223dfbf9a95930066d000b24400cd2a80f');
INSERT INTO `track` VALUES ('19989', 'front', '2019-01-11 01:21:22', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', '7bbd94223dfbf9a95930066d000b24400cd2a80f');
INSERT INTO `track` VALUES ('19990', 'front', '2019-01-11 01:24:06', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'd5792e1955a5acc7f6d1c2fb2cab6251a5c18b91');
INSERT INTO `track` VALUES ('19991', 'front', '2019-01-11 01:28:20', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'd5792e1955a5acc7f6d1c2fb2cab6251a5c18b91');
INSERT INTO `track` VALUES ('19992', 'front', '2019-01-11 01:29:04', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'd5792e1955a5acc7f6d1c2fb2cab6251a5c18b91');
INSERT INTO `track` VALUES ('19993', 'front', '2019-01-11 01:30:50', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '719813c7a084592208e6c7c2ef6eb9751e32f5d0');
INSERT INTO `track` VALUES ('19994', 'front', '2019-01-11 01:31:27', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '719813c7a084592208e6c7c2ef6eb9751e32f5d0');
INSERT INTO `track` VALUES ('19995', 'front', '2019-01-11 01:44:39', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'a457a8537237156fac2e842f38b25ca7cf57b2b8');
INSERT INTO `track` VALUES ('19996', 'front', '2019-01-11 01:48:22', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'a457a8537237156fac2e842f38b25ca7cf57b2b8');
INSERT INTO `track` VALUES ('19997', 'front', '2019-01-11 01:49:05', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'a457a8537237156fac2e842f38b25ca7cf57b2b8');
INSERT INTO `track` VALUES ('19998', 'front', '2019-01-11 01:49:14', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'a457a8537237156fac2e842f38b25ca7cf57b2b8');
INSERT INTO `track` VALUES ('19999', 'front', '2019-01-11 01:49:30', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'a457a8537237156fac2e842f38b25ca7cf57b2b8');
INSERT INTO `track` VALUES ('20000', 'front', '2019-01-11 01:49:55', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '5c404abc66301eeb544102da2ab81c5c6cd380f0');
INSERT INTO `track` VALUES ('20001', 'front', '2019-01-11 02:03:11', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', '4b263990bda1b526ea518a2721f2804ac503880c');
INSERT INTO `track` VALUES ('20002', 'front', '2019-01-11 02:03:14', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', '4b263990bda1b526ea518a2721f2804ac503880c');
INSERT INTO `track` VALUES ('20003', 'front', '2019-01-11 02:10:53', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '519a32e2b88761ecd896d5c190d54da4a5edf555');
INSERT INTO `track` VALUES ('20004', 'front', '2019-01-11 02:16:14', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '485f727c1ddd10f2fcc2ab3b07e809be907b75d8');
INSERT INTO `track` VALUES ('20005', 'front', '2019-01-11 02:16:35', 'course', 'detail', '5', '26', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '485f727c1ddd10f2fcc2ab3b07e809be907b75d8');
INSERT INTO `track` VALUES ('20006', 'front', '2019-01-11 02:16:51', 'course', 'detail', '5', '26', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '485f727c1ddd10f2fcc2ab3b07e809be907b75d8');
INSERT INTO `track` VALUES ('20007', 'front', '2019-01-11 02:18:56', 'course', 'detail', '5', '26', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '485f727c1ddd10f2fcc2ab3b07e809be907b75d8');
INSERT INTO `track` VALUES ('20008', 'front', '2019-01-11 02:19:16', 'course', 'detail', '5', '26', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '485f727c1ddd10f2fcc2ab3b07e809be907b75d8');
INSERT INTO `track` VALUES ('20009', 'front', '2019-01-11 02:19:19', 'course', 'detail', '5', '26', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '485f727c1ddd10f2fcc2ab3b07e809be907b75d8');
INSERT INTO `track` VALUES ('20010', 'front', '2019-01-11 02:26:21', 'home', 'index', '0', '26', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'dc9e4de3b447e9fb8514ed9521e3292d7156507f');
INSERT INTO `track` VALUES ('20011', 'front', '2019-01-11 02:27:34', 'home', 'index', '0', '26', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'dc9e4de3b447e9fb8514ed9521e3292d7156507f');
INSERT INTO `track` VALUES ('20012', 'front', '2019-01-11 02:27:35', 'home', 'index', '0', '26', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'dc9e4de3b447e9fb8514ed9521e3292d7156507f');
INSERT INTO `track` VALUES ('20013', 'front', '2019-01-11 02:27:35', 'home', 'index', '0', '26', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'dc9e4de3b447e9fb8514ed9521e3292d7156507f');
INSERT INTO `track` VALUES ('20014', 'front', '2019-01-11 02:27:35', 'home', 'index', '0', '26', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'dc9e4de3b447e9fb8514ed9521e3292d7156507f');
INSERT INTO `track` VALUES ('20015', 'front', '2019-01-11 02:27:35', 'home', 'index', '0', '26', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'dc9e4de3b447e9fb8514ed9521e3292d7156507f');
INSERT INTO `track` VALUES ('20016', 'front', '2019-01-11 02:27:35', 'home', 'index', '0', '26', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'dc9e4de3b447e9fb8514ed9521e3292d7156507f');
INSERT INTO `track` VALUES ('20017', 'front', '2019-01-11 02:28:01', 'course', 'detail', '5', '26', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'dc9e4de3b447e9fb8514ed9521e3292d7156507f');
INSERT INTO `track` VALUES ('20018', 'front', '2019-01-11 02:28:35', 'home', 'index', '0', '26', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'dc9e4de3b447e9fb8514ed9521e3292d7156507f');
INSERT INTO `track` VALUES ('20019', 'front', '2019-01-11 13:13:09', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '4f52c89ddc545da1080a6e770b9c130f83eead42');
INSERT INTO `track` VALUES ('20020', 'front', '2019-01-11 13:13:18', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '4f52c89ddc545da1080a6e770b9c130f83eead42');
INSERT INTO `track` VALUES ('20021', 'front', '2019-01-11 13:14:49', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '4f52c89ddc545da1080a6e770b9c130f83eead42');
INSERT INTO `track` VALUES ('20022', 'front', '2019-01-11 13:18:30', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '73c54c624a4d4ab4df47bfd0abaae7c10d586552');
INSERT INTO `track` VALUES ('20023', 'front', '2019-01-11 13:19:21', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '73c54c624a4d4ab4df47bfd0abaae7c10d586552');
INSERT INTO `track` VALUES ('20024', 'front', '2019-01-11 13:20:38', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '73c54c624a4d4ab4df47bfd0abaae7c10d586552');
INSERT INTO `track` VALUES ('20025', 'front', '2019-01-11 13:27:36', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'c8da8f4290092f2f7a3055732b126778029daf12');
INSERT INTO `track` VALUES ('20026', 'front', '2019-01-11 13:28:02', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'c8da8f4290092f2f7a3055732b126778029daf12');
INSERT INTO `track` VALUES ('20027', 'front', '2019-01-11 13:28:11', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'c8da8f4290092f2f7a3055732b126778029daf12');
INSERT INTO `track` VALUES ('20028', 'front', '2019-01-11 13:29:04', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'c8da8f4290092f2f7a3055732b126778029daf12');
INSERT INTO `track` VALUES ('20029', 'front', '2019-01-11 13:30:13', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'c8da8f4290092f2f7a3055732b126778029daf12');
INSERT INTO `track` VALUES ('20030', 'front', '2019-01-11 13:30:32', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'c8da8f4290092f2f7a3055732b126778029daf12');
INSERT INTO `track` VALUES ('20031', 'front', '2019-01-11 13:30:34', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'c8da8f4290092f2f7a3055732b126778029daf12');
INSERT INTO `track` VALUES ('20032', 'front', '2019-01-11 13:30:35', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'c8da8f4290092f2f7a3055732b126778029daf12');
INSERT INTO `track` VALUES ('20033', 'front', '2019-01-11 13:31:44', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'c8da8f4290092f2f7a3055732b126778029daf12');
INSERT INTO `track` VALUES ('20034', 'front', '2019-01-11 13:32:53', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'f7653317780ba75d60f263cf631952748dc48e8e');
INSERT INTO `track` VALUES ('20035', 'front', '2019-01-11 13:33:06', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'f7653317780ba75d60f263cf631952748dc48e8e');
INSERT INTO `track` VALUES ('20036', 'front', '2019-01-11 13:34:53', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'f7653317780ba75d60f263cf631952748dc48e8e');
INSERT INTO `track` VALUES ('20037', 'front', '2019-01-11 13:35:47', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'f7653317780ba75d60f263cf631952748dc48e8e');
INSERT INTO `track` VALUES ('20038', 'front', '2019-01-11 13:36:12', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'f7653317780ba75d60f263cf631952748dc48e8e');
INSERT INTO `track` VALUES ('20039', 'front', '2019-01-11 13:36:27', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'f7653317780ba75d60f263cf631952748dc48e8e');
INSERT INTO `track` VALUES ('20040', 'front', '2019-01-11 13:37:09', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'f7653317780ba75d60f263cf631952748dc48e8e');
INSERT INTO `track` VALUES ('20041', 'front', '2019-01-11 13:47:58', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '969c35d165c89d091886a6de5d647d8dc6d18d5d');
INSERT INTO `track` VALUES ('20042', 'front', '2019-01-11 13:49:00', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', '969c35d165c89d091886a6de5d647d8dc6d18d5d');
INSERT INTO `track` VALUES ('20043', 'front', '2019-01-11 13:49:11', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '969c35d165c89d091886a6de5d647d8dc6d18d5d');
INSERT INTO `track` VALUES ('20044', 'front', '2019-01-11 13:49:15', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '969c35d165c89d091886a6de5d647d8dc6d18d5d');
INSERT INTO `track` VALUES ('20045', 'front', '2019-01-11 13:49:41', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', '969c35d165c89d091886a6de5d647d8dc6d18d5d');
INSERT INTO `track` VALUES ('20046', 'front', '2019-01-11 13:57:08', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'faec7c26aeca1425ea4049a0712e4e36f3dfc99b');
INSERT INTO `track` VALUES ('20047', 'front', '2019-01-11 13:57:11', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'faec7c26aeca1425ea4049a0712e4e36f3dfc99b');
INSERT INTO `track` VALUES ('20048', 'front', '2019-01-11 14:42:00', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'd64758b59cffc4887659f531cedd6ffffbfeef79');
INSERT INTO `track` VALUES ('20049', 'front', '2019-01-11 14:42:05', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'd64758b59cffc4887659f531cedd6ffffbfeef79');
INSERT INTO `track` VALUES ('20050', 'front', '2019-01-11 14:43:55', 'home', 'payment_method', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/home/payment_method', 'd64758b59cffc4887659f531cedd6ffffbfeef79');
INSERT INTO `track` VALUES ('20051', 'front', '2019-01-11 14:50:58', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '8e6cc6713c04b1d796604c3d3f9f01f0c5fe5c0e');
INSERT INTO `track` VALUES ('20052', 'front', '2019-01-11 14:53:08', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '8e6cc6713c04b1d796604c3d3f9f01f0c5fe5c0e');
INSERT INTO `track` VALUES ('20053', 'front', '2019-01-11 14:54:25', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '8e6cc6713c04b1d796604c3d3f9f01f0c5fe5c0e');
INSERT INTO `track` VALUES ('20054', 'front', '2019-01-11 14:55:03', 'course', 'detail', '5', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '8e6cc6713c04b1d796604c3d3f9f01f0c5fe5c0e');
INSERT INTO `track` VALUES ('20055', 'front', '2019-01-11 14:55:28', 'course', 'detail', '5', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '8e6cc6713c04b1d796604c3d3f9f01f0c5fe5c0e');
INSERT INTO `track` VALUES ('20056', 'front', '2019-01-11 14:55:38', 'course', 'detail', '5', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '8e6cc6713c04b1d796604c3d3f9f01f0c5fe5c0e');
INSERT INTO `track` VALUES ('20057', 'front', '2019-01-11 14:56:00', 'course', 'detail', '5', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '3a1a318640c498110920b77e644acbf235020a8b');
INSERT INTO `track` VALUES ('20058', 'front', '2019-01-11 14:56:16', 'course', 'detail', '5', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '3a1a318640c498110920b77e644acbf235020a8b');
INSERT INTO `track` VALUES ('20059', 'front', '2019-01-11 14:56:31', 'course', 'detail', '5', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '3a1a318640c498110920b77e644acbf235020a8b');
INSERT INTO `track` VALUES ('20060', 'front', '2019-01-11 14:56:43', 'course', 'detail', '5', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '3a1a318640c498110920b77e644acbf235020a8b');
INSERT INTO `track` VALUES ('20061', 'front', '2019-01-11 14:56:49', 'course', 'detail', '5', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '3a1a318640c498110920b77e644acbf235020a8b');
INSERT INTO `track` VALUES ('20062', 'front', '2019-01-11 14:57:51', 'course', 'detail', '5', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '3a1a318640c498110920b77e644acbf235020a8b');
INSERT INTO `track` VALUES ('20063', 'front', '2019-01-11 14:58:29', 'course', 'detail', '5', null, '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '3a1a318640c498110920b77e644acbf235020a8b');
INSERT INTO `track` VALUES ('20064', 'front', '2019-01-11 14:59:23', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '3a1a318640c498110920b77e644acbf235020a8b');
INSERT INTO `track` VALUES ('20065', 'front', '2019-01-11 15:00:04', 'course', 'detail', '5', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '3a1a318640c498110920b77e644acbf235020a8b');
INSERT INTO `track` VALUES ('20066', 'front', '2019-01-11 15:00:17', 'course', 'detail', '5', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '3a1a318640c498110920b77e644acbf235020a8b');
INSERT INTO `track` VALUES ('20067', 'front', '2019-01-11 15:01:41', 'course', 'detail', '5', null, '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '6210151c2801db31fd3789df290aeb0044419a85');
INSERT INTO `track` VALUES ('20068', 'front', '2019-01-11 15:02:06', 'course', 'detail', '5', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '6210151c2801db31fd3789df290aeb0044419a85');
INSERT INTO `track` VALUES ('20069', 'front', '2019-01-11 15:02:17', 'course', 'detail', '5', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '6210151c2801db31fd3789df290aeb0044419a85');
INSERT INTO `track` VALUES ('20070', 'front', '2019-01-11 15:02:23', 'course', 'detail', '5', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '6210151c2801db31fd3789df290aeb0044419a85');
INSERT INTO `track` VALUES ('20071', 'front', '2019-01-11 15:03:38', 'course', 'detail', '5', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '6210151c2801db31fd3789df290aeb0044419a85');
INSERT INTO `track` VALUES ('20072', 'front', '2019-01-11 15:09:37', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'aad422b53e734f5ac578d4adf20a988a5a9926ff');
INSERT INTO `track` VALUES ('20073', 'front', '2019-01-11 22:17:38', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '7d531c2196c0ac059337bf5cb03add67db5d930b');
INSERT INTO `track` VALUES ('20074', 'front', '2019-01-11 23:23:37', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/member/edit/YjY0NDFANzA=', 'Windows 10', '::1', '/_mebook/', '2032dd06588d485a81aeae06490c4e25518749b7');
INSERT INTO `track` VALUES ('20075', 'front', '2019-01-11 23:33:05', 'home', 'index', '0', '48', '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/home', '6d31edc6e6b623c601a2ec0483bad6aed9d0b920');
INSERT INTO `track` VALUES ('20076', 'front', '2019-01-12 00:13:18', 'home', 'index', '0', '48', '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/', '0d4ad7e93bbfcf6b03473c8aecb67707810e0aac');
INSERT INTO `track` VALUES ('20077', 'front', '2019-01-12 00:52:44', 'home', 'index', '0', '48', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '7a5a30d3b59a0dd35a5fb40d65a91a792b2ee72f');
INSERT INTO `track` VALUES ('20078', 'front', '2019-01-12 00:55:20', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/home', '7a5a30d3b59a0dd35a5fb40d65a91a792b2ee72f');
INSERT INTO `track` VALUES ('20079', 'front', '2019-01-12 00:55:56', 'home', 'index', '0', '49', '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/home', '7a5a30d3b59a0dd35a5fb40d65a91a792b2ee72f');
INSERT INTO `track` VALUES ('20080', 'front', '2019-01-12 00:57:10', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/home', '7a5a30d3b59a0dd35a5fb40d65a91a792b2ee72f');
INSERT INTO `track` VALUES ('20081', 'front', '2019-01-12 00:57:46', 'home', 'index', '0', '50', '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/home', '96352d076639baebcecf8da260f9272ffd3ad57b');
INSERT INTO `track` VALUES ('20082', 'front', '2019-01-12 00:59:52', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/home', '96352d076639baebcecf8da260f9272ffd3ad57b');
INSERT INTO `track` VALUES ('20083', 'front', '2019-01-12 01:00:28', 'home', 'index', '0', '51', '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/home', '96352d076639baebcecf8da260f9272ffd3ad57b');
INSERT INTO `track` VALUES ('20084', 'front', '2019-01-12 01:03:07', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/home', '386094b478b9ba395aff3379fcff8176046b2b12');
INSERT INTO `track` VALUES ('20085', 'front', '2019-01-12 01:03:40', 'home', 'index', '0', '52', '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/home', '386094b478b9ba395aff3379fcff8176046b2b12');
INSERT INTO `track` VALUES ('20086', 'front', '2019-01-12 01:04:00', 'home', 'index', '0', '52', '0', 'http://localhost:81/_mebook/user/profile/', 'Windows 10', '::1', '/_mebook/', '386094b478b9ba395aff3379fcff8176046b2b12');
INSERT INTO `track` VALUES ('20087', 'front', '2019-01-12 01:07:01', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/home', '386094b478b9ba395aff3379fcff8176046b2b12');
INSERT INTO `track` VALUES ('20088', 'front', '2019-01-12 01:07:34', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/home', '386094b478b9ba395aff3379fcff8176046b2b12');
INSERT INTO `track` VALUES ('20089', 'front', '2019-01-12 01:11:01', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'Android', '::1', '/_mebook/home', '6f89f1445db3530a7b54c238e6b15fa34a2fda75');
INSERT INTO `track` VALUES ('20090', 'front', '2019-01-12 01:11:09', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/home', 'Android', '::1', '/_mebook/', '6f89f1445db3530a7b54c238e6b15fa34a2fda75');
INSERT INTO `track` VALUES ('20091', 'front', '2019-01-12 01:11:25', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '6f89f1445db3530a7b54c238e6b15fa34a2fda75');
INSERT INTO `track` VALUES ('20092', 'front', '2019-01-12 01:35:15', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/member', 'Windows 10', '::1', '/_mebook/', '4050ea812b956e9b1daa7c3e266f67e93e524984');
INSERT INTO `track` VALUES ('20093', 'front', '2019-01-12 01:35:51', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/home', '4050ea812b956e9b1daa7c3e266f67e93e524984');
INSERT INTO `track` VALUES ('20094', 'front', '2019-01-12 01:46:39', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register/', 'Windows 10', '::1', '/_mebook/home', '146a0787c082c4f2c0c0f4b99d19855c412ef51b');
INSERT INTO `track` VALUES ('20095', 'front', '2019-01-12 01:49:33', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/home', '146a0787c082c4f2c0c0f4b99d19855c412ef51b');
INSERT INTO `track` VALUES ('20096', 'front', '2019-01-12 01:58:10', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/home', '28c5ff505b176982165f957aa3f7d2a875ad76bb');
INSERT INTO `track` VALUES ('20097', 'front', '2019-01-12 02:04:04', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/home', '8cfd590e688d0011e5f1eaf1f0aa025d00b1ffe6');
INSERT INTO `track` VALUES ('20098', 'front', '2019-01-12 02:05:42', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/home', '8cfd590e688d0011e5f1eaf1f0aa025d00b1ffe6');
INSERT INTO `track` VALUES ('20099', 'front', '2019-01-12 02:05:45', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/', '8cfd590e688d0011e5f1eaf1f0aa025d00b1ffe6');
INSERT INTO `track` VALUES ('20100', 'front', '2019-01-12 19:35:34', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '33f1e7f9f080ec6c8cddcc65ff1cdfd520369015');
INSERT INTO `track` VALUES ('20101', 'front', '2019-01-13 12:25:38', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'a368a84773552e7631d744faf5532a7ffa18ed84');
INSERT INTO `track` VALUES ('20102', 'front', '2019-01-13 12:25:51', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'a368a84773552e7631d744faf5532a7ffa18ed84');
INSERT INTO `track` VALUES ('20103', 'front', '2019-01-13 12:26:20', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'a368a84773552e7631d744faf5532a7ffa18ed84');
INSERT INTO `track` VALUES ('20104', 'front', '2019-01-13 20:32:50', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', '33b045cf453ff56299653165c7ba8bcf7dbece6b');
INSERT INTO `track` VALUES ('20105', 'front', '2019-01-14 02:06:41', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '0603fee149f907345744a84dd9130c57c68ef912');
INSERT INTO `track` VALUES ('20106', 'front', '2019-01-14 14:29:33', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'ff90b3a0c6e9bb1742a6a565d8f72e9f8a8d5168');
INSERT INTO `track` VALUES ('20107', 'front', '2019-01-14 14:29:47', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'ff90b3a0c6e9bb1742a6a565d8f72e9f8a8d5168');
INSERT INTO `track` VALUES ('20108', 'front', '2019-01-14 14:30:09', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'ff90b3a0c6e9bb1742a6a565d8f72e9f8a8d5168');
INSERT INTO `track` VALUES ('20109', 'front', '2019-01-14 14:30:29', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'ff90b3a0c6e9bb1742a6a565d8f72e9f8a8d5168');
INSERT INTO `track` VALUES ('20110', 'front', '2019-01-14 14:30:39', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'ff90b3a0c6e9bb1742a6a565d8f72e9f8a8d5168');
INSERT INTO `track` VALUES ('20111', 'front', '2019-01-14 14:30:54', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'ff90b3a0c6e9bb1742a6a565d8f72e9f8a8d5168');
INSERT INTO `track` VALUES ('20112', 'front', '2019-01-14 14:41:08', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '1e2b416547bbd09984414e0a2caeef1cd2198f1b');
INSERT INTO `track` VALUES ('20113', 'front', '2019-01-14 14:41:22', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'ebf6a6328c41440abf63c974a38f2979293a861e');
INSERT INTO `track` VALUES ('20114', 'front', '2019-01-14 14:41:36', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'ebf6a6328c41440abf63c974a38f2979293a861e');
INSERT INTO `track` VALUES ('20115', 'front', '2019-01-14 14:41:40', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '1e2b416547bbd09984414e0a2caeef1cd2198f1b');
INSERT INTO `track` VALUES ('20116', 'front', '2019-01-14 14:41:44', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '1e2b416547bbd09984414e0a2caeef1cd2198f1b');
INSERT INTO `track` VALUES ('20117', 'front', '2019-01-14 14:41:47', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '1e2b416547bbd09984414e0a2caeef1cd2198f1b');
INSERT INTO `track` VALUES ('20118', 'front', '2019-01-14 14:41:50', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'ebf6a6328c41440abf63c974a38f2979293a861e');
INSERT INTO `track` VALUES ('20119', 'front', '2019-01-14 14:42:23', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '1e2b416547bbd09984414e0a2caeef1cd2198f1b');
INSERT INTO `track` VALUES ('20120', 'front', '2019-01-14 14:42:33', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '1e2b416547bbd09984414e0a2caeef1cd2198f1b');
INSERT INTO `track` VALUES ('20121', 'front', '2019-01-14 14:42:35', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '1e2b416547bbd09984414e0a2caeef1cd2198f1b');
INSERT INTO `track` VALUES ('20122', 'front', '2019-01-14 14:42:36', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '1e2b416547bbd09984414e0a2caeef1cd2198f1b');
INSERT INTO `track` VALUES ('20123', 'front', '2019-01-14 14:42:41', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '1e2b416547bbd09984414e0a2caeef1cd2198f1b');
INSERT INTO `track` VALUES ('20124', 'front', '2019-01-14 14:42:52', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '1e2b416547bbd09984414e0a2caeef1cd2198f1b');
INSERT INTO `track` VALUES ('20125', 'front', '2019-01-14 14:43:00', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'ebf6a6328c41440abf63c974a38f2979293a861e');
INSERT INTO `track` VALUES ('20126', 'front', '2019-01-14 14:43:09', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'ebf6a6328c41440abf63c974a38f2979293a861e');
INSERT INTO `track` VALUES ('20127', 'front', '2019-01-14 14:43:26', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '1e2b416547bbd09984414e0a2caeef1cd2198f1b');
INSERT INTO `track` VALUES ('20128', 'front', '2019-01-14 14:43:37', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '1e2b416547bbd09984414e0a2caeef1cd2198f1b');
INSERT INTO `track` VALUES ('20129', 'front', '2019-01-14 14:43:56', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'ebf6a6328c41440abf63c974a38f2979293a861e');
INSERT INTO `track` VALUES ('20130', 'front', '2019-01-14 14:44:05', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'ebf6a6328c41440abf63c974a38f2979293a861e');
INSERT INTO `track` VALUES ('20131', 'front', '2019-01-14 14:44:08', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '1e2b416547bbd09984414e0a2caeef1cd2198f1b');
INSERT INTO `track` VALUES ('20132', 'front', '2019-01-14 14:44:13', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '1e2b416547bbd09984414e0a2caeef1cd2198f1b');
INSERT INTO `track` VALUES ('20133', 'front', '2019-01-14 14:44:20', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'ebf6a6328c41440abf63c974a38f2979293a861e');
INSERT INTO `track` VALUES ('20134', 'front', '2019-01-14 14:46:09', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '1e2b416547bbd09984414e0a2caeef1cd2198f1b');
INSERT INTO `track` VALUES ('20135', 'front', '2019-01-14 14:46:16', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'ebf6a6328c41440abf63c974a38f2979293a861e');
INSERT INTO `track` VALUES ('20136', 'front', '2019-01-14 14:46:18', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '1e2b416547bbd09984414e0a2caeef1cd2198f1b');
INSERT INTO `track` VALUES ('20137', 'front', '2019-01-14 14:46:21', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '1e2b416547bbd09984414e0a2caeef1cd2198f1b');
INSERT INTO `track` VALUES ('20138', 'front', '2019-01-14 15:01:12', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'a2cb898e22ad2f2a6924e78bd2372438e9d02809');
INSERT INTO `track` VALUES ('20139', 'front', '2019-01-15 20:59:18', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '358a2b1811cb6496e8d1160a7214bee125544b74');
INSERT INTO `track` VALUES ('20140', 'front', '2019-01-15 21:02:31', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '358a2b1811cb6496e8d1160a7214bee125544b74');
INSERT INTO `track` VALUES ('20141', 'front', '2019-01-15 21:04:19', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'c8c2f27623aa48c0d2459ba82e0bd9d716c7ebb1');
INSERT INTO `track` VALUES ('20142', 'front', '2019-01-15 21:05:07', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'c8c2f27623aa48c0d2459ba82e0bd9d716c7ebb1');
INSERT INTO `track` VALUES ('20143', 'front', '2019-01-15 21:05:45', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'c8c2f27623aa48c0d2459ba82e0bd9d716c7ebb1');
INSERT INTO `track` VALUES ('20144', 'front', '2019-01-15 21:05:47', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'c8c2f27623aa48c0d2459ba82e0bd9d716c7ebb1');
INSERT INTO `track` VALUES ('20145', 'front', '2019-01-15 21:05:55', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'c8c2f27623aa48c0d2459ba82e0bd9d716c7ebb1');
INSERT INTO `track` VALUES ('20146', 'front', '2019-01-15 21:07:02', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'c8c2f27623aa48c0d2459ba82e0bd9d716c7ebb1');
INSERT INTO `track` VALUES ('20147', 'front', '2019-01-15 21:07:13', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'c8c2f27623aa48c0d2459ba82e0bd9d716c7ebb1');
INSERT INTO `track` VALUES ('20148', 'front', '2019-01-15 21:07:15', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'c8c2f27623aa48c0d2459ba82e0bd9d716c7ebb1');
INSERT INTO `track` VALUES ('20149', 'front', '2019-01-15 21:09:14', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'c8c2f27623aa48c0d2459ba82e0bd9d716c7ebb1');
INSERT INTO `track` VALUES ('20150', 'front', '2019-01-15 21:10:25', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '34eac9292eee230f8040b742028c49ed90577d9d');
INSERT INTO `track` VALUES ('20151', 'front', '2019-01-15 21:10:29', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '34eac9292eee230f8040b742028c49ed90577d9d');
INSERT INTO `track` VALUES ('20152', 'front', '2019-01-15 21:11:13', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '34eac9292eee230f8040b742028c49ed90577d9d');
INSERT INTO `track` VALUES ('20153', 'front', '2019-01-15 21:12:25', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '34eac9292eee230f8040b742028c49ed90577d9d');
INSERT INTO `track` VALUES ('20154', 'front', '2019-01-15 21:15:36', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '12861ab1fbcf8dee00efae7ed12a574bb0793f07');
INSERT INTO `track` VALUES ('20155', 'front', '2019-01-15 21:15:56', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '12861ab1fbcf8dee00efae7ed12a574bb0793f07');
INSERT INTO `track` VALUES ('20156', 'front', '2019-01-15 21:17:22', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '12861ab1fbcf8dee00efae7ed12a574bb0793f07');
INSERT INTO `track` VALUES ('20157', 'front', '2019-01-15 21:19:19', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '12861ab1fbcf8dee00efae7ed12a574bb0793f07');
INSERT INTO `track` VALUES ('20158', 'front', '2019-01-15 21:22:33', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'ddc37458921a78f5be0f647d07875cfa54210e5c');
INSERT INTO `track` VALUES ('20159', 'front', '2019-01-15 21:23:45', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'ddc37458921a78f5be0f647d07875cfa54210e5c');
INSERT INTO `track` VALUES ('20160', 'front', '2019-01-15 21:27:03', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'ddc37458921a78f5be0f647d07875cfa54210e5c');
INSERT INTO `track` VALUES ('20161', 'front', '2019-01-15 21:27:10', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'ddc37458921a78f5be0f647d07875cfa54210e5c');
INSERT INTO `track` VALUES ('20162', 'front', '2019-01-15 21:27:53', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '4e3574cca08590a55871f60770400a6359705a45');
INSERT INTO `track` VALUES ('20163', 'front', '2019-01-15 21:29:47', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '4e3574cca08590a55871f60770400a6359705a45');
INSERT INTO `track` VALUES ('20164', 'front', '2019-01-15 21:33:17', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'a5bb02c1840b0d3c7b686adca3bb1a0939846381');
INSERT INTO `track` VALUES ('20165', 'front', '2019-01-15 21:34:57', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'a5bb02c1840b0d3c7b686adca3bb1a0939846381');
INSERT INTO `track` VALUES ('20166', 'front', '2019-01-15 21:38:33', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3de4a76f3f9039cbfc686d4aaac03010b01a78fe');
INSERT INTO `track` VALUES ('20167', 'front', '2019-01-15 21:38:55', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3de4a76f3f9039cbfc686d4aaac03010b01a78fe');
INSERT INTO `track` VALUES ('20168', 'front', '2019-01-15 21:38:58', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3de4a76f3f9039cbfc686d4aaac03010b01a78fe');
INSERT INTO `track` VALUES ('20169', 'front', '2019-01-15 21:39:16', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3de4a76f3f9039cbfc686d4aaac03010b01a78fe');
INSERT INTO `track` VALUES ('20170', 'front', '2019-01-15 21:40:03', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3de4a76f3f9039cbfc686d4aaac03010b01a78fe');
INSERT INTO `track` VALUES ('20171', 'front', '2019-01-15 21:40:25', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3de4a76f3f9039cbfc686d4aaac03010b01a78fe');
INSERT INTO `track` VALUES ('20172', 'front', '2019-01-15 21:40:42', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3de4a76f3f9039cbfc686d4aaac03010b01a78fe');
INSERT INTO `track` VALUES ('20173', 'front', '2019-01-15 21:42:32', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3de4a76f3f9039cbfc686d4aaac03010b01a78fe');
INSERT INTO `track` VALUES ('20174', 'front', '2019-01-15 21:42:34', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3de4a76f3f9039cbfc686d4aaac03010b01a78fe');
INSERT INTO `track` VALUES ('20175', 'front', '2019-01-15 21:43:24', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3de4a76f3f9039cbfc686d4aaac03010b01a78fe');
INSERT INTO `track` VALUES ('20176', 'front', '2019-01-15 21:50:35', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '6d0dba8be6c046ce728060d2199ef2bb4f22e022');
INSERT INTO `track` VALUES ('20177', 'front', '2019-01-15 21:52:15', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '6d0dba8be6c046ce728060d2199ef2bb4f22e022');
INSERT INTO `track` VALUES ('20178', 'front', '2019-01-15 21:52:33', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '6d0dba8be6c046ce728060d2199ef2bb4f22e022');
INSERT INTO `track` VALUES ('20179', 'front', '2019-01-15 21:53:30', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '6d0dba8be6c046ce728060d2199ef2bb4f22e022');
INSERT INTO `track` VALUES ('20180', 'front', '2019-01-15 21:54:09', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '6d0dba8be6c046ce728060d2199ef2bb4f22e022');
INSERT INTO `track` VALUES ('20181', 'front', '2019-01-15 22:02:34', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '93b0eb6682192ecdbdf481315cb380b488d6a49e');
INSERT INTO `track` VALUES ('20182', 'front', '2019-01-15 22:03:21', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '93b0eb6682192ecdbdf481315cb380b488d6a49e');
INSERT INTO `track` VALUES ('20183', 'front', '2019-01-15 22:03:35', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '93b0eb6682192ecdbdf481315cb380b488d6a49e');
INSERT INTO `track` VALUES ('20184', 'front', '2019-01-15 22:05:23', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '93b0eb6682192ecdbdf481315cb380b488d6a49e');
INSERT INTO `track` VALUES ('20185', 'front', '2019-01-15 22:06:07', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '93b0eb6682192ecdbdf481315cb380b488d6a49e');
INSERT INTO `track` VALUES ('20186', 'front', '2019-01-15 22:06:55', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '93b0eb6682192ecdbdf481315cb380b488d6a49e');
INSERT INTO `track` VALUES ('20187', 'front', '2019-01-15 22:08:03', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'fde401c4713480a47e08a043d36189f84cdce810');
INSERT INTO `track` VALUES ('20188', 'front', '2019-01-15 22:08:15', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'fde401c4713480a47e08a043d36189f84cdce810');
INSERT INTO `track` VALUES ('20189', 'front', '2019-01-15 22:09:01', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'fde401c4713480a47e08a043d36189f84cdce810');
INSERT INTO `track` VALUES ('20190', 'front', '2019-01-15 22:09:40', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'fde401c4713480a47e08a043d36189f84cdce810');
INSERT INTO `track` VALUES ('20191', 'front', '2019-01-15 22:09:58', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'fde401c4713480a47e08a043d36189f84cdce810');
INSERT INTO `track` VALUES ('20192', 'front', '2019-01-15 22:10:29', 'course', 'detail', '9', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'fde401c4713480a47e08a043d36189f84cdce810');
INSERT INTO `track` VALUES ('20193', 'front', '2019-01-15 22:10:35', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'fde401c4713480a47e08a043d36189f84cdce810');
INSERT INTO `track` VALUES ('20194', 'front', '2019-01-15 22:10:40', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'fde401c4713480a47e08a043d36189f84cdce810');
INSERT INTO `track` VALUES ('20195', 'front', '2019-01-15 22:11:41', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'fde401c4713480a47e08a043d36189f84cdce810');
INSERT INTO `track` VALUES ('20196', 'front', '2019-01-15 22:11:52', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'fde401c4713480a47e08a043d36189f84cdce810');
INSERT INTO `track` VALUES ('20197', 'front', '2019-01-15 22:12:12', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'fde401c4713480a47e08a043d36189f84cdce810');
INSERT INTO `track` VALUES ('20198', 'front', '2019-01-15 22:12:18', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'fde401c4713480a47e08a043d36189f84cdce810');
INSERT INTO `track` VALUES ('20199', 'front', '2019-01-15 22:12:39', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'fde401c4713480a47e08a043d36189f84cdce810');
INSERT INTO `track` VALUES ('20200', 'front', '2019-01-15 22:12:42', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'fde401c4713480a47e08a043d36189f84cdce810');
INSERT INTO `track` VALUES ('20201', 'front', '2019-01-15 22:13:29', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'da3963e4fb6784f374910d533c0c42c59849d410');
INSERT INTO `track` VALUES ('20202', 'front', '2019-01-15 22:14:03', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'da3963e4fb6784f374910d533c0c42c59849d410');
INSERT INTO `track` VALUES ('20203', 'front', '2019-01-15 22:15:30', 'home', 'index', '0', null, '1', '', 'Android', '::1', '/_mebook/', 'da3963e4fb6784f374910d533c0c42c59849d410');
INSERT INTO `track` VALUES ('20204', 'front', '2019-01-15 22:15:34', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'da3963e4fb6784f374910d533c0c42c59849d410');
INSERT INTO `track` VALUES ('20205', 'front', '2019-01-15 22:15:36', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'da3963e4fb6784f374910d533c0c42c59849d410');
INSERT INTO `track` VALUES ('20206', 'front', '2019-01-15 22:15:36', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'da3963e4fb6784f374910d533c0c42c59849d410');
INSERT INTO `track` VALUES ('20207', 'front', '2019-01-15 22:21:33', 'home', 'index', '0', null, '1', '', 'Android', '::1', '/_mebook/', '95e2801beaf4557561c23f1e80c755fe8edb2a7f');
INSERT INTO `track` VALUES ('20208', 'front', '2019-01-15 22:22:02', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '95e2801beaf4557561c23f1e80c755fe8edb2a7f');
INSERT INTO `track` VALUES ('20209', 'front', '2019-01-15 22:22:52', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '95e2801beaf4557561c23f1e80c755fe8edb2a7f');
INSERT INTO `track` VALUES ('20210', 'front', '2019-01-15 22:26:08', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '95e2801beaf4557561c23f1e80c755fe8edb2a7f');
INSERT INTO `track` VALUES ('20211', 'front', '2019-01-15 22:26:32', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', '95e2801beaf4557561c23f1e80c755fe8edb2a7f');
INSERT INTO `track` VALUES ('20212', 'front', '2019-01-15 22:26:45', 'course', 'detail', '6', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-1', 'cb85389a20e9b332a2cb54f062e862b81da628d1');
INSERT INTO `track` VALUES ('20213', 'front', '2019-01-15 22:30:04', 'course', 'detail', '6', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-1', 'cb85389a20e9b332a2cb54f062e862b81da628d1');
INSERT INTO `track` VALUES ('20214', 'front', '2019-01-15 22:30:06', 'course', 'detail', '6', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-1', 'cb85389a20e9b332a2cb54f062e862b81da628d1');
INSERT INTO `track` VALUES ('20215', 'front', '2019-01-15 22:30:39', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-1', 'Windows 10', '::1', '/_mebook/', 'cb85389a20e9b332a2cb54f062e862b81da628d1');
INSERT INTO `track` VALUES ('20216', 'front', '2019-01-15 22:48:09', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-1', 'Windows 10', '::1', '/_mebook/', 'e4f414d25aa81d779eb01cc980db06d8753b5288');
INSERT INTO `track` VALUES ('20217', 'front', '2019-01-15 22:48:45', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-1', 'Windows 10', '::1', '/_mebook/', 'e4f414d25aa81d779eb01cc980db06d8753b5288');
INSERT INTO `track` VALUES ('20218', 'front', '2019-01-15 22:49:17', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-1', 'Windows 10', '::1', '/_mebook/', 'e4f414d25aa81d779eb01cc980db06d8753b5288');
INSERT INTO `track` VALUES ('20219', 'front', '2019-01-15 22:49:53', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-1', 'Android', '::1', '/_mebook/', 'e4f414d25aa81d779eb01cc980db06d8753b5288');
INSERT INTO `track` VALUES ('20220', 'front', '2019-01-15 22:51:05', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-1', 'Android', '::1', '/_mebook/', 'e4f414d25aa81d779eb01cc980db06d8753b5288');
INSERT INTO `track` VALUES ('20221', 'front', '2019-01-15 22:51:10', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-1', 'Android', '::1', '/_mebook/', 'e4f414d25aa81d779eb01cc980db06d8753b5288');
INSERT INTO `track` VALUES ('20222', 'front', '2019-01-15 22:51:43', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-1', 'Windows 10', '::1', '/_mebook/', 'e4f414d25aa81d779eb01cc980db06d8753b5288');
INSERT INTO `track` VALUES ('20223', 'front', '2019-01-15 22:51:44', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-1', 'Windows 10', '::1', '/_mebook/', 'e4f414d25aa81d779eb01cc980db06d8753b5288');
INSERT INTO `track` VALUES ('20224', 'front', '2019-01-15 22:51:44', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-1', 'Windows 10', '::1', '/_mebook/', 'e4f414d25aa81d779eb01cc980db06d8753b5288');
INSERT INTO `track` VALUES ('20225', 'front', '2019-01-15 22:51:44', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-1', 'Windows 10', '::1', '/_mebook/', 'e4f414d25aa81d779eb01cc980db06d8753b5288');
INSERT INTO `track` VALUES ('20226', 'front', '2019-01-15 23:08:41', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-1', 'Windows 10', '::1', '/_mebook/', 'dcd0250d671257e0ee0fbac30430f58472cc9927');
INSERT INTO `track` VALUES ('20227', 'front', '2019-01-15 23:09:04', 'course', 'detail', '9', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'dcd0250d671257e0ee0fbac30430f58472cc9927');
INSERT INTO `track` VALUES ('20228', 'front', '2019-01-15 23:09:19', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', 'dcd0250d671257e0ee0fbac30430f58472cc9927');
INSERT INTO `track` VALUES ('20229', 'front', '2019-01-15 23:09:46', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', 'dcd0250d671257e0ee0fbac30430f58472cc9927');
INSERT INTO `track` VALUES ('20230', 'front', '2019-01-15 23:10:08', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', 'dcd0250d671257e0ee0fbac30430f58472cc9927');
INSERT INTO `track` VALUES ('20231', 'front', '2019-01-15 23:10:19', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', 'dcd0250d671257e0ee0fbac30430f58472cc9927');
INSERT INTO `track` VALUES ('20232', 'front', '2019-01-15 23:11:42', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', 'dcd0250d671257e0ee0fbac30430f58472cc9927');
INSERT INTO `track` VALUES ('20233', 'front', '2019-01-16 00:15:39', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', 'db7e128dd718c5c61cea4f9656e4714f345c6a52');
INSERT INTO `track` VALUES ('20234', 'front', '2019-01-16 00:16:34', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', 'db7e128dd718c5c61cea4f9656e4714f345c6a52');
INSERT INTO `track` VALUES ('20235', 'front', '2019-01-16 00:16:41', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', 'db7e128dd718c5c61cea4f9656e4714f345c6a52');
INSERT INTO `track` VALUES ('20236', 'front', '2019-01-16 00:16:47', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', 'db7e128dd718c5c61cea4f9656e4714f345c6a52');
INSERT INTO `track` VALUES ('20237', 'front', '2019-01-16 00:17:01', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', 'db7e128dd718c5c61cea4f9656e4714f345c6a52');
INSERT INTO `track` VALUES ('20238', 'front', '2019-01-16 00:17:47', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', 'db7e128dd718c5c61cea4f9656e4714f345c6a52');
INSERT INTO `track` VALUES ('20239', 'front', '2019-01-16 00:18:29', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', 'f375f0141c33ae499cffd619acb97b583e0c4e7f');
INSERT INTO `track` VALUES ('20240', 'front', '2019-01-16 00:20:54', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', 'f375f0141c33ae499cffd619acb97b583e0c4e7f');
INSERT INTO `track` VALUES ('20241', 'front', '2019-01-16 00:20:57', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', 'f375f0141c33ae499cffd619acb97b583e0c4e7f');
INSERT INTO `track` VALUES ('20242', 'front', '2019-01-16 00:24:10', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', '6fae61e0e90b54e1853bf80b2d7760b9a7afa842');
INSERT INTO `track` VALUES ('20243', 'front', '2019-01-16 00:25:07', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', '6fae61e0e90b54e1853bf80b2d7760b9a7afa842');
INSERT INTO `track` VALUES ('20244', 'front', '2019-01-16 00:27:13', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', '6fae61e0e90b54e1853bf80b2d7760b9a7afa842');
INSERT INTO `track` VALUES ('20245', 'front', '2019-01-16 00:27:16', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', '6fae61e0e90b54e1853bf80b2d7760b9a7afa842');
INSERT INTO `track` VALUES ('20246', 'front', '2019-01-16 00:28:24', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', '6fae61e0e90b54e1853bf80b2d7760b9a7afa842');
INSERT INTO `track` VALUES ('20247', 'front', '2019-01-16 00:29:23', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', '3bfcf934c757a565307d354fbee66c89bc0706af');
INSERT INTO `track` VALUES ('20248', 'front', '2019-01-16 00:30:29', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', '3bfcf934c757a565307d354fbee66c89bc0706af');
INSERT INTO `track` VALUES ('20249', 'front', '2019-01-16 00:31:10', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Android', '::1', '/_mebook/', '3bfcf934c757a565307d354fbee66c89bc0706af');
INSERT INTO `track` VALUES ('20250', 'front', '2019-01-16 00:47:54', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', '5198deda1c0fb2b99895e115fffd343140fa41ab');
INSERT INTO `track` VALUES ('20251', 'front', '2019-01-16 00:56:47', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', 'c0589c30f61046acb5a8e6e00633a359e2c5a9de');
INSERT INTO `track` VALUES ('20252', 'front', '2019-01-16 01:01:16', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', '97b58a3a0d6d8b16e919100c9fd5d872fc977a0f');
INSERT INTO `track` VALUES ('20253', 'front', '2019-01-16 01:02:05', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Android', '::1', '/_mebook/', '97b58a3a0d6d8b16e919100c9fd5d872fc977a0f');
INSERT INTO `track` VALUES ('20254', 'front', '2019-01-16 01:02:06', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Android', '::1', '/_mebook/', '97b58a3a0d6d8b16e919100c9fd5d872fc977a0f');
INSERT INTO `track` VALUES ('20255', 'front', '2019-01-16 01:02:06', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Android', '::1', '/_mebook/', '97b58a3a0d6d8b16e919100c9fd5d872fc977a0f');
INSERT INTO `track` VALUES ('20256', 'front', '2019-01-16 01:02:06', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Android', '::1', '/_mebook/', '97b58a3a0d6d8b16e919100c9fd5d872fc977a0f');
INSERT INTO `track` VALUES ('20257', 'front', '2019-01-16 01:02:06', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Android', '::1', '/_mebook/', '97b58a3a0d6d8b16e919100c9fd5d872fc977a0f');
INSERT INTO `track` VALUES ('20258', 'front', '2019-01-16 01:02:07', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Android', '::1', '/_mebook/', '97b58a3a0d6d8b16e919100c9fd5d872fc977a0f');
INSERT INTO `track` VALUES ('20259', 'front', '2019-01-16 01:02:07', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Android', '::1', '/_mebook/', '97b58a3a0d6d8b16e919100c9fd5d872fc977a0f');
INSERT INTO `track` VALUES ('20260', 'front', '2019-01-16 01:02:07', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Android', '::1', '/_mebook/', '97b58a3a0d6d8b16e919100c9fd5d872fc977a0f');
INSERT INTO `track` VALUES ('20261', 'front', '2019-01-16 01:02:07', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Android', '::1', '/_mebook/', '97b58a3a0d6d8b16e919100c9fd5d872fc977a0f');
INSERT INTO `track` VALUES ('20262', 'front', '2019-01-16 01:02:07', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Android', '::1', '/_mebook/', '97b58a3a0d6d8b16e919100c9fd5d872fc977a0f');
INSERT INTO `track` VALUES ('20263', 'front', '2019-01-16 01:02:07', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Android', '::1', '/_mebook/', '97b58a3a0d6d8b16e919100c9fd5d872fc977a0f');
INSERT INTO `track` VALUES ('20264', 'front', '2019-01-16 01:02:07', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Android', '::1', '/_mebook/', '97b58a3a0d6d8b16e919100c9fd5d872fc977a0f');
INSERT INTO `track` VALUES ('20265', 'front', '2019-01-16 01:02:08', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/detail/Sales-Training:-Practical-Sales-Techniques-4', 'Android', '::1', '/_mebook/', '97b58a3a0d6d8b16e919100c9fd5d872fc977a0f');
INSERT INTO `track` VALUES ('20266', 'front', '2019-01-16 21:08:03', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '0b072e8c9cedbe74f71e02d7df42631df505d4a5');
INSERT INTO `track` VALUES ('20267', 'front', '2019-01-16 22:01:23', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/category/type/article', 'Windows 10', '::1', '/_mebook/', '5c9f43819ef35bf994e28657288bf360cf3927c4');
INSERT INTO `track` VALUES ('20268', 'front', '2019-01-16 22:01:34', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/category/type/article', 'Windows 10', '::1', '/_mebook/', '5c9f43819ef35bf994e28657288bf360cf3927c4');
INSERT INTO `track` VALUES ('20269', 'front', '2019-01-16 22:01:46', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/category/type/article', 'Windows 10', '::1', '/_mebook/', '5c9f43819ef35bf994e28657288bf360cf3927c4');
INSERT INTO `track` VALUES ('20270', 'front', '2019-01-16 22:26:15', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '02409fbf3194947dc293a6c6e7002906ea04798e');
INSERT INTO `track` VALUES ('20271', 'front', '2019-01-16 22:26:38', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'd3f79bc559652e5c37ec98d17cfd112a56b8c38b');
INSERT INTO `track` VALUES ('20272', 'front', '2019-01-17 00:30:57', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'f3452d500a7c30e389445efb484a5f8ee0e3a092');
INSERT INTO `track` VALUES ('20273', 'front', '2019-01-17 12:08:39', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3452f91f781c296387c3c3d7255f1b615c482610');
INSERT INTO `track` VALUES ('20274', 'front', '2019-01-17 12:09:53', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3452f91f781c296387c3c3d7255f1b615c482610');
INSERT INTO `track` VALUES ('20275', 'front', '2019-01-17 12:10:10', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3452f91f781c296387c3c3d7255f1b615c482610');
INSERT INTO `track` VALUES ('20276', 'front', '2019-01-17 12:10:39', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3452f91f781c296387c3c3d7255f1b615c482610');
INSERT INTO `track` VALUES ('20277', 'front', '2019-01-17 12:13:21', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3452f91f781c296387c3c3d7255f1b615c482610');
INSERT INTO `track` VALUES ('20278', 'front', '2019-01-17 12:17:45', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '15aa38bc208ee98d42307dc2b0b9218778df0392');
INSERT INTO `track` VALUES ('20279', 'front', '2019-01-17 12:19:50', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '35e8bbaf413e956ecfd3136440d2caec24f61eca');
INSERT INTO `track` VALUES ('20280', 'front', '2019-01-17 12:19:53', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '35e8bbaf413e956ecfd3136440d2caec24f61eca');
INSERT INTO `track` VALUES ('20281', 'front', '2019-01-17 12:38:39', 'home', 'payment_method', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/home/payment_method', 'ffc76d9e6155658b94f43b9490377431df8b8918');
INSERT INTO `track` VALUES ('20282', 'front', '2019-01-17 12:42:33', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'ffc76d9e6155658b94f43b9490377431df8b8918');
INSERT INTO `track` VALUES ('20283', 'front', '2019-01-17 12:43:52', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'f2fac92dca106f04ce305b2238dad6ed5f537aeb');
INSERT INTO `track` VALUES ('20284', 'front', '2019-01-17 12:44:37', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'f2fac92dca106f04ce305b2238dad6ed5f537aeb');
INSERT INTO `track` VALUES ('20285', 'front', '2019-01-17 12:46:23', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'f2fac92dca106f04ce305b2238dad6ed5f537aeb');
INSERT INTO `track` VALUES ('20286', 'front', '2019-01-17 12:47:33', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'f2fac92dca106f04ce305b2238dad6ed5f537aeb');
INSERT INTO `track` VALUES ('20287', 'front', '2019-01-17 12:49:16', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '2e1e7c700339e5671ff14553cc9db70aa2d02240');
INSERT INTO `track` VALUES ('20288', 'front', '2019-01-17 12:53:13', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '2e1e7c700339e5671ff14553cc9db70aa2d02240');
INSERT INTO `track` VALUES ('20289', 'front', '2019-01-17 13:00:40', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '1ba19b0fc1aae6d0b56acf2aa7670999278e3e3c');
INSERT INTO `track` VALUES ('20290', 'front', '2019-01-17 13:01:21', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '1ba19b0fc1aae6d0b56acf2aa7670999278e3e3c');
INSERT INTO `track` VALUES ('20291', 'front', '2019-01-17 13:01:29', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '1ba19b0fc1aae6d0b56acf2aa7670999278e3e3c');
INSERT INTO `track` VALUES ('20292', 'front', '2019-01-17 13:03:42', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '1ba19b0fc1aae6d0b56acf2aa7670999278e3e3c');
INSERT INTO `track` VALUES ('20293', 'front', '2019-01-17 13:04:36', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '1ba19b0fc1aae6d0b56acf2aa7670999278e3e3c');
INSERT INTO `track` VALUES ('20294', 'front', '2019-01-17 13:07:21', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'a095ae9e517737595b3069c4747224efe2b974e3');
INSERT INTO `track` VALUES ('20295', 'front', '2019-01-17 13:07:53', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'a095ae9e517737595b3069c4747224efe2b974e3');
INSERT INTO `track` VALUES ('20296', 'front', '2019-01-17 13:11:27', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'a095ae9e517737595b3069c4747224efe2b974e3');
INSERT INTO `track` VALUES ('20297', 'front', '2019-01-17 13:19:10', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/home/payment_method', 'Android', '::1', '/_mebook/', '159756bedfabb1d27871705ce681edd86a620028');
INSERT INTO `track` VALUES ('20298', 'front', '2019-01-17 13:19:42', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '159756bedfabb1d27871705ce681edd86a620028');
INSERT INTO `track` VALUES ('20299', 'front', '2019-01-17 13:19:47', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '159756bedfabb1d27871705ce681edd86a620028');
INSERT INTO `track` VALUES ('20300', 'front', '2019-01-17 13:19:48', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '159756bedfabb1d27871705ce681edd86a620028');
INSERT INTO `track` VALUES ('20301', 'front', '2019-01-17 13:19:50', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '159756bedfabb1d27871705ce681edd86a620028');
INSERT INTO `track` VALUES ('20302', 'front', '2019-01-17 13:19:51', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '159756bedfabb1d27871705ce681edd86a620028');
INSERT INTO `track` VALUES ('20303', 'front', '2019-01-17 13:23:31', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '159756bedfabb1d27871705ce681edd86a620028');
INSERT INTO `track` VALUES ('20304', 'front', '2019-01-17 13:24:02', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '159756bedfabb1d27871705ce681edd86a620028');
INSERT INTO `track` VALUES ('20305', 'front', '2019-01-17 13:24:11', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '94fb60cdd8e7b2956deab3105300785fbab02b1f');
INSERT INTO `track` VALUES ('20306', 'front', '2019-01-17 13:24:58', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '94fb60cdd8e7b2956deab3105300785fbab02b1f');
INSERT INTO `track` VALUES ('20307', 'front', '2019-01-17 13:27:59', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '94fb60cdd8e7b2956deab3105300785fbab02b1f');
INSERT INTO `track` VALUES ('20308', 'front', '2019-01-17 13:44:56', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', 'ea9d92994e052e3174a8c1ceff442e93b7106c04');
INSERT INTO `track` VALUES ('20309', 'front', '2019-01-17 13:44:59', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'ea9d92994e052e3174a8c1ceff442e93b7106c04');
INSERT INTO `track` VALUES ('20310', 'front', '2019-01-17 13:52:22', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'bfcd2b13f1cde3081ba7dbc2b8106962618c39aa');
INSERT INTO `track` VALUES ('20311', 'front', '2019-01-17 14:01:34', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '584373aed970a16e42ac431fac902bbc959dc56c');
INSERT INTO `track` VALUES ('20312', 'front', '2019-01-17 14:02:23', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '584373aed970a16e42ac431fac902bbc959dc56c');
INSERT INTO `track` VALUES ('20313', 'front', '2019-01-17 14:03:17', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '584373aed970a16e42ac431fac902bbc959dc56c');
INSERT INTO `track` VALUES ('20314', 'front', '2019-01-17 14:44:58', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '61f8747b54603be987dea12843512dabdbfee59e');
INSERT INTO `track` VALUES ('20315', 'front', '2019-01-17 14:45:07', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '61f8747b54603be987dea12843512dabdbfee59e');
INSERT INTO `track` VALUES ('20316', 'front', '2019-01-17 14:45:18', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '61f8747b54603be987dea12843512dabdbfee59e');
INSERT INTO `track` VALUES ('20317', 'front', '2019-01-17 14:47:44', 'home', 'help', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/home/help', '61f8747b54603be987dea12843512dabdbfee59e');
INSERT INTO `track` VALUES ('20318', 'front', '2019-01-17 14:47:49', 'home', 'payment_method', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/home/payment_method', '61f8747b54603be987dea12843512dabdbfee59e');
INSERT INTO `track` VALUES ('20319', 'front', '2019-01-17 14:47:56', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '61f8747b54603be987dea12843512dabdbfee59e');
INSERT INTO `track` VALUES ('20320', 'front', '2019-01-17 23:44:40', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'b2272a496676008dd6f6dc3f7a4842e6147c1b0c');
INSERT INTO `track` VALUES ('20321', 'front', '2019-01-17 23:45:02', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'b2272a496676008dd6f6dc3f7a4842e6147c1b0c');
INSERT INTO `track` VALUES ('20322', 'front', '2019-01-17 23:45:11', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'b2272a496676008dd6f6dc3f7a4842e6147c1b0c');
INSERT INTO `track` VALUES ('20323', 'front', '2019-01-17 23:45:16', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'b2272a496676008dd6f6dc3f7a4842e6147c1b0c');
INSERT INTO `track` VALUES ('20324', 'front', '2019-01-17 23:45:29', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'b2272a496676008dd6f6dc3f7a4842e6147c1b0c');
INSERT INTO `track` VALUES ('20325', 'front', '2019-01-18 00:06:33', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/', '1589976f4023d4abad3431eefce7fb313e8b3ec5');
INSERT INTO `track` VALUES ('20326', 'front', '2019-01-18 00:06:36', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/article', '1589976f4023d4abad3431eefce7fb313e8b3ec5');
INSERT INTO `track` VALUES ('20327', 'front', '2019-01-18 00:06:39', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/', '1589976f4023d4abad3431eefce7fb313e8b3ec5');
INSERT INTO `track` VALUES ('20328', 'front', '2019-01-18 00:14:34', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/', 'f5a0719930bfffd6a58b0369ab8a301610444374');
INSERT INTO `track` VALUES ('20329', 'front', '2019-01-18 00:15:08', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/', 'f5a0719930bfffd6a58b0369ab8a301610444374');
INSERT INTO `track` VALUES ('20330', 'front', '2019-01-18 00:15:14', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/', 'f5a0719930bfffd6a58b0369ab8a301610444374');
INSERT INTO `track` VALUES ('20331', 'front', '2019-01-18 00:17:17', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/', 'f5a0719930bfffd6a58b0369ab8a301610444374');
INSERT INTO `track` VALUES ('20332', 'front', '2019-01-18 00:17:22', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/', 'f5a0719930bfffd6a58b0369ab8a301610444374');
INSERT INTO `track` VALUES ('20333', 'front', '2019-01-18 00:37:22', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '6028a96d214e4ebad1cbcb8c0aae6346934d40e8');
INSERT INTO `track` VALUES ('20334', 'front', '2019-01-18 00:37:24', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '6028a96d214e4ebad1cbcb8c0aae6346934d40e8');
INSERT INTO `track` VALUES ('20335', 'front', '2019-01-18 00:39:16', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '6028a96d214e4ebad1cbcb8c0aae6346934d40e8');
INSERT INTO `track` VALUES ('20336', 'front', '2019-01-18 00:39:37', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '6028a96d214e4ebad1cbcb8c0aae6346934d40e8');
INSERT INTO `track` VALUES ('20337', 'front', '2019-01-18 00:40:02', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '6028a96d214e4ebad1cbcb8c0aae6346934d40e8');
INSERT INTO `track` VALUES ('20338', 'front', '2019-01-18 00:40:22', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '6028a96d214e4ebad1cbcb8c0aae6346934d40e8');
INSERT INTO `track` VALUES ('20339', 'front', '2019-01-18 00:41:34', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'cd6d7a6a914b0b9f0dd525d1c53977d94ca0c6ca');
INSERT INTO `track` VALUES ('20340', 'front', '2019-01-18 00:42:17', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'cd6d7a6a914b0b9f0dd525d1c53977d94ca0c6ca');
INSERT INTO `track` VALUES ('20341', 'front', '2019-01-18 00:43:36', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'cd6d7a6a914b0b9f0dd525d1c53977d94ca0c6ca');
INSERT INTO `track` VALUES ('20342', 'front', '2019-01-18 00:44:00', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'cd6d7a6a914b0b9f0dd525d1c53977d94ca0c6ca');
INSERT INTO `track` VALUES ('20343', 'front', '2019-01-18 00:44:25', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'cd6d7a6a914b0b9f0dd525d1c53977d94ca0c6ca');
INSERT INTO `track` VALUES ('20344', 'front', '2019-01-18 00:44:39', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'cd6d7a6a914b0b9f0dd525d1c53977d94ca0c6ca');
INSERT INTO `track` VALUES ('20345', 'front', '2019-01-18 00:44:57', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'cd6d7a6a914b0b9f0dd525d1c53977d94ca0c6ca');
INSERT INTO `track` VALUES ('20346', 'front', '2019-01-18 00:45:05', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'cd6d7a6a914b0b9f0dd525d1c53977d94ca0c6ca');
INSERT INTO `track` VALUES ('20347', 'front', '2019-01-18 00:45:20', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'cd6d7a6a914b0b9f0dd525d1c53977d94ca0c6ca');
INSERT INTO `track` VALUES ('20348', 'front', '2019-01-18 00:45:30', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'cd6d7a6a914b0b9f0dd525d1c53977d94ca0c6ca');
INSERT INTO `track` VALUES ('20349', 'front', '2019-01-18 00:45:57', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'cd6d7a6a914b0b9f0dd525d1c53977d94ca0c6ca');
INSERT INTO `track` VALUES ('20350', 'front', '2019-01-18 00:46:02', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'cd6d7a6a914b0b9f0dd525d1c53977d94ca0c6ca');
INSERT INTO `track` VALUES ('20351', 'front', '2019-01-18 00:46:32', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'cd6d7a6a914b0b9f0dd525d1c53977d94ca0c6ca');
INSERT INTO `track` VALUES ('20352', 'front', '2019-01-18 00:47:08', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '533a4e3c27d7135437cf8a7f97ce73b0761cc096');
INSERT INTO `track` VALUES ('20353', 'front', '2019-01-18 01:07:33', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', 'b2f885f39b9d12e04fe9be7f4e96d8728fecd4da');
INSERT INTO `track` VALUES ('20354', 'front', '2019-01-18 01:33:10', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '0fb182ffed8acbd528f79b32a1db7b19c6441d66');
INSERT INTO `track` VALUES ('20355', 'front', '2019-01-18 01:35:50', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%A3%E0%B8%B2%E0%B8%9F%E0%B8%9F%E0%B8%B4%E0%B8%81%E0%B8%94%E0%B8%B5%E0%B9%84%E0%B8%8B%E0%B8%99%E0%B9%8C', 'Windows 10', '::1', '/_mebook/', 'b5bc99a55fee466e86291c45643da3ba2a5b300c');
INSERT INTO `track` VALUES ('20356', 'front', '2019-01-18 01:42:31', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '1dbd102e1fe212fa87848523169bfa0a461e1bdf');
INSERT INTO `track` VALUES ('20357', 'front', '2019-01-18 01:42:53', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '1dbd102e1fe212fa87848523169bfa0a461e1bdf');
INSERT INTO `track` VALUES ('20358', 'front', '2019-01-18 01:43:43', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '1dbd102e1fe212fa87848523169bfa0a461e1bdf');
INSERT INTO `track` VALUES ('20359', 'front', '2019-01-18 01:45:31', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '1dbd102e1fe212fa87848523169bfa0a461e1bdf');
INSERT INTO `track` VALUES ('20360', 'front', '2019-01-18 01:45:36', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '1dbd102e1fe212fa87848523169bfa0a461e1bdf');
INSERT INTO `track` VALUES ('20361', 'front', '2019-01-18 01:46:20', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner', 'Windows 10', '::1', '/_mebook/', 'b70ddb7294148937de6b6b5552ecdf4971426d2b');
INSERT INTO `track` VALUES ('20362', 'front', '2019-01-18 01:55:21', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/', '05a0e5936cace3ea156c9d79e49c73238e0d690c');
INSERT INTO `track` VALUES ('20363', 'front', '2019-01-18 09:22:21', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'ed150e4780b0dbc367c14df79e8d4b7bb21b5ffb');
INSERT INTO `track` VALUES ('20364', 'front', '2019-01-18 09:24:36', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/', 'ed150e4780b0dbc367c14df79e8d4b7bb21b5ffb');
INSERT INTO `track` VALUES ('20365', 'front', '2019-01-18 09:25:49', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/', 'ed150e4780b0dbc367c14df79e8d4b7bb21b5ffb');
INSERT INTO `track` VALUES ('20366', 'front', '2019-01-18 09:30:23', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/', 'b8ed8fa0f620c0c1fc35d2c837e5ed558a178c77');
INSERT INTO `track` VALUES ('20367', 'front', '2019-01-20 19:54:45', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'a8800c91afb9b18a330162aec022b38a93bd2978');
INSERT INTO `track` VALUES ('20368', 'front', '2019-01-20 20:14:34', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'a11410ba3e643d996463e801444e3eaad75dd0fd');
INSERT INTO `track` VALUES ('20369', 'front', '2019-01-20 20:15:51', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/home', 'a11410ba3e643d996463e801444e3eaad75dd0fd');
INSERT INTO `track` VALUES ('20370', 'front', '2019-01-20 20:25:30', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/', '3c046ca32c63e506ccec0b030d574b0c11be0908');
INSERT INTO `track` VALUES ('20371', 'front', '2019-01-20 20:25:35', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '3c046ca32c63e506ccec0b030d574b0c11be0908');
INSERT INTO `track` VALUES ('20372', 'front', '2019-01-20 20:25:42', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register/userConferm/YjY0NjlAOTE=', 'Windows 10', '::1', '/_mebook/', '3c046ca32c63e506ccec0b030d574b0c11be0908');
INSERT INTO `track` VALUES ('20373', 'front', '2019-01-20 20:25:45', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '3c046ca32c63e506ccec0b030d574b0c11be0908');
INSERT INTO `track` VALUES ('20374', 'front', '2019-01-20 20:25:46', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '3c046ca32c63e506ccec0b030d574b0c11be0908');
INSERT INTO `track` VALUES ('20375', 'front', '2019-01-20 20:29:22', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '3c046ca32c63e506ccec0b030d574b0c11be0908');
INSERT INTO `track` VALUES ('20376', 'front', '2019-01-20 20:29:35', 'home', 'index', '0', '69', '0', 'http://localhost:81/_mebook/user/register/userConferm/YjY0NjlAOTE=', 'Windows 10', '::1', '/_mebook/home', '3c046ca32c63e506ccec0b030d574b0c11be0908');
INSERT INTO `track` VALUES ('20377', 'front', '2019-01-20 20:29:39', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/home', '3c046ca32c63e506ccec0b030d574b0c11be0908');
INSERT INTO `track` VALUES ('20378', 'front', '2019-01-20 20:29:47', 'home', 'index', '0', '69', '0', 'http://localhost:81/_mebook/user/register/userConferm/YjY0NjlAOTE=', 'Windows 10', '::1', '/_mebook/home', '3c046ca32c63e506ccec0b030d574b0c11be0908');
INSERT INTO `track` VALUES ('20379', 'front', '2019-01-20 20:29:56', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/home', '3c046ca32c63e506ccec0b030d574b0c11be0908');
INSERT INTO `track` VALUES ('20380', 'front', '2019-01-20 20:30:12', 'home', 'index', '0', '69', '0', 'http://localhost:81/_mebook/user/register/userConferm/YjY0NjlAOTE=', 'Windows 10', '::1', '/_mebook/home', '3c046ca32c63e506ccec0b030d574b0c11be0908');
INSERT INTO `track` VALUES ('20381', 'front', '2019-01-20 20:30:21', 'home', 'index', '0', '69', '0', 'http://localhost:81/_mebook/user/register/userConferm/YjY0NjlAOTE=', 'Windows 10', '::1', '/_mebook/home', '3c046ca32c63e506ccec0b030d574b0c11be0908');
INSERT INTO `track` VALUES ('20382', 'front', '2019-01-20 20:30:27', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/home', '3c046ca32c63e506ccec0b030d574b0c11be0908');
INSERT INTO `track` VALUES ('20383', 'front', '2019-01-20 20:30:37', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/home', 'a7ccc16021409c18d35fb226e7d0f2ceb646d2b9');
INSERT INTO `track` VALUES ('20384', 'front', '2019-01-20 20:32:15', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'a7ccc16021409c18d35fb226e7d0f2ceb646d2b9');
INSERT INTO `track` VALUES ('20385', 'front', '2019-01-20 20:32:31', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/profile/', 'Windows 10', '::1', '/_mebook/home', 'a7ccc16021409c18d35fb226e7d0f2ceb646d2b9');
INSERT INTO `track` VALUES ('20386', 'front', '2019-01-20 20:33:13', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/home', 'a7ccc16021409c18d35fb226e7d0f2ceb646d2b9');
INSERT INTO `track` VALUES ('20387', 'front', '2019-01-20 20:33:38', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/user/register/userConferm/YjY0NzBANjU=', 'Windows 10', '::1', '/_mebook/home', 'a7ccc16021409c18d35fb226e7d0f2ceb646d2b9');
INSERT INTO `track` VALUES ('20388', 'front', '2019-01-20 20:34:27', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/user/register/userConferm/YjY0NzBANjU=', 'Windows 10', '::1', '/_mebook/home', 'a7ccc16021409c18d35fb226e7d0f2ceb646d2b9');
INSERT INTO `track` VALUES ('20389', 'front', '2019-01-20 20:34:29', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/user/register/userConferm/YjY0NzBANjU=', 'Windows 10', '::1', '/_mebook/home', 'a7ccc16021409c18d35fb226e7d0f2ceb646d2b9');
INSERT INTO `track` VALUES ('20390', 'front', '2019-01-20 20:35:29', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/user/register/userConferm/YjY0NzBANjU=', 'Windows 10', '::1', '/_mebook/home', 'a7ccc16021409c18d35fb226e7d0f2ceb646d2b9');
INSERT INTO `track` VALUES ('20391', 'front', '2019-01-20 20:36:49', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/user/register/userConferm/YjY0NzBANjU=', 'Windows 10', '::1', '/_mebook/home', '9f1e87462403ead057d0c42c8c94803952cfaa1e');
INSERT INTO `track` VALUES ('20392', 'front', '2019-01-20 20:38:32', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/user/register/userConferm/YjY0NzBANjU=', 'Windows 10', '::1', '/_mebook/home', '9f1e87462403ead057d0c42c8c94803952cfaa1e');
INSERT INTO `track` VALUES ('20393', 'front', '2019-01-20 20:38:41', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/user/register/userConferm/YjY0NzBANjU=', 'Windows 10', '::1', '/_mebook/home', '9f1e87462403ead057d0c42c8c94803952cfaa1e');
INSERT INTO `track` VALUES ('20394', 'front', '2019-01-20 20:38:59', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/user/register/userConferm/YjY0NzBANjU=', 'Windows 10', '::1', '/_mebook/home', '9f1e87462403ead057d0c42c8c94803952cfaa1e');
INSERT INTO `track` VALUES ('20395', 'front', '2019-01-20 20:40:20', 'home', 'index', '0', '70', '1', 'http://localhost:81/_mebook/user/register/userConferm/YjY0NzBANjU=', 'Android', '::1', '/_mebook/home', '9f1e87462403ead057d0c42c8c94803952cfaa1e');
INSERT INTO `track` VALUES ('20396', 'front', '2019-01-20 20:40:50', 'home', 'index', '0', '70', '1', 'http://localhost:81/_mebook/user/register/userConferm/YjY0NzBANjU=', 'Android', '::1', '/_mebook/home', '9f1e87462403ead057d0c42c8c94803952cfaa1e');
INSERT INTO `track` VALUES ('20397', 'front', '2019-01-20 20:41:54', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/home', '055cfebfd2c8880c37b4e1d433fd094abb1c9b8b');
INSERT INTO `track` VALUES ('20398', 'front', '2019-01-20 20:41:59', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/user/register/userConferm/YjY0NzBANjU=', 'Windows 10', '::1', '/_mebook/home', '055cfebfd2c8880c37b4e1d433fd094abb1c9b8b');
INSERT INTO `track` VALUES ('20399', 'front', '2019-01-20 20:43:57', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/', '055cfebfd2c8880c37b4e1d433fd094abb1c9b8b');
INSERT INTO `track` VALUES ('20400', 'front', '2019-01-20 20:44:02', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/home', '055cfebfd2c8880c37b4e1d433fd094abb1c9b8b');
INSERT INTO `track` VALUES ('20401', 'front', '2019-01-20 20:45:56', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/home', '055cfebfd2c8880c37b4e1d433fd094abb1c9b8b');
INSERT INTO `track` VALUES ('20402', 'front', '2019-01-20 20:46:12', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/', '055cfebfd2c8880c37b4e1d433fd094abb1c9b8b');
INSERT INTO `track` VALUES ('20403', 'front', '2019-01-20 22:02:18', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/profile', 'Windows 10', '::1', '/_mebook/home', '97edb3b68fd4e61f99eb4ff0076c6e046deda7ab');
INSERT INTO `track` VALUES ('20404', 'front', '2019-01-20 22:02:38', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '97edb3b68fd4e61f99eb4ff0076c6e046deda7ab');
INSERT INTO `track` VALUES ('20405', 'front', '2019-01-20 22:13:29', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '5e5bc608795aa0bf282c7b697488502ce5864d74');
INSERT INTO `track` VALUES ('20406', 'front', '2019-01-20 22:15:33', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '5e5bc608795aa0bf282c7b697488502ce5864d74');
INSERT INTO `track` VALUES ('20407', 'front', '2019-01-20 22:28:09', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/home', '19210ac423891dce7be004baec7c41a4eb3f93ac');
INSERT INTO `track` VALUES ('20408', 'front', '2019-01-20 22:28:13', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/home', '19210ac423891dce7be004baec7c41a4eb3f93ac');
INSERT INTO `track` VALUES ('20409', 'front', '2019-01-20 22:28:15', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/home', '19210ac423891dce7be004baec7c41a4eb3f93ac');
INSERT INTO `track` VALUES ('20410', 'front', '2019-01-20 22:28:20', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/profile', 'Windows 10', '::1', '/_mebook/home', '19210ac423891dce7be004baec7c41a4eb3f93ac');
INSERT INTO `track` VALUES ('20411', 'front', '2019-01-20 22:29:15', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/profile', 'Windows 10', '::1', '/_mebook/home', '19210ac423891dce7be004baec7c41a4eb3f93ac');
INSERT INTO `track` VALUES ('20412', 'front', '2019-01-20 22:29:19', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/profile', 'Windows 10', '::1', '/_mebook/home', '19210ac423891dce7be004baec7c41a4eb3f93ac');
INSERT INTO `track` VALUES ('20413', 'front', '2019-01-20 22:29:22', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/profile', 'Windows 10', '::1', '/_mebook/home', '19210ac423891dce7be004baec7c41a4eb3f93ac');
INSERT INTO `track` VALUES ('20414', 'front', '2019-01-20 22:30:29', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/profile', 'Windows 10', '::1', '/_mebook/home', '19210ac423891dce7be004baec7c41a4eb3f93ac');
INSERT INTO `track` VALUES ('20415', 'front', '2019-01-20 22:32:51', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/profile', 'Windows 10', '::1', '/_mebook/home', '19210ac423891dce7be004baec7c41a4eb3f93ac');
INSERT INTO `track` VALUES ('20416', 'front', '2019-01-20 22:33:48', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/profile', 'Windows 10', '::1', '/_mebook/home', '134448edd9a6418cf718114996f552232af90cd0');
INSERT INTO `track` VALUES ('20417', 'front', '2019-01-20 22:34:42', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/profile', 'Windows 10', '::1', '/_mebook/home', '134448edd9a6418cf718114996f552232af90cd0');
INSERT INTO `track` VALUES ('20418', 'front', '2019-01-20 22:39:34', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/', '65dd3f028e338a0f76cd74929511d5016d917295');
INSERT INTO `track` VALUES ('20419', 'front', '2019-01-20 22:43:34', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/', '65dd3f028e338a0f76cd74929511d5016d917295');
INSERT INTO `track` VALUES ('20420', 'front', '2019-01-20 22:43:43', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '65dd3f028e338a0f76cd74929511d5016d917295');
INSERT INTO `track` VALUES ('20421', 'front', '2019-01-20 22:59:57', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '9c5f434cf499df4a96b669e8e278ff7f66dff917');
INSERT INTO `track` VALUES ('20422', 'front', '2019-01-20 23:00:54', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '9c5f434cf499df4a96b669e8e278ff7f66dff917');
INSERT INTO `track` VALUES ('20423', 'front', '2019-01-20 23:01:32', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '9c5f434cf499df4a96b669e8e278ff7f66dff917');
INSERT INTO `track` VALUES ('20424', 'front', '2019-01-20 23:02:03', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '9c5f434cf499df4a96b669e8e278ff7f66dff917');
INSERT INTO `track` VALUES ('20425', 'front', '2019-01-20 23:02:23', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '9c5f434cf499df4a96b669e8e278ff7f66dff917');
INSERT INTO `track` VALUES ('20426', 'front', '2019-01-20 23:09:12', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '1b095f7d35aa1cca580321e596b63799bd9ee25d');
INSERT INTO `track` VALUES ('20427', 'front', '2019-01-20 23:11:39', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '1b095f7d35aa1cca580321e596b63799bd9ee25d');
INSERT INTO `track` VALUES ('20428', 'front', '2019-01-20 23:12:14', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '1b095f7d35aa1cca580321e596b63799bd9ee25d');
INSERT INTO `track` VALUES ('20429', 'front', '2019-01-20 23:12:41', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '1b095f7d35aa1cca580321e596b63799bd9ee25d');
INSERT INTO `track` VALUES ('20430', 'front', '2019-01-20 23:13:24', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '1b095f7d35aa1cca580321e596b63799bd9ee25d');
INSERT INTO `track` VALUES ('20431', 'front', '2019-01-20 23:16:02', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'f7c9941cfac409070df853fc6e032b4a3d5cb1a1');
INSERT INTO `track` VALUES ('20432', 'front', '2019-01-20 23:16:36', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'f7c9941cfac409070df853fc6e032b4a3d5cb1a1');
INSERT INTO `track` VALUES ('20433', 'front', '2019-01-20 23:16:38', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'f7c9941cfac409070df853fc6e032b4a3d5cb1a1');
INSERT INTO `track` VALUES ('20434', 'front', '2019-01-20 23:18:02', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'f7c9941cfac409070df853fc6e032b4a3d5cb1a1');
INSERT INTO `track` VALUES ('20435', 'front', '2019-01-20 23:21:20', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'c8144896a68721570ef497849ee52d0705f13537');
INSERT INTO `track` VALUES ('20436', 'front', '2019-01-20 23:21:27', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/home', 'c8144896a68721570ef497849ee52d0705f13537');
INSERT INTO `track` VALUES ('20437', 'front', '2019-01-20 23:21:39', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/', 'c8144896a68721570ef497849ee52d0705f13537');
INSERT INTO `track` VALUES ('20438', 'front', '2019-01-20 23:21:52', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/', 'c8144896a68721570ef497849ee52d0705f13537');
INSERT INTO `track` VALUES ('20439', 'front', '2019-01-20 23:21:59', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/', 'c8144896a68721570ef497849ee52d0705f13537');
INSERT INTO `track` VALUES ('20440', 'front', '2019-01-20 23:34:01', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/', 'de43dc0b411552c5bf85a2b329214dbaac965820');
INSERT INTO `track` VALUES ('20441', 'front', '2019-01-20 23:37:20', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/course/favorite', 'Windows 10', '::1', '/_mebook/', 'de43dc0b411552c5bf85a2b329214dbaac965820');
INSERT INTO `track` VALUES ('20442', 'front', '2019-01-20 23:37:24', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/favorite', 'Windows 10', '::1', '/_mebook/home', 'de43dc0b411552c5bf85a2b329214dbaac965820');
INSERT INTO `track` VALUES ('20443', 'front', '2019-01-20 23:38:34', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/favorite', 'Windows 10', '::1', '/_mebook/home', 'de43dc0b411552c5bf85a2b329214dbaac965820');
INSERT INTO `track` VALUES ('20444', 'front', '2019-01-20 23:38:46', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/', 'de43dc0b411552c5bf85a2b329214dbaac965820');
INSERT INTO `track` VALUES ('20445', 'front', '2019-01-20 23:46:22', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/favorite', 'Windows 10', '::1', '/_mebook/home', '4ddfa8d4474d19271a7558a8aea06d2b4a8dcba3');
INSERT INTO `track` VALUES ('20446', 'front', '2019-01-20 23:46:35', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/favorite', 'Windows 10', '::1', '/_mebook/home', '4ddfa8d4474d19271a7558a8aea06d2b4a8dcba3');
INSERT INTO `track` VALUES ('20447', 'front', '2019-01-20 23:46:46', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/', '4ddfa8d4474d19271a7558a8aea06d2b4a8dcba3');
INSERT INTO `track` VALUES ('20448', 'front', '2019-01-20 23:48:36', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/favorite', 'Windows 10', '::1', '/_mebook/home', '38133e40c164a43015fe4c6e548a75e6b7cc8168');
INSERT INTO `track` VALUES ('20449', 'front', '2019-01-20 23:48:39', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/favorite', 'Windows 10', '::1', '/_mebook/', '38133e40c164a43015fe4c6e548a75e6b7cc8168');
INSERT INTO `track` VALUES ('20450', 'front', '2019-01-20 23:48:44', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '38133e40c164a43015fe4c6e548a75e6b7cc8168');
INSERT INTO `track` VALUES ('20451', 'front', '2019-01-20 23:49:05', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/', '38133e40c164a43015fe4c6e548a75e6b7cc8168');
INSERT INTO `track` VALUES ('20452', 'front', '2019-01-20 23:50:57', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '38133e40c164a43015fe4c6e548a75e6b7cc8168');
INSERT INTO `track` VALUES ('20453', 'front', '2019-01-20 23:51:06', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', '38133e40c164a43015fe4c6e548a75e6b7cc8168');
INSERT INTO `track` VALUES ('20454', 'front', '2019-01-21 00:00:17', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '332b86b01541a2cf2563781a69c9a81828d8a65b');
INSERT INTO `track` VALUES ('20455', 'front', '2019-01-21 00:00:47', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/home', '332b86b01541a2cf2563781a69c9a81828d8a65b');
INSERT INTO `track` VALUES ('20456', 'front', '2019-01-21 00:01:10', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/', '332b86b01541a2cf2563781a69c9a81828d8a65b');
INSERT INTO `track` VALUES ('20457', 'front', '2019-01-21 00:15:20', 'article', 'index', '0', '70', '0', 'http://localhost:81/_mebook/course/favorite', 'Windows 10', '::1', '/_mebook/article', '6976c27424af1ea4b84d8cd5ddc714689794f1cf');
INSERT INTO `track` VALUES ('20458', 'front', '2019-01-21 00:19:02', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/', '6976c27424af1ea4b84d8cd5ddc714689794f1cf');
INSERT INTO `track` VALUES ('20459', 'front', '2019-01-21 00:35:45', 'article', 'index', '0', '70', '0', 'http://localhost:81/_mebook/course/favorite', 'Windows 10', '::1', '/_mebook/article', '2939c49dabe3fdf9d99495cccbd51cc77cc740f1');
INSERT INTO `track` VALUES ('20460', 'front', '2019-01-21 00:35:47', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/', '2939c49dabe3fdf9d99495cccbd51cc77cc740f1');
INSERT INTO `track` VALUES ('20461', 'front', '2019-01-21 01:27:07', 'course', 'detail', '5', '70', '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '3abfa6e887a3d50f3425b9324a9cc040b0565e15');
INSERT INTO `track` VALUES ('20462', 'front', '2019-01-21 01:29:24', 'course', 'detail', '5', '70', '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '3abfa6e887a3d50f3425b9324a9cc040b0565e15');
INSERT INTO `track` VALUES ('20463', 'front', '2019-01-21 01:29:45', 'course', 'detail', '5', '70', '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '3abfa6e887a3d50f3425b9324a9cc040b0565e15');
INSERT INTO `track` VALUES ('20464', 'front', '2019-01-21 01:30:41', 'course', 'detail', '5', '70', '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '3abfa6e887a3d50f3425b9324a9cc040b0565e15');
INSERT INTO `track` VALUES ('20465', 'front', '2019-01-21 01:31:19', 'course', 'detail', '5', '70', '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '3abfa6e887a3d50f3425b9324a9cc040b0565e15');
INSERT INTO `track` VALUES ('20466', 'front', '2019-01-21 01:34:23', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', '74c3e92835b078c139587c1ac38ce7b6aff7b781');
INSERT INTO `track` VALUES ('20467', 'front', '2019-01-21 01:34:29', 'course', 'detail', '5', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '74c3e92835b078c139587c1ac38ce7b6aff7b781');
INSERT INTO `track` VALUES ('20468', 'front', '2019-01-21 01:35:16', 'course', 'detail', '5', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '74c3e92835b078c139587c1ac38ce7b6aff7b781');
INSERT INTO `track` VALUES ('20469', 'front', '2019-01-21 01:36:03', 'course', 'detail', '5', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '74c3e92835b078c139587c1ac38ce7b6aff7b781');
INSERT INTO `track` VALUES ('20470', 'front', '2019-01-21 01:36:07', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', '74c3e92835b078c139587c1ac38ce7b6aff7b781');
INSERT INTO `track` VALUES ('20471', 'front', '2019-01-21 01:36:12', 'course', 'detail', '5', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '74c3e92835b078c139587c1ac38ce7b6aff7b781');
INSERT INTO `track` VALUES ('20472', 'front', '2019-01-21 01:37:22', 'course', 'detail', '5', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '74c3e92835b078c139587c1ac38ce7b6aff7b781');
INSERT INTO `track` VALUES ('20473', 'front', '2019-01-21 01:37:38', 'course', 'detail', '5', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '74c3e92835b078c139587c1ac38ce7b6aff7b781');
INSERT INTO `track` VALUES ('20474', 'front', '2019-01-21 01:38:03', 'course', 'detail', '5', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '74c3e92835b078c139587c1ac38ce7b6aff7b781');
INSERT INTO `track` VALUES ('20475', 'front', '2019-01-21 01:38:24', 'course', 'detail', '5', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '74c3e92835b078c139587c1ac38ce7b6aff7b781');
INSERT INTO `track` VALUES ('20476', 'front', '2019-01-21 01:38:55', 'course', 'detail', '5', '70', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass:-A-Complete-Guide-to-Photography', '74c3e92835b078c139587c1ac38ce7b6aff7b781');
INSERT INTO `track` VALUES ('20477', 'front', '2019-01-21 22:17:16', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'fdd9d2f3878c460ca906bdf6c82ac541118c85c5');
INSERT INTO `track` VALUES ('20478', 'front', '2019-01-21 22:17:24', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/article', 'fdd9d2f3878c460ca906bdf6c82ac541118c85c5');
INSERT INTO `track` VALUES ('20479', 'front', '2019-01-21 22:36:25', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/', '67833112a84afc2eaaec7fb31730b233fc35d559');
INSERT INTO `track` VALUES ('20480', 'front', '2019-01-21 22:42:36', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/article', '792b3e72439147a9931adb18b9448ce8b23c023f');
INSERT INTO `track` VALUES ('20481', 'front', '2019-01-21 22:44:24', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/article', '792b3e72439147a9931adb18b9448ce8b23c023f');
INSERT INTO `track` VALUES ('20482', 'front', '2019-01-21 22:44:50', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/article', '792b3e72439147a9931adb18b9448ce8b23c023f');
INSERT INTO `track` VALUES ('20483', 'front', '2019-01-21 22:47:33', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/article', '792b3e72439147a9931adb18b9448ce8b23c023f');
INSERT INTO `track` VALUES ('20484', 'front', '2019-01-21 22:48:14', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/article', '3e5505a89c21f0e50e5406c5c02f20bb7f86f7d0');
INSERT INTO `track` VALUES ('20485', 'front', '2019-01-21 22:48:20', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/article', '3e5505a89c21f0e50e5406c5c02f20bb7f86f7d0');
INSERT INTO `track` VALUES ('20486', 'front', '2019-01-21 22:49:13', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/article', '3e5505a89c21f0e50e5406c5c02f20bb7f86f7d0');
INSERT INTO `track` VALUES ('20487', 'front', '2019-01-21 22:49:15', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', '3e5505a89c21f0e50e5406c5c02f20bb7f86f7d0');
INSERT INTO `track` VALUES ('20488', 'front', '2019-01-21 22:50:08', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', '3e5505a89c21f0e50e5406c5c02f20bb7f86f7d0');
INSERT INTO `track` VALUES ('20489', 'front', '2019-01-21 22:56:42', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', '0271ce014123cd71cc16c7577b0331498e9a46be');
INSERT INTO `track` VALUES ('20490', 'front', '2019-01-21 22:57:21', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', '0271ce014123cd71cc16c7577b0331498e9a46be');
INSERT INTO `track` VALUES ('20491', 'front', '2019-01-21 22:58:48', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', '0271ce014123cd71cc16c7577b0331498e9a46be');
INSERT INTO `track` VALUES ('20492', 'front', '2019-01-21 22:59:10', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', '0271ce014123cd71cc16c7577b0331498e9a46be');
INSERT INTO `track` VALUES ('20493', 'front', '2019-01-21 22:59:16', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', '0271ce014123cd71cc16c7577b0331498e9a46be');
INSERT INTO `track` VALUES ('20494', 'front', '2019-01-22 01:29:07', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', '52c96eb40b4cdd9e5a3eb48c66aa3e29b886314d');
INSERT INTO `track` VALUES ('20495', 'front', '2019-01-22 01:34:00', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', '52c96eb40b4cdd9e5a3eb48c66aa3e29b886314d');
INSERT INTO `track` VALUES ('20496', 'front', '2019-01-22 01:34:37', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'ecc62a3dc5972cd149e9f3412be51ecdd295785a');
INSERT INTO `track` VALUES ('20497', 'front', '2019-01-22 01:34:50', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'ecc62a3dc5972cd149e9f3412be51ecdd295785a');
INSERT INTO `track` VALUES ('20498', 'front', '2019-01-22 01:35:32', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'ecc62a3dc5972cd149e9f3412be51ecdd295785a');
INSERT INTO `track` VALUES ('20499', 'front', '2019-01-22 01:36:16', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'ecc62a3dc5972cd149e9f3412be51ecdd295785a');
INSERT INTO `track` VALUES ('20500', 'front', '2019-01-22 01:40:50', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'b00b789ebb1385492febb236c090bd43327008c1');
INSERT INTO `track` VALUES ('20501', 'front', '2019-01-22 01:43:03', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'b00b789ebb1385492febb236c090bd43327008c1');
INSERT INTO `track` VALUES ('20502', 'front', '2019-01-22 01:49:05', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'a266b22a9a6704f415c82b13d30cb048977f23c0');
INSERT INTO `track` VALUES ('20503', 'front', '2019-01-22 01:49:12', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'a266b22a9a6704f415c82b13d30cb048977f23c0');
INSERT INTO `track` VALUES ('20504', 'front', '2019-01-22 01:49:16', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'a266b22a9a6704f415c82b13d30cb048977f23c0');
INSERT INTO `track` VALUES ('20505', 'front', '2019-01-22 01:49:22', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'Windows 10', '::1', '/_mebook/article?keyword=areena', 'a266b22a9a6704f415c82b13d30cb048977f23c0');
INSERT INTO `track` VALUES ('20506', 'front', '2019-01-22 01:49:26', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'a266b22a9a6704f415c82b13d30cb048977f23c0');
INSERT INTO `track` VALUES ('20507', 'front', '2019-01-22 01:49:30', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'Windows 10', '::1', '/_mebook/', 'a266b22a9a6704f415c82b13d30cb048977f23c0');
INSERT INTO `track` VALUES ('20508', 'front', '2019-01-22 01:49:31', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/article', 'a266b22a9a6704f415c82b13d30cb048977f23c0');
INSERT INTO `track` VALUES ('20509', 'front', '2019-01-22 01:49:33', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'a266b22a9a6704f415c82b13d30cb048977f23c0');
INSERT INTO `track` VALUES ('20510', 'front', '2019-01-22 01:49:35', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/article', 'a266b22a9a6704f415c82b13d30cb048977f23c0');
INSERT INTO `track` VALUES ('20511', 'front', '2019-01-22 01:49:57', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/article', 'a266b22a9a6704f415c82b13d30cb048977f23c0');
INSERT INTO `track` VALUES ('20512', 'front', '2019-01-22 01:49:58', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'a266b22a9a6704f415c82b13d30cb048977f23c0');
INSERT INTO `track` VALUES ('20513', 'front', '2019-01-22 01:50:10', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'a266b22a9a6704f415c82b13d30cb048977f23c0');
INSERT INTO `track` VALUES ('20514', 'front', '2019-01-22 01:50:16', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'Windows 10', '::1', '/_mebook/article', 'a266b22a9a6704f415c82b13d30cb048977f23c0');
INSERT INTO `track` VALUES ('20515', 'front', '2019-01-22 01:50:34', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'Windows 10', '::1', '/_mebook/article', 'a266b22a9a6704f415c82b13d30cb048977f23c0');
INSERT INTO `track` VALUES ('20516', 'front', '2019-01-22 01:51:18', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'Windows 10', '::1', '/_mebook/article', 'a266b22a9a6704f415c82b13d30cb048977f23c0');
INSERT INTO `track` VALUES ('20517', 'front', '2019-01-22 01:52:27', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'Windows 10', '::1', '/_mebook/article', '0e3600a5f6163a5014aff512c56a11329a9f7574');
INSERT INTO `track` VALUES ('20518', 'front', '2019-01-22 01:52:33', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article', '0e3600a5f6163a5014aff512c56a11329a9f7574');
INSERT INTO `track` VALUES ('20519', 'front', '2019-01-22 01:52:38', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article', '0e3600a5f6163a5014aff512c56a11329a9f7574');
INSERT INTO `track` VALUES ('20520', 'front', '2019-01-22 01:52:55', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article', '0e3600a5f6163a5014aff512c56a11329a9f7574');
INSERT INTO `track` VALUES ('20521', 'front', '2019-01-22 01:54:15', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article', '0e3600a5f6163a5014aff512c56a11329a9f7574');
INSERT INTO `track` VALUES ('20522', 'front', '2019-01-22 01:54:20', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article', '0e3600a5f6163a5014aff512c56a11329a9f7574');
INSERT INTO `track` VALUES ('20523', 'front', '2019-01-22 01:54:24', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/article', '0e3600a5f6163a5014aff512c56a11329a9f7574');
INSERT INTO `track` VALUES ('20524', 'front', '2019-01-22 01:54:29', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article', '0e3600a5f6163a5014aff512c56a11329a9f7574');
INSERT INTO `track` VALUES ('20525', 'front', '2019-01-22 01:55:17', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article', '0e3600a5f6163a5014aff512c56a11329a9f7574');
INSERT INTO `track` VALUES ('20526', 'front', '2019-01-22 01:56:28', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article', '0e3600a5f6163a5014aff512c56a11329a9f7574');
INSERT INTO `track` VALUES ('20527', 'front', '2019-01-22 01:56:44', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article', '0e3600a5f6163a5014aff512c56a11329a9f7574');
INSERT INTO `track` VALUES ('20528', 'front', '2019-01-22 01:58:01', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article', 'aa94717d34d7c9f375a0615ac0c1bcd34698a1e8');
INSERT INTO `track` VALUES ('20529', 'front', '2019-01-22 01:58:04', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article', 'aa94717d34d7c9f375a0615ac0c1bcd34698a1e8');
INSERT INTO `track` VALUES ('20530', 'front', '2019-01-22 01:58:06', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article', 'aa94717d34d7c9f375a0615ac0c1bcd34698a1e8');
INSERT INTO `track` VALUES ('20531', 'front', '2019-01-22 01:59:19', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article', 'aa94717d34d7c9f375a0615ac0c1bcd34698a1e8');
INSERT INTO `track` VALUES ('20532', 'front', '2019-01-22 01:59:37', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article', 'aa94717d34d7c9f375a0615ac0c1bcd34698a1e8');
INSERT INTO `track` VALUES ('20533', 'front', '2019-01-22 02:00:02', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article', 'aa94717d34d7c9f375a0615ac0c1bcd34698a1e8');
INSERT INTO `track` VALUES ('20534', 'front', '2019-01-22 02:00:04', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article?per_page=2', 'aa94717d34d7c9f375a0615ac0c1bcd34698a1e8');
INSERT INTO `track` VALUES ('20535', 'front', '2019-01-22 02:00:06', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article?per_page=2', 'Windows 10', '::1', '/_mebook/article', 'aa94717d34d7c9f375a0615ac0c1bcd34698a1e8');
INSERT INTO `track` VALUES ('20536', 'front', '2019-01-22 02:00:07', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article?per_page=2', 'aa94717d34d7c9f375a0615ac0c1bcd34698a1e8');
INSERT INTO `track` VALUES ('20537', 'front', '2019-01-22 02:00:09', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article?per_page=2', 'Windows 10', '::1', '/_mebook/article', 'aa94717d34d7c9f375a0615ac0c1bcd34698a1e8');
INSERT INTO `track` VALUES ('20538', 'front', '2019-01-22 02:00:23', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article?per_page=2', 'Windows 10', '::1', '/_mebook/article', 'aa94717d34d7c9f375a0615ac0c1bcd34698a1e8');
INSERT INTO `track` VALUES ('20539', 'front', '2019-01-22 02:01:08', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article?per_page=2', 'Windows 10', '::1', '/_mebook/article', 'aa94717d34d7c9f375a0615ac0c1bcd34698a1e8');
INSERT INTO `track` VALUES ('20540', 'front', '2019-01-22 02:01:28', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article?per_page=2', 'Windows 10', '::1', '/_mebook/article', 'aa94717d34d7c9f375a0615ac0c1bcd34698a1e8');
INSERT INTO `track` VALUES ('20541', 'front', '2019-01-22 02:02:08', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article?per_page=2', 'Windows 10', '::1', '/_mebook/article', 'aa94717d34d7c9f375a0615ac0c1bcd34698a1e8');
INSERT INTO `track` VALUES ('20542', 'front', '2019-01-22 02:02:20', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article', 'aa94717d34d7c9f375a0615ac0c1bcd34698a1e8');
INSERT INTO `track` VALUES ('20543', 'front', '2019-01-22 02:02:24', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'aa94717d34d7c9f375a0615ac0c1bcd34698a1e8');
INSERT INTO `track` VALUES ('20544', 'front', '2019-01-22 02:02:27', 'article', 'detail', '2', null, '0', 'http://localhost:81/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'Windows 10', '::1', '/_mebook/article/detail/We-Ensure-better-education--for-a-better-world', 'aa94717d34d7c9f375a0615ac0c1bcd34698a1e8');
INSERT INTO `track` VALUES ('20545', 'front', '2019-01-22 02:02:30', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article/detail/We-Ensure-better-education--for-a-better-world', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'aa94717d34d7c9f375a0615ac0c1bcd34698a1e8');
INSERT INTO `track` VALUES ('20546', 'front', '2019-01-22 02:02:33', 'article', 'detail', '2', null, '0', 'http://localhost:81/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'Windows 10', '::1', '/_mebook/article/detail/We-Ensure-better-education--for-a-better-world', 'aa94717d34d7c9f375a0615ac0c1bcd34698a1e8');
INSERT INTO `track` VALUES ('20547', 'front', '2019-01-22 02:03:23', 'article', 'detail', '2', null, '0', 'http://localhost:81/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'Windows 10', '::1', '/_mebook/article/detail/We-Ensure-better-education--for-a-better-world', 'f8cb3c927582726f38a3cfe35e61689e8ffde966');
INSERT INTO `track` VALUES ('20548', 'front', '2019-01-22 02:03:25', 'article', 'detail', '2', null, '0', 'http://localhost:81/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'Windows 10', '::1', '/_mebook/article/detail/We-Ensure-better-education--for-a-better-world', 'f8cb3c927582726f38a3cfe35e61689e8ffde966');
INSERT INTO `track` VALUES ('20549', 'front', '2019-01-22 02:04:27', 'article', 'detail', '2', null, '0', 'http://localhost:81/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'Windows 10', '::1', '/_mebook/article/detail/We-Ensure-better-education--for-a-better-world', 'f8cb3c927582726f38a3cfe35e61689e8ffde966');
INSERT INTO `track` VALUES ('20550', 'front', '2019-01-22 02:08:15', 'article', 'detail', '2', null, '0', 'http://localhost:81/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'Windows 10', '::1', '/_mebook/article/detail/We-Ensure-better-education--for-a-better-world', 'f8cb3c927582726f38a3cfe35e61689e8ffde966');
INSERT INTO `track` VALUES ('20551', 'front', '2019-01-22 02:08:17', 'article', 'detail', '2', null, '0', 'http://localhost:81/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'Windows 10', '::1', '/_mebook/article/detail/We-Ensure-better-education--for-a-better-world', 'f8cb3c927582726f38a3cfe35e61689e8ffde966');
INSERT INTO `track` VALUES ('20552', 'front', '2019-01-22 02:09:04', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article/detail/We-Ensure-better-education--for-a-better-world', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'be33c2c414b5efd6f61e3c0ad660ea62d50a300e');
INSERT INTO `track` VALUES ('20553', 'front', '2019-01-22 02:09:14', 'article', 'detail', '2', null, '0', 'http://localhost:81/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'Windows 10', '::1', '/_mebook/article/detail/We-Ensure-better-education--for-a-better-world', 'be33c2c414b5efd6f61e3c0ad660ea62d50a300e');
INSERT INTO `track` VALUES ('20554', 'front', '2019-01-22 02:09:27', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article/detail/We-Ensure-better-education--for-a-better-world', 'Windows 10', '::1', '/_mebook/article', 'be33c2c414b5efd6f61e3c0ad660ea62d50a300e');
INSERT INTO `track` VALUES ('20555', 'front', '2019-01-22 02:09:48', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/article', 'be33c2c414b5efd6f61e3c0ad660ea62d50a300e');
INSERT INTO `track` VALUES ('20556', 'front', '2019-01-22 02:09:58', 'article', 'detail', '2', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/We-Ensure-better-education--for-a-better-world', 'be33c2c414b5efd6f61e3c0ad660ea62d50a300e');
INSERT INTO `track` VALUES ('20557', 'front', '2019-01-22 02:10:29', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/article/category/%E0%B8%9A%E0%B8%97%E0%B8%84%E0%B8%A7%E0%B8%B2%E0%B8%A1%E0%B8%97%E0%B8%B1%E0%B9%88%E0%B8%A7%E0%B9%84%E0%B8%9B', 'Windows 10', '::1', '/_mebook/', 'be33c2c414b5efd6f61e3c0ad660ea62d50a300e');
INSERT INTO `track` VALUES ('20558', 'front', '2019-01-22 02:10:31', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/article', 'be33c2c414b5efd6f61e3c0ad660ea62d50a300e');
INSERT INTO `track` VALUES ('20559', 'front', '2019-01-22 10:29:05', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'ce340a39dc0786c42ea16b7847aa127b8a393db5');
INSERT INTO `track` VALUES ('20560', 'front', '2019-01-22 10:29:30', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'ce340a39dc0786c42ea16b7847aa127b8a393db5');
INSERT INTO `track` VALUES ('20561', 'front', '2019-01-22 10:30:19', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'ce340a39dc0786c42ea16b7847aa127b8a393db5');
INSERT INTO `track` VALUES ('20562', 'front', '2019-01-22 10:30:30', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'ce340a39dc0786c42ea16b7847aa127b8a393db5');
INSERT INTO `track` VALUES ('20563', 'front', '2019-01-22 10:36:50', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'e26a894d31b51a409bcc3716a6dde509f9f8cac1');
INSERT INTO `track` VALUES ('20564', 'front', '2019-01-22 10:37:01', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'e26a894d31b51a409bcc3716a6dde509f9f8cac1');
INSERT INTO `track` VALUES ('20565', 'front', '2019-01-22 10:37:55', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'e26a894d31b51a409bcc3716a6dde509f9f8cac1');
INSERT INTO `track` VALUES ('20566', 'front', '2019-01-22 10:38:37', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'e26a894d31b51a409bcc3716a6dde509f9f8cac1');
INSERT INTO `track` VALUES ('20567', 'front', '2019-01-22 10:39:38', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'e26a894d31b51a409bcc3716a6dde509f9f8cac1');
INSERT INTO `track` VALUES ('20568', 'front', '2019-01-22 10:41:23', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'e26a894d31b51a409bcc3716a6dde509f9f8cac1');
INSERT INTO `track` VALUES ('20569', 'front', '2019-01-22 10:41:54', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'e7f84c5a64761f034e8bf9cdae60cf32391465e1');
INSERT INTO `track` VALUES ('20570', 'front', '2019-01-22 10:42:40', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'e7f84c5a64761f034e8bf9cdae60cf32391465e1');
INSERT INTO `track` VALUES ('20571', 'front', '2019-01-22 10:43:36', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'e7f84c5a64761f034e8bf9cdae60cf32391465e1');
INSERT INTO `track` VALUES ('20572', 'front', '2019-01-22 10:43:38', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'e7f84c5a64761f034e8bf9cdae60cf32391465e1');
INSERT INTO `track` VALUES ('20573', 'front', '2019-01-22 10:45:43', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'e7f84c5a64761f034e8bf9cdae60cf32391465e1');
INSERT INTO `track` VALUES ('20574', 'front', '2019-01-22 10:54:36', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'eca47b94eae4a9d8a0434f0411ab411f4fa3fa78');
INSERT INTO `track` VALUES ('20575', 'front', '2019-01-22 10:56:58', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'eca47b94eae4a9d8a0434f0411ab411f4fa3fa78');
INSERT INTO `track` VALUES ('20576', 'front', '2019-01-22 11:00:32', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '76313939bd05f404d18ec93902fb4b119002463a');
INSERT INTO `track` VALUES ('20577', 'front', '2019-01-22 11:01:50', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '76313939bd05f404d18ec93902fb4b119002463a');
INSERT INTO `track` VALUES ('20578', 'front', '2019-01-22 11:02:03', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '76313939bd05f404d18ec93902fb4b119002463a');
INSERT INTO `track` VALUES ('20579', 'front', '2019-01-22 11:02:21', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '76313939bd05f404d18ec93902fb4b119002463a');
INSERT INTO `track` VALUES ('20580', 'front', '2019-01-22 11:02:42', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '76313939bd05f404d18ec93902fb4b119002463a');
INSERT INTO `track` VALUES ('20581', 'front', '2019-01-22 12:51:23', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '9ee8f06955bb53197f56ac38d937e0a107f1aec7');
INSERT INTO `track` VALUES ('20582', 'front', '2019-01-22 12:51:30', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '9ee8f06955bb53197f56ac38d937e0a107f1aec7');
INSERT INTO `track` VALUES ('20583', 'front', '2019-01-22 12:51:33', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '9ee8f06955bb53197f56ac38d937e0a107f1aec7');
INSERT INTO `track` VALUES ('20584', 'front', '2019-01-22 12:52:04', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '9ee8f06955bb53197f56ac38d937e0a107f1aec7');
INSERT INTO `track` VALUES ('20585', 'front', '2019-01-22 12:52:08', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/article', '9ee8f06955bb53197f56ac38d937e0a107f1aec7');
INSERT INTO `track` VALUES ('20586', 'front', '2019-01-22 12:52:10', 'article', 'detail', '2', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/We-Ensure-better-education--for-a-better-world', '9ee8f06955bb53197f56ac38d937e0a107f1aec7');
INSERT INTO `track` VALUES ('20587', 'front', '2019-01-22 13:12:14', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article/detail/We-Ensure-better-education--for-a-better-world', 'Windows 10', '::1', '/_mebook/article', 'ca3ef640288298cc358c7337543d02c79ae9f67f');
INSERT INTO `track` VALUES ('20588', 'front', '2019-01-22 15:25:38', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '500cc51085b4d7eb7070a6cd88f3b1edfb86837c');
INSERT INTO `track` VALUES ('20589', 'front', '2019-01-22 20:06:33', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/course_content/order/5', 'Windows 10', '::1', '/_mebook/', 'fa2d3d846ef1a7b3a66455ffd5a361fa515428c7');
INSERT INTO `track` VALUES ('20590', 'front', '2019-01-22 20:23:25', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/course_content/order/5', 'Windows 10', '::1', '/_mebook/', 'e9efcb557f0b59155996950e2fddecca777bab8a');
INSERT INTO `track` VALUES ('20591', 'front', '2019-01-22 20:23:46', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/course_content/order/5', 'Windows 10', '::1', '/_mebook/', 'e9efcb557f0b59155996950e2fddecca777bab8a');
INSERT INTO `track` VALUES ('20592', 'front', '2019-01-22 20:23:48', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/course_content/order/5', 'Windows 10', '::1', '/_mebook/', 'e9efcb557f0b59155996950e2fddecca777bab8a');
INSERT INTO `track` VALUES ('20593', 'front', '2019-01-22 20:23:49', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/course_content/order/5', 'Windows 10', '::1', '/_mebook/', 'e9efcb557f0b59155996950e2fddecca777bab8a');
INSERT INTO `track` VALUES ('20594', 'front', '2019-01-22 20:23:49', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/course_content/order/5', 'Windows 10', '::1', '/_mebook/', 'e9efcb557f0b59155996950e2fddecca777bab8a');
INSERT INTO `track` VALUES ('20595', 'front', '2019-01-22 20:23:49', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/course_content/order/5', 'Windows 10', '::1', '/_mebook/', 'e9efcb557f0b59155996950e2fddecca777bab8a');
INSERT INTO `track` VALUES ('20596', 'front', '2019-01-22 20:23:50', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/course_content/order/5', 'Windows 10', '::1', '/_mebook/', 'e9efcb557f0b59155996950e2fddecca777bab8a');
INSERT INTO `track` VALUES ('20597', 'front', '2019-01-22 20:32:24', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/course_content/order/5', 'Windows 10', '::1', '/_mebook/', 'e7dbb3c6ef4c4a218a25d1c64a77afecb6a9abcc');
INSERT INTO `track` VALUES ('20598', 'front', '2019-01-22 20:32:27', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/course_content/order/5', 'Windows 10', '::1', '/_mebook/', 'e7dbb3c6ef4c4a218a25d1c64a77afecb6a9abcc');
INSERT INTO `track` VALUES ('20599', 'front', '2019-01-22 20:32:29', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/course_content/order/5', 'Windows 10', '::1', '/_mebook/', 'e7dbb3c6ef4c4a218a25d1c64a77afecb6a9abcc');
INSERT INTO `track` VALUES ('20600', 'front', '2019-01-22 20:32:29', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/course_content/order/5', 'Windows 10', '::1', '/_mebook/', 'e7dbb3c6ef4c4a218a25d1c64a77afecb6a9abcc');
INSERT INTO `track` VALUES ('20601', 'front', '2019-01-22 20:32:30', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/course_content/order/5', 'Windows 10', '::1', '/_mebook/', 'e7dbb3c6ef4c4a218a25d1c64a77afecb6a9abcc');
INSERT INTO `track` VALUES ('20602', 'front', '2019-01-22 20:32:30', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/course_content/order/5', 'Windows 10', '::1', '/_mebook/', 'e7dbb3c6ef4c4a218a25d1c64a77afecb6a9abcc');
INSERT INTO `track` VALUES ('20603', 'front', '2019-01-22 20:32:30', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/course_content/order/5', 'Windows 10', '::1', '/_mebook/', 'e7dbb3c6ef4c4a218a25d1c64a77afecb6a9abcc');
INSERT INTO `track` VALUES ('20604', 'front', '2019-01-22 20:32:30', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/course_content/order/5', 'Windows 10', '::1', '/_mebook/', 'e7dbb3c6ef4c4a218a25d1c64a77afecb6a9abcc');
INSERT INTO `track` VALUES ('20605', 'front', '2019-01-22 20:51:03', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '476b2593ca23726a3c74791f89aa12332b51baae');
INSERT INTO `track` VALUES ('20606', 'front', '2019-01-22 21:07:32', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/course_content/order/5', 'Windows 10', '::1', '/_mebook/', '7b6dc627aad1e186ae339313877dfd1732156cf4');
INSERT INTO `track` VALUES ('20607', 'front', '2019-01-22 21:09:26', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/course_content/order/5', 'iOS', '::1', '/_mebook/', '7b6dc627aad1e186ae339313877dfd1732156cf4');
INSERT INTO `track` VALUES ('20608', 'front', '2019-01-22 21:18:13', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/course_content/order/5', 'iOS', '::1', '/_mebook/', '96c11b31bec1363cff9fe698ecbbeadd1053bcc8');
INSERT INTO `track` VALUES ('20609', 'front', '2019-01-22 21:18:15', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/course_content/order/5', 'iOS', '::1', '/_mebook/', '96c11b31bec1363cff9fe698ecbbeadd1053bcc8');
INSERT INTO `track` VALUES ('20610', 'front', '2019-01-22 21:18:28', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/course_content/order/5', 'iOS', '::1', '/_mebook/', '96c11b31bec1363cff9fe698ecbbeadd1053bcc8');
INSERT INTO `track` VALUES ('20611', 'front', '2019-01-22 21:18:41', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/course_content/order/5', 'iOS', '::1', '/_mebook/', '96c11b31bec1363cff9fe698ecbbeadd1053bcc8');
INSERT INTO `track` VALUES ('20612', 'front', '2019-01-22 21:18:43', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/course_content/order/5', 'iOS', '::1', '/_mebook/', '96c11b31bec1363cff9fe698ecbbeadd1053bcc8');
INSERT INTO `track` VALUES ('20613', 'front', '2019-01-22 21:18:43', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/course_content/order/5', 'iOS', '::1', '/_mebook/', '96c11b31bec1363cff9fe698ecbbeadd1053bcc8');
INSERT INTO `track` VALUES ('20614', 'front', '2019-01-22 21:19:30', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/course_content/order/5', 'Windows 10', '::1', '/_mebook/', '96c11b31bec1363cff9fe698ecbbeadd1053bcc8');
INSERT INTO `track` VALUES ('20615', 'front', '2019-01-22 22:12:29', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/dashboard', 'Windows 10', '::1', '/_mebook/', 'e938cf714ed2bdc582f8f73ff05e695c626e0356');
INSERT INTO `track` VALUES ('20616', 'front', '2019-01-22 22:13:32', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'e938cf714ed2bdc582f8f73ff05e695c626e0356');
INSERT INTO `track` VALUES ('20617', 'front', '2019-01-22 22:14:14', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'e938cf714ed2bdc582f8f73ff05e695c626e0356');
INSERT INTO `track` VALUES ('20618', 'front', '2019-01-22 22:15:04', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/dashboard', 'Windows 10', '::1', '/_mebook/', 'e938cf714ed2bdc582f8f73ff05e695c626e0356');
INSERT INTO `track` VALUES ('20619', 'front', '2019-01-22 22:15:13', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/dashboard', 'Windows 10', '::1', '/_mebook/', 'e938cf714ed2bdc582f8f73ff05e695c626e0356');
INSERT INTO `track` VALUES ('20620', 'front', '2019-01-22 22:19:07', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/dashboard', 'Windows 10', '::1', '/_mebook/', 'b3969709a4154bbc078340f413fab4a85523f42d');
INSERT INTO `track` VALUES ('20621', 'front', '2019-01-22 22:19:39', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'b3969709a4154bbc078340f413fab4a85523f42d');
INSERT INTO `track` VALUES ('20622', 'front', '2019-01-22 22:21:04', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'b3969709a4154bbc078340f413fab4a85523f42d');
INSERT INTO `track` VALUES ('20623', 'front', '2019-01-22 22:21:05', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'b3969709a4154bbc078340f413fab4a85523f42d');
INSERT INTO `track` VALUES ('20624', 'front', '2019-01-22 22:21:05', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'b3969709a4154bbc078340f413fab4a85523f42d');
INSERT INTO `track` VALUES ('20625', 'front', '2019-01-22 22:21:05', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'b3969709a4154bbc078340f413fab4a85523f42d');
INSERT INTO `track` VALUES ('20626', 'front', '2019-01-22 22:21:05', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'b3969709a4154bbc078340f413fab4a85523f42d');
INSERT INTO `track` VALUES ('20627', 'front', '2019-01-22 22:21:06', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'b3969709a4154bbc078340f413fab4a85523f42d');
INSERT INTO `track` VALUES ('20628', 'front', '2019-01-22 22:21:06', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'b3969709a4154bbc078340f413fab4a85523f42d');
INSERT INTO `track` VALUES ('20629', 'front', '2019-01-22 22:21:06', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'b3969709a4154bbc078340f413fab4a85523f42d');
INSERT INTO `track` VALUES ('20630', 'front', '2019-01-22 22:21:06', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'b3969709a4154bbc078340f413fab4a85523f42d');
INSERT INTO `track` VALUES ('20631', 'front', '2019-01-22 22:21:06', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'b3969709a4154bbc078340f413fab4a85523f42d');
INSERT INTO `track` VALUES ('20632', 'front', '2019-01-22 22:21:07', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'b3969709a4154bbc078340f413fab4a85523f42d');
INSERT INTO `track` VALUES ('20633', 'front', '2019-01-22 22:21:07', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'b3969709a4154bbc078340f413fab4a85523f42d');
INSERT INTO `track` VALUES ('20634', 'front', '2019-01-22 22:21:07', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'b3969709a4154bbc078340f413fab4a85523f42d');
INSERT INTO `track` VALUES ('20635', 'front', '2019-01-22 22:21:07', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'b3969709a4154bbc078340f413fab4a85523f42d');
INSERT INTO `track` VALUES ('20636', 'front', '2019-01-22 22:21:07', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'b3969709a4154bbc078340f413fab4a85523f42d');
INSERT INTO `track` VALUES ('20637', 'front', '2019-01-22 22:22:12', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'b3969709a4154bbc078340f413fab4a85523f42d');
INSERT INTO `track` VALUES ('20638', 'front', '2019-01-22 22:24:26', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20639', 'front', '2019-01-22 22:24:27', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20640', 'front', '2019-01-22 22:24:27', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20641', 'front', '2019-01-22 22:24:27', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20642', 'front', '2019-01-22 22:24:27', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20643', 'front', '2019-01-22 22:24:28', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20644', 'front', '2019-01-22 22:24:28', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20645', 'front', '2019-01-22 22:24:28', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20646', 'front', '2019-01-22 22:24:28', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20647', 'front', '2019-01-22 22:24:28', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20648', 'front', '2019-01-22 22:24:42', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20649', 'front', '2019-01-22 22:24:43', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20650', 'front', '2019-01-22 22:24:44', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20651', 'front', '2019-01-22 22:24:44', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20652', 'front', '2019-01-22 22:24:44', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20653', 'front', '2019-01-22 22:24:44', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20654', 'front', '2019-01-22 22:24:44', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20655', 'front', '2019-01-22 22:24:45', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20656', 'front', '2019-01-22 22:24:45', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20657', 'front', '2019-01-22 22:24:45', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20658', 'front', '2019-01-22 22:24:45', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20659', 'front', '2019-01-22 22:24:45', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20660', 'front', '2019-01-22 22:24:45', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20661', 'front', '2019-01-22 22:24:46', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'iOS', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20662', 'front', '2019-01-22 22:28:15', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20663', 'front', '2019-01-22 22:28:16', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20664', 'front', '2019-01-22 22:28:16', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20665', 'front', '2019-01-22 22:28:16', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20666', 'front', '2019-01-22 22:28:17', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20667', 'front', '2019-01-22 22:28:17', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20668', 'front', '2019-01-22 22:28:17', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20669', 'front', '2019-01-22 22:28:17', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20670', 'front', '2019-01-22 22:28:17', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20671', 'front', '2019-01-22 22:28:17', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20672', 'front', '2019-01-22 22:28:18', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20673', 'front', '2019-01-22 22:28:18', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20674', 'front', '2019-01-22 22:28:18', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20675', 'front', '2019-01-22 22:28:18', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20676', 'front', '2019-01-22 22:28:19', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20677', 'front', '2019-01-22 22:29:17', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', 'a3ec27ec9105405271b7dd6761780a65561b1803');
INSERT INTO `track` VALUES ('20678', 'front', '2019-01-22 22:30:33', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20679', 'front', '2019-01-22 22:30:34', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20680', 'front', '2019-01-22 22:30:34', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20681', 'front', '2019-01-22 22:30:34', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20682', 'front', '2019-01-22 22:30:34', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20683', 'front', '2019-01-22 22:30:35', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20684', 'front', '2019-01-22 22:30:35', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20685', 'front', '2019-01-22 22:30:35', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20686', 'front', '2019-01-22 22:30:35', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20687', 'front', '2019-01-22 22:30:35', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20688', 'front', '2019-01-22 22:30:36', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20689', 'front', '2019-01-22 22:30:36', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20690', 'front', '2019-01-22 22:30:36', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20691', 'front', '2019-01-22 22:30:36', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20692', 'front', '2019-01-22 22:30:37', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20693', 'front', '2019-01-22 22:30:37', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20694', 'front', '2019-01-22 22:30:37', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20695', 'front', '2019-01-22 22:30:37', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20696', 'front', '2019-01-22 22:30:37', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20697', 'front', '2019-01-22 22:30:37', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20698', 'front', '2019-01-22 22:30:38', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20699', 'front', '2019-01-22 22:30:38', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20700', 'front', '2019-01-22 22:30:38', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20701', 'front', '2019-01-22 22:30:38', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20702', 'front', '2019-01-22 22:30:38', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20703', 'front', '2019-01-22 22:32:03', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/dashboard', 'Windows 10', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20704', 'front', '2019-01-22 22:32:04', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/dashboard', 'Windows 10', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20705', 'front', '2019-01-22 22:32:04', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/dashboard', 'Windows 10', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20706', 'front', '2019-01-22 22:32:04', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/dashboard', 'Windows 10', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20707', 'front', '2019-01-22 22:32:05', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/dashboard', 'Windows 10', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20708', 'front', '2019-01-22 22:32:45', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/dashboard', 'Windows 10', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20709', 'front', '2019-01-22 22:32:46', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/dashboard', 'Windows 10', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20710', 'front', '2019-01-22 22:32:46', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/dashboard', 'Windows 10', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20711', 'front', '2019-01-22 22:32:46', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/dashboard', 'Windows 10', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20712', 'front', '2019-01-22 22:32:47', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/dashboard', 'Windows 10', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20713', 'front', '2019-01-22 22:32:47', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/dashboard', 'Windows 10', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20714', 'front', '2019-01-22 22:35:15', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/dashboard', 'Android', '::1', '/_mebook/', '4771e67dfb26bd3ce997ee5b04de7de80fbfbaa5');
INSERT INTO `track` VALUES ('20715', 'front', '2019-01-22 22:54:51', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'Android', '::1', '/_mebook/', 'fe1e43dbd0223196b6b7127ad8dc6807a63bc4e2');
INSERT INTO `track` VALUES ('20716', 'front', '2019-01-22 22:55:23', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', 'fe1e43dbd0223196b6b7127ad8dc6807a63bc4e2');
INSERT INTO `track` VALUES ('20717', 'front', '2019-01-22 22:56:52', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '0f49ede43c72917da26af1bc0fd0c042525d3c97');
INSERT INTO `track` VALUES ('20718', 'front', '2019-01-22 22:57:43', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '0f49ede43c72917da26af1bc0fd0c042525d3c97');
INSERT INTO `track` VALUES ('20719', 'front', '2019-01-22 23:01:30', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '0f49ede43c72917da26af1bc0fd0c042525d3c97');
INSERT INTO `track` VALUES ('20720', 'front', '2019-01-22 23:01:31', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '0f49ede43c72917da26af1bc0fd0c042525d3c97');
INSERT INTO `track` VALUES ('20721', 'front', '2019-01-22 23:01:31', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '0f49ede43c72917da26af1bc0fd0c042525d3c97');
INSERT INTO `track` VALUES ('20722', 'front', '2019-01-22 23:01:32', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '0f49ede43c72917da26af1bc0fd0c042525d3c97');
INSERT INTO `track` VALUES ('20723', 'front', '2019-01-22 23:01:32', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '0f49ede43c72917da26af1bc0fd0c042525d3c97');
INSERT INTO `track` VALUES ('20724', 'front', '2019-01-22 23:01:32', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '0f49ede43c72917da26af1bc0fd0c042525d3c97');
INSERT INTO `track` VALUES ('20725', 'front', '2019-01-22 23:01:32', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '0f49ede43c72917da26af1bc0fd0c042525d3c97');
INSERT INTO `track` VALUES ('20726', 'front', '2019-01-22 23:01:32', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '0f49ede43c72917da26af1bc0fd0c042525d3c97');
INSERT INTO `track` VALUES ('20727', 'front', '2019-01-22 23:01:33', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '0f49ede43c72917da26af1bc0fd0c042525d3c97');
INSERT INTO `track` VALUES ('20728', 'front', '2019-01-22 23:01:33', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '0f49ede43c72917da26af1bc0fd0c042525d3c97');
INSERT INTO `track` VALUES ('20729', 'front', '2019-01-22 23:01:33', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '0f49ede43c72917da26af1bc0fd0c042525d3c97');
INSERT INTO `track` VALUES ('20730', 'front', '2019-01-22 23:01:33', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '0f49ede43c72917da26af1bc0fd0c042525d3c97');
INSERT INTO `track` VALUES ('20731', 'front', '2019-01-22 23:01:33', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '0f49ede43c72917da26af1bc0fd0c042525d3c97');
INSERT INTO `track` VALUES ('20732', 'front', '2019-01-22 23:01:33', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '0f49ede43c72917da26af1bc0fd0c042525d3c97');
INSERT INTO `track` VALUES ('20733', 'front', '2019-01-22 23:01:34', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '0f49ede43c72917da26af1bc0fd0c042525d3c97');
INSERT INTO `track` VALUES ('20734', 'front', '2019-01-22 23:01:34', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', '0f49ede43c72917da26af1bc0fd0c042525d3c97');
INSERT INTO `track` VALUES ('20735', 'front', '2019-01-22 23:03:22', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Android', '::1', '/_mebook/', 'd8e5b7fa9b2ccdcaf46863e13b553af30f7a8e4b');
INSERT INTO `track` VALUES ('20736', 'front', '2019-01-22 23:03:23', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Android', '::1', '/_mebook/', 'd8e5b7fa9b2ccdcaf46863e13b553af30f7a8e4b');
INSERT INTO `track` VALUES ('20737', 'front', '2019-01-22 23:03:23', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Android', '::1', '/_mebook/', 'd8e5b7fa9b2ccdcaf46863e13b553af30f7a8e4b');
INSERT INTO `track` VALUES ('20738', 'front', '2019-01-22 23:03:23', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Android', '::1', '/_mebook/', 'd8e5b7fa9b2ccdcaf46863e13b553af30f7a8e4b');
INSERT INTO `track` VALUES ('20739', 'front', '2019-01-22 23:03:24', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Android', '::1', '/_mebook/', 'd8e5b7fa9b2ccdcaf46863e13b553af30f7a8e4b');
INSERT INTO `track` VALUES ('20740', 'front', '2019-01-22 23:03:47', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', 'd8e5b7fa9b2ccdcaf46863e13b553af30f7a8e4b');
INSERT INTO `track` VALUES ('20741', 'front', '2019-01-22 23:20:32', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'e9c8a1b2089b438d2f1491ca0f19c7794d0ed150');
INSERT INTO `track` VALUES ('20742', 'front', '2019-01-22 23:20:34', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'e9c8a1b2089b438d2f1491ca0f19c7794d0ed150');
INSERT INTO `track` VALUES ('20743', 'front', '2019-01-22 23:20:48', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/', 'e9c8a1b2089b438d2f1491ca0f19c7794d0ed150');
INSERT INTO `track` VALUES ('20744', 'front', '2019-01-22 23:53:07', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/', '43ddb16d6cb2848a060256f4954a33cc31fad07c');
INSERT INTO `track` VALUES ('20745', 'front', '2019-01-22 23:53:23', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/', 'e891c3fbb63565779f49629a56b1d6980d42a236');
INSERT INTO `track` VALUES ('20746', 'front', '2019-01-22 23:55:44', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/', 'e891c3fbb63565779f49629a56b1d6980d42a236');
INSERT INTO `track` VALUES ('20747', 'front', '2019-01-22 23:58:06', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/home', 'e891c3fbb63565779f49629a56b1d6980d42a236');
INSERT INTO `track` VALUES ('20748', 'front', '2019-01-22 23:59:20', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20749', 'front', '2019-01-23 00:03:21', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20750', 'front', '2019-01-23 00:03:45', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20751', 'front', '2019-01-23 00:03:51', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20752', 'front', '2019-01-23 00:04:00', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20753', 'front', '2019-01-23 00:04:00', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20754', 'front', '2019-01-23 00:04:01', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20755', 'front', '2019-01-23 00:04:01', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20756', 'front', '2019-01-23 00:04:01', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20757', 'front', '2019-01-23 00:04:01', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20758', 'front', '2019-01-23 00:04:01', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20759', 'front', '2019-01-23 00:04:02', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20760', 'front', '2019-01-23 00:04:02', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20761', 'front', '2019-01-23 00:04:02', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20762', 'front', '2019-01-23 00:04:02', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20763', 'front', '2019-01-23 00:04:02', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20764', 'front', '2019-01-23 00:04:02', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20765', 'front', '2019-01-23 00:04:03', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20766', 'front', '2019-01-23 00:04:03', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20767', 'front', '2019-01-23 00:04:03', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20768', 'front', '2019-01-23 00:04:03', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20769', 'front', '2019-01-23 00:04:03', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20770', 'front', '2019-01-23 00:04:04', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20771', 'front', '2019-01-23 00:04:04', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20772', 'front', '2019-01-23 00:04:04', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20773', 'front', '2019-01-23 00:04:04', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20774', 'front', '2019-01-23 00:04:04', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20775', 'front', '2019-01-23 00:04:04', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20776', 'front', '2019-01-23 00:04:05', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20777', 'front', '2019-01-23 00:04:05', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20778', 'front', '2019-01-23 00:04:05', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20779', 'front', '2019-01-23 00:04:05', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20780', 'front', '2019-01-23 00:04:05', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20781', 'front', '2019-01-23 00:04:05', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20782', 'front', '2019-01-23 00:04:05', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20783', 'front', '2019-01-23 00:04:06', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20784', 'front', '2019-01-23 00:04:06', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '975fa4e87ccff1b6716d2c8da9649f2ffc941e60');
INSERT INTO `track` VALUES ('20785', 'front', '2019-01-23 00:04:27', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '2f7d9d3249a76142b10ee4fca767100a00421a6c');
INSERT INTO `track` VALUES ('20786', 'front', '2019-01-23 00:06:40', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/home', '2f7d9d3249a76142b10ee4fca767100a00421a6c');
INSERT INTO `track` VALUES ('20787', 'front', '2019-01-23 00:09:58', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/home', '09baba8d8bc11369605f1eae5afc16bd112b3713');
INSERT INTO `track` VALUES ('20788', 'front', '2019-01-23 00:11:43', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'Android', '::1', '/_mebook/home', '09baba8d8bc11369605f1eae5afc16bd112b3713');
INSERT INTO `track` VALUES ('20789', 'front', '2019-01-23 00:14:59', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/featured', 'iOS', '::1', '/_mebook/', '99817372b847ab78e3a4862de4bd650cd2f9a064');
INSERT INTO `track` VALUES ('20790', 'front', '2019-01-23 00:15:02', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/', '99817372b847ab78e3a4862de4bd650cd2f9a064');
INSERT INTO `track` VALUES ('20791', 'front', '2019-01-23 00:15:26', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/', '99817372b847ab78e3a4862de4bd650cd2f9a064');
INSERT INTO `track` VALUES ('20792', 'front', '2019-01-23 00:16:34', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/', '99817372b847ab78e3a4862de4bd650cd2f9a064');
INSERT INTO `track` VALUES ('20793', 'front', '2019-01-23 00:21:14', 'course', 'detail', '5', null, '0', '', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', '684c32fa9e396e0e04d8aaf8e2ff6f4ba6581c90');
INSERT INTO `track` VALUES ('20794', 'front', '2019-01-23 00:24:18', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '684c32fa9e396e0e04d8aaf8e2ff6f4ba6581c90');
INSERT INTO `track` VALUES ('20795', 'front', '2019-01-23 00:26:47', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '9167aadf2b9ab0bd946ba44f2ee5d4fdaa4a782b');
INSERT INTO `track` VALUES ('20796', 'front', '2019-01-23 00:28:48', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '9167aadf2b9ab0bd946ba44f2ee5d4fdaa4a782b');
INSERT INTO `track` VALUES ('20797', 'front', '2019-01-23 00:28:49', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '9167aadf2b9ab0bd946ba44f2ee5d4fdaa4a782b');
INSERT INTO `track` VALUES ('20798', 'front', '2019-01-23 00:28:49', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '9167aadf2b9ab0bd946ba44f2ee5d4fdaa4a782b');
INSERT INTO `track` VALUES ('20799', 'front', '2019-01-23 00:28:49', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '9167aadf2b9ab0bd946ba44f2ee5d4fdaa4a782b');
INSERT INTO `track` VALUES ('20800', 'front', '2019-01-23 00:28:50', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '9167aadf2b9ab0bd946ba44f2ee5d4fdaa4a782b');
INSERT INTO `track` VALUES ('20801', 'front', '2019-01-23 00:28:50', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '9167aadf2b9ab0bd946ba44f2ee5d4fdaa4a782b');
INSERT INTO `track` VALUES ('20802', 'front', '2019-01-23 00:28:50', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '9167aadf2b9ab0bd946ba44f2ee5d4fdaa4a782b');
INSERT INTO `track` VALUES ('20803', 'front', '2019-01-23 00:30:21', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '9167aadf2b9ab0bd946ba44f2ee5d4fdaa4a782b');
INSERT INTO `track` VALUES ('20804', 'front', '2019-01-23 00:32:23', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '713faf27ad535f52d6127084aa8761893a572641');
INSERT INTO `track` VALUES ('20805', 'front', '2019-01-23 00:32:24', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '713faf27ad535f52d6127084aa8761893a572641');
INSERT INTO `track` VALUES ('20806', 'front', '2019-01-23 00:32:24', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '713faf27ad535f52d6127084aa8761893a572641');
INSERT INTO `track` VALUES ('20807', 'front', '2019-01-23 00:32:24', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '713faf27ad535f52d6127084aa8761893a572641');
INSERT INTO `track` VALUES ('20808', 'front', '2019-01-23 00:32:24', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '713faf27ad535f52d6127084aa8761893a572641');
INSERT INTO `track` VALUES ('20809', 'front', '2019-01-23 00:32:24', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '713faf27ad535f52d6127084aa8761893a572641');
INSERT INTO `track` VALUES ('20810', 'front', '2019-01-23 00:32:25', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '713faf27ad535f52d6127084aa8761893a572641');
INSERT INTO `track` VALUES ('20811', 'front', '2019-01-23 00:32:25', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '713faf27ad535f52d6127084aa8761893a572641');
INSERT INTO `track` VALUES ('20812', 'front', '2019-01-23 00:32:25', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '713faf27ad535f52d6127084aa8761893a572641');
INSERT INTO `track` VALUES ('20813', 'front', '2019-01-23 00:34:11', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '713faf27ad535f52d6127084aa8761893a572641');
INSERT INTO `track` VALUES ('20814', 'front', '2019-01-23 00:43:08', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'bbc6151a63accb697f694b36b299862dc511af04');
INSERT INTO `track` VALUES ('20815', 'front', '2019-01-23 10:59:40', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '392239911302b462477e48730457494a33db0230');
INSERT INTO `track` VALUES ('20816', 'front', '2019-01-23 11:06:39', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '318c05a12512ab7cee9b192a971c2180f6de50c7');
INSERT INTO `track` VALUES ('20817', 'front', '2019-01-23 11:06:46', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '318c05a12512ab7cee9b192a971c2180f6de50c7');
INSERT INTO `track` VALUES ('20818', 'front', '2019-01-23 11:08:03', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '318c05a12512ab7cee9b192a971c2180f6de50c7');
INSERT INTO `track` VALUES ('20819', 'front', '2019-01-23 11:08:04', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '318c05a12512ab7cee9b192a971c2180f6de50c7');
INSERT INTO `track` VALUES ('20820', 'front', '2019-01-23 11:08:04', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '318c05a12512ab7cee9b192a971c2180f6de50c7');
INSERT INTO `track` VALUES ('20821', 'front', '2019-01-23 11:08:04', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '318c05a12512ab7cee9b192a971c2180f6de50c7');
INSERT INTO `track` VALUES ('20822', 'front', '2019-01-23 11:08:05', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '318c05a12512ab7cee9b192a971c2180f6de50c7');
INSERT INTO `track` VALUES ('20823', 'front', '2019-01-23 11:08:05', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '318c05a12512ab7cee9b192a971c2180f6de50c7');
INSERT INTO `track` VALUES ('20824', 'front', '2019-01-23 11:08:05', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '318c05a12512ab7cee9b192a971c2180f6de50c7');
INSERT INTO `track` VALUES ('20825', 'front', '2019-01-23 11:08:05', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '318c05a12512ab7cee9b192a971c2180f6de50c7');
INSERT INTO `track` VALUES ('20826', 'front', '2019-01-23 11:09:17', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '318c05a12512ab7cee9b192a971c2180f6de50c7');
INSERT INTO `track` VALUES ('20827', 'front', '2019-01-23 11:09:22', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '318c05a12512ab7cee9b192a971c2180f6de50c7');
INSERT INTO `track` VALUES ('20828', 'front', '2019-01-23 11:09:33', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '318c05a12512ab7cee9b192a971c2180f6de50c7');
INSERT INTO `track` VALUES ('20829', 'front', '2019-01-23 11:09:45', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/', '318c05a12512ab7cee9b192a971c2180f6de50c7');
INSERT INTO `track` VALUES ('20830', 'front', '2019-01-23 11:12:02', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/', '86942a01da0eb688eeb3c7127ce864e7ca7e259d');
INSERT INTO `track` VALUES ('20831', 'front', '2019-01-23 11:12:27', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/article', '86942a01da0eb688eeb3c7127ce864e7ca7e259d');
INSERT INTO `track` VALUES ('20832', 'front', '2019-01-23 11:25:56', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '2743fff6e9c1a27ed951a7a6fbc8086aa63ce7c0');
INSERT INTO `track` VALUES ('20833', 'front', '2019-01-23 11:26:41', 'home', 'payment_method', '0', null, '1', 'http://localhost:81/_mebook/', 'Android', '::1', '/_mebook/home/payment_method', '2743fff6e9c1a27ed951a7a6fbc8086aa63ce7c0');
INSERT INTO `track` VALUES ('20834', 'front', '2019-01-23 11:26:45', 'home', 'help', '0', null, '1', 'http://localhost:81/_mebook/home/payment_method', 'Android', '::1', '/_mebook/home/help', '2743fff6e9c1a27ed951a7a6fbc8086aa63ce7c0');
INSERT INTO `track` VALUES ('20835', 'front', '2019-01-23 11:36:46', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'iOS', '::1', '/_mebook/', '362d8b991f9ac013af89662446e1a72c87213b4d');
INSERT INTO `track` VALUES ('20836', 'front', '2019-01-23 11:38:19', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/user/register', 'iOS', '::1', '/_mebook/', '362d8b991f9ac013af89662446e1a72c87213b4d');
INSERT INTO `track` VALUES ('20837', 'front', '2019-01-23 11:39:08', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/article', '362d8b991f9ac013af89662446e1a72c87213b4d');
INSERT INTO `track` VALUES ('20838', 'front', '2019-01-23 13:52:18', 'home', 'index', '0', '70', '0', 'http://localhost:81/_mebook/user/register/userConferm/YjY0NzBANjU=', 'Windows 10', '::1', '/_mebook/home', 'd4954f05f78f7283097cdf899750dcc1378bf1d5');
INSERT INTO `track` VALUES ('20839', 'front', '2019-01-23 13:52:31', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/favorite', 'Windows 10', '::1', '/_mebook/home', 'd4954f05f78f7283097cdf899750dcc1378bf1d5');
INSERT INTO `track` VALUES ('20840', 'front', '2019-01-24 09:54:45', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '14547fd78c1719b898db423c2a3edd706db38b0b');
INSERT INTO `track` VALUES ('20841', 'front', '2019-01-24 12:09:47', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'c07e09b764dfb814df71fd8d04aeafb28699a1ad');
INSERT INTO `track` VALUES ('20842', 'front', '2019-01-24 12:12:59', 'home', 'index', '0', null, '1', '', 'iOS', '::1', '/_mebook/', 'c07e09b764dfb814df71fd8d04aeafb28699a1ad');
INSERT INTO `track` VALUES ('20843', 'front', '2019-01-24 12:17:10', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '871a299308941b68a81dee5b89c13d120dacf9b1');
INSERT INTO `track` VALUES ('20844', 'front', '2019-01-24 12:17:18', 'home', 'index', '0', null, '1', '', 'iOS', '::1', '/_mebook/', '871a299308941b68a81dee5b89c13d120dacf9b1');
INSERT INTO `track` VALUES ('20845', 'front', '2019-01-24 12:18:36', 'home', 'index', '0', null, '1', '', 'iOS', '::1', '/_mebook/', '871a299308941b68a81dee5b89c13d120dacf9b1');
INSERT INTO `track` VALUES ('20846', 'front', '2019-01-24 12:18:38', 'home', 'index', '0', null, '1', '', 'iOS', '::1', '/_mebook/', '871a299308941b68a81dee5b89c13d120dacf9b1');
INSERT INTO `track` VALUES ('20847', 'front', '2019-01-24 12:19:10', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '871a299308941b68a81dee5b89c13d120dacf9b1');
INSERT INTO `track` VALUES ('20848', 'front', '2019-01-24 12:19:50', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '871a299308941b68a81dee5b89c13d120dacf9b1');
INSERT INTO `track` VALUES ('20849', 'front', '2019-01-24 12:21:21', 'home', 'index', '0', null, '1', '', 'iOS', '::1', '/_mebook/', '871a299308941b68a81dee5b89c13d120dacf9b1');
INSERT INTO `track` VALUES ('20850', 'front', '2019-01-24 12:22:08', 'home', 'index', '0', null, '1', '', 'iOS', '::1', '/_mebook/', 'b1e5411090ad401a3107a28c1b3e45332291eb57');
INSERT INTO `track` VALUES ('20851', 'front', '2019-01-24 12:22:10', 'home', 'index', '0', null, '1', '', 'iOS', '::1', '/_mebook/', 'b1e5411090ad401a3107a28c1b3e45332291eb57');
INSERT INTO `track` VALUES ('20852', 'front', '2019-01-24 12:22:57', 'home', 'index', '0', null, '1', '', 'iOS', '::1', '/_mebook/', 'b1e5411090ad401a3107a28c1b3e45332291eb57');
INSERT INTO `track` VALUES ('20853', 'front', '2019-01-24 12:24:05', 'home', 'index', '0', null, '1', '', 'iOS', '::1', '/_mebook/', 'b1e5411090ad401a3107a28c1b3e45332291eb57');
INSERT INTO `track` VALUES ('20854', 'front', '2019-01-24 12:25:08', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'b1e5411090ad401a3107a28c1b3e45332291eb57');
INSERT INTO `track` VALUES ('20855', 'front', '2019-01-24 12:28:40', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'Windows 10', '::1', '/_mebook/', 'baca80b2d22b2d52a2c37f68bfec809758808540');
INSERT INTO `track` VALUES ('20856', 'front', '2019-01-24 12:28:43', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'Windows 10', '::1', '/_mebook/', 'baca80b2d22b2d52a2c37f68bfec809758808540');
INSERT INTO `track` VALUES ('20857', 'front', '2019-01-24 12:28:57', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'Windows 10', '::1', '/_mebook/', 'baca80b2d22b2d52a2c37f68bfec809758808540');
INSERT INTO `track` VALUES ('20858', 'front', '2019-01-24 12:29:04', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'Windows 10', '::1', '/_mebook/', 'baca80b2d22b2d52a2c37f68bfec809758808540');
INSERT INTO `track` VALUES ('20859', 'front', '2019-01-24 12:33:05', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'iOS', '::1', '/_mebook/', '1406e863874df858f1b2b6599afe5821b42aea4b');
INSERT INTO `track` VALUES ('20860', 'front', '2019-01-24 12:33:43', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'iOS', '::1', '/_mebook/', '1406e863874df858f1b2b6599afe5821b42aea4b');
INSERT INTO `track` VALUES ('20861', 'front', '2019-01-24 12:34:31', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'iOS', '::1', '/_mebook/', '1406e863874df858f1b2b6599afe5821b42aea4b');
INSERT INTO `track` VALUES ('20862', 'front', '2019-01-24 12:35:26', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'Windows 10', '::1', '/_mebook/', '1406e863874df858f1b2b6599afe5821b42aea4b');
INSERT INTO `track` VALUES ('20863', 'front', '2019-01-24 12:37:23', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'iOS', '::1', '/_mebook/', '1406e863874df858f1b2b6599afe5821b42aea4b');
INSERT INTO `track` VALUES ('20864', 'front', '2019-01-24 12:37:59', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'iOS', '::1', '/_mebook/', '1406e863874df858f1b2b6599afe5821b42aea4b');
INSERT INTO `track` VALUES ('20865', 'front', '2019-01-24 12:40:08', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'Android', '::1', '/_mebook/', 'edfd097ecb4c473368aceb4b5697185292c73273');
INSERT INTO `track` VALUES ('20866', 'front', '2019-01-24 12:40:54', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'iOS', '::1', '/_mebook/', 'edfd097ecb4c473368aceb4b5697185292c73273');
INSERT INTO `track` VALUES ('20867', 'front', '2019-01-24 12:40:56', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'iOS', '::1', '/_mebook/', 'edfd097ecb4c473368aceb4b5697185292c73273');
INSERT INTO `track` VALUES ('20868', 'front', '2019-01-24 12:40:56', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'iOS', '::1', '/_mebook/', 'edfd097ecb4c473368aceb4b5697185292c73273');
INSERT INTO `track` VALUES ('20869', 'front', '2019-01-24 12:42:02', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'iOS', '::1', '/_mebook/', 'edfd097ecb4c473368aceb4b5697185292c73273');
INSERT INTO `track` VALUES ('20870', 'front', '2019-01-24 12:42:05', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'iOS', '::1', '/_mebook/', 'edfd097ecb4c473368aceb4b5697185292c73273');
INSERT INTO `track` VALUES ('20871', 'front', '2019-01-24 12:43:42', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'iOS', '::1', '/_mebook/', 'edfd097ecb4c473368aceb4b5697185292c73273');
INSERT INTO `track` VALUES ('20872', 'front', '2019-01-24 12:44:11', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'iOS', '::1', '/_mebook/', 'edfd097ecb4c473368aceb4b5697185292c73273');
INSERT INTO `track` VALUES ('20873', 'front', '2019-01-24 12:44:33', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'iOS', '::1', '/_mebook/', 'edfd097ecb4c473368aceb4b5697185292c73273');
INSERT INTO `track` VALUES ('20874', 'front', '2019-01-24 12:44:44', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'iOS', '::1', '/_mebook/', 'edfd097ecb4c473368aceb4b5697185292c73273');
INSERT INTO `track` VALUES ('20875', 'front', '2019-01-24 12:45:57', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'iOS', '::1', '/_mebook/', '83456dcf8a3d9ec1dd0f92507a68c9c2f894d1c3');
INSERT INTO `track` VALUES ('20876', 'front', '2019-01-24 12:54:13', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'iOS', '::1', '/_mebook/', 'fd14d81b252e62c13853fe785339d28f47c6d664');
INSERT INTO `track` VALUES ('20877', 'front', '2019-01-24 12:54:44', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'iOS', '::1', '/_mebook/', 'fd14d81b252e62c13853fe785339d28f47c6d664');
INSERT INTO `track` VALUES ('20878', 'front', '2019-01-24 12:54:47', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'iOS', '::1', '/_mebook/', 'fd14d81b252e62c13853fe785339d28f47c6d664');
INSERT INTO `track` VALUES ('20879', 'front', '2019-01-24 12:56:23', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'iOS', '::1', '/_mebook/', 'fd14d81b252e62c13853fe785339d28f47c6d664');
INSERT INTO `track` VALUES ('20880', 'front', '2019-01-24 12:56:41', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'iOS', '::1', '/_mebook/', 'fd14d81b252e62c13853fe785339d28f47c6d664');
INSERT INTO `track` VALUES ('20881', 'front', '2019-01-24 12:56:56', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'iOS', '::1', '/_mebook/', 'fd14d81b252e62c13853fe785339d28f47c6d664');
INSERT INTO `track` VALUES ('20882', 'front', '2019-01-24 12:57:49', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'Android', '::1', '/_mebook/', 'fd14d81b252e62c13853fe785339d28f47c6d664');
INSERT INTO `track` VALUES ('20883', 'front', '2019-01-24 12:58:24', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'Android', '::1', '/_mebook/', 'fd14d81b252e62c13853fe785339d28f47c6d664');
INSERT INTO `track` VALUES ('20884', 'front', '2019-01-24 12:59:16', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'Android', '::1', '/_mebook/', 'b5c7d60c2d6818dc7151a93cf9ee324833d95e0c');
INSERT INTO `track` VALUES ('20885', 'front', '2019-01-24 13:00:12', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/banner/edit/YjY0MTNANw==', 'Windows 10', '::1', '/_mebook/', 'b5c7d60c2d6818dc7151a93cf9ee324833d95e0c');
INSERT INTO `track` VALUES ('20886', 'front', '2019-01-24 13:00:45', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', 'b5c7d60c2d6818dc7151a93cf9ee324833d95e0c');
INSERT INTO `track` VALUES ('20887', 'front', '2019-01-24 13:00:53', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'b5c7d60c2d6818dc7151a93cf9ee324833d95e0c');
INSERT INTO `track` VALUES ('20888', 'front', '2019-01-24 13:00:56', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/article', 'b5c7d60c2d6818dc7151a93cf9ee324833d95e0c');
INSERT INTO `track` VALUES ('20889', 'front', '2019-01-24 13:01:01', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/article', 'b5c7d60c2d6818dc7151a93cf9ee324833d95e0c');
INSERT INTO `track` VALUES ('20890', 'front', '2019-01-24 13:01:11', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'b5c7d60c2d6818dc7151a93cf9ee324833d95e0c');
INSERT INTO `track` VALUES ('20891', 'front', '2019-01-24 13:01:38', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', 'b5c7d60c2d6818dc7151a93cf9ee324833d95e0c');
INSERT INTO `track` VALUES ('20892', 'front', '2019-01-24 13:07:38', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'Android', '::1', '/_mebook/', 'f44d9f4781fc1b7c48474dc27e9e6283ae5daa19');
INSERT INTO `track` VALUES ('20893', 'front', '2019-01-24 13:08:07', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'Android', '::1', '/_mebook/', 'f44d9f4781fc1b7c48474dc27e9e6283ae5daa19');
INSERT INTO `track` VALUES ('20894', 'front', '2019-01-24 13:08:38', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'iOS', '::1', '/_mebook/', 'f44d9f4781fc1b7c48474dc27e9e6283ae5daa19');
INSERT INTO `track` VALUES ('20895', 'front', '2019-01-24 13:08:51', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'iOS', '::1', '/_mebook/', 'f44d9f4781fc1b7c48474dc27e9e6283ae5daa19');
INSERT INTO `track` VALUES ('20896', 'front', '2019-01-24 13:16:53', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', '06c3b59168fcb4022d5c45d9010d52148facdfa7');
INSERT INTO `track` VALUES ('20897', 'front', '2019-01-24 13:18:13', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', '06c3b59168fcb4022d5c45d9010d52148facdfa7');
INSERT INTO `track` VALUES ('20898', 'front', '2019-01-24 13:18:30', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', '06c3b59168fcb4022d5c45d9010d52148facdfa7');
INSERT INTO `track` VALUES ('20899', 'front', '2019-01-24 13:19:09', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', '06c3b59168fcb4022d5c45d9010d52148facdfa7');
INSERT INTO `track` VALUES ('20900', 'front', '2019-01-24 13:19:19', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', '06c3b59168fcb4022d5c45d9010d52148facdfa7');
INSERT INTO `track` VALUES ('20901', 'front', '2019-01-24 13:19:51', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', '06c3b59168fcb4022d5c45d9010d52148facdfa7');
INSERT INTO `track` VALUES ('20902', 'front', '2019-01-24 13:20:14', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/article', '06c3b59168fcb4022d5c45d9010d52148facdfa7');
INSERT INTO `track` VALUES ('20903', 'front', '2019-01-24 13:20:16', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/', '06c3b59168fcb4022d5c45d9010d52148facdfa7');
INSERT INTO `track` VALUES ('20904', 'front', '2019-01-24 13:21:47', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/', '06c3b59168fcb4022d5c45d9010d52148facdfa7');
INSERT INTO `track` VALUES ('20905', 'front', '2019-01-24 13:43:19', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'ddcda21b749b83d029603d7e2a28418e8ee89167');
INSERT INTO `track` VALUES ('20906', 'front', '2019-01-24 16:49:48', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'b42aa30aa6b769d67aa1c9e728cad17ca84fd1b9');
INSERT INTO `track` VALUES ('20907', 'front', '2019-01-24 21:42:28', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '97457b1ba2d566c3c3e597dcffbb54b5fa3dba12');
INSERT INTO `track` VALUES ('20908', 'front', '2019-01-24 21:48:37', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '6f10426250dd19f4eeaf255f30292b8207732718');
INSERT INTO `track` VALUES ('20909', 'front', '2019-01-24 21:48:37', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '6f10426250dd19f4eeaf255f30292b8207732718');
INSERT INTO `track` VALUES ('20910', 'front', '2019-01-24 21:48:37', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '6f10426250dd19f4eeaf255f30292b8207732718');
INSERT INTO `track` VALUES ('20911', 'front', '2019-01-24 21:48:38', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '6f10426250dd19f4eeaf255f30292b8207732718');
INSERT INTO `track` VALUES ('20912', 'front', '2019-01-24 21:48:38', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '6f10426250dd19f4eeaf255f30292b8207732718');
INSERT INTO `track` VALUES ('20913', 'front', '2019-01-24 21:48:38', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '6f10426250dd19f4eeaf255f30292b8207732718');
INSERT INTO `track` VALUES ('20914', 'front', '2019-01-24 21:49:01', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '6f10426250dd19f4eeaf255f30292b8207732718');
INSERT INTO `track` VALUES ('20915', 'front', '2019-01-24 21:57:45', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/', '4edc1c7bae87112fc97d7af827bb0d1b678630d8');
INSERT INTO `track` VALUES ('20916', 'front', '2019-01-24 22:03:51', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '4f63432c03fd3771b6e015d3932f840abc8fd601');
INSERT INTO `track` VALUES ('20917', 'front', '2019-01-24 22:21:51', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '1fa6c241920ead9ff5865a9f2ac88de4e985058c');
INSERT INTO `track` VALUES ('20918', 'front', '2019-01-24 22:27:03', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/', 'fe89e73a832c0dc70c2bf60160bafdde8db5038e');
INSERT INTO `track` VALUES ('20919', 'front', '2019-01-24 22:31:02', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/', '9f12d6c39eccca74c50cc0287d9ca1fb067cc7b4');
INSERT INTO `track` VALUES ('20920', 'front', '2019-01-24 23:10:49', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/', '68c7ebb8e39ab7aef751306dfa1f881678597676');
INSERT INTO `track` VALUES ('20921', 'front', '2019-01-24 23:15:29', 'article', 'index', '0', '23', '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/article', 'ab7e574b6a47e98ffe01d3a57dbd5f9c0bda33de');
INSERT INTO `track` VALUES ('20922', 'front', '2019-01-24 23:16:05', 'article', 'index', '0', '23', '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/article', 'b82910e28bb2012731a6057a7c6071dc286940e3');
INSERT INTO `track` VALUES ('20923', 'front', '2019-01-24 23:17:00', 'article', 'index', '0', '23', '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/article', 'b82910e28bb2012731a6057a7c6071dc286940e3');
INSERT INTO `track` VALUES ('20924', 'front', '2019-01-24 23:17:26', 'article', 'index', '0', '23', '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/article', 'b82910e28bb2012731a6057a7c6071dc286940e3');
INSERT INTO `track` VALUES ('20925', 'front', '2019-01-24 23:17:49', 'article', 'index', '0', '23', '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/article', 'b82910e28bb2012731a6057a7c6071dc286940e3');
INSERT INTO `track` VALUES ('20926', 'front', '2019-01-24 23:18:22', 'article', 'index', '0', '23', '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/article', 'b82910e28bb2012731a6057a7c6071dc286940e3');
INSERT INTO `track` VALUES ('20927', 'front', '2019-01-24 23:24:33', 'home', 'index', '0', '23', '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/', '32a6fa3f169c5b59f1c19b168d834d96734760f5');
INSERT INTO `track` VALUES ('20928', 'front', '2019-01-24 23:39:10', 'article', 'index', '0', '23', '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/article', '3da75729993c93421df1d842bc5e93ff82c85f6e');
INSERT INTO `track` VALUES ('20929', 'front', '2019-01-24 23:56:22', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/home', '648377ad937e94c20100eb452228908e3af2c4b4');
INSERT INTO `track` VALUES ('20930', 'front', '2019-01-25 00:06:12', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '2968752ecdad2464e0f27a5a63da23d2e0206efd');
INSERT INTO `track` VALUES ('20931', 'front', '2019-01-25 13:45:09', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3d72e333e1cf3066f4abb7a24497c2236eed058e');
INSERT INTO `track` VALUES ('20932', 'front', '2019-01-25 14:13:13', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '650f73f4d07a5cd6a43ee9ec5aefb7078700e86e');
INSERT INTO `track` VALUES ('20933', 'front', '2019-01-25 15:20:04', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '707586a1a1ef3e3cc785663a8b43e80c158adae8');
INSERT INTO `track` VALUES ('20934', 'front', '2019-01-25 16:02:19', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '06972417f983a03cd3428bf7eb9fa0147009f254');
INSERT INTO `track` VALUES ('20935', 'front', '2019-01-26 14:04:04', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '88d53e5e319d1d81c707b45a757126fe516d0113');
INSERT INTO `track` VALUES ('20936', 'front', '2019-01-26 14:20:31', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'c4c6551cdae466cbd0bdd53e53a9f89c55bf4598');
INSERT INTO `track` VALUES ('20937', 'front', '2019-01-26 14:20:37', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/article', 'c4c6551cdae466cbd0bdd53e53a9f89c55bf4598');
INSERT INTO `track` VALUES ('20938', 'front', '2019-01-26 14:41:54', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'adc33bfa89d91f518d8b35591eef0796790a6e81');
INSERT INTO `track` VALUES ('20939', 'front', '2019-01-26 15:24:34', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3b55c35d1da36c30fca1d93d6e3f8083b7bc6ffb');
INSERT INTO `track` VALUES ('20940', 'front', '2019-01-26 21:21:58', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '7a8d473c28c2729f91ac4d52d177e12b8c71b6a1');
INSERT INTO `track` VALUES ('20941', 'front', '2019-01-26 21:57:34', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'ae584c0381d7c635537a48c12aa1f0e59e4e3965');
INSERT INTO `track` VALUES ('20942', 'front', '2019-01-26 23:53:59', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '95c6468169f95f9b69ce2683fda5fdfadbd1b1df');
INSERT INTO `track` VALUES ('20943', 'front', '2019-01-28 09:43:43', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '59b453f92e682ab08f021a24a03f24586327adff');
INSERT INTO `track` VALUES ('20944', 'front', '2019-01-29 15:12:05', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'eade1ed24d4d5ad7cca2523ce25b74fd1c8194e1');
INSERT INTO `track` VALUES ('20945', 'front', '2019-01-29 19:55:40', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '890d92a0200060a6bcf00d8abf669629f460964a');
INSERT INTO `track` VALUES ('20946', 'front', '2019-01-30 00:44:03', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'bfc344537e1f39d7530fbebcc5384044e1812d85');
INSERT INTO `track` VALUES ('20947', 'front', '2019-01-30 13:34:53', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '8064ee1e0b0d030e4ccba6cfc42884e853e87ec6');
INSERT INTO `track` VALUES ('20948', 'front', '2019-01-30 15:57:05', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '395801c179c9214a8b9f0fc8ae5afbe14f154f2a');
INSERT INTO `track` VALUES ('20949', 'front', '2019-01-30 21:48:29', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '26a9a14feb9061d924ead9a0f659a0e7256d111c');
INSERT INTO `track` VALUES ('20950', 'front', '2019-01-31 02:21:42', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '7bd66b8eda45206adde96cc87ed7e067daadd358');
INSERT INTO `track` VALUES ('20951', 'front', '2019-01-31 11:24:42', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '79ac4f5265ccd15bf34c9a15eab946a161d0ad56');
INSERT INTO `track` VALUES ('20952', 'front', '2019-01-31 22:03:47', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '757ea0359353646046273ef065053d9a8b8aef17');
INSERT INTO `track` VALUES ('20953', 'front', '2019-01-31 22:39:05', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'b8c7666bdc581a295c2b3d0d6d70e6997cca2fe2');
INSERT INTO `track` VALUES ('20954', 'front', '2019-02-01 00:47:04', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '0853df1d1acb8937a0fd0c3e3fd248962ac021e2');
INSERT INTO `track` VALUES ('20955', 'front', '2019-02-01 09:24:54', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'ef999ed068f67c490b339f8d90407fe88f45a2d4');
INSERT INTO `track` VALUES ('20956', 'front', '2019-02-02 10:48:44', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '55befe1b80a5ae7032bba479f829daaedad71e54');
INSERT INTO `track` VALUES ('20957', 'front', '2019-02-03 15:25:15', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'dfae0f9468b599ce36c7cdb3053ee31de3cff6a8');
INSERT INTO `track` VALUES ('20958', 'front', '2019-02-03 15:26:03', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/article', 'dfae0f9468b599ce36c7cdb3053ee31de3cff6a8');
INSERT INTO `track` VALUES ('20959', 'front', '2019-02-03 15:32:51', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'a0b4c496c4e9ee22215763a01929374011a5bade');
INSERT INTO `track` VALUES ('20960', 'front', '2019-02-03 15:37:44', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/article', 'a0b4c496c4e9ee22215763a01929374011a5bade');
INSERT INTO `track` VALUES ('20961', 'front', '2019-02-03 16:24:20', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'a81a67c68ff9ccabb5f83e7f7539b28e1ed3a6f7');
INSERT INTO `track` VALUES ('20962', 'front', '2019-02-03 16:31:29', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '36182d2675e3a4307fabce80758c924aaf0f4e14');
INSERT INTO `track` VALUES ('20963', 'front', '2019-02-03 22:23:59', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '8e138be57a4abe19fab77abc17880784ec49fd45');
INSERT INTO `track` VALUES ('20964', 'front', '2019-02-04 13:08:25', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '8d99817e7eea1d591724d53ee04b4f6fd02506a0');
INSERT INTO `track` VALUES ('20965', 'front', '2019-02-05 10:25:26', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '269e0796d8523867b391137f5bc3b070b9e48dbd');
INSERT INTO `track` VALUES ('20966', 'front', '2019-02-05 22:50:18', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'df57d996d00f570fb5d2691370e9bd5abde86c00');
INSERT INTO `track` VALUES ('20967', 'front', '2019-02-06 10:03:52', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '0aabafbc297bb7af9fa9d8e38f568991b760f9d6');
INSERT INTO `track` VALUES ('20968', 'front', '2019-02-06 13:46:59', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '74be469b427036f7e022541a03dc26ba15ca3d2d');
INSERT INTO `track` VALUES ('20969', 'front', '2019-02-06 16:20:20', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '524ac6f365a4381ad9eeca84ab017b3954ae0c00');
INSERT INTO `track` VALUES ('20970', 'front', '2019-02-06 16:39:13', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'ff5e79ce331b0bc75bc04fc2d85c7358bf5bc355');
INSERT INTO `track` VALUES ('20971', 'front', '2019-02-07 09:16:43', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '6d11ebd5f841c02554843dfc8745b805ed55804a');
INSERT INTO `track` VALUES ('20972', 'front', '2019-02-08 09:32:10', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '3cc318a34ccd15fdef4079266e7ba4e6e78c1f2d');
INSERT INTO `track` VALUES ('20973', 'front', '2019-02-08 22:32:59', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'a1eb0253d8eb73145bb5bd92629c9c3c9e278d28');
INSERT INTO `track` VALUES ('20974', 'front', '2019-02-08 22:33:07', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/article', 'a1eb0253d8eb73145bb5bd92629c9c3c9e278d28');
INSERT INTO `track` VALUES ('20975', 'front', '2019-02-08 22:34:15', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/', 'a1eb0253d8eb73145bb5bd92629c9c3c9e278d28');
INSERT INTO `track` VALUES ('20976', 'front', '2019-02-08 22:35:05', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/article', 'a1eb0253d8eb73145bb5bd92629c9c3c9e278d28');
INSERT INTO `track` VALUES ('20977', 'front', '2019-02-08 22:58:39', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/course_content/index/4', 'Windows 10', '::1', '/_mebook/', 'eb486fd7f9704d2911ece1ce20ce4d763a369613');
INSERT INTO `track` VALUES ('20978', 'front', '2019-02-08 22:58:42', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/article', 'eb486fd7f9704d2911ece1ce20ce4d763a369613');
INSERT INTO `track` VALUES ('20979', 'front', '2019-02-08 23:01:19', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/article', 'eb486fd7f9704d2911ece1ce20ce4d763a369613');
INSERT INTO `track` VALUES ('20980', 'front', '2019-02-08 23:11:24', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/article', '4ba06cfdf118b8b74fdf7d82f934b3bcb955bd79');
INSERT INTO `track` VALUES ('20981', 'front', '2019-02-08 23:11:41', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', '4ba06cfdf118b8b74fdf7d82f934b3bcb955bd79');
INSERT INTO `track` VALUES ('20982', 'front', '2019-02-08 23:11:49', 'article', 'detail', '2', null, '0', 'http://localhost:81/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'Windows 10', '::1', '/_mebook/article/detail/We-Ensure-better-education--for-a-better-world', '4ba06cfdf118b8b74fdf7d82f934b3bcb955bd79');
INSERT INTO `track` VALUES ('20983', 'front', '2019-02-08 23:11:56', 'article', 'detail', '2', null, '0', 'http://localhost:81/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'Windows 10', '::1', '/_mebook/article/detail/We-Ensure-better-education--for-a-better-world', '4ba06cfdf118b8b74fdf7d82f934b3bcb955bd79');
INSERT INTO `track` VALUES ('20984', 'front', '2019-02-08 23:12:20', 'article', 'detail', '2', null, '0', 'http://localhost:81/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'Windows 10', '::1', '/_mebook/article/detail/We-Ensure-better-education--for-a-better-world', '4ba06cfdf118b8b74fdf7d82f934b3bcb955bd79');
INSERT INTO `track` VALUES ('20985', 'front', '2019-02-08 23:39:47', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '175c2fb0acd5808ea6425534574fc91b666173e5');
INSERT INTO `track` VALUES ('20986', 'front', '2019-02-08 23:50:40', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '40e7dfe6428834a330976bbc6ec2e04e61abe9d9');
INSERT INTO `track` VALUES ('20987', 'front', '2019-02-08 23:55:02', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/article/detail/We-Ensure-better-education--for-a-better-world', 'Windows 10', '::1', '/_mebook/', '40e7dfe6428834a330976bbc6ec2e04e61abe9d9');
INSERT INTO `track` VALUES ('20988', 'front', '2019-02-08 23:55:34', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '40e7dfe6428834a330976bbc6ec2e04e61abe9d9');
INSERT INTO `track` VALUES ('20989', 'front', '2019-02-08 23:55:41', 'course', 'detail', '9', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'f3d78c9782f52155c53cf34cb1f6212f7589dbb1');
INSERT INTO `track` VALUES ('20990', 'front', '2019-02-10 14:33:54', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '02d174878bcb5cee420669949b5411fb4be5d74a');
INSERT INTO `track` VALUES ('20991', 'front', '2019-02-11 21:34:51', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '833af73334126401d7ea7c59685f0f5cdca0a0b3');
INSERT INTO `track` VALUES ('20992', 'front', '2019-02-12 23:18:37', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '95f678fdd4c229547eaaee0a9547274cb641c157');
INSERT INTO `track` VALUES ('20993', 'front', '2019-02-13 00:17:53', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'b9b74e1b7d0313359db1f16266a406cc63ba338c');
INSERT INTO `track` VALUES ('20994', 'front', '2019-02-13 01:23:33', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '9ded8c5b7e5e9f1595cd70ebfc39ea8474e91280');
INSERT INTO `track` VALUES ('20995', 'front', '2019-02-13 09:29:03', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'bce71c1bbede003fad12c173db4629c9c9ae0310');
INSERT INTO `track` VALUES ('20996', 'front', '2019-02-13 21:32:15', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'e7d43ba7cd9db99b6ea3d1324f16e9ece93b8369');
INSERT INTO `track` VALUES ('20997', 'front', '2019-02-14 08:48:07', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '871f0524209c51894fc53d2339c42c85bbd00b8e');
INSERT INTO `track` VALUES ('20998', 'front', '2019-02-14 14:28:45', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '307a6a382f2eaabf6d1fde14e074b2faf059763b');
INSERT INTO `track` VALUES ('20999', 'front', '2019-02-15 09:23:25', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '4daa8e8e7a18d361d4b80b2f4c1223f54a021011');
INSERT INTO `track` VALUES ('21000', 'front', '2019-02-16 15:18:39', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '056e1fc353f9ac3939506c36fcabb366d7184c89');
INSERT INTO `track` VALUES ('21001', 'front', '2019-02-17 02:02:44', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '85e7bb950c0bb0258a68fba184f90d373f5bff21');
INSERT INTO `track` VALUES ('21002', 'front', '2019-02-17 02:03:02', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/article', '85e7bb950c0bb0258a68fba184f90d373f5bff21');
INSERT INTO `track` VALUES ('21003', 'front', '2019-02-17 02:03:14', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/article', '85e7bb950c0bb0258a68fba184f90d373f5bff21');
INSERT INTO `track` VALUES ('21004', 'front', '2019-02-17 12:05:51', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '0047658d490c832fcb9cc0a715b9e54e124fcfcc');
INSERT INTO `track` VALUES ('21005', 'front', '2019-02-17 12:05:56', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/article', '0047658d490c832fcb9cc0a715b9e54e124fcfcc');
INSERT INTO `track` VALUES ('21006', 'front', '2019-02-17 12:05:59', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', '0047658d490c832fcb9cc0a715b9e54e124fcfcc');
INSERT INTO `track` VALUES ('21007', 'front', '2019-02-17 12:06:04', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', '0047658d490c832fcb9cc0a715b9e54e124fcfcc');
INSERT INTO `track` VALUES ('21008', 'front', '2019-02-17 12:06:13', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', '0047658d490c832fcb9cc0a715b9e54e124fcfcc');
INSERT INTO `track` VALUES ('21009', 'front', '2019-02-17 12:06:17', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', '0047658d490c832fcb9cc0a715b9e54e124fcfcc');
INSERT INTO `track` VALUES ('21010', 'front', '2019-02-17 12:06:35', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category', 'Windows 10', '::1', '/_mebook/article', '0047658d490c832fcb9cc0a715b9e54e124fcfcc');
INSERT INTO `track` VALUES ('21011', 'front', '2019-02-17 12:07:03', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/', '0047658d490c832fcb9cc0a715b9e54e124fcfcc');
INSERT INTO `track` VALUES ('21012', 'front', '2019-02-17 12:17:37', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/article', '722c60c52bdb35b1e84e879ae34f64ee1c9ff8f8');
INSERT INTO `track` VALUES ('21013', 'front', '2019-02-17 12:17:40', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', '722c60c52bdb35b1e84e879ae34f64ee1c9ff8f8');
INSERT INTO `track` VALUES ('21014', 'front', '2019-02-17 12:26:27', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'dafbd079bd5fdba48db3afae0d3458452e25477c');
INSERT INTO `track` VALUES ('21015', 'front', '2019-02-17 12:26:32', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article/category/%E0%B8%9A%E0%B8%97%E0%B8%84%E0%B8%A7%E0%B8%B2%E0%B8%A1%E0%B8%97%E0%B8%B1%E0%B9%88%E0%B8%A7%E0%B9%84%E0%B8%9B', 'Windows 10', '::1', '/_mebook/article', 'dafbd079bd5fdba48db3afae0d3458452e25477c');
INSERT INTO `track` VALUES ('21016', 'front', '2019-02-17 12:26:35', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article', 'dafbd079bd5fdba48db3afae0d3458452e25477c');
INSERT INTO `track` VALUES ('21017', 'front', '2019-02-17 12:26:36', 'article', 'detail', '2', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/We-Ensure-better-education--for-a-better-world', 'dafbd079bd5fdba48db3afae0d3458452e25477c');
INSERT INTO `track` VALUES ('21018', 'front', '2019-02-17 12:27:54', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/article', 'dafbd079bd5fdba48db3afae0d3458452e25477c');
INSERT INTO `track` VALUES ('21019', 'front', '2019-02-17 12:27:56', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'dafbd079bd5fdba48db3afae0d3458452e25477c');
INSERT INTO `track` VALUES ('21020', 'front', '2019-02-17 12:28:03', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'dafbd079bd5fdba48db3afae0d3458452e25477c');
INSERT INTO `track` VALUES ('21021', 'front', '2019-02-17 12:28:07', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'dafbd079bd5fdba48db3afae0d3458452e25477c');
INSERT INTO `track` VALUES ('21022', 'front', '2019-02-17 12:28:31', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'dafbd079bd5fdba48db3afae0d3458452e25477c');
INSERT INTO `track` VALUES ('21023', 'front', '2019-02-17 12:28:35', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'dafbd079bd5fdba48db3afae0d3458452e25477c');
INSERT INTO `track` VALUES ('21024', 'front', '2019-02-17 12:29:29', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'dafbd079bd5fdba48db3afae0d3458452e25477c');
INSERT INTO `track` VALUES ('21025', 'front', '2019-02-17 12:29:34', 'article', 'detail', '2', null, '0', 'http://localhost:81/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'Windows 10', '::1', '/_mebook/article/detail/We-Ensure-better-education--for-a-better-world', 'dafbd079bd5fdba48db3afae0d3458452e25477c');
INSERT INTO `track` VALUES ('21026', 'front', '2019-02-17 12:29:37', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article/detail/We-Ensure-better-education--for-a-better-world', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'dafbd079bd5fdba48db3afae0d3458452e25477c');
INSERT INTO `track` VALUES ('21027', 'front', '2019-02-17 12:29:48', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/article', 'dafbd079bd5fdba48db3afae0d3458452e25477c');
INSERT INTO `track` VALUES ('21028', 'front', '2019-02-17 12:29:54', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/article', 'dafbd079bd5fdba48db3afae0d3458452e25477c');
INSERT INTO `track` VALUES ('21029', 'front', '2019-02-17 12:29:55', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'dafbd079bd5fdba48db3afae0d3458452e25477c');
INSERT INTO `track` VALUES ('21030', 'front', '2019-02-17 12:29:59', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'dafbd079bd5fdba48db3afae0d3458452e25477c');
INSERT INTO `track` VALUES ('21031', 'front', '2019-02-17 12:30:03', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'dafbd079bd5fdba48db3afae0d3458452e25477c');
INSERT INTO `track` VALUES ('21032', 'front', '2019-02-17 12:31:37', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', '78ce897e5fcd8265262f59b349185b062fcdad26');
INSERT INTO `track` VALUES ('21033', 'front', '2019-02-17 12:31:39', 'activity', 'index', '0', null, '0', 'http://localhost:81/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'Windows 10', '::1', '/_mebook/activity', '78ce897e5fcd8265262f59b349185b062fcdad26');
INSERT INTO `track` VALUES ('21034', 'front', '2019-02-17 12:33:10', 'activity', 'index', '0', null, '0', 'http://localhost:81/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'Windows 10', '::1', '/_mebook/activity', '78ce897e5fcd8265262f59b349185b062fcdad26');
INSERT INTO `track` VALUES ('21035', 'front', '2019-02-17 12:33:30', 'activity', 'index', '0', null, '0', 'http://localhost:81/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'Windows 10', '::1', '/_mebook/activity', '78ce897e5fcd8265262f59b349185b062fcdad26');
INSERT INTO `track` VALUES ('21036', 'front', '2019-02-17 12:33:42', 'activity', 'index', '0', null, '0', 'http://localhost:81/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'Windows 10', '::1', '/_mebook/activity', '78ce897e5fcd8265262f59b349185b062fcdad26');
INSERT INTO `track` VALUES ('21037', 'front', '2019-02-17 12:34:42', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/activity', 'Windows 10', '::1', '/_mebook/', '78ce897e5fcd8265262f59b349185b062fcdad26');
INSERT INTO `track` VALUES ('21038', 'front', '2019-02-17 12:34:49', 'activity', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/activity', '78ce897e5fcd8265262f59b349185b062fcdad26');
INSERT INTO `track` VALUES ('21039', 'front', '2019-02-17 12:34:51', 'activity', 'detail', null, null, '0', 'http://localhost:81/_mebook/activity', 'Windows 10', '::1', '/_mebook/activity/detail/Sales-Training:-Practical-Sales-Techniques', '78ce897e5fcd8265262f59b349185b062fcdad26');
INSERT INTO `track` VALUES ('21040', 'front', '2019-02-17 12:35:12', 'activity', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/activity', '78ce897e5fcd8265262f59b349185b062fcdad26');
INSERT INTO `track` VALUES ('21041', 'front', '2019-02-17 12:35:26', 'activity', 'detail', null, null, '0', 'http://localhost:81/_mebook/activity', 'Windows 10', '::1', '/_mebook/activity/detail/Sales-Training:-Practical-Sales-Techniques', '78ce897e5fcd8265262f59b349185b062fcdad26');
INSERT INTO `track` VALUES ('21042', 'front', '2019-02-17 12:38:49', 'activity', 'detail', null, null, '0', 'http://localhost:81/_mebook/activity', 'Windows 10', '::1', '/_mebook/activity/detail/Sales-Training:-Practical-Sales-Techniques', 'db527e64aab9b5720f402c05b0a201728d234dda');
INSERT INTO `track` VALUES ('21043', 'front', '2019-02-17 12:39:29', 'activity', 'detail', null, null, '0', 'http://localhost:81/_mebook/activity', 'Windows 10', '::1', '/_mebook/activity/detail/Sales-Training:-Practical-Sales-Techniques', 'db527e64aab9b5720f402c05b0a201728d234dda');
INSERT INTO `track` VALUES ('21044', 'front', '2019-02-17 13:08:48', 'activity', 'detail', null, null, '0', 'http://localhost:81/_mebook/activity', 'Windows 10', '::1', '/_mebook/activity/detail/Sales-Training:-Practical-Sales-Techniques', '302acff9707326bdc037a96aad4a6b3ba78690b7');
INSERT INTO `track` VALUES ('21045', 'front', '2019-02-17 13:10:10', 'activity', 'detail', null, null, '0', 'http://localhost:81/_mebook/activity', 'Windows 10', '::1', '/_mebook/activity/detail/Sales-Training:-Practical-Sales-Techniques', '302acff9707326bdc037a96aad4a6b3ba78690b7');
INSERT INTO `track` VALUES ('21046', 'front', '2019-02-17 13:10:14', 'activity', 'detail', null, null, '0', 'http://localhost:81/_mebook/activity', 'Windows 10', '::1', '/_mebook/activity/detail/Sales-Training:-Practical-Sales-Techniques', '302acff9707326bdc037a96aad4a6b3ba78690b7');
INSERT INTO `track` VALUES ('21047', 'front', '2019-02-17 13:14:07', 'activity', 'detail', null, null, '0', 'http://localhost:81/_mebook/activity', 'Windows 10', '::1', '/_mebook/activity/detail/Sales-Training:-Practical-Sales-Techniques', 'f40ce6e5ffde37351823a0f565138f13f1ba1a25');
INSERT INTO `track` VALUES ('21048', 'front', '2019-02-17 13:17:12', 'activity', 'detail', null, null, '0', 'http://localhost:81/_mebook/activity', 'Windows 10', '::1', '/_mebook/activity/detail/Sales-Training:-Practical-Sales-Techniques', 'f40ce6e5ffde37351823a0f565138f13f1ba1a25');
INSERT INTO `track` VALUES ('21049', 'front', '2019-02-17 13:18:33', 'activity', 'detail', null, null, '0', 'http://localhost:81/_mebook/activity', 'Windows 10', '::1', '/_mebook/activity/detail/Sales-Training:-Practical-Sales-Techniques', 'f40ce6e5ffde37351823a0f565138f13f1ba1a25');
INSERT INTO `track` VALUES ('21050', 'front', '2019-02-17 13:20:19', 'activity', 'detail', null, null, '0', 'http://localhost:81/_mebook/activity', 'Windows 10', '::1', '/_mebook/activity/detail/Sales-Training:-Practical-Sales-Techniques', 'e8f86e614fd10fd38f4237396afb1dba4c03a26f');
INSERT INTO `track` VALUES ('21051', 'front', '2019-02-17 13:21:08', 'activity', 'detail', null, null, '0', 'http://localhost:81/_mebook/activity', 'Windows 10', '::1', '/_mebook/activity/detail/Sales-Training:-Practical-Sales-Techniques', 'e8f86e614fd10fd38f4237396afb1dba4c03a26f');
INSERT INTO `track` VALUES ('21052', 'front', '2019-02-17 13:21:13', 'activity', 'index', '0', null, '0', 'http://localhost:81/_mebook/activity/detail/Sales-Training:-Practical-Sales-Techniques', 'Windows 10', '::1', '/_mebook/activity', 'e8f86e614fd10fd38f4237396afb1dba4c03a26f');
INSERT INTO `track` VALUES ('21053', 'front', '2019-02-17 13:21:17', 'activity', 'detail', null, null, '0', 'http://localhost:81/_mebook/activity', 'Windows 10', '::1', '/_mebook/activity/detail/Sales-Training:-Practical-Sales-Techniques', 'e8f86e614fd10fd38f4237396afb1dba4c03a26f');
INSERT INTO `track` VALUES ('21054', 'front', '2019-02-17 13:21:23', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/activity/detail/Sales-Training:-Practical-Sales-Techniques', 'Windows 10', '::1', '/_mebook/', 'e8f86e614fd10fd38f4237396afb1dba4c03a26f');
INSERT INTO `track` VALUES ('21055', 'front', '2019-02-17 13:30:23', 'activity', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/activity', 'b87250fac814ba05d0489ff162bac4e0c004049f');
INSERT INTO `track` VALUES ('21056', 'front', '2019-02-17 13:30:53', 'activity', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/activity', 'b87250fac814ba05d0489ff162bac4e0c004049f');
INSERT INTO `track` VALUES ('21057', 'front', '2019-02-17 13:30:57', 'activity', 'detail', null, null, '0', 'http://localhost:81/_mebook/activity', 'Windows 10', '::1', '/_mebook/activity/detail/Sales-Training:-Practical-Sales-Techniques', 'b87250fac814ba05d0489ff162bac4e0c004049f');
INSERT INTO `track` VALUES ('21058', 'front', '2019-02-17 13:31:01', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/activity/detail/Sales-Training:-Practical-Sales-Techniques', 'Windows 10', '::1', '/_mebook/', 'b87250fac814ba05d0489ff162bac4e0c004049f');
INSERT INTO `track` VALUES ('21059', 'front', '2019-02-17 13:31:07', 'course', 'detail', '9', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'b87250fac814ba05d0489ff162bac4e0c004049f');
INSERT INTO `track` VALUES ('21060', 'front', '2019-02-17 13:31:12', 'course', 'detail', '8', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-3', 'b87250fac814ba05d0489ff162bac4e0c004049f');
INSERT INTO `track` VALUES ('21061', 'front', '2019-02-17 13:31:18', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-3', 'Windows 10', '::1', '/_mebook/', 'b87250fac814ba05d0489ff162bac4e0c004049f');
INSERT INTO `track` VALUES ('21062', 'front', '2019-02-17 13:33:04', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/package/edit/YjY0NUA5OA==', 'Windows 10', '::1', '/_mebook/', 'b87250fac814ba05d0489ff162bac4e0c004049f');
INSERT INTO `track` VALUES ('21063', 'front', '2019-02-17 13:33:40', 'activity', 'index', '0', null, '1', 'http://localhost:81/_mebook/', 'iOS', '::1', '/_mebook/activity', 'b87250fac814ba05d0489ff162bac4e0c004049f');
INSERT INTO `track` VALUES ('21064', 'front', '2019-02-17 13:33:49', 'activity', 'detail', null, null, '1', 'http://localhost:81/_mebook/activity', 'iOS', '::1', '/_mebook/activity/detail/Sales-Training:-Practical-Sales-Techniques', 'b87250fac814ba05d0489ff162bac4e0c004049f');
INSERT INTO `track` VALUES ('21065', 'front', '2019-02-17 13:33:59', 'home', 'index', '0', null, '1', 'http://localhost:81/_mebook/activity/detail/Sales-Training:-Practical-Sales-Techniques', 'iOS', '::1', '/_mebook/', 'b87250fac814ba05d0489ff162bac4e0c004049f');
INSERT INTO `track` VALUES ('21066', 'front', '2019-02-17 13:34:28', 'course', 'detail', '9', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'b87250fac814ba05d0489ff162bac4e0c004049f');
INSERT INTO `track` VALUES ('21067', 'front', '2019-02-17 13:34:42', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', 'b87250fac814ba05d0489ff162bac4e0c004049f');
INSERT INTO `track` VALUES ('21068', 'front', '2019-02-17 13:34:56', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/', 'b87250fac814ba05d0489ff162bac4e0c004049f');
INSERT INTO `track` VALUES ('21069', 'front', '2019-02-17 13:35:14', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/article', 'b87250fac814ba05d0489ff162bac4e0c004049f');
INSERT INTO `track` VALUES ('21070', 'front', '2019-02-17 13:36:51', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/article', '342bfb96f57d129696edfbfe2038e1c60176e555');
INSERT INTO `track` VALUES ('21071', 'front', '2019-02-17 13:37:14', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/article', '342bfb96f57d129696edfbfe2038e1c60176e555');
INSERT INTO `track` VALUES ('21072', 'front', '2019-02-17 13:37:19', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/article', '342bfb96f57d129696edfbfe2038e1c60176e555');
INSERT INTO `track` VALUES ('21073', 'front', '2019-02-17 13:38:06', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/article', '342bfb96f57d129696edfbfe2038e1c60176e555');
INSERT INTO `track` VALUES ('21074', 'front', '2019-02-17 13:38:11', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/article', '342bfb96f57d129696edfbfe2038e1c60176e555');
INSERT INTO `track` VALUES ('21075', 'front', '2019-02-17 13:38:13', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', '342bfb96f57d129696edfbfe2038e1c60176e555');
INSERT INTO `track` VALUES ('21076', 'front', '2019-02-17 13:38:29', 'activity', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/activity', '342bfb96f57d129696edfbfe2038e1c60176e555');
INSERT INTO `track` VALUES ('21077', 'front', '2019-02-17 13:38:31', 'home', 'payment_method', '0', null, '0', 'http://localhost:81/_mebook/activity', 'Windows 10', '::1', '/_mebook/home/payment_method', '342bfb96f57d129696edfbfe2038e1c60176e555');
INSERT INTO `track` VALUES ('21078', 'front', '2019-02-17 13:38:33', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/home/payment_method', 'Windows 10', '::1', '/_mebook/', '342bfb96f57d129696edfbfe2038e1c60176e555');
INSERT INTO `track` VALUES ('21079', 'front', '2019-02-17 13:38:35', 'course', 'detail', '9', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '342bfb96f57d129696edfbfe2038e1c60176e555');
INSERT INTO `track` VALUES ('21080', 'front', '2019-02-17 13:44:04', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '63c913dcfa1453b15f25743ee2b2ba95458dbcd2');
INSERT INTO `track` VALUES ('21081', 'front', '2019-02-17 14:45:51', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '2cd52e0fcaeb0ef3b06cab320cc934f65f7eaf69');
INSERT INTO `track` VALUES ('21082', 'front', '2019-02-17 14:45:52', 'activity', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/activity', '2cd52e0fcaeb0ef3b06cab320cc934f65f7eaf69');
INSERT INTO `track` VALUES ('21083', 'front', '2019-02-17 14:46:03', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/activity', 'Windows 10', '::1', '/_mebook/', '2cd52e0fcaeb0ef3b06cab320cc934f65f7eaf69');
INSERT INTO `track` VALUES ('21084', 'front', '2019-02-17 15:12:36', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%95%E0%B8%A5%E0%B8%B2%E0%B8%94', 'Windows 10', '::1', '/_mebook/article', '51936e87fd00d76bc13911df3039670d5bfaccc1');
INSERT INTO `track` VALUES ('21085', 'front', '2019-02-17 15:12:39', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', '51936e87fd00d76bc13911df3039670d5bfaccc1');
INSERT INTO `track` VALUES ('21086', 'front', '2019-02-17 15:12:46', 'article', 'detail', '2', null, '0', 'http://localhost:81/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', 'Windows 10', '::1', '/_mebook/article/detail/We-Ensure-better-education--for-a-better-world', '51936e87fd00d76bc13911df3039670d5bfaccc1');
INSERT INTO `track` VALUES ('21087', 'front', '2019-02-17 15:12:56', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '51936e87fd00d76bc13911df3039670d5bfaccc1');
INSERT INTO `track` VALUES ('21088', 'front', '2019-02-17 15:12:58', 'activity', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/activity', '51936e87fd00d76bc13911df3039670d5bfaccc1');
INSERT INTO `track` VALUES ('21089', 'front', '2019-02-17 15:13:03', 'activity', 'detail', null, null, '0', 'http://localhost:81/_mebook/activity', 'Windows 10', '::1', '/_mebook/activity/detail/Sales-Training:-Practical-Sales-Techniques', '51936e87fd00d76bc13911df3039670d5bfaccc1');
INSERT INTO `track` VALUES ('21090', 'front', '2019-02-17 15:13:14', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/activity/detail/Sales-Training:-Practical-Sales-Techniques', 'Windows 10', '::1', '/_mebook/', '51936e87fd00d76bc13911df3039670d5bfaccc1');
INSERT INTO `track` VALUES ('21091', 'front', '2019-02-17 15:33:03', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '5c4b9e7731089ed5509b9972c711fc1110a94678');
INSERT INTO `track` VALUES ('21092', 'front', '2019-02-17 15:39:03', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/activity', 'Windows 10', '::1', '/_mebook/', 'ac54fb3d7dce93a38b73c323b4e421ecdb2ddbca');
INSERT INTO `track` VALUES ('21093', 'front', '2019-02-17 15:56:31', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '263453d248edb7ff66d1d01f0a30a4f03a381ff5');
INSERT INTO `track` VALUES ('21094', 'front', '2019-02-17 15:56:38', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '263453d248edb7ff66d1d01f0a30a4f03a381ff5');
INSERT INTO `track` VALUES ('21095', 'front', '2019-02-17 15:56:41', 'course', 'detail', '9', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '263453d248edb7ff66d1d01f0a30a4f03a381ff5');
INSERT INTO `track` VALUES ('21096', 'front', '2019-02-17 15:59:57', 'course', 'detail', '9', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'c50a0b909ee75075d678b51c157e5036ee58e9ca');
INSERT INTO `track` VALUES ('21097', 'front', '2019-02-17 16:03:30', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/home', 'c50a0b909ee75075d678b51c157e5036ee58e9ca');
INSERT INTO `track` VALUES ('21098', 'front', '2019-02-17 16:03:40', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/user/register/userConferm/YjY0NzFANg==', 'Windows 10', '::1', '/_mebook/home', 'c50a0b909ee75075d678b51c157e5036ee58e9ca');
INSERT INTO `track` VALUES ('21099', 'front', '2019-02-17 16:04:44', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/user/profile/', 'Windows 10', '::1', '/_mebook/', 'c50a0b909ee75075d678b51c157e5036ee58e9ca');
INSERT INTO `track` VALUES ('21100', 'front', '2019-02-17 16:09:24', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/profile', 'Windows 10', '::1', '/_mebook/home', '1dfd026e95e0dc7f07160828b4e4fcb5c7e777fe');
INSERT INTO `track` VALUES ('21101', 'front', '2019-02-17 20:08:23', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/article', '9d264dd15fade2779699da62d059ff47a75af894');
INSERT INTO `track` VALUES ('21102', 'front', '2019-02-17 20:08:31', 'article', 'detail', '1', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/Astronomy-Binoculars-A-Great-Alternative', '9d264dd15fade2779699da62d059ff47a75af894');
INSERT INTO `track` VALUES ('21103', 'front', '2019-02-17 20:08:38', 'article', 'index', '0', null, '0', 'http://localhost:81/_mebook/article/category/%E0%B8%9A%E0%B8%97%E0%B8%84%E0%B8%A7%E0%B8%B2%E0%B8%A1%E0%B8%AA%E0%B8%AD%E0%B8%99%E0%B8%AA%E0%B8%94', 'Windows 10', '::1', '/_mebook/article', '9d264dd15fade2779699da62d059ff47a75af894');
INSERT INTO `track` VALUES ('21104', 'front', '2019-02-17 20:08:40', 'article', 'detail', '2', null, '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/article/detail/We-Ensure-better-education--for-a-better-world', '9d264dd15fade2779699da62d059ff47a75af894');
INSERT INTO `track` VALUES ('21105', 'front', '2019-02-17 20:08:56', 'course', 'detail', '8', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-3', '9d264dd15fade2779699da62d059ff47a75af894');
INSERT INTO `track` VALUES ('21106', 'front', '2019-02-17 20:12:46', 'course', 'detail', '8', '71', '0', 'http://localhost:81/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-3', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-3', '9d264dd15fade2779699da62d059ff47a75af894');
INSERT INTO `track` VALUES ('21107', 'front', '2019-02-17 20:22:23', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/user/profile', 'Windows 10', '::1', '/_mebook/', 'd0a050fb7e95af6735070de49523eac4ee278dc5');
INSERT INTO `track` VALUES ('21108', 'front', '2019-02-17 20:22:30', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/user/profile/', 'Windows 10', '::1', '/_mebook/', 'd0a050fb7e95af6735070de49523eac4ee278dc5');
INSERT INTO `track` VALUES ('21109', 'front', '2019-02-17 20:22:33', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'd0a050fb7e95af6735070de49523eac4ee278dc5');
INSERT INTO `track` VALUES ('21110', 'front', '2019-02-17 20:36:42', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '81685d1f6682267b37ebdc80c6e443c57156b0c8');
INSERT INTO `track` VALUES ('21111', 'front', '2019-02-17 20:36:57', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '81685d1f6682267b37ebdc80c6e443c57156b0c8');
INSERT INTO `track` VALUES ('21112', 'front', '2019-02-17 20:37:52', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '81685d1f6682267b37ebdc80c6e443c57156b0c8');
INSERT INTO `track` VALUES ('21113', 'front', '2019-02-17 20:38:40', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '81685d1f6682267b37ebdc80c6e443c57156b0c8');
INSERT INTO `track` VALUES ('21114', 'front', '2019-02-17 20:38:58', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '81685d1f6682267b37ebdc80c6e443c57156b0c8');
INSERT INTO `track` VALUES ('21115', 'front', '2019-02-17 20:39:20', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '81685d1f6682267b37ebdc80c6e443c57156b0c8');
INSERT INTO `track` VALUES ('21116', 'front', '2019-02-17 20:39:50', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '81685d1f6682267b37ebdc80c6e443c57156b0c8');
INSERT INTO `track` VALUES ('21117', 'front', '2019-02-17 20:39:58', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '81685d1f6682267b37ebdc80c6e443c57156b0c8');
INSERT INTO `track` VALUES ('21118', 'front', '2019-02-17 20:40:33', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '81685d1f6682267b37ebdc80c6e443c57156b0c8');
INSERT INTO `track` VALUES ('21119', 'front', '2019-02-17 20:40:49', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '81685d1f6682267b37ebdc80c6e443c57156b0c8');
INSERT INTO `track` VALUES ('21120', 'front', '2019-02-17 20:41:03', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '81685d1f6682267b37ebdc80c6e443c57156b0c8');
INSERT INTO `track` VALUES ('21121', 'front', '2019-02-17 20:41:41', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '81685d1f6682267b37ebdc80c6e443c57156b0c8');
INSERT INTO `track` VALUES ('21122', 'front', '2019-02-17 20:42:37', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'f83cc02bbc0012668ccf22ac048a6cbc9a691af4');
INSERT INTO `track` VALUES ('21123', 'front', '2019-02-17 20:42:45', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'f83cc02bbc0012668ccf22ac048a6cbc9a691af4');
INSERT INTO `track` VALUES ('21124', 'front', '2019-02-17 20:43:03', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'f83cc02bbc0012668ccf22ac048a6cbc9a691af4');
INSERT INTO `track` VALUES ('21125', 'front', '2019-02-17 20:43:29', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'f83cc02bbc0012668ccf22ac048a6cbc9a691af4');
INSERT INTO `track` VALUES ('21126', 'front', '2019-02-17 20:43:51', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'f83cc02bbc0012668ccf22ac048a6cbc9a691af4');
INSERT INTO `track` VALUES ('21127', 'front', '2019-02-17 20:44:24', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'f83cc02bbc0012668ccf22ac048a6cbc9a691af4');
INSERT INTO `track` VALUES ('21128', 'front', '2019-02-17 20:44:26', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', 'f83cc02bbc0012668ccf22ac048a6cbc9a691af4');
INSERT INTO `track` VALUES ('21129', 'front', '2019-02-17 20:44:28', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'f83cc02bbc0012668ccf22ac048a6cbc9a691af4');
INSERT INTO `track` VALUES ('21130', 'front', '2019-02-17 20:44:47', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'f83cc02bbc0012668ccf22ac048a6cbc9a691af4');
INSERT INTO `track` VALUES ('21131', 'front', '2019-02-17 20:44:50', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', 'f83cc02bbc0012668ccf22ac048a6cbc9a691af4');
INSERT INTO `track` VALUES ('21132', 'front', '2019-02-17 20:44:52', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'f83cc02bbc0012668ccf22ac048a6cbc9a691af4');
INSERT INTO `track` VALUES ('21133', 'front', '2019-02-17 21:06:17', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', 'bfe2e32f3122d03a8ff4b874467058cdd605aae9');
INSERT INTO `track` VALUES ('21134', 'front', '2019-02-17 21:23:45', 'course', 'detail', '5', '71', '0', 'http://localhost:81/_mebook/course/top_chart', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', '00388c79e6c53d7f937d4da296cf263e9f330e63');
INSERT INTO `track` VALUES ('21135', 'front', '2019-02-17 21:24:05', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', '00388c79e6c53d7f937d4da296cf263e9f330e63');
INSERT INTO `track` VALUES ('21136', 'front', '2019-02-17 22:08:14', 'article', 'index', '0', '71', '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/article', '29cf3591520b72d42a94e20863566943b6fe5da7');
INSERT INTO `track` VALUES ('21137', 'front', '2019-02-18 09:21:33', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/course/category/type/%E0%B8%81%E0%B8%A3%E0%B8%B2%E0%B8%9F%E0%B8%9F%E0%B8%B4%E0%B8%81%E0%B8%94%E0%B8%B5%E0%B9%84%E0%B8%8B%E0%B8%99%E0%B9%8C', 'Windows 10', '::1', '/_mebook/', '08d3e3eb2f1fc2c9dade9113fcde36107e475615');
INSERT INTO `track` VALUES ('21138', 'front', '2019-02-18 22:59:26', 'home', 'index', '0', '71', '0', '', 'Windows 10', '::1', '/_mebook/', '259d5cca2f43b825ca1dc880d04af6f445d82fd8');
INSERT INTO `track` VALUES ('21139', 'front', '2019-02-20 10:37:57', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'f388a22f645c22006ad3c991004c19130278a24c');
INSERT INTO `track` VALUES ('21140', 'front', '2019-02-20 10:38:02', 'course', 'detail', '9', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'f388a22f645c22006ad3c991004c19130278a24c');
INSERT INTO `track` VALUES ('21141', 'front', '2019-02-20 10:38:07', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/', 'f388a22f645c22006ad3c991004c19130278a24c');
INSERT INTO `track` VALUES ('21142', 'front', '2019-02-20 10:38:09', 'course', 'detail', '9', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'f388a22f645c22006ad3c991004c19130278a24c');
INSERT INTO `track` VALUES ('21143', 'front', '2019-02-20 10:44:00', 'course', 'detail', '9', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '6ce37c19177058a9de35fa37d076f0cdccf3aca5');
INSERT INTO `track` VALUES ('21144', 'front', '2019-02-20 10:55:46', 'course', 'detail', '9', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '1ae54f4e424be234dc86b7f5bfd7889b3c78446e');
INSERT INTO `track` VALUES ('21145', 'front', '2019-02-20 11:04:53', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/', '193994739e7a5867532685200598c2f86f335642');
INSERT INTO `track` VALUES ('21146', 'front', '2019-02-20 11:04:59', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '193994739e7a5867532685200598c2f86f335642');
INSERT INTO `track` VALUES ('21147', 'front', '2019-02-20 11:06:13', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '193994739e7a5867532685200598c2f86f335642');
INSERT INTO `track` VALUES ('21148', 'front', '2019-02-20 11:06:26', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '193994739e7a5867532685200598c2f86f335642');
INSERT INTO `track` VALUES ('21149', 'front', '2019-02-20 11:07:28', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '193994739e7a5867532685200598c2f86f335642');
INSERT INTO `track` VALUES ('21150', 'front', '2019-02-20 11:07:54', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '193994739e7a5867532685200598c2f86f335642');
INSERT INTO `track` VALUES ('21151', 'front', '2019-02-20 11:09:22', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '011b1abb107b4d8e5cbd023b12f571257eaa06dd');
INSERT INTO `track` VALUES ('21152', 'front', '2019-02-20 11:27:00', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '1cfb0763ca2d7ed27d32059e6fb3d7b6cc4f7bd7');
INSERT INTO `track` VALUES ('21153', 'front', '2019-02-20 11:28:04', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '1cfb0763ca2d7ed27d32059e6fb3d7b6cc4f7bd7');
INSERT INTO `track` VALUES ('21154', 'front', '2019-02-20 11:28:43', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '1cfb0763ca2d7ed27d32059e6fb3d7b6cc4f7bd7');
INSERT INTO `track` VALUES ('21155', 'front', '2019-02-20 11:33:12', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '90f2853a23ceba57beb2211be3b0cfe62117f0d2');
INSERT INTO `track` VALUES ('21156', 'front', '2019-02-20 11:33:39', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '90f2853a23ceba57beb2211be3b0cfe62117f0d2');
INSERT INTO `track` VALUES ('21157', 'front', '2019-02-20 11:33:58', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '90f2853a23ceba57beb2211be3b0cfe62117f0d2');
INSERT INTO `track` VALUES ('21158', 'front', '2019-02-20 11:35:42', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '90f2853a23ceba57beb2211be3b0cfe62117f0d2');
INSERT INTO `track` VALUES ('21159', 'front', '2019-02-20 11:36:44', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '90f2853a23ceba57beb2211be3b0cfe62117f0d2');
INSERT INTO `track` VALUES ('21160', 'front', '2019-02-20 11:37:05', 'home', 'help', '0', '71', '0', 'http://localhost:81/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/home/help', '90f2853a23ceba57beb2211be3b0cfe62117f0d2');
INSERT INTO `track` VALUES ('21161', 'front', '2019-02-20 11:37:10', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '90f2853a23ceba57beb2211be3b0cfe62117f0d2');
INSERT INTO `track` VALUES ('21162', 'front', '2019-02-20 11:37:30', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '90f2853a23ceba57beb2211be3b0cfe62117f0d2');
INSERT INTO `track` VALUES ('21163', 'front', '2019-02-20 11:40:14', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'f435b6b06da164fe5e8739538e18950b8d996a3b');
INSERT INTO `track` VALUES ('21164', 'front', '2019-02-20 11:40:47', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'f435b6b06da164fe5e8739538e18950b8d996a3b');
INSERT INTO `track` VALUES ('21165', 'front', '2019-02-20 11:42:56', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'f435b6b06da164fe5e8739538e18950b8d996a3b');
INSERT INTO `track` VALUES ('21166', 'front', '2019-02-20 11:43:25', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'f435b6b06da164fe5e8739538e18950b8d996a3b');
INSERT INTO `track` VALUES ('21167', 'front', '2019-02-20 11:44:26', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'f435b6b06da164fe5e8739538e18950b8d996a3b');
INSERT INTO `track` VALUES ('21168', 'front', '2019-02-20 11:44:47', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'f435b6b06da164fe5e8739538e18950b8d996a3b');
INSERT INTO `track` VALUES ('21169', 'front', '2019-02-20 11:45:44', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'e8ff083ececb30d19ce6c4785cbe810612bd2359');
INSERT INTO `track` VALUES ('21170', 'front', '2019-02-20 11:46:12', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'e8ff083ececb30d19ce6c4785cbe810612bd2359');
INSERT INTO `track` VALUES ('21171', 'front', '2019-02-20 11:46:38', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'e8ff083ececb30d19ce6c4785cbe810612bd2359');
INSERT INTO `track` VALUES ('21172', 'front', '2019-02-20 11:49:40', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'e8ff083ececb30d19ce6c4785cbe810612bd2359');
INSERT INTO `track` VALUES ('21173', 'front', '2019-02-20 11:54:27', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '196bd8acc94ad870b6751a1a370fd46a3854e2cb');
INSERT INTO `track` VALUES ('21174', 'front', '2019-02-20 11:54:59', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '196bd8acc94ad870b6751a1a370fd46a3854e2cb');
INSERT INTO `track` VALUES ('21175', 'front', '2019-02-20 11:55:34', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '196bd8acc94ad870b6751a1a370fd46a3854e2cb');
INSERT INTO `track` VALUES ('21176', 'front', '2019-02-20 11:55:55', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '196bd8acc94ad870b6751a1a370fd46a3854e2cb');
INSERT INTO `track` VALUES ('21177', 'front', '2019-02-20 11:56:53', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/home', '196bd8acc94ad870b6751a1a370fd46a3854e2cb');
INSERT INTO `track` VALUES ('21178', 'front', '2019-02-20 11:56:56', 'course', 'detail', '9', null, '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '196bd8acc94ad870b6751a1a370fd46a3854e2cb');
INSERT INTO `track` VALUES ('21179', 'front', '2019-02-20 11:57:00', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', '196bd8acc94ad870b6751a1a370fd46a3854e2cb');
INSERT INTO `track` VALUES ('21180', 'front', '2019-02-20 11:57:36', 'course', 'detail', '8', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-3', '196bd8acc94ad870b6751a1a370fd46a3854e2cb');
INSERT INTO `track` VALUES ('21181', 'front', '2019-02-20 11:57:39', 'course', 'detail', '5', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-3', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', '196bd8acc94ad870b6751a1a370fd46a3854e2cb');
INSERT INTO `track` VALUES ('21182', 'front', '2019-02-20 11:57:48', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', '196bd8acc94ad870b6751a1a370fd46a3854e2cb');
INSERT INTO `track` VALUES ('21183', 'front', '2019-02-20 11:57:52', 'course', 'detail', '9', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '196bd8acc94ad870b6751a1a370fd46a3854e2cb');
INSERT INTO `track` VALUES ('21184', 'front', '2019-02-20 12:00:01', 'course', 'detail', '9', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '13ec580dba2cf5036b937ad2c2efb348cf0bf565');
INSERT INTO `track` VALUES ('21185', 'front', '2019-02-20 12:00:18', 'course', 'detail', '9', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '13ec580dba2cf5036b937ad2c2efb348cf0bf565');
INSERT INTO `track` VALUES ('21186', 'front', '2019-02-20 12:01:34', 'course', 'detail', '9', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '13ec580dba2cf5036b937ad2c2efb348cf0bf565');
INSERT INTO `track` VALUES ('21187', 'front', '2019-02-20 12:02:11', 'course', 'detail', '9', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '13ec580dba2cf5036b937ad2c2efb348cf0bf565');
INSERT INTO `track` VALUES ('21188', 'front', '2019-02-20 12:02:16', 'course', 'detail', '9', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '13ec580dba2cf5036b937ad2c2efb348cf0bf565');
INSERT INTO `track` VALUES ('21189', 'front', '2019-02-20 12:02:25', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '13ec580dba2cf5036b937ad2c2efb348cf0bf565');
INSERT INTO `track` VALUES ('21190', 'front', '2019-02-20 12:02:33', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '13ec580dba2cf5036b937ad2c2efb348cf0bf565');
INSERT INTO `track` VALUES ('21191', 'front', '2019-02-20 12:45:08', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/course/', 'Windows 10', '::1', '/_mebook/', '6b087e512621b306c5df405fbb25a6f0090634cd');
INSERT INTO `track` VALUES ('21192', 'front', '2019-02-20 12:45:14', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '6b087e512621b306c5df405fbb25a6f0090634cd');
INSERT INTO `track` VALUES ('21193', 'front', '2019-02-20 12:46:17', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '6b087e512621b306c5df405fbb25a6f0090634cd');
INSERT INTO `track` VALUES ('21194', 'front', '2019-02-20 12:47:06', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '6b087e512621b306c5df405fbb25a6f0090634cd');
INSERT INTO `track` VALUES ('21195', 'front', '2019-02-20 12:49:38', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '69c272326c0a7da3b73d3658f779732ea00cfa27');
INSERT INTO `track` VALUES ('21196', 'front', '2019-02-20 12:50:21', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '69c272326c0a7da3b73d3658f779732ea00cfa27');
INSERT INTO `track` VALUES ('21197', 'front', '2019-02-20 12:50:33', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '69c272326c0a7da3b73d3658f779732ea00cfa27');
INSERT INTO `track` VALUES ('21198', 'front', '2019-02-20 12:53:18', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '69c272326c0a7da3b73d3658f779732ea00cfa27');
INSERT INTO `track` VALUES ('21199', 'front', '2019-02-20 12:53:26', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '69c272326c0a7da3b73d3658f779732ea00cfa27');
INSERT INTO `track` VALUES ('21200', 'front', '2019-02-20 12:54:43', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '67197bde8af958bec198fdf5ce302e6df9e59b3c');
INSERT INTO `track` VALUES ('21201', 'front', '2019-02-20 12:55:24', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '67197bde8af958bec198fdf5ce302e6df9e59b3c');
INSERT INTO `track` VALUES ('21202', 'front', '2019-02-20 12:56:50', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '67197bde8af958bec198fdf5ce302e6df9e59b3c');
INSERT INTO `track` VALUES ('21203', 'front', '2019-02-20 12:56:57', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '67197bde8af958bec198fdf5ce302e6df9e59b3c');
INSERT INTO `track` VALUES ('21204', 'front', '2019-02-20 12:59:48', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '91093d0d00057191942286e9619a52f9a336dbb5');
INSERT INTO `track` VALUES ('21205', 'front', '2019-02-20 12:59:54', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '91093d0d00057191942286e9619a52f9a336dbb5');
INSERT INTO `track` VALUES ('21206', 'front', '2019-02-20 13:01:08', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '91093d0d00057191942286e9619a52f9a336dbb5');
INSERT INTO `track` VALUES ('21207', 'front', '2019-02-20 13:03:16', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '91093d0d00057191942286e9619a52f9a336dbb5');
INSERT INTO `track` VALUES ('21208', 'front', '2019-02-20 13:03:20', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '91093d0d00057191942286e9619a52f9a336dbb5');
INSERT INTO `track` VALUES ('21209', 'front', '2019-02-20 13:04:11', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '91093d0d00057191942286e9619a52f9a336dbb5');
INSERT INTO `track` VALUES ('21210', 'front', '2019-02-20 13:05:54', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '93eff7d93b25249de0f683ef2e9bcdbed9558252');
INSERT INTO `track` VALUES ('21211', 'front', '2019-02-20 13:06:07', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '93eff7d93b25249de0f683ef2e9bcdbed9558252');
INSERT INTO `track` VALUES ('21212', 'front', '2019-02-20 13:08:43', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '93eff7d93b25249de0f683ef2e9bcdbed9558252');
INSERT INTO `track` VALUES ('21213', 'front', '2019-02-20 13:08:57', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '93eff7d93b25249de0f683ef2e9bcdbed9558252');
INSERT INTO `track` VALUES ('21214', 'front', '2019-02-20 13:09:01', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '93eff7d93b25249de0f683ef2e9bcdbed9558252');
INSERT INTO `track` VALUES ('21215', 'front', '2019-02-20 13:09:14', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '93eff7d93b25249de0f683ef2e9bcdbed9558252');
INSERT INTO `track` VALUES ('21216', 'front', '2019-02-20 13:11:36', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'f3ea6aea081e7e327468016941316b5c73f43ff7');
INSERT INTO `track` VALUES ('21217', 'front', '2019-02-20 13:12:59', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'f3ea6aea081e7e327468016941316b5c73f43ff7');
INSERT INTO `track` VALUES ('21218', 'front', '2019-02-20 13:13:03', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/home', 'f3ea6aea081e7e327468016941316b5c73f43ff7');
INSERT INTO `track` VALUES ('21219', 'front', '2019-02-20 13:13:05', 'course', 'detail', '9', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'f3ea6aea081e7e327468016941316b5c73f43ff7');
INSERT INTO `track` VALUES ('21220', 'front', '2019-02-20 13:13:25', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'f3ea6aea081e7e327468016941316b5c73f43ff7');
INSERT INTO `track` VALUES ('21221', 'front', '2019-02-20 13:22:25', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'Windows 10', '::1', '/_mebook/home', 'f53b7221bc9866d270afdbaf5d0a4d1d7c0ac33e');
INSERT INTO `track` VALUES ('21222', 'front', '2019-02-20 13:28:57', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/admin/package/edit/YjY0NUAzMQ==', 'Windows 10', '::1', '/_mebook/', '371e6fe3500ddf2daf8aab84bd3e625a31edf853');
INSERT INTO `track` VALUES ('21223', 'front', '2019-02-20 13:29:13', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '371e6fe3500ddf2daf8aab84bd3e625a31edf853');
INSERT INTO `track` VALUES ('21224', 'front', '2019-02-20 13:29:16', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '371e6fe3500ddf2daf8aab84bd3e625a31edf853');
INSERT INTO `track` VALUES ('21225', 'front', '2019-02-20 13:30:42', 'home', 'payment_method', '0', '71', '0', 'http://localhost:81/_mebook/package', 'Windows 10', '::1', '/_mebook/home/payment_method', '371e6fe3500ddf2daf8aab84bd3e625a31edf853');
INSERT INTO `track` VALUES ('21226', 'front', '2019-02-20 14:08:40', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', 'd30ea734927d52aae16856b2e486fdbafbdd6648');
INSERT INTO `track` VALUES ('21227', 'front', '2019-02-20 14:08:45', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'd30ea734927d52aae16856b2e486fdbafbdd6648');
INSERT INTO `track` VALUES ('21228', 'front', '2019-02-20 14:09:41', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/package', 'Windows 10', '::1', '/_mebook/home', 'd30ea734927d52aae16856b2e486fdbafbdd6648');
INSERT INTO `track` VALUES ('21229', 'front', '2019-02-20 14:28:08', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/package', 'Windows 10', '::1', '/_mebook/', 'a1deddd55540e4ed45d264481cb5d518ae47a469');
INSERT INTO `track` VALUES ('21230', 'front', '2019-02-20 14:28:10', 'course', 'detail', '9', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'a1deddd55540e4ed45d264481cb5d518ae47a469');
INSERT INTO `track` VALUES ('21231', 'front', '2019-02-20 14:28:23', 'course', 'detail', '9', null, '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'a1deddd55540e4ed45d264481cb5d518ae47a469');
INSERT INTO `track` VALUES ('21232', 'front', '2019-02-20 14:30:27', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/package', 'Windows 10', '::1', '/_mebook/', 'a1deddd55540e4ed45d264481cb5d518ae47a469');
INSERT INTO `track` VALUES ('21233', 'front', '2019-02-20 14:30:34', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'a1deddd55540e4ed45d264481cb5d518ae47a469');
INSERT INTO `track` VALUES ('21234', 'front', '2019-02-20 14:33:09', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/package', 'Windows 10', '::1', '/_mebook/', 'dfc95516c49f4429aadcf038e52687c29dd15792');
INSERT INTO `track` VALUES ('21235', 'front', '2019-02-20 14:49:49', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/package', 'Windows 10', '::1', '/_mebook/home', 'b76b705a5e9a3186f77b31c719d9b8eeb2219ff9');
INSERT INTO `track` VALUES ('21236', 'front', '2019-02-20 14:49:51', 'course', 'detail', '9', null, '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', 'b76b705a5e9a3186f77b31c719d9b8eeb2219ff9');
INSERT INTO `track` VALUES ('21237', 'front', '2019-02-20 15:02:48', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/package', 'Windows 10', '::1', '/_mebook/', '9ae3f5c1dd479589e854c6cd92120ebab5cea46c');
INSERT INTO `track` VALUES ('21238', 'front', '2019-02-20 15:02:51', 'course', 'detail', '8', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-3', '9ae3f5c1dd479589e854c6cd92120ebab5cea46c');
INSERT INTO `track` VALUES ('21239', 'front', '2019-02-20 16:34:25', 'home', 'index', '0', '71', '0', '', 'Windows 10', '::1', '/_mebook/', 'aed9b197defdcb91014c7c26761b786a90fb0310');
INSERT INTO `track` VALUES ('21240', 'front', '2019-02-20 16:42:15', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/contact', 'Windows 10', '::1', '/_mebook/', '7f90c519254f3ac11c5788c77220912f8182ee27');
INSERT INTO `track` VALUES ('21241', 'front', '2019-02-20 16:42:39', 'article', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/article', '7f90c519254f3ac11c5788c77220912f8182ee27');
INSERT INTO `track` VALUES ('21242', 'front', '2019-02-20 16:43:05', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/', '7f90c519254f3ac11c5788c77220912f8182ee27');
INSERT INTO `track` VALUES ('21243', 'front', '2019-02-20 16:47:14', 'course', 'detail', '7', '71', '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-2', 'ae838d7be7a91a9ca1e96111d415a1faabf917dd');
INSERT INTO `track` VALUES ('21244', 'front', '2019-02-20 17:00:18', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/package', 'Windows 10', '::1', '/_mebook/home', 'fcf11596590d882e6d1f1a931c7e321e9e90d4ea');
INSERT INTO `track` VALUES ('21245', 'front', '2019-02-20 17:00:51', 'home', 'payment_method', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/home/payment_method', 'fcf11596590d882e6d1f1a931c7e321e9e90d4ea');
INSERT INTO `track` VALUES ('21246', 'front', '2019-02-20 17:02:46', 'home', 'index', '0', null, '0', 'http://localhost:81/_mebook/user/register', 'Windows 10', '::1', '/_mebook/home', 'fcf11596590d882e6d1f1a931c7e321e9e90d4ea');
INSERT INTO `track` VALUES ('21247', 'front', '2019-02-20 17:04:08', 'home', 'index', '0', '72', '0', 'http://localhost:81/_mebook/user/register/userConferm/YjY0NzJAMTA=', 'Windows 10', '::1', '/_mebook/home', '1c7461fa900837621f63a8176b179cc1182bd7db');
INSERT INTO `track` VALUES ('21248', 'front', '2019-02-20 17:04:11', 'course', 'detail', '7', '72', '0', 'http://localhost:81/_mebook/home', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-2', '1c7461fa900837621f63a8176b179cc1182bd7db');
INSERT INTO `track` VALUES ('21249', 'front', '2019-02-20 17:29:10', 'course', 'detail', '8', null, '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-3', 'ea86b72babe23f7f9b9315ca5cc3d4a5eb6a4e8d');
INSERT INTO `track` VALUES ('21250', 'front', '2019-02-20 17:52:12', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/package', 'Windows 10', '::1', '/_mebook/', 'aa2498f304122b17d96b6f49f8dd4898183a8272');
INSERT INTO `track` VALUES ('21251', 'front', '2019-02-20 17:52:29', 'home', 'index', '0', '72', '0', 'http://localhost:81/_mebook/package', 'Windows 10', '::1', '/_mebook/', '05860a81fdc15574254d8f6bbc680e387ca86918');
INSERT INTO `track` VALUES ('21252', 'front', '2019-02-20 17:52:32', 'home', 'index', '0', '72', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '05860a81fdc15574254d8f6bbc680e387ca86918');
INSERT INTO `track` VALUES ('21253', 'front', '2019-02-20 17:52:34', 'home', 'index', '0', '72', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '05860a81fdc15574254d8f6bbc680e387ca86918');
INSERT INTO `track` VALUES ('21254', 'front', '2019-02-20 17:52:37', 'home', 'index', '0', '72', '0', '', 'Windows 10', '::1', '/_mebook/', '05860a81fdc15574254d8f6bbc680e387ca86918');
INSERT INTO `track` VALUES ('21255', 'front', '2019-02-20 17:55:07', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/package', 'Windows 10', '::1', '/_mebook/', 'aa2498f304122b17d96b6f49f8dd4898183a8272');
INSERT INTO `track` VALUES ('21256', 'front', '2019-02-20 17:55:11', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'aa2498f304122b17d96b6f49f8dd4898183a8272');
INSERT INTO `track` VALUES ('21257', 'front', '2019-02-20 17:55:15', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'aa2498f304122b17d96b6f49f8dd4898183a8272');
INSERT INTO `track` VALUES ('21258', 'front', '2019-02-20 17:55:33', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'aa2498f304122b17d96b6f49f8dd4898183a8272');
INSERT INTO `track` VALUES ('21259', 'front', '2019-02-20 17:55:34', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'aa2498f304122b17d96b6f49f8dd4898183a8272');
INSERT INTO `track` VALUES ('21260', 'front', '2019-02-20 17:56:06', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'aa2498f304122b17d96b6f49f8dd4898183a8272');
INSERT INTO `track` VALUES ('21261', 'front', '2019-02-20 17:56:13', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'aa2498f304122b17d96b6f49f8dd4898183a8272');
INSERT INTO `track` VALUES ('21262', 'front', '2019-02-20 17:56:41', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', 'aa2498f304122b17d96b6f49f8dd4898183a8272');
INSERT INTO `track` VALUES ('21263', 'front', '2019-02-20 17:57:21', 'activity', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/activity', '7f0f5ac0e0c6985c8c4c1e4238022c243900c61a');
INSERT INTO `track` VALUES ('21264', 'front', '2019-02-20 17:57:24', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/activity', 'Windows 10', '::1', '/_mebook/', '7f0f5ac0e0c6985c8c4c1e4238022c243900c61a');
INSERT INTO `track` VALUES ('21265', 'front', '2019-02-20 17:57:30', 'article', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/article', '7f0f5ac0e0c6985c8c4c1e4238022c243900c61a');
INSERT INTO `track` VALUES ('21266', 'front', '2019-02-20 17:57:35', 'article', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/article', '7f0f5ac0e0c6985c8c4c1e4238022c243900c61a');
INSERT INTO `track` VALUES ('21267', 'front', '2019-02-20 18:00:43', 'article', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/article', '7f0f5ac0e0c6985c8c4c1e4238022c243900c61a');
INSERT INTO `track` VALUES ('21268', 'front', '2019-02-20 18:00:44', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/', '7f0f5ac0e0c6985c8c4c1e4238022c243900c61a');
INSERT INTO `track` VALUES ('21269', 'front', '2019-02-20 18:01:03', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/article', 'Windows 10', '::1', '/_mebook/', '7f0f5ac0e0c6985c8c4c1e4238022c243900c61a');
INSERT INTO `track` VALUES ('21270', 'front', '2019-02-20 18:01:04', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '7f0f5ac0e0c6985c8c4c1e4238022c243900c61a');
INSERT INTO `track` VALUES ('21271', 'front', '2019-02-20 18:01:20', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '7f0f5ac0e0c6985c8c4c1e4238022c243900c61a');
INSERT INTO `track` VALUES ('21272', 'front', '2019-02-20 18:01:34', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '7f0f5ac0e0c6985c8c4c1e4238022c243900c61a');
INSERT INTO `track` VALUES ('21273', 'front', '2019-02-20 18:01:35', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '7f0f5ac0e0c6985c8c4c1e4238022c243900c61a');
INSERT INTO `track` VALUES ('21274', 'front', '2019-02-20 18:02:05', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '7f0f5ac0e0c6985c8c4c1e4238022c243900c61a');
INSERT INTO `track` VALUES ('21275', 'front', '2019-02-20 18:02:06', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '7f0f5ac0e0c6985c8c4c1e4238022c243900c61a');
INSERT INTO `track` VALUES ('21276', 'front', '2019-02-20 18:02:22', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '8ca8301d8867473535ab87ec63dc8f136992f7c0');
INSERT INTO `track` VALUES ('21277', 'front', '2019-02-20 18:02:23', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '8ca8301d8867473535ab87ec63dc8f136992f7c0');
INSERT INTO `track` VALUES ('21278', 'front', '2019-02-20 18:02:24', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '8ca8301d8867473535ab87ec63dc8f136992f7c0');
INSERT INTO `track` VALUES ('21279', 'front', '2019-02-20 18:02:24', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '8ca8301d8867473535ab87ec63dc8f136992f7c0');
INSERT INTO `track` VALUES ('21280', 'front', '2019-02-20 18:02:25', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '8ca8301d8867473535ab87ec63dc8f136992f7c0');
INSERT INTO `track` VALUES ('21281', 'front', '2019-02-20 18:02:38', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '8ca8301d8867473535ab87ec63dc8f136992f7c0');
INSERT INTO `track` VALUES ('21282', 'front', '2019-02-20 18:02:39', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/', '8ca8301d8867473535ab87ec63dc8f136992f7c0');
INSERT INTO `track` VALUES ('21283', 'front', '2019-02-20 18:02:48', 'activity', 'index', '0', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/activity', '8ca8301d8867473535ab87ec63dc8f136992f7c0');
INSERT INTO `track` VALUES ('21284', 'front', '2019-02-20 18:02:51', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/activity', 'Windows 10', '::1', '/_mebook/', '8ca8301d8867473535ab87ec63dc8f136992f7c0');
INSERT INTO `track` VALUES ('21285', 'front', '2019-02-20 18:03:52', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/', '8ca8301d8867473535ab87ec63dc8f136992f7c0');
INSERT INTO `track` VALUES ('21286', 'front', '2019-02-20 18:03:56', 'course', 'detail', '9', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-4', '8ca8301d8867473535ab87ec63dc8f136992f7c0');
INSERT INTO `track` VALUES ('21287', 'front', '2019-02-20 18:04:00', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/', '8ca8301d8867473535ab87ec63dc8f136992f7c0');
INSERT INTO `track` VALUES ('21288', 'front', '2019-02-20 18:04:03', 'course', 'detail', '6', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-1', '8ca8301d8867473535ab87ec63dc8f136992f7c0');
INSERT INTO `track` VALUES ('21289', 'front', '2019-02-20 18:04:06', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/', '8ca8301d8867473535ab87ec63dc8f136992f7c0');
INSERT INTO `track` VALUES ('21290', 'front', '2019-02-20 18:04:11', 'course', 'detail', '5', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', '8ca8301d8867473535ab87ec63dc8f136992f7c0');
INSERT INTO `track` VALUES ('21291', 'front', '2019-02-20 18:04:55', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', '8ca8301d8867473535ab87ec63dc8f136992f7c0');
INSERT INTO `track` VALUES ('21292', 'front', '2019-02-20 18:05:03', 'course', 'detail', '5', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', '8ca8301d8867473535ab87ec63dc8f136992f7c0');
INSERT INTO `track` VALUES ('21293', 'front', '2019-02-20 18:05:08', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', '8ca8301d8867473535ab87ec63dc8f136992f7c0');
INSERT INTO `track` VALUES ('21294', 'front', '2019-02-20 18:05:37', 'course', 'detail', '5', '71', '0', 'http://localhost:81/_mebook/', 'Windows 10', '::1', '/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', '8ca8301d8867473535ab87ec63dc8f136992f7c0');
INSERT INTO `track` VALUES ('21295', 'front', '2019-02-20 18:05:41', 'course', 'list_video', '5', '71', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/course/list_video/Photography-Masterclass-A-Complete-Guide-to-Photography/16', '8ca8301d8867473535ab87ec63dc8f136992f7c0');
INSERT INTO `track` VALUES ('21296', 'front', '2019-02-20 18:08:57', 'course', 'list_video', '5', '71', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/course/list_video/Photography-Masterclass-A-Complete-Guide-to-Photography/16', 'f6e0622095e2b4e3bebc0ea6c609945519cda40b');
INSERT INTO `track` VALUES ('21297', 'front', '2019-02-20 18:09:01', 'course', 'list_video', '5', '71', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/course/list_video/Photography-Masterclass-A-Complete-Guide-to-Photography/16', 'f6e0622095e2b4e3bebc0ea6c609945519cda40b');
INSERT INTO `track` VALUES ('21298', 'front', '2019-02-20 18:09:37', 'course', 'list_video', '5', '71', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/course/list_video/Photography-Masterclass-A-Complete-Guide-to-Photography/16', 'f6e0622095e2b4e3bebc0ea6c609945519cda40b');
INSERT INTO `track` VALUES ('21299', 'front', '2019-02-20 18:09:40', 'course', 'list_video', '5', '71', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/course/list_video/Photography-Masterclass-A-Complete-Guide-to-Photography/16', 'f6e0622095e2b4e3bebc0ea6c609945519cda40b');
INSERT INTO `track` VALUES ('21300', 'front', '2019-02-20 18:11:08', 'course', 'list_video', '5', '71', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/course/list_video/Photography-Masterclass-A-Complete-Guide-to-Photography/16', 'f6e0622095e2b4e3bebc0ea6c609945519cda40b');
INSERT INTO `track` VALUES ('21301', 'front', '2019-02-20 18:11:22', 'course', 'list_video', '5', '71', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/course/list_video/Photography-Masterclass-A-Complete-Guide-to-Photography/16', 'f6e0622095e2b4e3bebc0ea6c609945519cda40b');
INSERT INTO `track` VALUES ('21302', 'front', '2019-02-20 18:11:28', 'course', 'list_video', '5', '71', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/course/list_video/Photography-Masterclass-A-Complete-Guide-to-Photography/16', 'f6e0622095e2b4e3bebc0ea6c609945519cda40b');
INSERT INTO `track` VALUES ('21303', 'front', '2019-02-20 18:11:41', 'course', 'list_video', '5', '71', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/course/list_video/Photography-Masterclass-A-Complete-Guide-to-Photography/16', 'f6e0622095e2b4e3bebc0ea6c609945519cda40b');
INSERT INTO `track` VALUES ('21304', 'front', '2019-02-20 18:12:15', 'course', 'list_video', '5', '71', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/course/list_video/Photography-Masterclass-A-Complete-Guide-to-Photography/16', 'f6e0622095e2b4e3bebc0ea6c609945519cda40b');
INSERT INTO `track` VALUES ('21305', 'front', '2019-02-20 18:12:33', 'course', 'list_video', '5', '71', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/course/list_video/Photography-Masterclass-A-Complete-Guide-to-Photography/16', 'f6e0622095e2b4e3bebc0ea6c609945519cda40b');
INSERT INTO `track` VALUES ('21306', 'front', '2019-02-20 18:36:19', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/course/detail/Photography-Masterclass-A-Complete-Guide-to-Photography', 'Windows 10', '::1', '/_mebook/', '5ae8e648cba23dbbebdc21f5d025060ceab546e8');
INSERT INTO `track` VALUES ('21307', 'front', '2019-02-20 18:36:26', 'course', 'detail', '8', '71', '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-3', '5ae8e648cba23dbbebdc21f5d025060ceab546e8');
INSERT INTO `track` VALUES ('21308', 'front', '2019-02-20 18:37:20', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/package', 'Windows 10', '::1', '/_mebook/', '5ae8e648cba23dbbebdc21f5d025060ceab546e8');
INSERT INTO `track` VALUES ('21309', 'front', '2019-02-20 18:38:20', 'home', 'index', '0', '71', '0', 'http://localhost:81/_mebook/package', 'Windows 10', '::1', '/_mebook/', '5ae8e648cba23dbbebdc21f5d025060ceab546e8');
INSERT INTO `track` VALUES ('21310', 'front', '2019-02-20 18:38:24', 'course', 'detail', '8', '71', '0', 'http://localhost:81/_mebook/course/featured', 'Windows 10', '::1', '/_mebook/course/detail/Sales-Training-Practical-Sales-Techniques-3', '5ae8e648cba23dbbebdc21f5d025060ceab546e8');
INSERT INTO `track` VALUES ('21311', 'front', '2019-02-21 10:30:39', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '16a8cb7f66763a00d7e92a585e4b59992debf4e6');
INSERT INTO `track` VALUES ('21312', 'front', '2019-02-22 22:49:26', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', '7fee9e514377d4f63bd31f230c07c7a2f50f6b8f');
INSERT INTO `track` VALUES ('21313', 'front', '2019-03-05 23:03:15', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'd4a86b1f88f85a83d572dd5dfc3ee6dd975d16b4');
INSERT INTO `track` VALUES ('21314', 'front', '2019-04-04 16:21:37', 'home', 'index', '0', null, '0', '', 'Windows 10', '::1', '/_mebook/', 'f10d1a4d74fdd2fa3ee1c94d1aeef1531588139e');

-- ----------------------------
-- Table structure for upload
-- ----------------------------
DROP TABLE IF EXISTS `upload`;
CREATE TABLE `upload` (
  `uploadId` int(11) NOT NULL AUTO_INCREMENT,
  `grpContent` varchar(30) DEFAULT NULL,
  `filename` varchar(200) DEFAULT NULL,
  `path` varchar(100) DEFAULT NULL,
  `size` int(11) DEFAULT NULL COMMENT 'KB',
  `extension` varchar(100) DEFAULT NULL,
  `mime` varchar(100) DEFAULT NULL,
  `private` int(11) DEFAULT '2' COMMENT '1:private, 2:published',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) DEFAULT NULL,
  `modifyDate` datetime DEFAULT NULL,
  `modifyBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`uploadId`)
) ENGINE=InnoDB AUTO_INCREMENT=954 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of upload
-- ----------------------------
INSERT INTO `upload` VALUES ('667', 'banner', 'f0eafabce1b39c656d18cd891e4ed595.jpg', 'uploads/2018/09/banner/', '45660', 'jpg', 'image/jpeg', '2', '2018-09-23 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('671', 'article', '99538951854cd9cd32cefa7a3509f90a.jpg', 'uploads/2018/09/article/', '151859', 'jpg', 'image/jpeg', '2', '2018-09-24 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('672', 'article', 'a8a9e2a2bb74590cf13bf9e53e38b834.jpg', 'uploads/2018/09/article/', '206316', 'jpg', 'image/jpeg', '2', '2018-09-24 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('678', 'course', '637b16f3919ba8ca17ffd394a9c2e493.jpg', 'uploads/2018/09/course/', '3160515', 'jpg', 'image/jpeg', '2', '2018-09-25 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('679', 'course', '233c035b6f0e47e7b49cdfbb22685e53.jpg', 'uploads/2018/09/course/', '580198', 'jpg', 'image/jpeg', '2', '2018-09-25 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('682', 'course_content', 'bf8f8c6ef0f0b1527904808b9af7019a.pdf', 'uploads/2018/10/course_content/', '130652', 'pdf', 'application/pdf', '2', '2018-10-01 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('699', 'article', '1b8065971de8772db9f0a6992564e7ef.jpg', 'uploads/2018/10/article/', '805294', 'jpg', 'image/jpeg', '2', '2018-10-10 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('706', 'course', 'b256906c3087482360dd90d7f51b6eac.svg', 'uploads/2018/10/course/', '502', 'svg', 'image/svg+xml', '2', '2018-10-11 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('715', 'activity', '1ae7bc566923414700c4b8d711cf3fc8.jpg', 'uploads/2018/10/', '77679', 'jpg', 'image/jpeg', '0', '2018-10-11 15:10:26', '3', null, null);
INSERT INTO `upload` VALUES ('716', 'activity', '8ddbdce9862fef96ec76a69872e64358.jpg', 'uploads/2018/10/', '89255', 'jpg', 'image/jpeg', '0', '2018-10-11 15:10:27', '3', null, null);
INSERT INTO `upload` VALUES ('717', 'activity', 'bcde3f1685cbe248f9f4313a387816e3.jpg', 'uploads/2018/10/activity/', '89252', 'jpg', 'image/jpeg', '2', '2018-10-11 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('722', 'activity', '9def3e7370c799b7f7d09f16448e795b.jpg', 'uploads/2018/10/', '117588', 'jpg', 'image/jpeg', '0', '2018-10-11 15:30:50', '3', null, null);
INSERT INTO `upload` VALUES ('723', 'activity', '544d6fb520dc14ec50f67d9318a0cd4a.jpg', 'uploads/2018/10/', '54135', 'jpg', 'image/jpeg', '0', '2018-10-11 15:30:50', '3', null, null);
INSERT INTO `upload` VALUES ('724', 'activity', 'be10878ae294df65ed909110caf1f3b7.jpg', 'uploads/2018/10/', '99685', 'jpg', 'image/jpeg', '0', '2018-10-11 15:30:51', '3', null, null);
INSERT INTO `upload` VALUES ('725', 'activity', '35409a22744f633c6d2d0764a5c1a28a.jpg', 'uploads/2018/10/', '59030', 'jpg', 'image/jpeg', '0', '2018-10-11 15:30:51', '3', null, null);
INSERT INTO `upload` VALUES ('726', 'activity', '8c99f7e9239b81e8adfe85408437092f.jpg', 'uploads/2018/10/', '32281', 'jpg', 'image/jpeg', '0', '2018-10-11 15:30:51', '3', null, null);
INSERT INTO `upload` VALUES ('727', 'activity', '7d4e7b26653e7755718ac986f289737c.jpg', 'uploads/2018/10/', '118276', 'jpg', 'image/jpeg', '0', '2018-10-11 15:30:51', '3', null, null);
INSERT INTO `upload` VALUES ('731', 'seo_article', 'b6beb53dc2018a304d9f43d9c4bb3dbc.jpg', 'uploads/2018/10/seo_article/', '114391', 'jpg', 'image/jpeg', '2', '2018-10-13 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('732', 'seo_home', '219ace5d0f2d552bf0d2f5b1102ea2f3.jpg', 'uploads/2018/10/seo_home/', '114391', 'jpg', 'image/jpeg', '2', '2018-10-13 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('733', 'seo_course', '26d3b642c901f07a9b50d1e6c23ae079.jpg', 'uploads/2018/10/seo_course/', '114391', 'jpg', 'image/jpeg', '2', '2018-10-13 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('734', 'seo_activity', 'b483c55aa1e541ed43f242b326633b2f.jpg', 'uploads/2018/10/seo_activity/', '114391', 'jpg', 'image/jpeg', '2', '2018-10-13 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('735', 'course', '66dff7571e4d2eead14b70c2d226952d.svg', 'uploads/2018/10/course/', '502', 'svg', 'image/svg+xml', '2', '2018-10-15 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('740', 'reviews', '0081c27164da6476879f5912fd56603b.jpg', 'uploads/2018/10/reviews/', '114391', 'jpg', 'image/jpeg', '2', '2018-10-18 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('741', 'reviews', '19ca1518c8bd1d6c274f01a246a0eb05.jpg', 'uploads/2018/10/reviews/', '114391', 'jpg', 'image/jpeg', '2', '2018-10-18 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('742', 'reviews', '75423722746acf3eca57aba80455d4f7.jpg', 'uploads/2018/10/reviews/', '114391', 'jpg', 'image/jpeg', '2', '2018-10-18 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('743', 'reviews', '2bfac67eec06dadf2d6b84b3ccc105ff.jpg', 'uploads/2018/10/reviews/', '67205', 'jpg', 'image/jpeg', '2', '2018-10-18 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('744', 'article', 'fc5601eef06fe861a5a1a8bec09e98d0.jpg', 'uploads/2018/10/article/', '139121', 'jpg', 'image/jpeg', '2', '2018-10-20 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('749', 'course_content', '9938b55032bf4b51932428bf051ab013.psd', 'uploads/2018/10/course_content/', '191908', 'psd', 'application/octet-stream', '2', '2018-10-21 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('757', 'payment', '396c45acce92fb5cd16a926309bc2bab.jpg', 'uploads/2018/10/payment/', '1712', 'jpg', 'image/jpeg', '2', '2018-10-21 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('758', 'payment', '6f8eb588945a0d5785704f3296fb2f0c.jpg', 'uploads/2018/10/payment/', '1712', 'jpg', 'image/jpeg', '2', '2018-10-21 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('759', 'payment', '0f302e0639029a18bad0a2912cfa069f.jpg', 'uploads/2018/10/payment/', '1712', 'jpg', 'image/jpeg', '2', '2018-10-21 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('760', 'payment', 'dff36f3794fbb811416ba2f4ef01bca5.jpg', 'uploads/2018/10/payment/', '1712', 'jpg', 'image/jpeg', '2', '2018-10-21 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('761', 'payment', 'b756d0508b8fa19c0587a19a3ffa7d5d.jpg', 'uploads/2018/10/payment/', '1712', 'jpg', 'image/jpeg', '2', '2018-10-21 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('762', 'payment', '424957caf5c5e7164f98f08ee1ac1564.jpg', 'uploads/2018/10/payment/', '1712', 'jpg', 'image/jpeg', '2', '2018-10-21 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('763', 'payment', '9cbfb91cb9b1bcd4d0ae5c8be83857f8.jpg', 'uploads/2018/10/payment/', '1712', 'jpg', 'image/jpeg', '2', '2018-10-21 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('764', 'payment', '23e174f1884c80904354c63040d9e369.jpg', 'uploads/2018/10/payment/', '1712', 'jpg', 'image/jpeg', '2', '2018-10-21 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('765', 'payment', '1477a11be898bbcbc2bd1cdfd7108d23.jpg', 'uploads/2018/10/payment/', '1712', 'jpg', 'image/jpeg', '2', '2018-10-21 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('766', 'payment', 'b51e5f55a923ff4047bf48bacd1e2f89.jpg', 'uploads/2018/10/payment/', '1712', 'jpg', 'image/jpeg', '2', '2018-10-21 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('767', 'payment', 'a299280f44d57d9eaa3b7fded8bd8a0c.jpg', 'uploads/2018/10/payment/', '1712', 'jpg', 'image/jpeg', '2', '2018-10-21 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('768', 'payment', '722ec2bf102e5a9ed29e5510c0221645.jpg', 'uploads/2018/10/payment/', '1712', 'jpg', 'image/jpeg', '2', '2018-10-21 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('769', 'payment', '86039b0a7edd3b8ad6c2a86c8d3f176c.jpg', 'uploads/2018/10/payment/', '1712', 'jpg', 'image/jpeg', '2', '2018-10-21 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('833', 'article', 'dee98bbd58a19976f9d2d338365efd90.jpg', 'uploads/2018/10/article/', '114391', 'jpg', 'image/jpeg', '2', '2018-10-24 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('834', 'article', '91ee44522de4affc1cb8e87f9d5176c6.jpg', 'uploads/2018/10/article/', '67205', 'jpg', 'image/jpeg', '2', '2018-10-24 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('835', 'article', '3c1c0fe0d6df7559adc2fb4cfd28ad70.jpg', 'uploads/2018/10/article/', '114391', 'jpg', 'image/jpeg', '2', '2018-10-24 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('836', 'course', '0aa91ec3f53a4e7a0b0d0ea0c545082b.jpg', 'uploads/2018/11/course/', '114391', 'jpg', 'image/jpeg', '2', '2018-11-10 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('837', 'course', 'b3af9cfd1542cd2e4fa812f605136636.jpg', 'uploads/2018/11/course/', '2068', 'jpg', 'image/jpeg', '2', '2018-11-10 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('838', 'course_register', '07c4b02826ca45aedb35197b0e3df9bc.jpg', 'uploads/2018/11/payment/', '65', 'jpg', 'image/jpeg', '2', '2018-11-11 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('839', 'course_register', '3e713b13cd9e68fa8644e956f1a39034.jpg', 'uploads/2018/11/payment/', '65', 'jpg', 'image/jpeg', '2', '2018-11-11 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('840', 'course_register', 'ea193363f11c743e594c3bc634ddf44f.jpg', 'uploads/2018/11/payment/', '60', 'jpg', 'image/jpeg', '2', '2018-11-11 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('841', 'course_register', '9265b996ffff5cf3e126346d0b714cd2.jpg', 'uploads/2018/11/payment/', '486', 'jpg', 'image/jpeg', '2', '2018-11-13 00:00:00', null, null, null);
INSERT INTO `upload` VALUES ('842', 'course_register', '5a913f9ee3eb4aa46ae1eccf772ad4b6.jpg', 'uploads/2018/11/payment/', '543', 'jpg', 'image/jpeg', '2', '2018-11-13 00:00:00', null, null, null);
INSERT INTO `upload` VALUES ('848', 'banner', '07b053eb3f4fdf9d1bae3428e4dccc4f.jpg', 'uploads/2018/12/banner/', '461292', 'jpg', 'image/jpeg', '2', '2018-12-01 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('849', 'banner', '08d9292c1ebd25a045caf012c0ab51cf.jpg', 'uploads/2018/12/banner/', '139121', 'jpg', 'image/jpeg', '2', '2018-12-01 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('852', 'course_register', '0075b452e16b972d7196191d09b6e398.jpg', 'uploads/2018/12/payment/', '136', 'jpg', 'image/jpeg', '2', '2018-12-03 00:00:00', null, null, null);
INSERT INTO `upload` VALUES ('853', 'course_register', '75f44689e6941c122ef892fd6c2f064b.jpg', 'uploads/2018/12/payment/', '136', 'jpg', 'image/jpeg', '2', '2018-12-03 00:00:00', null, null, null);
INSERT INTO `upload` VALUES ('865', 'course_register', '38b09106d23def6f91a85479ff47ff35.jpg', 'uploads/2018/12/payment/', '136', 'jpg', 'image/jpeg', '2', '2018-12-03 00:00:00', null, null, null);
INSERT INTO `upload` VALUES ('866', 'course_register', '22e076d964759c5f1df26425aca1023c.jpg', 'uploads/2018/12/payment/', '136', 'jpg', 'image/jpeg', '2', '2018-12-03 00:00:00', null, null, null);
INSERT INTO `upload` VALUES ('867', 'course_register', 'c6791393d5a5c5b6962a1dd355cad49e.png', 'uploads/2018/12/payment/', '757', 'png', 'image/png', '2', '2018-12-08 00:00:00', null, null, null);
INSERT INTO `upload` VALUES ('868', 'course_register', 'b54bd22d5652f67e1aef3d5d54990bd6.png', 'uploads/2018/12/payment/', '877', 'png', 'image/png', '2', '2018-12-08 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('869', 'course_register', '1150a9490d4a8e8c23e61421b8034a75.png', 'uploads/2018/12/payment/', '558', 'png', 'image/png', '2', '2018-12-09 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('870', 'course_register', 'c50133617d37e2016995555c3cc9e4df.png', 'uploads/2018/12/payment/', '558', 'png', 'image/png', '2', '2018-12-10 00:00:00', null, null, null);
INSERT INTO `upload` VALUES ('871', 'course_register', '073043e8bbe6becc0706d70ad43ec944.png', 'uploads/2018/12/payment/', '558', 'png', 'image/png', '2', '2018-12-10 00:00:00', null, null, null);
INSERT INTO `upload` VALUES ('872', 'course_register', '5016999ceaf013a709341f7c8a8df9c5.png', 'uploads/2018/12/payment/', '558', 'png', 'image/png', '2', '2018-12-10 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('873', 'course_register', '50e139f1381f983fb9805c37a82f214d.png', 'uploads/2018/12/payment/', '558', 'png', 'image/png', '2', '2018-12-10 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('874', 'course_register', '31d2455ae2edda1908ed50f334f37ac8.png', 'uploads/2018/12/payment/', '558', 'png', 'image/png', '2', '2018-12-10 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('875', 'course_register', '0b68befa76ebb4551b486baf26338502.png', 'uploads/2018/12/payment/', '558', 'png', 'image/png', '2', '2018-12-10 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('876', 'course_register', '9c291d1099c6ce9220f63396e56cc414.png', 'uploads/2018/12/payment/', '558', 'png', 'image/png', '2', '2018-12-10 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('877', 'course_register', '1a7b415503a31c4266d6720865057a56.png', 'uploads/2018/12/payment/', '558', 'png', 'image/png', '2', '2018-12-10 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('878', 'course_register', 'e0189e1f06415eadd15991212f48553d.png', 'uploads/2018/12/payment/', '558', 'png', 'image/png', '2', '2018-12-10 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('879', 'course_register', '2b9703ffaf06368e5f85ee90bdfb4dcf.png', 'uploads/2018/12/payment/', '558', 'png', 'image/png', '2', '2018-12-10 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('882', 'course', '93714da69171343d484a78bf27c59662.jpg', 'uploads/2018/12/course/', '194468', 'jpg', 'image/jpeg', '2', '2018-12-12 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('883', 'course', '2726a1a85019aea58bba9cddb423bcf1.jpg', 'uploads/2018/12/course/', '194468', 'jpg', 'image/jpeg', '2', '2018-12-12 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('884', 'banner', '7cc0e959513ac5ef001e244d202d6ba5.jpg', 'uploads/2018/12/banner/', '793641', 'jpg', 'image/jpeg', '2', '2018-12-30 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('885', 'banner', 'fc43b445c89001ddbe5d7b8dbf1d35cb.jpg', 'uploads/2018/12/banner/', '930693', 'jpg', 'image/jpeg', '2', '2018-12-30 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('886', 'package', '1a4baa947e70a11269aeb13889c82487.png', 'uploads/2019/01/package/', '102984', 'png', 'image/png', '2', '2019-01-06 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('897', 'banner', '0fc5fdbb964d052d1292e4aa2d217525.png', 'uploads/2019/01/banner/', '1137', 'png', 'image/png', '2', '2019-01-09 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('900', 'banner', 'aecf2e9672e5d4a784f1f9975c521a5d.png', 'uploads/2019/01/banner/', '3031', 'png', 'image/png', '2', '2019-01-10 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('901', 'banner', '01055b9096a3091e1a70d39a4ed370ae.png', 'uploads/2019/01/banner/', '3287', 'png', 'image/png', '2', '2019-01-10 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('902', 'banner', 'c59f0f806433c8b03fbb2f2fd327774e.png', 'uploads/2019/01/banner/', '3748', 'png', 'image/png', '2', '2019-01-10 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('904', 'banner', '710a01dd8ab51729528c87b2d308194f.png', 'uploads/2019/01/banner/', '5868', 'png', 'image/png', '2', '2019-01-10 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('905', 'course', '86002470b1ec42369d9f873d5de0cf6b.jpg', 'uploads/2019/01/course/', '8479', 'jpg', 'image/jpeg', '2', '2019-01-11 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('906', 'instructor', '8a123fbc6dd15370396932eea6941ab9.jpg', 'uploads/2019/01/instructor/', '2335', 'jpg', 'image/jpeg', '2', '2019-01-11 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('917', 'user', 'b6b08dcd690cb976c44c85705831fe1d.jpg', 'uploads/2019/01/user/', '2959', 'jpg', 'image/jpeg', '2', '2019-01-12 00:00:00', null, null, null);
INSERT INTO `upload` VALUES ('918', 'user', 'e70114aa9ae1d66ef72a868ba63cc396.jpg', 'uploads/2019/01/user/', '2959', 'jpg', 'image/jpeg', '2', '2019-01-12 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('919', 'user', '9207a5ff962d1df36625f3b55c43dfb7.jpg', 'uploads/2019/01/user/', '2335', 'jpg', 'image/jpeg', '2', '2019-01-12 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('920', 'user', 'd36f5deae05e4cafa78cbfb2b1179bd7.jpg', 'uploads/2019/01/user/', '2335', 'jpg', 'image/jpeg', '2', '2019-01-12 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('921', 'user', '5bdb2ebe962218472a633bdd0085800d.jpg', 'uploads/2019/01/user/', '2335', 'jpg', 'image/jpeg', '2', '2019-01-12 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('922', 'user', '1e60d2b3d526accefab0ad00185fa8bb.jpg', 'uploads/2019/01/user/', '2335', 'jpg', 'image/jpeg', '2', '2019-01-12 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('923', 'course', 'd6fc28eac66cfc07cb13b43398d7460d.jpg', 'uploads/2019/01/course/', '18350', 'jpg', 'image/jpeg', '2', '2019-01-15 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('924', 'course', 'd77dd998b13ef19cc3f4008b21f40206.jpg', 'uploads/2019/01/course/', '24648', 'jpg', 'image/jpeg', '2', '2019-01-15 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('925', 'course', '29c49d0266ad675e438d39ed339d4775.jpg', 'uploads/2019/01/course/', '18821', 'jpg', 'image/jpeg', '2', '2019-01-15 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('926', 'course', '6b98e7d9cba339e8d5751babfeb7c79e.jpg', 'uploads/2019/01/course/', '25600', 'jpg', 'image/jpeg', '2', '2019-01-15 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('927', 'banner', '943d34f8834521c613bf624a342cab70.jpg', 'uploads/2019/01/banner/', '35185', 'jpg', 'image/jpeg', '2', '2019-01-16 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('928', 'banner', '0b3929cff47ec802187d5cbdc383a387.jpg', 'uploads/2019/01/banner/', '35185', 'jpg', 'image/jpeg', '2', '2019-01-16 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('932', 'user', 'dd3ebdc5209806d2b1c9638fcacfb428.jpg', 'uploads/2019/01/user/', '18821', 'jpg', 'image/jpeg', '2', '2019-01-20 00:00:00', null, null, null);
INSERT INTO `upload` VALUES ('933', 'article', 'a5b9a058abfe7f521a06d09bc924747c.jpg', 'uploads/2019/01/article/', '346460', 'jpg', 'image/jpeg', '2', '2019-01-21 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('934', 'article', '9119e54dff0862ed69825a9a30a68bc3.jpg', 'uploads/2019/01/article/', '290591', 'jpg', 'image/jpeg', '2', '2019-01-22 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('936', 'category', '9918d7a05ad2864df1faf377440c8b9e.jpg', 'uploads/2019/01/category/', '61921', 'jpg', 'image/jpeg', '2', '2019-01-22 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('937', 'category', 'cc8e977109c498f32726877762d86a5f.jpg', 'uploads/2019/01/category/', '567337', 'jpg', 'image/jpeg', '2', '2019-01-22 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('938', 'category', '9ac4f2220a4ed2dbbeffc32f908c41dc.png', 'uploads/2019/01/category/', '44247', 'png', 'image/png', '2', '2019-01-26 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('939', 'activity', 'd7ca137d5e7f802a6aab5bd19225b25f.jpg', 'uploads/2019/02/activity/', '11018', 'jpg', 'image/jpeg', '2', '2019-02-17 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('942', 'user', '98692c9e47d44f4141d6a1938e4560de.jpg', 'uploads/2019/02/user/', '8479', 'jpg', 'image/jpeg', '2', '2019-02-17 00:00:00', null, null, null);
INSERT INTO `upload` VALUES ('943', 'bank', '2c0474a67709ce2610cd452bf8e2b252.jpg', 'uploads/2019/02/bank/', '2263', 'jpg', 'image/jpeg', '2', '2019-02-20 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('944', 'bank', '8fd88b00f3a66afbea2bcc2672289ad8.jpg', 'uploads/2019/02/bank/', '2355', 'jpg', 'image/jpeg', '2', '2019-02-20 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('945', 'bank', '1d8151fbf2e592ad19ff279bc3cf968b.jpg', 'uploads/2019/02/bank/', '2365', 'jpg', 'image/jpeg', '2', '2019-02-20 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('948', 'course_register', '1c6cd07f52f90de4ed7cd9330324626b.jpg', 'uploads/2019/02/payment/', '391', 'jpg', 'image/jpeg', '2', '2019-02-20 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('949', 'course_register', '8fdd694fbf6bfb2459f8bf44996cf9fc.jpg', 'uploads/2019/02/payment/', '92', 'jpg', 'image/jpeg', '2', '2019-02-20 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('950', 'course_register', '243065b955f0b4514161c2420ddea0b7.jpg', 'uploads/2019/02/payment/', '92', 'jpg', 'image/jpeg', '2', '2019-02-20 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('951', 'course_register', '6718f65a4e0ea16c3b5c6a0a60004a17.jpg', 'uploads/2019/02/payment/', '92', 'jpg', 'image/jpeg', '2', '2019-02-20 00:00:00', '3', null, null);
INSERT INTO `upload` VALUES ('952', 'user', '8cfaa4ad3516be8e7d31a0bb50050d4d.jpg', 'uploads/2019/02/user/', '11018', 'jpg', 'image/jpeg', '2', '2019-02-20 00:00:00', null, null, null);
INSERT INTO `upload` VALUES ('953', 'course_register', 'c0ef6040f49d1cae13e5d37e72914e93.png', 'uploads/2019/02/payment/', '43', 'png', 'image/png', '2', '2019-02-20 00:00:00', null, null, null);

-- ----------------------------
-- Table structure for upload_content
-- ----------------------------
DROP TABLE IF EXISTS `upload_content`;
CREATE TABLE `upload_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grpContent` varchar(50) NOT NULL,
  `grpType` varchar(50) NOT NULL,
  `contentId` int(11) NOT NULL,
  `uploadId` int(11) NOT NULL,
  `lang` varchar(3) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1235 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of upload_content
-- ----------------------------
INSERT INTO `upload_content` VALUES ('958', 'banner', 'coverImage', '14', '667', null, null);
INSERT INTO `upload_content` VALUES ('962', 'article', 'coverImage', '47', '671', null, null);
INSERT INTO `upload_content` VALUES ('963', 'article', 'coverImage', '46', '672', null, null);
INSERT INTO `upload_content` VALUES ('972', 'course_content', 'docAttach', '5', '682', null, null);
INSERT INTO `upload_content` VALUES ('989', 'article', 'coverImage', '49', '699', null, null);
INSERT INTO `upload_content` VALUES ('997', 'course', 'coverImage', '2', '679', null, null);
INSERT INTO `upload_content` VALUES ('998', 'course', 'contentImage', '2', '706', null, null);
INSERT INTO `upload_content` VALUES ('1018', 'activity', 'coverImage', '2', '717', null, null);
INSERT INTO `upload_content` VALUES ('1019', 'activity', 'galleryImage', '2', '716', null, null);
INSERT INTO `upload_content` VALUES ('1020', 'activity', 'galleryImage', '2', '715', null, null);
INSERT INTO `upload_content` VALUES ('1021', 'activity', 'galleryImage', '2', '722', null, null);
INSERT INTO `upload_content` VALUES ('1022', 'activity', 'galleryImage', '2', '724', null, null);
INSERT INTO `upload_content` VALUES ('1023', 'activity', 'galleryImage', '2', '725', null, null);
INSERT INTO `upload_content` VALUES ('1024', 'activity', 'galleryImage', '2', '723', null, null);
INSERT INTO `upload_content` VALUES ('1025', 'activity', 'galleryImage', '2', '726', null, null);
INSERT INTO `upload_content` VALUES ('1026', 'activity', 'galleryImage', '2', '727', null, null);
INSERT INTO `upload_content` VALUES ('1030', 'seo_article', 'coverImage', '1', '731', null, null);
INSERT INTO `upload_content` VALUES ('1031', 'seo_home', 'coverImage', '8', '732', null, null);
INSERT INTO `upload_content` VALUES ('1032', 'seo_course', 'coverImage', '9', '733', null, null);
INSERT INTO `upload_content` VALUES ('1033', 'seo_activity', 'coverImage', '10', '734', null, null);
INSERT INTO `upload_content` VALUES ('1034', 'course', 'coverImage', '1', '678', null, null);
INSERT INTO `upload_content` VALUES ('1035', 'course', 'contentImage', '1', '735', null, null);
INSERT INTO `upload_content` VALUES ('1041', 'reviews', 'coverImage', '1', '740', null, null);
INSERT INTO `upload_content` VALUES ('1042', 'reviews', 'contentImage', '1', '741', null, null);
INSERT INTO `upload_content` VALUES ('1045', 'reviews', 'coverImage', '20', '742', null, null);
INSERT INTO `upload_content` VALUES ('1046', 'reviews', 'contentImage', '20', '743', null, null);
INSERT INTO `upload_content` VALUES ('1047', 'article', 'coverImage', '50', '744', null, null);
INSERT INTO `upload_content` VALUES ('1050', 'course_content', 'docAttach', '12', '749', null, null);
INSERT INTO `upload_content` VALUES ('1058', 'course', 'docAttach', '10', '757', null, null);
INSERT INTO `upload_content` VALUES ('1059', 'course', 'docAttach', '11', '758', null, null);
INSERT INTO `upload_content` VALUES ('1060', 'course', 'docAttach', '12', '759', null, null);
INSERT INTO `upload_content` VALUES ('1061', 'course', 'docAttach', '13', '760', null, null);
INSERT INTO `upload_content` VALUES ('1062', 'course', 'docAttach', '14', '761', null, null);
INSERT INTO `upload_content` VALUES ('1063', 'course', 'docAttach', '15', '762', null, null);
INSERT INTO `upload_content` VALUES ('1064', 'course', 'docAttach', '16', '763', null, null);
INSERT INTO `upload_content` VALUES ('1065', 'course', 'docAttach', '17', '764', null, null);
INSERT INTO `upload_content` VALUES ('1066', 'course', 'docAttach', '18', '765', null, null);
INSERT INTO `upload_content` VALUES ('1067', 'course', 'docAttach', '19', '766', null, null);
INSERT INTO `upload_content` VALUES ('1068', 'course', 'docAttach', '20', '767', null, null);
INSERT INTO `upload_content` VALUES ('1069', 'course', 'docAttach', '21', '768', null, null);
INSERT INTO `upload_content` VALUES ('1070', 'course', 'docAttach', '22', '769', null, null);
INSERT INTO `upload_content` VALUES ('1134', 'article', 'coverImage', '51', '833', null, null);
INSERT INTO `upload_content` VALUES ('1135', 'article', 'coverImage', '52', '834', null, null);
INSERT INTO `upload_content` VALUES ('1136', 'article', 'coverImage', '53', '835', null, null);
INSERT INTO `upload_content` VALUES ('1137', 'course', 'coverImage', '3', '836', null, null);
INSERT INTO `upload_content` VALUES ('1138', 'course', 'contentImage', '3', '837', null, null);
INSERT INTO `upload_content` VALUES ('1139', 'course_register', 'docAttach', '29', '838', null, null);
INSERT INTO `upload_content` VALUES ('1140', 'course_register', 'docAttach', '30', '839', null, null);
INSERT INTO `upload_content` VALUES ('1141', 'course_register', 'docAttach', '31', '840', null, null);
INSERT INTO `upload_content` VALUES ('1142', 'course_register', 'docAttach', '39', '841', null, null);
INSERT INTO `upload_content` VALUES ('1143', 'course_register', 'docAttach', '47', '842', null, null);
INSERT INTO `upload_content` VALUES ('1149', 'banner', 'coverImage', '18', '848', null, null);
INSERT INTO `upload_content` VALUES ('1150', 'banner', 'contentImage', '18', '849', null, null);
INSERT INTO `upload_content` VALUES ('1153', 'course_register', 'docAttach', '852', '852', null, null);
INSERT INTO `upload_content` VALUES ('1154', 'course_register', 'docAttach', '853', '853', null, null);
INSERT INTO `upload_content` VALUES ('1156', 'course_register', 'docAttach', '6', '865', null, null);
INSERT INTO `upload_content` VALUES ('1157', 'course_register', 'docAttach', '7', '866', null, null);
INSERT INTO `upload_content` VALUES ('1158', 'course_register', 'docAttach', '8', '867', null, null);
INSERT INTO `upload_content` VALUES ('1159', 'course_register', 'docAttach', '9', '868', null, null);
INSERT INTO `upload_content` VALUES ('1160', 'course_register', 'docAttach', '10', '869', null, null);
INSERT INTO `upload_content` VALUES ('1161', 'course_register', 'docAttach', '16', '870', null, null);
INSERT INTO `upload_content` VALUES ('1162', 'course_register', 'docAttach', '17', '871', null, null);
INSERT INTO `upload_content` VALUES ('1163', 'course_register', 'docAttach', '18', '872', null, null);
INSERT INTO `upload_content` VALUES ('1164', 'course_register', 'docAttach', '19', '873', null, null);
INSERT INTO `upload_content` VALUES ('1165', 'course_register', 'docAttach', '20', '874', null, null);
INSERT INTO `upload_content` VALUES ('1166', 'course_register', 'docAttach', '23', '875', null, null);
INSERT INTO `upload_content` VALUES ('1167', 'course_register', 'docAttach', '24', '876', null, null);
INSERT INTO `upload_content` VALUES ('1168', 'course_register', 'docAttach', '25', '877', null, null);
INSERT INTO `upload_content` VALUES ('1169', 'course_register', 'docAttach', '26', '878', null, null);
INSERT INTO `upload_content` VALUES ('1170', 'course_register', 'docAttach', '27', '879', null, null);
INSERT INTO `upload_content` VALUES ('1173', 'course', 'coverImage', '4', '882', null, null);
INSERT INTO `upload_content` VALUES ('1174', 'course', 'contentImage', '4', '883', null, null);
INSERT INTO `upload_content` VALUES ('1175', 'banner', 'coverImage', '13', '884', null, null);
INSERT INTO `upload_content` VALUES ('1176', 'banner', 'contentImage', '13', '885', null, null);
INSERT INTO `upload_content` VALUES ('1177', 'package', 'coverImage', '5', '886', null, null);
INSERT INTO `upload_content` VALUES ('1188', 'banner', 'coverImage', '19', '897', null, null);
INSERT INTO `upload_content` VALUES ('1191', 'banner', 'coverImage', '16', '900', null, null);
INSERT INTO `upload_content` VALUES ('1192', 'banner', 'coverImage', '20', '901', null, null);
INSERT INTO `upload_content` VALUES ('1193', 'banner', 'coverImage', '17', '902', null, null);
INSERT INTO `upload_content` VALUES ('1195', 'banner', 'coverImage', '21', '904', null, null);
INSERT INTO `upload_content` VALUES ('1196', 'course', 'coverImage', '5', '905', null, null);
INSERT INTO `upload_content` VALUES ('1197', 'instructor', 'coverImage', '1', '906', null, null);
INSERT INTO `upload_content` VALUES ('1198', 'user', 'coverImage', '52', '917', null, null);
INSERT INTO `upload_content` VALUES ('1199', 'user', 'coverImage', '55', '918', null, null);
INSERT INTO `upload_content` VALUES ('1200', 'user', 'coverImage', '61', '919', null, null);
INSERT INTO `upload_content` VALUES ('1201', 'user', 'coverImage', '62', '920', null, null);
INSERT INTO `upload_content` VALUES ('1202', 'user', 'coverImage', '63', '921', null, null);
INSERT INTO `upload_content` VALUES ('1203', 'user', 'coverImage', '66', '922', null, null);
INSERT INTO `upload_content` VALUES ('1204', 'course', 'coverImage', '6', '923', null, null);
INSERT INTO `upload_content` VALUES ('1205', 'course', 'coverImage', '7', '924', null, null);
INSERT INTO `upload_content` VALUES ('1206', 'course', 'coverImage', '8', '925', null, null);
INSERT INTO `upload_content` VALUES ('1207', 'course', 'coverImage', '9', '926', null, null);
INSERT INTO `upload_content` VALUES ('1208', 'banner', 'coverImage', '15', '927', null, null);
INSERT INTO `upload_content` VALUES ('1209', 'banner', 'contentImage', '15', '928', null, null);
INSERT INTO `upload_content` VALUES ('1213', 'user', 'coverImage', '70', '932', null, null);
INSERT INTO `upload_content` VALUES ('1214', 'article', 'coverImage', '1', '933', null, null);
INSERT INTO `upload_content` VALUES ('1215', 'article', 'coverImage', '2', '934', null, null);
INSERT INTO `upload_content` VALUES ('1217', 'category', 'coverImage', '27', '936', null, null);
INSERT INTO `upload_content` VALUES ('1218', 'category', 'coverImage', '28', '937', null, null);
INSERT INTO `upload_content` VALUES ('1219', 'category', 'coverImage', '29', '938', null, null);
INSERT INTO `upload_content` VALUES ('1220', 'activity', 'coverImage', '1', '939', null, null);
INSERT INTO `upload_content` VALUES ('1223', 'user', 'coverImage', '71', '942', null, null);
INSERT INTO `upload_content` VALUES ('1224', 'bank', 'coverImage', '22', '943', null, null);
INSERT INTO `upload_content` VALUES ('1225', 'bank', 'coverImage', '21', '944', null, null);
INSERT INTO `upload_content` VALUES ('1226', 'bank', 'coverImage', '20', '945', null, null);
INSERT INTO `upload_content` VALUES ('1229', 'course_register', 'docAttach', '3', '948', null, null);
INSERT INTO `upload_content` VALUES ('1230', 'course_register', 'docAttach', '4', '949', null, null);
INSERT INTO `upload_content` VALUES ('1231', 'course_register', 'docAttach', '5', '950', null, null);
INSERT INTO `upload_content` VALUES ('1232', 'course_register', 'docAttach', '1', '951', null, null);
INSERT INTO `upload_content` VALUES ('1233', 'user', 'coverImage', '72', '952', null, null);
INSERT INTO `upload_content` VALUES ('1234', 'course_register', 'docAttach', '2', '953', null, null);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `type` varchar(100) DEFAULT NULL,
  `lastLogin` datetime DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT '1',
  `verify` int(11) DEFAULT '0',
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `policyId` int(11) unsigned DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `createBy` int(11) unsigned DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) unsigned DEFAULT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` varchar(255) DEFAULT NULL,
  `sectionId` int(11) DEFAULT NULL,
  `partyId` int(11) DEFAULT NULL,
  `positionId` int(11) DEFAULT NULL,
  `degree` int(2) DEFAULT NULL,
  `oauth_provider` varchar(150) DEFAULT NULL,
  `oauth_uid` varchar(150) DEFAULT '',
  `picture` text,
  `phone` varchar(25) DEFAULT '',
  `user_login_status` int(1) DEFAULT '0',
  `user_datetime_using` datetime DEFAULT NULL,
  `couponCode` varchar(150) DEFAULT NULL,
  `isCommunity` int(1) DEFAULT '0',
  `session_id` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `sex` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`),
  KEY `oem_users_ibfk_1` (`policyId`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'administrator', '03f7d1801a07265823dfb824d34b167cca7c8a6e8f6c3627675d4e7446e65abf9ed45d771a59639767d5ee23a542e1669b24ba273b677fb89d64cbb1c9ab2818gCxgV8dToDExXM3ROcarkyChVrZiy8Tsq8nweyvzEwc=', 'admin@admin.com', 'admin', '2018-07-03 04:20:26', '1', '1', 'Admin', 'istrator', '1', '2016-07-10 09:06:53', null, '2018-07-09 08:58:31', '3', '2', '2018-01-18 10:00:51', '1', '14', '17', '18', '10', null, null, null, '', '0', null, null, '0', null, null, null);
INSERT INTO `user` VALUES ('3', 'super_admin', '03f7d1801a07265823dfb824d34b167cca7c8a6e8f6c3627675d4e7446e65abf9ed45d771a59639767d5ee23a542e1669b24ba273b677fb89d64cbb1c9ab2818gCxgV8dToDExXM3ROcarkyChVrZiy8Tsq8nweyvzEwc=', 'programmer@ait.com', 'developer', '2019-04-04 16:21:44', '1', '1', 'โปรแกรมเมอร์', 'เดฟ', '1', '2016-07-14 10:17:38', '1', '2018-07-09 09:47:51', '3', '0', '2018-01-18 08:09:05', null, null, null, null, null, null, null, null, '', '0', null, null, '0', null, null, null);
INSERT INTO `user` VALUES ('22', 'Admin', 'cebd18473e3808ce458cc4522af2a6d1e7e0561edd0b64c225cc25486cd5cf4c14073316037e7cee6b1fe5383915e193d850c432d6ff71a8f5600e39af4fa29bMdjnsRU/2f3hyKzVqlTFdhvP7jr7K04HXibG1IfvKT4=', 'administrator@admin.com', 'officer', '2018-10-06 20:28:12', '1', '1', 'Admin', 'Administrator', '1', '2018-08-04 23:17:50', '3', '2018-10-05 02:13:06', '3', '0', null, null, null, null, null, null, null, null, null, '', '0', null, null, '0', null, null, null);
INSERT INTO `user` VALUES ('71', 'satari.dop@gmail.com', '3e0647172d48a475849844ba787fb1ea855d66a611002006a38efdb33f4c317472cfa1b328bb12be5dea89996140fd9eb5243ed7666297fa17de8e77ce0881c6gIgiQDWvmgemgyZIcVhhZl/whctQsKMJVLk7TMXyEAw=', 'satari.dop@gmail.com', 'member', '2019-02-20 17:47:00', '1', '1', 'satari', 'maseng', '0', '2019-02-17 16:03:14', null, '2019-02-17 20:22:16', null, '0', null, null, null, null, null, null, 'register', '', null, '0857997445', '1', '2019-02-20 17:47:00', '7K6EWAH71', '0', 'ea86b72babe23f7f9b9315ca5cc3d4a5eb6a4e8d', '1990-01-11', 'male');
INSERT INTO `user` VALUES ('72', 'satari_pas@hotmail.com', 'b0844082acb3ebd6416e38cd585027db6f84a3d14194a6587773fbc1a06b46207686be9cb2722dcbee647c076837df3d996270ccb8e08c1092bce700c509191327MQSgApwfPqnHX6nJz+28WlpRXoni+EDctwORW6kz4=', 'satari_pas@hotmail.com', 'member', '2019-02-20 17:04:05', '1', '1', 'สาทาริ', 'มะเซ็ง', '0', '2019-02-20 17:02:41', null, null, null, '0', null, null, null, null, null, null, 'register', '', null, '', '1', '2019-02-20 17:04:05', null, '0', '1c7461fa900837621f63a8176b179cc1182bd7db', '0000-00-00', 'female');

-- ----------------------------
-- Table structure for usertracking
-- ----------------------------
DROP TABLE IF EXISTS `usertracking`;
CREATE TABLE `usertracking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT '0',
  `class` varchar(50) DEFAULT NULL,
  `function` varchar(50) DEFAULT NULL,
  `method` varchar(20) DEFAULT NULL,
  `sessionId` varchar(100) DEFAULT NULL,
  `identifier` varchar(255) DEFAULT NULL,
  `requestUri` text,
  `timestamp` varchar(20) DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `agent` text,
  `isBrowser` int(11) DEFAULT NULL,
  `browser` varchar(50) DEFAULT NULL,
  `isMobile` int(11) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `isRobot` tinyint(4) DEFAULT NULL,
  `robot` varchar(50) DEFAULT NULL,
  `platform` varchar(50) DEFAULT NULL,
  `isBackend` tinyint(4) DEFAULT '0',
  `referer` text,
  `recycle` int(11) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` varchar(255) DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `updateBy` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usertracking
-- ----------------------------
